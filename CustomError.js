var CustomError = {
	success : {
		status: "200",
		message: "OK"
	},
	alert_201: {
		status: "201",
		message: "Created"
	},
	alert_202: {
		status: "202",
		message: "Accepted",
	},
	alert_203: {
		status: "203",
		message: "Non-Authoritative Information (since HTTP/1.1)"
	},
	alert_204: {
		status: "204",
		message: "No Content"
	},
	alert_205: {
		status: "205",
		message: "Reset Content"
	},
	alert_206: {
		status: "206",
		message: "Partial Content"
	},
	alert_207: {
		status: "207",
		message: "Multi-Status (WebDAV)"
	},
	alert_500: {
		status: "500",
		message: "Internal Server Error"
	},
	alert_501: {
		status: "501",
		message: "Not Implemented"
	},
	alert_502: {
		status: "502",
		message: "Bad Gateway"
	},
	alert_503: {
		status: "503",
		message: "Service Unavailable"
	},
	alert_504: {
		status: "504",
		message: "Gateway Timeout"
	},
	alert_505: {
		status: "505",
		message: "HTTP Version Not Supported"
	},
	alert_506: {
		status: "506",
		message: "Variant Also Negotiates (RFC 2295)"
	},
	alert_507: {
		status: "507",
		message: "Insufficient Storage (WebDAV) (RFC 4918)"
	},
	alert_509: {
		status: "509",
		message: "Bandwith Limit Exceeded (Apache bw/limited extension)"
	},
	alert_510: {
		status: "510",
		message: "Not Extended (RFC 2774)"
	}
};