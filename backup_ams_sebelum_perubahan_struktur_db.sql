-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16 Jul 2017 pada 12.43
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `Kode` char(2) NOT NULL DEFAULT '',
  `Nama` varchar(20) NOT NULL DEFAULT '',
  `Nilai` smallint(3) DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`Kode`, `Nama`, `Nilai`, `NotActive`) VALUES
('H', 'Hadir', 1, 'N'),
('S', 'Sakit', 1, 'N'),
('I', 'Ijin', 1, 'N'),
('M', 'Mangkir', 0, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `ID` int(11) NOT NULL,
  `Login` varchar(20) NOT NULL DEFAULT '',
  `Password` varchar(10) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) NOT NULL DEFAULT '',
  `Phone` varchar(30) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`ID`, `Login`, `Password`, `Description`, `Name`, `Email`, `Phone`, `NotActive`) VALUES
(1, 'superadmin', '*240107317', 'System Developer', 'superadmin', '', '', 'Y'),
(6, 'admin', '*4ACFE3202', 'Administrator Default', 'Administrator', '', '', 'N'),
(8, 'donal', 'donal', 'donal', 'Donal Siagian', 'donal.itb@gmail.com', '081397932320', 'Y'),
(9, 'donal1', 'donal1', 'donal1', 'Donal Siagian1', 'donal.itb@gmail.com', '081397932320', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agama`
--

CREATE TABLE `agama` (
  `AgamaID` int(11) NOT NULL DEFAULT '0',
  `Agama` varchar(20) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `agama`
--

INSERT INTO `agama` (`AgamaID`, `Agama`, `NotActive`) VALUES
(1, 'KATOLIK', 'N'),
(2, 'KRISTEN', 'N'),
(3, 'ISLAM', 'N'),
(4, 'HINDU', 'N'),
(5, 'BUDHA', 'N'),
(6, 'ALIRAN KEPERCAYAAN', 'N'),
(7, 'PROTESTAN', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akreditasi`
--

CREATE TABLE `akreditasi` (
  `Kode` char(1) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akreditasi`
--

INSERT INTO `akreditasi` (`Kode`, `Nama`, `NotActive`) VALUES
('A', 'Berakreditasi A', 'N'),
('B', 'Berakreditasi B', 'N'),
('C', 'Berakreditasi C', 'N'),
('L', 'Belajar', 'N'),
('U', 'Unggul', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitasdosen`
--

CREATE TABLE `aktivitasdosen` (
  `ID` int(11) NOT NULL,
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `Tanggal` date NOT NULL DEFAULT '0000-00-00',
  `Aktivitas` int(11) NOT NULL DEFAULT '0',
  `Judul` varchar(255) NOT NULL DEFAULT '',
  `Publikasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `Deskripsi` varchar(255) DEFAULT NULL,
  `Lokasi` varchar(255) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `Audit` enum('Y','N') DEFAULT 'N',
  `TglAudit` date DEFAULT NULL,
  `Login` varchar(10) NOT NULL DEFAULT '',
  `TglLogin` date DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aktivitasdosen`
--

INSERT INTO `aktivitasdosen` (`ID`, `IDDosen`, `Tanggal`, `Aktivitas`, `Judul`, `Publikasi`, `Deskripsi`, `Lokasi`, `Keterangan`, `Audit`, `TglAudit`, `Login`, `TglLogin`, `NotActive`) VALUES
(1, 1, '2005-03-21', 1, 'Sisfo Kampus 2oo5', 'Y', 'Sistem Informasi Pengelolaan Perguruan Tinggi Open Source: Sisfo Kampus 2oo5', 'www.sisfokampus.net', NULL, 'N', NULL, 'admin', '2005-03-21', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alumni`
--

CREATE TABLE `alumni` (
  `ID` bigint(20) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `PMBID` varchar(20) DEFAULT NULL,
  `NIRM` varchar(20) DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Sex` char(1) NOT NULL DEFAULT 'L',
  `TempatLahir` varchar(50) DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `RT` varchar(4) DEFAULT NULL,
  `RW` varchar(4) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `KodePos` varchar(10) DEFAULT NULL,
  `KodeTelp` varchar(5) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `AgamaID` int(11) DEFAULT '0',
  `WargaNegara` varchar(30) DEFAULT NULL,
  `SudahBekerja` enum('Y','N') NOT NULL DEFAULT 'N',
  `PusatKarir` enum('Y','N') NOT NULL DEFAULT 'N',
  `NamaPrsh` varchar(50) DEFAULT NULL,
  `Alamat1Prsh` varchar(100) DEFAULT NULL,
  `Alamat2Prsh` varchar(100) DEFAULT NULL,
  `KotaPrsh` varchar(50) DEFAULT NULL,
  `TelpPrsh` varchar(20) DEFAULT NULL,
  `FaxPrsh` varchar(20) DEFAULT NULL,
  `NamaOT` varchar(50) DEFAULT NULL,
  `PekerjaanOT` varchar(50) DEFAULT NULL,
  `AlamatOT1` varchar(100) DEFAULT NULL,
  `AlamatOT2` varchar(100) DEFAULT NULL,
  `RTOT` varchar(4) DEFAULT NULL,
  `RWOT` varchar(4) DEFAULT NULL,
  `KotaOT` varchar(50) DEFAULT NULL,
  `KodeTelpOT` varchar(4) DEFAULT NULL,
  `TelpOT` varchar(30) DEFAULT NULL,
  `EmailOT` varchar(50) DEFAULT NULL,
  `KodePosOT` varchar(10) DEFAULT NULL,
  `AsalSekolah` varchar(50) DEFAULT NULL,
  `PropSekolah` varchar(5) DEFAULT NULL,
  `JenisSekolah` varchar(10) DEFAULT NULL,
  `LulusSekolah` varchar(5) DEFAULT NULL,
  `IjazahSekolah` varchar(50) DEFAULT NULL,
  `NilaiSekolah` decimal(5,2) DEFAULT '0.00',
  `Pilihan1` varchar(100) DEFAULT NULL,
  `Pilihan2` varchar(100) DEFAULT NULL,
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `Status` varchar(5) DEFAULT NULL,
  `KodeProgram` varchar(10) DEFAULT NULL,
  `StatusAwal` varchar(5) DEFAULT NULL,
  `StatusPotongan` varchar(5) DEFAULT NULL,
  `SPP_D` int(11) NOT NULL DEFAULT '0',
  `Semester` int(11) DEFAULT NULL,
  `TahunAkademik` varchar(5) DEFAULT NULL,
  `KodeBiaya` varchar(5) DEFAULT NULL,
  `Posting` char(1) DEFAULT NULL,
  `Lulus` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglLulus` date DEFAULT NULL,
  `TahunLulus` varchar(5) DEFAULT NULL,
  `WaktuKuliah` varchar(10) DEFAULT NULL,
  `Keterangan` varchar(100) DEFAULT NULL,
  `Pinjaman` int(11) DEFAULT NULL,
  `KTahun` date DEFAULT NULL,
  `K_Dosen` varchar(10) DEFAULT NULL,
  `DosenID` int(11) NOT NULL DEFAULT '0',
  `Ranking` int(11) DEFAULT NULL,
  `mGroup` char(1) DEFAULT NULL,
  `Target` int(11) DEFAULT NULL,
  `Prop` varchar(10) DEFAULT NULL,
  `Masuk` date DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `TestScore` int(11) DEFAULT '0',
  `TA` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglTA` date DEFAULT NULL,
  `TotalSKS` int(11) NOT NULL DEFAULT '0',
  `IPK` decimal(5,2) NOT NULL DEFAULT '0.00',
  `JudulTA` varchar(255) DEFAULT NULL,
  `PembimbingTA` int(11) DEFAULT NULL,
  `CatatanTA` text,
  `PMBSyarat` varchar(100) DEFAULT NULL,
  `NomerIjazah` varchar(255) DEFAULT NULL,
  `MGM` enum('Y','N') NOT NULL DEFAULT 'N',
  `MGMOleh` int(11) DEFAULT NULL,
  `MGMHonor` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahasa`
--

CREATE TABLE `bahasa` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `Def` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahasa`
--

INSERT INTO `bahasa` (`Kode`, `Nama`, `Def`, `NotActive`) VALUES
('id', 'Indonesia', 'Y', 'N'),
('en', 'English', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bataskrs`
--

CREATE TABLE `bataskrs` (
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `KodeProgram` varchar(10) NOT NULL DEFAULT '',
  `krsm` date NOT NULL DEFAULT '0000-00-00',
  `krss` date NOT NULL DEFAULT '0000-00-00',
  `ukrsm` date NOT NULL DEFAULT '0000-00-00',
  `ukrss` date NOT NULL DEFAULT '0000-00-00',
  `MulaiBayar` date NOT NULL DEFAULT '0000-00-00',
  `AkhirBayar` date NOT NULL DEFAULT '0000-00-00',
  `MulaiBayar2` date NOT NULL DEFAULT '0000-00-00',
  `AkhirBayar2` date NOT NULL DEFAULT '0000-00-00',
  `Denda` enum('Y','N') NOT NULL DEFAULT 'N',
  `HargaDenda` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Absen` date NOT NULL DEFAULT '0000-00-00',
  `Tugas` date NOT NULL DEFAULT '0000-00-00',
  `UTS` date NOT NULL DEFAULT '0000-00-00',
  `UAS` date NOT NULL DEFAULT '0000-00-00',
  `SS` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bataskrs`
--

INSERT INTO `bataskrs` (`Tahun`, `KodeJurusan`, `KodeProgram`, `krsm`, `krss`, `ukrsm`, `ukrss`, `MulaiBayar`, `AkhirBayar`, `MulaiBayar2`, `AkhirBayar2`, `Denda`, `HargaDenda`, `NotActive`, `Absen`, `Tugas`, `UTS`, `UAS`, `SS`) VALUES
('20162', '61201', 'REG', '2017-02-20', '2017-04-25', '2017-03-06', '2017-03-11', '2017-02-13', '2017-02-25', '2017-02-13', '2017-02-25', 'N', 0, 'N', '2017-06-17', '2017-06-17', '2017-04-24', '2017-07-03', '2017-04-11'),
('20162', '62201', 'REG', '2017-02-20', '2017-04-25', '2017-03-06', '2017-03-11', '2017-02-13', '2017-02-25', '2017-02-13', '2017-02-25', 'N', 0, 'N', '2017-06-17', '2017-06-17', '2017-04-24', '2017-07-03', '2017-04-11'),
('20161', '62201', 'REG', '2016-09-05', '2016-09-08', '2016-09-20', '2016-09-24', '2016-08-15', '2016-08-20', '2016-08-15', '2016-08-20', 'N', 0, 'N', '2016-12-26', '2016-12-26', '2016-11-14', '2017-01-02', '2017-04-30'),
('20161', '61201', 'REG', '2016-09-05', '2016-09-08', '2016-09-20', '2016-09-24', '2016-08-15', '2016-08-20', '2016-08-15', '2016-08-20', 'N', 0, 'N', '2016-12-26', '2016-12-26', '2016-11-14', '2017-01-02', '2017-04-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bayar`
--

CREATE TABLE `bayar` (
  `ID` int(11) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `KodeBiaya` varchar(5) NOT NULL DEFAULT '',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `NamaBayar` varchar(100) DEFAULT NULL,
  `JenisTrx` int(11) NOT NULL DEFAULT '0',
  `Kali` int(11) NOT NULL DEFAULT '0',
  `JenisBayar` int(11) DEFAULT NULL,
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Kurs` decimal(8,6) NOT NULL DEFAULT '1.000000',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `Denda` enum('Y','N') NOT NULL DEFAULT 'N',
  `HariDenda` int(11) NOT NULL DEFAULT '0',
  `HariBebas` int(11) NOT NULL DEFAULT '0',
  `HargaDenda` int(11) NOT NULL DEFAULT '0',
  `Catatan` varchar(100) DEFAULT NULL,
  `BuktiBayar` varchar(50) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bayar2`
--

CREATE TABLE `bayar2` (
  `ID` int(11) NOT NULL,
  `BayarID` int(11) NOT NULL DEFAULT '0',
  `BiayaID` int(11) NOT NULL DEFAULT '0',
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Kurs` decimal(8,6) NOT NULL DEFAULT '1.000000',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `biaya`
--

CREATE TABLE `biaya` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `KodeProgram` varchar(10) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `KodeBPPP` int(11) NOT NULL DEFAULT '0',
  `Tgl` date NOT NULL DEFAULT '0000-00-00',
  `unip` varchar(10) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `biaya`
--

INSERT INTO `biaya` (`Kode`, `Nama`, `KodeProgram`, `KodeJurusan`, `KodeBPPP`, `Tgl`, `unip`, `NotActive`) VALUES
('2016', 'Biaya SPP', 'EXEC', '', 0, '2017-01-14', 'admin', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `biaya2`
--

CREATE TABLE `biaya2` (
  `ID` int(11) NOT NULL,
  `KodeBiaya` varchar(5) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(5) NOT NULL DEFAULT '',
  `KodeProgram` varchar(10) DEFAULT NULL,
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Kali` int(11) NOT NULL DEFAULT '1',
  `JenisBiaya` smallint(6) NOT NULL DEFAULT '0',
  `Denda` enum('Y','N') NOT NULL DEFAULT 'N',
  `Otomatis` enum('Y','N') NOT NULL DEFAULT 'N',
  `Status` varchar(5) DEFAULT NULL,
  `StatusAwal` varchar(5) DEFAULT NULL,
  `StatusPotongan` varchar(5) DEFAULT NULL,
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `PakaiScript` enum('Y','N') NOT NULL DEFAULT 'N',
  `NamaScript` varchar(100) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `biayamhsw`
--

CREATE TABLE `biayamhsw` (
  `ID` int(11) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `KodeBiaya` varchar(5) NOT NULL DEFAULT '',
  `IDBiaya2` int(11) NOT NULL DEFAULT '0',
  `NamaBiaya` varchar(100) DEFAULT NULL,
  `Kali` int(11) NOT NULL DEFAULT '1',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Biaya` int(11) NOT NULL DEFAULT '0',
  `Bayar` int(11) NOT NULL DEFAULT '0',
  `Denda` enum('Y','N') NOT NULL DEFAULT 'N',
  `IDBayar` int(11) DEFAULT '0',
  `TglBayar` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Catatan` varchar(100) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bimbinganta`
--

CREATE TABLE `bimbinganta` (
  `ID` int(11) NOT NULL,
  `Tanggal` date NOT NULL DEFAULT '0000-00-00',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `PembimbingTA` int(11) NOT NULL DEFAULT '0',
  `Catatan` text,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bpppokok`
--

CREATE TABLE `bpppokok` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `Catatan` text,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bpppokok1`
--

CREATE TABLE `bpppokok1` (
  `ID` int(11) NOT NULL,
  `KodeBPPP` int(11) NOT NULL DEFAULT '0',
  `MinSKS` int(11) NOT NULL DEFAULT '0',
  `MaxSKS` int(11) NOT NULL DEFAULT '0',
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Biaya` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `currency`
--

CREATE TABLE `currency` (
  `Symbol` varchar(5) NOT NULL DEFAULT '',
  `Name` varchar(30) NOT NULL DEFAULT '',
  `Unit` varchar(20) NOT NULL DEFAULT '',
  `Base` enum('Y','N') NOT NULL DEFAULT 'N',
  `Kali` int(11) NOT NULL DEFAULT '1',
  `Def` enum('Y','N') NOT NULL DEFAULT 'N',
  `DefaultString` varchar(30) DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `currency`
--

INSERT INTO `currency` (`Symbol`, `Name`, `Unit`, `Base`, `Kali`, `Def`, `DefaultString`, `NotActive`) VALUES
('IDR', 'ID Rupiah', 'Rupiah', 'Y', 1, 'Y', '- Default', 'N'),
('AUD', 'Dollar Australia', 'AUD', 'N', 0, 'N', '-', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `ID` int(11) NOT NULL,
  `OldID` varchar(10) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(10) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Gelar` varchar(100) NOT NULL DEFAULT '',
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `KodeGolongan` varchar(10) DEFAULT NULL,
  `TglMasuk` date DEFAULT '0000-00-00',
  `TglKeluar` date DEFAULT '0000-00-00',
  `KodeStatus` varchar(10) DEFAULT NULL,
  `InstansiInduk` varchar(10) DEFAULT NULL,
  `KodeDosen` varchar(10) DEFAULT NULL,
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `Propinsi` varchar(50) DEFAULT NULL,
  `Negara` varchar(50) DEFAULT NULL,
  `KodePos` varchar(20) DEFAULT NULL,
  `TempatLahir` varchar(100) DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `Sex` char(1) NOT NULL DEFAULT 'L',
  `AgamaID` int(11) DEFAULT NULL,
  `JabatanOrganisasi` varchar(10) DEFAULT NULL,
  `JabatanAkademik` char(1) DEFAULT NULL,
  `JabatanDikti` char(1) DEFAULT NULL,
  `KTP` varchar(50) DEFAULT NULL,
  `JenjangDosen` char(1) DEFAULT NULL,
  `LulusanPT` varchar(100) DEFAULT NULL,
  `Ilmu` varchar(100) DEFAULT NULL,
  `Akta` enum('Y','N','T') NOT NULL DEFAULT 'N',
  `Ijin` enum('Y','N','T') NOT NULL DEFAULT 'N',
  `Bank` varchar(100) DEFAULT NULL,
  `AccountName` varchar(100) DEFAULT NULL,
  `AccountNumber` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`ID`, `OldID`, `Login`, `Password`, `Description`, `Name`, `Email`, `Phone`, `NotActive`, `Gelar`, `KodeFakultas`, `KodeJurusan`, `KodeGolongan`, `TglMasuk`, `TglKeluar`, `KodeStatus`, `InstansiInduk`, `KodeDosen`, `Alamat1`, `Alamat2`, `Kota`, `Propinsi`, `Negara`, `KodePos`, `TempatLahir`, `TglLahir`, `Sex`, `AgamaID`, `JabatanOrganisasi`, `JabatanAkademik`, `JabatanDikti`, `KTP`, `JenjangDosen`, `LulusanPT`, `Ilmu`, `Akta`, `Ijin`, `Bank`, `AccountName`, `AccountNumber`) VALUES
(1, NULL, 'dewo', '*4ACFE3202', 'Admin', 'Donal Siagian', 'sgufakto@gmail.com', '81397932320', 'Y', 'ST, MM', 'TEK', 'ELA', NULL, '1975-01-01', '0000-00-00', 'E', '', 'H', 'Jl. Kasuari', 'Kranji', 'Bekasi', 'Jawa Barat', 'Indonesia', '', 'Tegal', '1972-12-22', 'L', 1, '', '', 'D', '', 'B', 'Universitas Diponegoro Semarang', 'Elektronika & Telekomunikasi; Magister Manajemen', 'Y', 'N', '', '', 'N'),
(2, NULL, 'Ryan', '*4ACFE3202', 'Dosen', 'Apriansyah Putra,M.Kom', 'Apriansyah@unsri.ac.id', '08117137760', 'Y', '', NULL, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', NULL, NULL, NULL),
(3, NULL, 'yudi', '*84AAC12F5', '', 'Yudi Tusri', 'yuditusri1@gmail.com', '082372110707', 'N', 'SE.,M.Si', 'STIE-P', '61201', NULL, '2003-09-01', '0000-00-00', 'A', '', 'T', 'Jalan Basuki Rahmad Kel. Tanjung Raman', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Tanjung Raman', '1940-01-01', 'L', 3, 'DSNTTP', '', 'A', '1674061408720001', 'B', 'Universitas Sriwijaya', 'Manajemen Pemasaran', 'N', 'N', '', '', ''),
(4, NULL, 'citra', '*84AAC12F5', '', 'Citra Etika', 'etikacitra@ymail.com', '081271081733', 'N', 'SE.,M.Si', 'STIE-P', '62201', NULL, '2011-03-01', '0000-00-00', 'A', '', 'T', 'Jalan Madrasah Aliyah No. 55 Rt. 002 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Barat', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Prabumulih', '1985-01-12', 'P', 3, 'DSNTTP', '', 'A', '1674025201850006', 'B', 'Universitas Sriwijaya', 'Akuntansi', 'N', 'N', '', '', ''),
(5, NULL, 'alfianti', '*84AAC12F5', '', 'Siska Alfianti', 'siska_alfiati@yahoo.com', '081367542578', 'N', 'SP., M.Si', 'STIE-P', '62201', NULL, '2005-01-01', '0000-00-00', 'C', '', 'T', 'Prabumulih', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'OGAN ILIR', '1982-03-11', 'P', 3, 'DSNTTP', '', 'B', '1610075103820002', 'B', 'Universitas Sriwijaya', 'Agribisnis', 'N', 'N', '', '', ''),
(6, NULL, 'harahap', '*84AAC12F5', '', 'Zakaria Harahap', 'zakariaharahap888@gmail.com', '081328721127', 'N', 'S.EI.,M.HI', 'STIE-P', '61201', NULL, '2005-01-01', '0000-00-00', 'A', '', 'T', 'Jalan Bukit Kecil Rt. 05 Rw. 01 Kel. Majasari Kec. Prabumulih Selatan', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Prabumulih', '1979-10-20', 'L', 3, 'DSNTTP', '', 'A', '1674062010790004', 'B', 'IAIN RADEN FATAH', 'Ekonomi Syariah', 'N', 'N', '', '', ''),
(7, NULL, 'ajabar', '*84AAC12F5', '', 'Ajabar', 'ajabar_02@yahoo.com', '', 'N', 'S.IP., M.M', 'STIE-P', '61201', NULL, '2003-10-01', '0000-00-00', 'A', '', 'T', 'Jalan Raya Baturaja Rt. 01 Rw. 01 Tanjung Raman', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Tanjung Raman', '1976-05-02', 'L', 3, 'DSNTTP', '', 'A', '', 'B', 'Universitas Sriwijaya', 'Manajemen', 'N', 'N', '', '', ''),
(8, NULL, 'suparni', '*84AAC12F5', '', 'Sri Suparni', 'niniksri30@yahoo.com', '082380840777', 'N', 'SS., M.Si', 'STIE-P', '61201', NULL, '2000-09-01', '0000-00-00', 'A', '', 'T', 'Jalan Sinta Wonosari', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'MUARA ENIM', '1969-08-30', 'P', 3, 'DSNTTP', '', 'A', '1674017008690002', 'B', 'Universitas Sriwijaya', 'Pengelolaan SDM', 'N', 'N', '', '', ''),
(9, NULL, 'susianti', '*84AAC12F5', '', 'Susianti', 'sy40062@gmail.com', '', 'N', 'SE., MM', 'STIE-P', '61201', NULL, '2016-10-01', '0000-00-00', 'A', '', 'T', 'Jalan Bukit Barisan No. 59 Tugu Kecil', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'MUARA ENIM ', '1970-08-13', 'L', 3, 'DSNTTP', '', 'A', '0604025308720001 ', 'B', 'Universitas Bina Dharma', 'Manajemen', 'N', 'N', '', '', ''),
(10, NULL, 'endrekson', '*84AAC12F5', '', 'Romsa Endrekson', 'romsaendrekson@gmail.com', '', 'N', 'SP., MM', 'STIE-P', '61201', NULL, '2015-07-14', '0000-00-00', 'A', '', 'T', 'Jalan Nias No. 402 Rt. 07 Rw. 02 Kel. Gunung Ibul Kec. Prabumulih Timur', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'PAGAR AGUNG ', '1972-05-13', 'L', 3, 'DSNTTP', '', 'A', '1674021305720002 ', 'B', 'Universitas Bina Dharma', 'Manajemen', 'N', 'N', '', '', ''),
(11, NULL, 'lingga', '*84AAC12F5', '', 'Linggariama', 'anggariyama@gmail.com', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2010-09-01', '0000-00-00', 'A', '', 'T', 'Jalan Krakatau No. 49 Rt. 01 Rw. 02 Kel. Gunung Ibul Kec. Prabumulih Selatan', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Tanjung Batu', '1982-01-21', 'L', 3, 'DSNTTP', '', 'A', '1674022101820003 ', 'B', 'Universitas Muhammadiyah Palembang', 'Manajemen', 'N', 'N', '', '', ''),
(12, NULL, 'rona', '*84AAC12F5', '', 'Rona Anggrainie', 'ronaanggra@yahoo.com', '', 'N', 'SP., M.Si', 'STIE-P', '62201', NULL, '2005-01-01', '0000-00-00', 'C', '', 'T', 'Jalan Vina Sejahtera II Blok DC No. 4 Gunung Ibul', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Teluk Padang', '1981-04-23', 'L', 3, 'DSNTTP', '', 'B', '1674026304810010 ', 'B', 'Universitas Sriwijaya', 'Agribisnis', 'N', 'N', '', '', ''),
(13, NULL, 'marlinda', '*84AAC12F5', '', 'Titi Marlinda', 'titimarlinda525@yahoo.co.id', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2005-01-01', '0000-00-00', 'A', '', 'T', 'Jalan Dempo Rt. 01 Rw. 08 Muara Dua', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Prabumulih', '1979-06-28', 'P', 3, 'DSNTTP', '', 'A', '1010060038028061 ', 'B', 'Universitas Sriwijaya', 'Ekonomi', 'N', 'N', '', '', ''),
(14, NULL, 'rika', '*84AAC12F5', '', 'Rika Fitri Ramayani', 'rika.fitri71@yahoo.com', '', 'N', 'S.Kom., MM', 'STIE-P', '62201', NULL, '2015-07-14', '0000-00-00', 'A', '', 'T', 'Jalan Ramakasih III No. 726 Rt. 33 Rw. 09 Kel. Duku Kec. Ilir Timur II', '', 'Palembang', 'Sumatera Selatan', 'Indonesia', '', 'Palembang', '1989-05-05', 'P', 3, 'DSNTTP', '', 'A', '1671064505890008 ', 'B', 'Universitas Bina Dharma', 'Manajemen', 'N', 'N', '', '', ''),
(15, NULL, 'sukmawati', '*84AAC12F5', '', 'Emy Sukmawati', 'e_sukmawati09@yahoo.com', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2005-01-01', '0000-00-00', 'A', '', 'T', 'Jalan KH. Ahmad Dahlan No. 19 Rt. 03 Rw. 02 Prabujaya', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Prabumulih', '1972-07-21', 'P', 3, 'DSNTTP', '', 'A', '00210719722008 ', 'B', 'Universitas Muhammadiyah Palembang', 'Manajemen', 'N', 'N', '', '', ''),
(16, NULL, 'resi', '*84AAC12F5', '', 'Resi Marina', 'resimarina50@gmail.com', '', 'N', 'SE., M.Si', 'STIE-P', '61201', NULL, '2005-01-01', '0000-00-00', 'A', '', 'T', 'Jalan Basuki Rahmat Kel. Tanjung Raman Kec. Prabumulih Selatan', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Tanjung Raman', '1977-06-26', 'P', 3, 'DSNTTP', '', 'A', '1674066606770001', 'B', 'Universitas Muhammadiyah Palembang', 'Manajemen', 'N', 'N', '', '', ''),
(17, NULL, 'samron', '*84AAC12F5', '', 'Samron Akhiri', 'sakhiri@telpp.com', '', 'N', 'SE., MM', 'STIE-P', '61201', NULL, '2014-05-01', '0000-00-00', 'A', '', 'T', 'Jalan Dul Mubin Rt. 4 Rw. 2 Mangga Besar', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'LAHAT', '1969-02-24', 'L', 3, 'DSNTTP', '', 'A', '1674052402690001', 'B', 'Universitas Bina Dharma', 'Manajemen SDM', 'N', 'N', '', '', ''),
(18, NULL, 'meirani', '*84AAC12F5', '', 'Meirani Betriana', 'mbetriana@yahoo.co.id', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2010-01-01', '0000-00-00', 'A', '', 'T', '', '', '', '', '', '', 'SUGIHWARAS', '1983-05-05', 'L', 3, 'DSNTTP', '', 'A', '', 'B', 'Universitas Muhammadiyah Palembang', 'Manajemen Keuangan', 'N', 'N', '', '', ''),
(19, NULL, 'yelli', '*84AAC12F5', '', 'Yelli Aswariningsih', 'yelliaswariningsih1@gmail.com', '', 'N', 'S.H., M.H', 'STIE-P', '61201', NULL, '2000-09-01', '0000-00-00', 'A', '', 'T', 'Jalan Jend. Sudirman No. 07 Muara Dua', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'WONOSOBO', '1970-07-04', 'P', 3, 'DSNTTP', '', 'A', '1674024407700002', 'B', 'Universitas Muhammadiyah Palembang', 'ILMU HUKUM', 'N', 'N', '', '', ''),
(20, NULL, 'suhardan', '*84AAC12F5', '', 'Suhardan', 'suhardan06@gmail.com', '', 'N', 'SE., MM', 'STIE-P', '61201', NULL, '2005-09-01', '0000-00-00', 'A', '', 'T', 'Jalan Sudirman No. 88 Patih Galung', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Prabumulih', '1958-09-06', 'L', 3, 'DSNTTP', '', 'A', '1674010609580004', 'B', 'Universitas Bina Dharma', 'Manajemen SDM', 'N', 'N', '', '', ''),
(21, NULL, 'amandin', '*84AAC12F5', '', 'Amandin', 'amandinstie@gmail.com', '', 'N', 'SE., MM', 'STIE-P', '61201', NULL, '2015-07-14', '0000-00-00', 'A', '', 'T', 'Prabumulih', '', 'Prabumulih', 'Sumatera Selatan', 'Indonesia', '', 'Koto Tebat', '1988-08-07', 'L', 3, 'DSNTTP', '', 'A', '', 'B', 'Universitas Bina Dharma', 'Manajemen', 'N', 'N', '', '', ''),
(22, NULL, 'nurdia', '*84AAC12F5', '', 'Nurdia', 'nurdia_2911@yahoo.com', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2016-08-19', '0000-00-00', 'A', '', 'T', 'Jalan May Zen Lr. Mufakat No. 59', '', 'Palembang', 'Sumatera Selatan', 'Indonesia', '', 'Palembang', '1986-11-29', 'P', 3, 'DSNTTP', '', 'A', '', 'B', 'Universitas Sriwijaya', 'Ilmu Ekonomi', 'N', 'N', '', '', ''),
(23, NULL, 'tifani', '*84AAC12F5', '', 'Tifani Rizki Amelia', 'tifanirizkiamel@gmail.com', '', 'N', 'SE., M.Si', 'STIE-P', '62201', NULL, '2016-10-15', '0000-00-00', 'A', '', 'T', 'KOMP PERUMAHAN POLITEKNIK NO 15, RT.41/RW.13, BUKIT LAMA', '', 'Palembang', 'Sumatera Selatan', 'Indonesia', '', 'Palembang', '1988-11-26', 'P', 3, 'DSNTTP', '', 'A', '', 'B', 'Universitas Sriwijaya', 'Ilmu Ekonomi', 'N', 'N', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama_Indonesia` varchar(100) NOT NULL DEFAULT '',
  `Nama_English` varchar(100) NOT NULL DEFAULT '',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`Kode`, `Nama_Indonesia`, `Nama_English`, `Login`, `Tgl`, `NotActive`) VALUES
('STIE-P', 'SEKOLAH TINGGI ILMU EKONOMI PRABUMULIH', 'SEKOLAH TINGGI ILMU EKONOMI PRABUMULIH', 'admin', '2017-01-27 10:33:45', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groupmodul`
--

CREATE TABLE `groupmodul` (
  `GroupModulID` int(11) NOT NULL,
  `GroupModul` varchar(20) NOT NULL DEFAULT '',
  `Level` smallint(6) NOT NULL DEFAULT '1',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `groupmodul`
--

INSERT INTO `groupmodul` (`GroupModulID`, `GroupModul`, `Level`, `NotActive`) VALUES
(100, 'Master', 1, 'N'),
(200, 'PMB', 2, 'N'),
(300, 'Akademik', 2, 'N'),
(50, 'Sistem', 1, 'N'),
(400, 'Administrasi', 1, 'N'),
(500, 'Mahasiswa', 14, 'N'),
(450, 'Dosen', 13, 'N'),
(600, 'Keuangan', 1, 'N'),
(55, 'KaAkademik', 1, 'N'),
(602, 'AdmDosen', 1, 'N'),
(603, 'Alumni', 1, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `ID` smallint(2) NOT NULL DEFAULT '0',
  `Nama` varchar(10) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`ID`, `Nama`, `NotActive`) VALUES
(2, 'Senin', 'N'),
(3, 'Selasa', 'N'),
(4, 'Rabu', 'N'),
(5, 'Kamis', 'N'),
(6, 'Jumat', 'N'),
(7, 'Sabtu', 'N'),
(1, 'Minggu', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `honormgm`
--

CREATE TABLE `honormgm` (
  `ID` int(11) NOT NULL,
  `KodeProgram` varchar(10) NOT NULL DEFAULT '',
  `StatusAwal` char(2) NOT NULL DEFAULT '',
  `PMBHonor` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas`
--

CREATE TABLE `identitas` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `KodeHukum` varchar(10) DEFAULT NULL,
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `TglMulai` date NOT NULL DEFAULT '0000-00-00',
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `KodePos` varchar(20) DEFAULT NULL,
  `Telp` varchar(20) DEFAULT NULL,
  `Fax` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Website` varchar(50) DEFAULT NULL,
  `NoAkta` varchar(50) DEFAULT NULL,
  `TglAkta` date DEFAULT NULL,
  `NoSah` varchar(50) DEFAULT NULL,
  `TglSah` date DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `identitas`
--

INSERT INTO `identitas` (`Kode`, `KodeHukum`, `Nama`, `TglMulai`, `Alamat1`, `Alamat2`, `Kota`, `KodePos`, `Telp`, `Fax`, `Email`, `Website`, `NoAkta`, `TglAkta`, `NoSah`, `TglSah`, `Logo`) VALUES
('023059', '', 'Sekolah Tinggi Ilmu Ekonomi Prabumulih', '2000-08-10', 'Jln. Patra No. 50 Rt. 01 Rw. 03 Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih Sumsel', 'Prabumulih', 'Prabumulih', '31122', '0713-322417', '0713-322418', 'stieprabumulihypp@gmail.com', 'stieypprabumulih.ac.id', '155/D/O/2000', '2000-08-10', '', '2000-08-10', 'image/logo.bmp');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatanakademik`
--

CREATE TABLE `jabatanakademik` (
  `Kode` char(1) NOT NULL DEFAULT '',
  `Nama` varchar(30) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatanakademik`
--

INSERT INTO `jabatanakademik` (`Kode`, `Nama`, `NotActive`) VALUES
('K', 'Kajur', 'N'),
('S', 'SEKJUR', 'N'),
('D', 'Dekan', 'N'),
('W', 'Wakil Dekan I', 'N'),
('Z', 'Wakil Dekan II', 'N'),
('Y', 'Wakil Dekan III', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatandikti`
--

CREATE TABLE `jabatandikti` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatandikti`
--

INSERT INTO `jabatandikti` (`Kode`, `Nama`, `NotActive`) VALUES
('01', 'AAM', 'N'),
('02', 'AA', 'N'),
('03', 'LMu', 'N'),
('04', 'LMa', 'N'),
('05', 'L', 'N'),
('06', 'LKM', 'N'),
('07', 'LK', 'N'),
('08', 'GBM', 'N'),
('09', 'GB', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatanorganisasi`
--

CREATE TABLE `jabatanorganisasi` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `SKS` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatanorganisasi`
--

INSERT INTO `jabatanorganisasi` (`Kode`, `Rank`, `Nama`, `SKS`, `NotActive`) VALUES
('DSN', 1, 'Dosen Luar Biasa', 12, 'N'),
('DSNTTP', 2, 'Dosen Tetap', 20, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `ID` int(11) NOT NULL,
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `Terjadwal` enum('Y','N') DEFAULT 'Y',
  `KodeFakultas` varchar(10) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `NamaMK` varchar(255) DEFAULT NULL,
  `Global` enum('Y','N') NOT NULL DEFAULT 'N',
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `IPDosen` decimal(5,2) NOT NULL DEFAULT '0.00',
  `Hari` smallint(2) NOT NULL DEFAULT '0',
  `Program` varchar(10) DEFAULT '0',
  `JamMulai` time NOT NULL DEFAULT '00:00:00',
  `JamSelesai` time NOT NULL DEFAULT '00:00:00',
  `KodeKampus` varchar(10) NOT NULL DEFAULT '',
  `KodeRuang` varchar(10) NOT NULL DEFAULT '',
  `Kelas` smallint(6) NOT NULL DEFAULT '0',
  `Rencana` int(11) DEFAULT '0',
  `Realisasi` int(11) DEFAULT '0',
  `PasswordNilai` varchar(10) DEFAULT NULL,
  `unip` varchar(20) DEFAULT NULL,
  `Tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `hd_1` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_2` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_3` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_4` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_5` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_6` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_7` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_8` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_9` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_10` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_11` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_12` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_13` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_14` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_15` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_16` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_17` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_18` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_19` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hd_20` enum('-1','0','1') NOT NULL DEFAULT '0',
  `hr_1` date NOT NULL DEFAULT '0000-00-00',
  `hr_2` date NOT NULL DEFAULT '0000-00-00',
  `hr_3` date NOT NULL DEFAULT '0000-00-00',
  `hr_4` date NOT NULL DEFAULT '0000-00-00',
  `hr_5` date NOT NULL DEFAULT '0000-00-00',
  `hr_6` date NOT NULL DEFAULT '0000-00-00',
  `hr_7` date NOT NULL DEFAULT '0000-00-00',
  `hr_8` date NOT NULL DEFAULT '0000-00-00',
  `hr_9` date NOT NULL DEFAULT '0000-00-00',
  `hr_10` date NOT NULL DEFAULT '0000-00-00',
  `hr_11` date NOT NULL DEFAULT '0000-00-00',
  `hr_12` date NOT NULL DEFAULT '0000-00-00',
  `hr_13` date NOT NULL DEFAULT '0000-00-00',
  `hr_14` date NOT NULL DEFAULT '0000-00-00',
  `hr_15` date NOT NULL DEFAULT '0000-00-00',
  `hr_16` date NOT NULL DEFAULT '0000-00-00',
  `hr_17` date NOT NULL DEFAULT '0000-00-00',
  `hr_18` date NOT NULL DEFAULT '0000-00-00',
  `hr_19` date NOT NULL DEFAULT '0000-00-00',
  `hr_20` date NOT NULL DEFAULT '0000-00-00',
  `PersenHadir` decimal(5,2) NOT NULL DEFAULT '10.00',
  `PersenTugas` decimal(5,2) NOT NULL DEFAULT '10.00',
  `JumlahTugas` smallint(6) NOT NULL DEFAULT '0',
  `PersenTugas1` int(11) NOT NULL DEFAULT '0',
  `PersenTugas2` int(11) NOT NULL DEFAULT '0',
  `PersenTugas3` int(11) NOT NULL DEFAULT '0',
  `PersenTugas4` int(11) NOT NULL DEFAULT '0',
  `PersenTugas5` int(11) NOT NULL DEFAULT '0',
  `PersenMID` decimal(5,2) NOT NULL DEFAULT '30.00',
  `PersenUjian` decimal(5,2) NOT NULL DEFAULT '50.00',
  `Tunda` enum('Y','N') NOT NULL DEFAULT 'N',
  `AlasanTunda` varchar(255) DEFAULT NULL,
  `UTSTanggal` date NOT NULL DEFAULT '0000-00-00',
  `UTSMulai` time NOT NULL DEFAULT '00:00:00',
  `UTSSelesai` time NOT NULL DEFAULT '00:00:00',
  `UTSKampus` varchar(10) DEFAULT NULL,
  `UTSRuang` varchar(10) DEFAULT NULL,
  `UASTanggal` date NOT NULL DEFAULT '0000-00-00',
  `UASMulai` time NOT NULL DEFAULT '00:00:00',
  `UASSelesai` time NOT NULL DEFAULT '00:00:00',
  `UASKampus` varchar(10) DEFAULT NULL,
  `UASRuang` varchar(10) DEFAULT NULL,
  `SSLTanggal` date NOT NULL DEFAULT '0000-00-00',
  `SSLMulai` time NOT NULL DEFAULT '00:00:00',
  `SSLSelesai` time NOT NULL DEFAULT '00:00:00',
  `SSLKampus` varchar(10) DEFAULT NULL,
  `SSLRuang` varchar(10) DEFAULT NULL,
  `NoSurat` varchar(100) DEFAULT NULL,
  `JabatanOrganisasi` varchar(10) NOT NULL DEFAULT '',
  `SKSHonor` int(11) NOT NULL DEFAULT '0',
  `Honor` int(11) NOT NULL DEFAULT '0',
  `Transport` int(11) NOT NULL DEFAULT '0',
  `Pembulatan` int(11) NOT NULL DEFAULT '0',
  `Tetap` int(11) NOT NULL DEFAULT '0',
  `KUM` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`ID`, `Tahun`, `Terjadwal`, `KodeFakultas`, `KodeJurusan`, `IDMK`, `KodeMK`, `SKS`, `NamaMK`, `Global`, `IDDosen`, `IPDosen`, `Hari`, `Program`, `JamMulai`, `JamSelesai`, `KodeKampus`, `KodeRuang`, `Kelas`, `Rencana`, `Realisasi`, `PasswordNilai`, `unip`, `Tanggal`, `NotActive`, `hd_1`, `hd_2`, `hd_3`, `hd_4`, `hd_5`, `hd_6`, `hd_7`, `hd_8`, `hd_9`, `hd_10`, `hd_11`, `hd_12`, `hd_13`, `hd_14`, `hd_15`, `hd_16`, `hd_17`, `hd_18`, `hd_19`, `hd_20`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `PersenHadir`, `PersenTugas`, `JumlahTugas`, `PersenTugas1`, `PersenTugas2`, `PersenTugas3`, `PersenTugas4`, `PersenTugas5`, `PersenMID`, `PersenUjian`, `Tunda`, `AlasanTunda`, `UTSTanggal`, `UTSMulai`, `UTSSelesai`, `UTSKampus`, `UTSRuang`, `UASTanggal`, `UASMulai`, `UASSelesai`, `UASKampus`, `UASRuang`, `SSLTanggal`, `SSLMulai`, `SSLSelesai`, `SSLKampus`, `SSLRuang`, `NoSurat`, `JabatanOrganisasi`, `SKSHonor`, `Honor`, `Transport`, `Pembulatan`, `Tetap`, `KUM`) VALUES
(10, '20161', 'Y', 'STIE-P', '61201', 57, 'MPK 101', 2, 'PENDIDIKAN AGAMA', 'N', 8, '0.00', 2, 'REG', '13:00:00', '14:40:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:25:56', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-30', '09:00:00', '10:30:00', '023059', 'R1A', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(11, '20161', 'Y', 'STIE-P', '61201', 62, 'MPK 106', 3, 'BAHASA INGGRIS', 'N', 8, '0.00', 2, 'REG', '15:30:00', '18:00:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:29:08', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(12, '20161', 'Y', 'STIE-P', '61201', 58, 'MPK 102', 2, 'PENDIDIKAN PANCASILA', 'N', 8, '0.00', 3, 'REG', '13:00:00', '14:40:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:29:54', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(13, '20161', 'Y', 'STIE-P', '61201', 65, 'MKK 202', 3, 'PENGANTAR AKUNTANSI', 'N', 3, '0.00', 3, 'REG', '15:30:00', '18:00:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:31:59', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(14, '20161', 'Y', 'STIE-P', '61201', 59, 'MPK 103', 2, 'ILMU ALAMIAH DASAR', 'N', 8, '0.00', 4, 'REG', '13:00:00', '14:40:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:32:42', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(15, '20161', 'Y', 'STIE-P', '61201', 64, 'MKK 201', 3, 'PENGANTAR EKONOMI MIKRO', 'N', 16, '0.00', 4, 'REG', '15:30:00', '18:00:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:34:29', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(16, '20161', 'Y', 'STIE-P', '61201', 66, 'MKK 203', 3, 'MATEMATIKA EKONOMI', 'N', 9, '0.00', 6, 'REG', '13:00:00', '15:25:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:35:16', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(17, '20161', 'Y', 'STIE-P', '61201', 61, 'MPK 105', 2, 'BAHASA INDONESIA', 'N', 8, '0.00', 6, 'REG', '15:30:00', '17:30:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-30 10:36:04', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(18, '20161', 'Y', 'STIE-P', '62201', 2, 'MPK 102', 2, 'PENDIDIKAN PANCASILA', 'N', 5, '0.00', 2, 'REG', '13:00:00', '14:40:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:04:21', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-19', '2016-09-26', '2016-10-03', '2016-10-10', '2016-10-17', '2016-10-24', '2016-10-31', '2016-11-07', '2016-11-14', '2016-11-21', '2016-11-28', '2016-12-05', '2016-12-12', '2016-12-19', '2016-12-26', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-04', '09:00:00', '10:30:00', '023059', 'R1F', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(19, '20161', 'Y', 'STIE-P', '62201', 9, 'MKK 203', 3, 'PENGANTAR AKUNTANSI', 'N', 4, '0.00', 2, 'REG', '15:30:00', '18:00:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:05:05', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-19', '2016-09-26', '2016-10-03', '2016-10-10', '2016-10-17', '2016-10-24', '2016-10-31', '2016-11-07', '2016-11-14', '2016-11-21', '2016-11-28', '2016-12-05', '2016-12-12', '2016-12-19', '2016-12-26', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-04', '10:45:00', '12:15:00', '023059', 'R1G', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(20, '20161', 'Y', 'STIE-P', '62201', 1, 'MPK 101', 2, 'PENDIDIKAN AGAMA', 'N', 20, '0.00', 3, 'REG', '13:00:00', '14:40:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:05:49', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-13', '2016-09-20', '2016-09-27', '2016-10-04', '2016-10-11', '2016-10-18', '2016-10-25', '2016-11-01', '2016-11-08', '2016-11-15', '2016-11-22', '2016-11-29', '2016-12-06', '2016-12-13', '2016-12-20', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-03', '09:00:00', '10:30:00', '023059', 'R1F', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(21, '20161', 'Y', 'STIE-P', '62201', 6, 'MPK 106', 3, 'BAHASA INGGRIS', 'N', 10, '0.00', 3, 'REG', '15:30:00', '18:00:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:07:09', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-13', '2016-09-20', '2016-09-27', '2016-10-04', '2016-10-11', '2016-10-18', '2016-10-25', '2016-11-01', '2016-11-08', '2016-11-15', '2016-11-22', '2016-11-29', '2016-12-06', '2016-12-13', '2016-12-20', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-03', '10:45:00', '12:15:00', '023059', 'R1G', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(22, '20161', 'Y', 'STIE-P', '62201', 24, 'MKK 219', 3, 'MATEMATIKA EKONOMI', 'N', 22, '0.00', 4, 'REG', '13:00:00', '15:25:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:08:01', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-14', '2016-09-21', '2016-09-28', '2016-10-05', '2016-10-12', '2016-10-19', '2016-10-26', '2016-11-02', '2016-11-09', '2016-11-16', '2016-11-23', '2016-11-30', '2016-12-07', '2016-12-14', '2016-12-21', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-06', '09:45:00', '11:15:00', '023059', 'R1G', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(23, '20161', 'Y', 'STIE-P', '62201', 3, 'MPK 103', 2, 'ILMU ALAMIAH DASAR', 'N', 12, '0.00', 4, 'REG', '15:30:00', '16:30:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:08:56', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-14', '2016-09-21', '2016-09-28', '2016-10-05', '2016-10-12', '2016-10-19', '2016-10-26', '2016-11-02', '2016-11-09', '2016-11-16', '2016-11-23', '2016-11-30', '2016-12-07', '2016-12-14', '2016-12-21', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-05', '09:00:00', '10:30:00', '023059', 'R1F', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(24, '20161', 'Y', 'STIE-P', '62201', 5, 'MPK 105', 2, 'BAHASA INDONESIA', 'N', 20, '0.00', 6, 'REG', '13:00:00', '14:40:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:09:43', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-16', '2016-09-23', '2016-09-30', '2016-10-07', '2016-10-14', '2016-10-21', '2016-10-28', '2016-11-04', '2016-11-11', '2016-11-18', '2016-11-25', '2016-12-02', '2016-12-09', '2016-12-16', '2016-12-23', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-06', '08:00:00', '09:30:00', '023059', 'R1F', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(25, '20161', 'Y', 'STIE-P', '62201', 7, 'MKK 201', 3, 'PENGANTAR EKONOMI MIKRO', 'N', 12, '0.00', 6, 'REG', '15:30:00', '18:00:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-30 11:10:24', 'N', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '2016-09-16', '2016-09-23', '2016-09-30', '2016-10-07', '2016-10-14', '2016-10-21', '2016-10-28', '2016-11-04', '2016-11-11', '2016-11-18', '2016-11-25', '2016-12-02', '2016-12-09', '2016-12-16', '2016-12-23', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0.00', '0.00', 0, 0, 0, 0, 0, 0, '0.00', '100.00', 'N', NULL, '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, '2017-01-05', '10:45:00', '12:15:00', '023059', 'R1G', '2017-04-30', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(26, '20162', 'Y', 'STIE-P', '62201', 14, 'MKK 208', 2, 'PRAKTIKUM PENGANTAR AKUNTANSI', 'N', 22, '0.00', 2, 'REG', '13:00:00', '15:30:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-11 16:00:50', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(27, '20162', 'Y', 'STIE-P', '62201', 26, 'MKK 221', 3, 'STATISTIK', 'N', 12, '0.00', 2, 'REG', '15:45:00', '18:00:00', '023059', 'R1H', 0, 16, 16, NULL, 'admin', '2017-04-11 16:01:50', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(28, '20162', 'Y', 'STIE-P', '62201', 4, 'MPK 104', 2, 'KEWARGANEGARAAN', 'N', 5, '0.00', 3, 'REG', '13:00:00', '14:40:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-11 16:03:01', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(29, '20162', 'Y', 'STIE-P', '62201', 12, 'MKK 206', 2, 'PENGANTAR EKONOMI PEMBANGUNAN', 'N', 12, '0.00', 3, 'REG', '15:00:00', '18:00:00', '023059', 'R1H', 0, 16, 16, NULL, 'admin', '2017-04-11 16:03:59', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(30, '20162', 'Y', 'STIE-P', '62201', 13, 'MKK 207', 3, 'PENGANTAR APLIKASI KOMPUTER', 'N', 14, '0.00', 4, 'REG', '13:00:00', '15:30:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-11 16:05:01', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(31, '20162', 'Y', 'STIE-P', '62201', 8, 'MKK 202', 3, 'PENGANTAR BISNIS', 'N', 11, '0.00', 4, 'REG', '15:45:00', '18:00:00', '023059', 'R1H', 0, 16, 16, NULL, 'admin', '2017-04-11 16:06:12', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(32, '20162', 'Y', 'STIE-P', '62201', 10, 'MKK 204', 3, 'PENGANTAR EKONOMI MAKRO', 'N', 5, '0.00', 6, 'REG', '13:00:00', '14:40:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-11 16:06:58', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(33, '20162', 'Y', 'STIE-P', '62201', 11, 'MKK 205', 3, 'PENGANTAR MANAJEMEN', 'N', 15, '0.00', 6, 'REG', '15:30:00', '18:00:00', '023059', 'R1H', 0, 16, 16, NULL, 'admin', '2017-04-11 16:07:59', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(34, '20162', 'Y', 'STIE-P', '62201', 45, 'MPB 502', 3, 'ENGLISH FOR BUSSINES', 'N', 23, '0.00', 5, 'REG', '13:00:00', '15:30:00', '023059', 'R1G', 0, 16, 16, NULL, 'admin', '2017-04-11 16:26:22', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, '2017-04-11', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(35, '20162', 'Y', 'STIE-P', '61201', 60, 'MPK 104', 2, 'KEWARGANEGARAAN', 'N', 8, '0.00', 2, 'REG', '13:00:00', '14:40:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-17 11:05:55', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 2, 0, 0, 0, 0, 0),
(36, '20162', 'Y', 'STIE-P', '61201', 67, 'MKK 204', 3, 'PENGANTAR EKONOMI MAKRO', 'N', 21, '0.00', 2, 'REG', '15:30:00', '18:00:00', '023059', 'R1F', 0, 16, 16, NULL, 'admin', '2017-04-17 11:06:38', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(37, '20162', 'Y', 'STIE-P', '61201', 68, 'MKK 205', 3, 'PENGANTAR MANAJEMEN', 'N', 16, '0.00', 3, 'REG', '13:00:00', '15:30:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-17 11:07:32', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(38, '20162', 'Y', 'STIE-P', '61201', 69, 'MKK 206', 3, 'PENGANTAR BISNIS', 'N', 16, '0.00', 3, 'REG', '15:30:00', '18:00:00', '023059', 'R1F', 0, 16, 16, NULL, 'admin', '2017-04-17 11:08:16', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(39, '20162', 'Y', 'STIE-P', '61201', 76, 'MKK 213', 3, 'PENGANTAR EKONOMI PEMBANGUNAN', 'N', 21, '0.00', 4, 'REG', '13:00:00', '15:30:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-17 11:10:17', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(40, '20162', 'Y', 'STIE-P', '61201', 78, 'MPB 401', 3, 'ENGLISH FOR BUSINESS', 'N', 6, '0.00', 4, 'REG', '15:30:00', '18:00:00', '023059', 'R1F', 0, 16, 16, NULL, 'admin', '2017-04-17 11:11:17', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(41, '20162', 'Y', 'STIE-P', '61201', 75, 'MKK 212', 3, 'PENGANTAR APLIKASI KOMPUTER', 'N', 19, '0.00', 6, 'REG', '13:00:00', '15:30:00', '023059', 'R1E', 0, 16, 16, NULL, 'admin', '2017-04-17 11:12:16', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0),
(42, '20162', 'Y', 'STIE-P', '61201', 70, 'MKK 207', 3, 'STATISTIK', 'N', 10, '0.00', 6, 'REG', '15:30:00', '18:00:00', '023059', 'R1F', 0, 16, 16, NULL, 'admin', '2017-04-17 11:13:07', 'N', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '10.00', '10.00', 0, 0, 0, 0, 0, 0, '30.00', '50.00', 'N', NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, '2017-04-17', '00:00:00', '00:00:00', NULL, NULL, NULL, '', 3, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwalassdsn`
--

CREATE TABLE `jadwalassdsn` (
  `ID` int(11) NOT NULL,
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwalgantidsn`
--

CREATE TABLE `jadwalgantidsn` (
  `ID` int(11) NOT NULL,
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `Pertemuan` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwalgantidsn`
--

INSERT INTO `jadwalgantidsn` (`ID`, `IDJadwal`, `IDDosen`, `Pertemuan`) VALUES
(3, 410, 172, 1),
(4, 393, 172, 1),
(5, 384, 41, 2),
(6, 312, 41, 2),
(7, 454, 292, 3),
(8, 361, 339, 2),
(9, 375, 339, 2),
(10, 411, 172, 1),
(11, 394, 172, 1),
(12, 411, 172, 5),
(13, 410, 172, 5),
(14, 393, 172, 5),
(15, 394, 172, 5),
(16, 439, 172, 5),
(17, 439, 172, 1),
(18, 408, 362, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenisbayar`
--

CREATE TABLE `jenisbayar` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Tunai` enum('Y','N') NOT NULL DEFAULT 'N',
  `Keterangan` varchar(255) DEFAULT NULL,
  `Def` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenisbayar`
--

INSERT INTO `jenisbayar` (`ID`, `Nama`, `Tunai`, `Keterangan`, `Def`, `NotActive`) VALUES
(1, 'Tunai', 'Y', 'Bag. Keuangan', 'Y', 'Y'),
(2, 'BCA', 'N', 'Tapres, GIRO, Transfer, DEBIT', 'N', 'Y'),
(3, 'BDN', 'N', '', 'N', 'Y'),
(4, 'LIPPO BANK', 'N', 'LIPPO', 'N', 'Y'),
(5, 'Bank Commonwealth', 'N', '', 'N', 'Y'),
(6, 'SUMSEL BABEL', 'N', 'PEMBAYARAN SPP', 'N', 'N'),
(7, 'BNI', 'N', 'Pembayaran uang KKN', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenisbiaya`
--

CREATE TABLE `jenisbiaya` (
  `ID` smallint(6) NOT NULL DEFAULT '0',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Otomatis` smallint(6) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenisbiaya`
--

INSERT INTO `jenisbiaya` (`ID`, `Nama`, `Otomatis`, `NotActive`) VALUES
(10, 'Awal Masuk', 0, 'N'),
(20, 'Tiap Semester', 1, 'N'),
(30, 'Lulus Studi', 0, 'N'),
(40, 'Penyetaraan', 0, 'N'),
(5, 'Sebelum Masuk', 1, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenismatakuliah`
--

CREATE TABLE `jenismatakuliah` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Wajib` enum('Y','N') NOT NULL DEFAULT 'N',
  `TA` enum('Y','N') NOT NULL DEFAULT 'N',
  `KodeFakultas` varchar(10) NOT NULL DEFAULT '',
  `Jenjang` varchar(5) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenismatakuliah`
--

INSERT INTO `jenismatakuliah` (`Kode`, `Rank`, `Nama`, `Wajib`, `TA`, `KodeFakultas`, `Jenjang`, `NotActive`) VALUES
('MKDU', 0, 'Mata Kuliah Dasar Umum', 'Y', 'N', 'TEK', 'C', 'N'),
('MKD', 1, 'Mata Kuliah Dasar', 'Y', 'N', 'TEK', 'C', 'N'),
('MKP', 2, 'Mata Kuliah Pilihan', 'N', 'N', 'TEK', 'C', 'N'),
('MPK', 1, 'MATA KULIAH PENGEMBANGAN KEPRIBADIAN', 'Y', 'N', 'STIE-P', 'C', 'N'),
('MKK', 2, 'MATA KULIAH KEILMUAN DAN KETERAMPILAN', 'Y', 'N', 'STIE-P', 'C', 'N'),
('MKB', 3, 'MATA KULIAH KEAHLIAN BERKARYA', 'Y', 'N', 'STIE-P', 'C', 'N'),
('MBB', 4, 'MATA KULIAH BERKEHIDUPAN BERMASYARAKAT', 'Y', 'Y', 'STIE-P', 'C', 'N'),
('MPB', 5, 'MATA KULIAH PERILAKU BERKARYA', 'Y', 'N', 'STIE-P', 'C', 'N'),
('MKP', 6, 'MATA KULIAH PILIHAN', 'N', 'N', 'STIE-P', 'C', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenistrx`
--

CREATE TABLE `jenistrx` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Kali` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenistrx`
--

INSERT INTO `jenistrx` (`ID`, `Nama`, `Kali`, `NotActive`) VALUES
(1, 'Pembayaran', 1, 'N'),
(2, 'Pengembalian Kelebihan', -1, 'N'),
(3, 'Koreksi/Batal', 0, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenjangps`
--

CREATE TABLE `jenjangps` (
  `Kode` char(1) NOT NULL DEFAULT '',
  `Nama` varchar(20) NOT NULL DEFAULT '',
  `Keterangan` varchar(100) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenjangps`
--

INSERT INTO `jenjangps` (`Kode`, `Nama`, `Keterangan`, `NotActive`) VALUES
('A', 'S3', 'Strata Tiga', 'N'),
('B', 'S2', 'Strata Dua', 'N'),
('C', 'S1', 'Strata Satu', 'N'),
('D', 'D4', 'Diploma 4', 'N'),
('E', 'D3', 'Diploma 3', 'N'),
('F', 'D2', 'Diploma 2', 'N'),
('G', 'D1', 'Diploma 1', 'N'),
('H', 'SP-1', 'Spesialis Satu', 'N'),
('I', 'SP-2', 'Spesialis Dua', 'N'),
('J', 'PROFESI', 'Profesi', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurprg`
--

CREATE TABLE `jurprg` (
  `ID` int(11) NOT NULL,
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `KodeProgram` varchar(20) NOT NULL DEFAULT '',
  `Prefix` varchar(5) NOT NULL DEFAULT '',
  `Honor` int(11) NOT NULL DEFAULT '0',
  `Transport` int(11) NOT NULL DEFAULT '0',
  `Pembulatan` int(11) NOT NULL DEFAULT '0',
  `Tetap` int(11) NOT NULL DEFAULT '0',
  `KUM` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurprg`
--

INSERT INTO `jurprg` (`ID`, `KodeJurusan`, `KodeProgram`, `Prefix`, `Honor`, `Transport`, `Pembulatan`, `Tetap`, `KUM`, `NotActive`) VALUES
(13, '62201', 'REG', '', 0, 0, 0, 0, 0, 'N'),
(12, '61201', 'REG', '', 0, 0, 0, 0, 0, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `KodePS` varchar(10) DEFAULT NULL,
  `Prefix` varchar(5) DEFAULT NULL,
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Nama_Indonesia` varchar(100) NOT NULL DEFAULT '',
  `Nama_English` varchar(100) NOT NULL DEFAULT '',
  `Jenjang` char(1) DEFAULT NULL,
  `TglMulai` date DEFAULT NULL,
  `KodeFakultas` varchar(10) NOT NULL DEFAULT '',
  `Tahun` varchar(5) DEFAULT NULL,
  `Sesi` varchar(25) DEFAULT NULL,
  `JmlSesi` int(11) NOT NULL DEFAULT '0',
  `KodeNilai` varchar(25) DEFAULT NULL,
  `Currency` varchar(5) NOT NULL DEFAULT '',
  `Bahasa` varchar(5) NOT NULL DEFAULT 'id',
  `Akreditasi` char(1) DEFAULT NULL,
  `NoSKDikti` varchar(50) DEFAULT NULL,
  `TglSKDikti` date DEFAULT NULL,
  `NoSKBAN` varchar(50) DEFAULT NULL,
  `TglSKBAN` date DEFAULT NULL,
  `PMBPrice` int(11) NOT NULL DEFAULT '0',
  `PMBHonor` int(11) NOT NULL DEFAULT '0',
  `MinSKS` int(11) NOT NULL DEFAULT '0',
  `DefSKS` int(11) DEFAULT '0',
  `MaxWaktu` int(11) DEFAULT '0',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `TargetMhs` int(11) NOT NULL DEFAULT '0',
  `Pendaftar` int(11) NOT NULL DEFAULT '0',
  `LulusSeleksi` int(11) NOT NULL DEFAULT '0',
  `PendaftarMhs` int(11) NOT NULL DEFAULT '0',
  `PendaftarMundur` int(11) NOT NULL DEFAULT '0',
  `MhsPindahan` int(11) NOT NULL DEFAULT '0',
  `TA` int(11) DEFAULT '0',
  `IjazahNomer` varchar(255) DEFAULT NULL,
  `IjazahAkreditasi` varchar(255) DEFAULT NULL,
  `Gelar` varchar(255) DEFAULT NULL,
  `Jabatan1` varchar(255) DEFAULT NULL,
  `Jabatan2` varchar(255) DEFAULT NULL,
  `Pejabat1` varchar(255) DEFAULT NULL,
  `Pejabat2` varchar(255) DEFAULT NULL,
  `IjazahTemplate` varchar(255) DEFAULT NULL,
  `TTJabatan1` varchar(255) DEFAULT NULL,
  `TTJabatan2` varchar(255) DEFAULT NULL,
  `TTPejabat1` varchar(255) DEFAULT NULL,
  `TTPejabat2` varchar(255) DEFAULT NULL,
  `TTHonorDosen1` varchar(100) DEFAULT NULL,
  `TTHonorDosen2` varchar(100) DEFAULT NULL,
  `TTHonorDosenJabatan1` varchar(100) DEFAULT NULL,
  `TTHonorDosenJabatan2` varchar(100) DEFAULT NULL,
  `PasswordNilai` enum('Y','N') NOT NULL DEFAULT 'N',
  `PrcKeuTahun` varchar(5) NOT NULL DEFAULT '0',
  `PrcKeuTgl` date NOT NULL DEFAULT '0000-00-00',
  `PrcKeu` int(11) NOT NULL DEFAULT '0',
  `NoDok` varchar(50) NOT NULL DEFAULT '~INPUT~',
  `Predikat` varchar(25) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`Kode`, `KodePS`, `Prefix`, `Rank`, `Nama_Indonesia`, `Nama_English`, `Jenjang`, `TglMulai`, `KodeFakultas`, `Tahun`, `Sesi`, `JmlSesi`, `KodeNilai`, `Currency`, `Bahasa`, `Akreditasi`, `NoSKDikti`, `TglSKDikti`, `NoSKBAN`, `TglSKBAN`, `PMBPrice`, `PMBHonor`, `MinSKS`, `DefSKS`, `MaxWaktu`, `Login`, `Tgl`, `NotActive`, `TargetMhs`, `Pendaftar`, `LulusSeleksi`, `PendaftarMhs`, `PendaftarMundur`, `MhsPindahan`, `TA`, `IjazahNomer`, `IjazahAkreditasi`, `Gelar`, `Jabatan1`, `Jabatan2`, `Pejabat1`, `Pejabat2`, `IjazahTemplate`, `TTJabatan1`, `TTJabatan2`, `TTPejabat1`, `TTPejabat2`, `TTHonorDosen1`, `TTHonorDosen2`, `TTHonorDosenJabatan1`, `TTHonorDosenJabatan2`, `PasswordNilai`, `PrcKeuTahun`, `PrcKeuTgl`, `PrcKeu`, `NoDok`, `Predikat`) VALUES
('62201', '62201', NULL, 1, 'EKONOMI AKUNTANSI', 'EKONOMI AKUNTANSI', 'C', NULL, 'STIE-P', '20162', 'Semester', 0, 'Umum', 'IDR', 'id', 'C', '3768/D/T/K-II/2009', '2009-10-30', '024/BAN-PT/AK-XV/S1/VIII/2012', '2009-08-10', 500000, 0, 145, 21, 10, 'admin', '2017-01-27 10:46:26', 'N', 0, 0, 0, 0, 0, 0, 0, '0001', 'C', 'SARJANA', 'KETUA STIE-P', 'PEMBANTU KETUA 1', 'YUDI TUSRI, SE., M.Si', 'AJABAR, S.IP., M.M', '', 'KETUA STIE-P', 'PEMBANTU KETUA 1', 'YUDI TUSRI, SE., M.Si', 'AJABAR, S.IP., M.M', NULL, NULL, NULL, NULL, 'N', '0', '0000-00-00', 0, '3768/D/T/K-II/2009', 'D3-S1'),
('61201', '61201', NULL, 2, 'EKONOMI MANAJEMEN', 'EKONOMI MANAJEMEN', 'C', NULL, 'STIE-P', '20162', 'Semester', 0, 'Umum', 'IDR', 'id', 'C', '3767/D/T/K-II/2009', '2009-10-30', '028/BAN-PT/AK-XV/S1/X/2012', '2010-10-18', 500000, 0, 145, 21, 10, 'admin', '2017-01-31 11:01:00', 'N', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', '0', '0000-00-00', 0, '', 'D3-S1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kampus`
--

CREATE TABLE `kampus` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Kampus` varchar(100) NOT NULL DEFAULT '',
  `Alamat` varchar(255) NOT NULL DEFAULT '',
  `Telepon` varchar(100) NOT NULL DEFAULT '',
  `ImgLink` varchar(100) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kampus`
--

INSERT INTO `kampus` (`Kode`, `Kampus`, `Alamat`, `Telepon`, `ImgLink`, `NotActive`) VALUES
('023059', 'STIE PRABUMULIH', 'Jl. Patra no 50 Rt.01 Rw.03 Kelurahan Sukaraja Kecamatan Prabumulih Selatan Kota Prabumulih Sumatera Selatan', '0713322417', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `ID` int(11) NOT NULL,
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Password` varchar(10) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) NOT NULL DEFAULT '',
  `Phone` varchar(30) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`ID`, `Login`, `Password`, `Description`, `Name`, `Email`, `Phone`, `NotActive`) VALUES
(1, 'ranti', '*84AAC12F5', '', 'Ranti Yuliningsih', 'ranti_yuliningsih@yahoo.com', '', 'N'),
(2, 'anggun', '*84AAC12F5', '', 'Anggun Kris Chindy', '', '', 'N'),
(3, 'farida', '*84AAC12F5', '', 'Farida', '', '', 'N'),
(4, 'santi', '*84AAC12F5', '', 'Roba&#39;ia Juniarti', '', '', 'N'),
(5, 'ade', '*84AAC12F5', '', 'Chairani Adelina Arifin', '', '', 'N'),
(6, 'heri', '*84AAC12F5', '', 'Heri Munandar', '', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `ID` smallint(6) NOT NULL DEFAULT '0',
  `Kelas` varchar(20) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kewajibanmengajar`
--

CREATE TABLE `kewajibanmengajar` (
  `ID` int(11) NOT NULL,
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `Bulan` date NOT NULL DEFAULT '0000-00-00',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `Login` int(11) NOT NULL DEFAULT '0',
  `Tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `khs`
--

CREATE TABLE `khs` (
  `ID` int(11) NOT NULL,
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Tahun` varchar(10) NOT NULL DEFAULT '',
  `Sesi` int(11) DEFAULT NULL,
  `Status` char(1) NOT NULL DEFAULT 'S',
  `Registrasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglRegistrasi` date NOT NULL DEFAULT '0000-00-00',
  `Nilai` decimal(5,2) DEFAULT '0.00',
  `GradeNilai` varchar(5) DEFAULT NULL,
  `Bobot` decimal(5,2) DEFAULT '0.00',
  `SKS` int(11) DEFAULT '0',
  `MaxSKS` int(11) DEFAULT '0',
  `JmlMK` int(11) DEFAULT '0',
  `Catatan` varchar(255) DEFAULT NULL,
  `KodeBiaya` varchar(5) DEFAULT NULL,
  `Biaya` int(11) DEFAULT '0',
  `Bayar` int(11) DEFAULT '0',
  `Potong` int(11) NOT NULL DEFAULT '0',
  `Tarik` int(11) NOT NULL DEFAULT '0',
  `TglKartu` date DEFAULT NULL,
  `TglUbah` date NOT NULL DEFAULT '0000-00-00',
  `CekUbah` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `khs`
--

INSERT INTO `khs` (`ID`, `NIM`, `Tahun`, `Sesi`, `Status`, `Registrasi`, `TglRegistrasi`, `Nilai`, `GradeNilai`, `Bobot`, `SKS`, `MaxSKS`, `JmlMK`, `Catatan`, `KodeBiaya`, `Biaya`, `Bayar`, `Potong`, `Tarik`, `TglKartu`, `TglUbah`, `CekUbah`, `NotActive`) VALUES
(307, '2016110033', '20161', 1, 'A', 'Y', '2017-05-01', '617.90', NULL, '60.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(306, '2016110003', '20161', 1, 'A', 'Y', '2017-05-01', '620.80', NULL, '60.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(305, '2016110018', '20161', 1, 'A', 'Y', '2017-05-01', '623.70', NULL, '63.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(304, '2016110004', '20161', 1, 'A', 'Y', '2017-05-01', '623.70', NULL, '64.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(303, '2016110043', '20161', 1, 'A', 'Y', '2017-05-01', '489.80', NULL, '47.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(302, '2016110044', '20161', 1, 'A', 'Y', '2017-05-01', '533.40', NULL, '49.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(301, '2016110007', '20161', 1, 'A', 'Y', '2017-05-01', '569.70', NULL, '54.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(300, '2016110032', '20161', 1, 'A', 'Y', '2017-05-01', '532.50', NULL, '51.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(299, '2016110040', '20161', 1, 'A', 'Y', '2017-05-01', '603.60', NULL, '60.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(298, '2016110005', '20161', 1, 'A', 'Y', '2017-05-01', '623.80', NULL, '65.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(297, '2016110025', '20161', 1, 'A', 'Y', '2017-05-01', '552.50', NULL, '49.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(296, '2016110019', '20161', 1, 'A', 'Y', '2017-05-01', '571.00', NULL, '55.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(295, '2016110017', '20161', 1, 'A', 'Y', '2017-05-01', '683.60', NULL, '78.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(294, '2016110035', '20161', 1, 'A', 'Y', '2017-05-01', '609.70', NULL, '58.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(293, '2016110030', '20161', 1, 'A', 'Y', '2017-05-01', '573.30', NULL, '55.00', 21, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-05-01', 'N', 'N'),
(292, '2016110020', '20161', 1, 'A', 'Y', '2017-05-01', '641.50', NULL, '68.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(291, '2016110016', '20161', 1, 'A', 'Y', '2017-05-01', '659.60', NULL, '70.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(290, '201611046P', '20161', 1, 'A', 'Y', '2017-05-01', '578.00', NULL, '58.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(289, '2016110028', '20161', 1, 'A', 'Y', '2017-05-01', '621.70', NULL, '60.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(288, '2016110014', '20161', 1, 'A', 'Y', '2017-05-01', '180.00', NULL, '14.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(287, '2016110027', '20161', 1, 'A', 'Y', '2017-05-01', '690.40', NULL, '75.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(286, '2016110012', '20161', 1, 'A', 'Y', '2017-05-01', '623.80', NULL, '60.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(285, '2016110031', '20161', 1, 'A', 'Y', '2017-05-01', '220.00', NULL, '14.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(284, '2016110011', '20161', 1, 'A', 'Y', '2017-05-01', '640.70', NULL, '64.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(283, '2016110002', '20161', 1, 'A', 'Y', '2017-04-30', '595.60', NULL, '56.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(282, '2016110021', '20161', 1, 'A', 'Y', '2017-05-01', '629.50', NULL, '66.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(281, '2016110010', '20161', 1, 'A', 'Y', '2017-05-01', '626.40', NULL, '60.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(280, '2016110047', '20161', 1, 'A', 'Y', '2017-05-01', '620.90', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(279, '2016110039', '20161', 1, 'A', 'Y', '2017-05-01', '643.30', NULL, '68.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(278, '2016110013', '20161', 1, 'A', 'Y', '2017-05-01', '636.80', NULL, '66.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(277, '2016110023', '20161', 1, 'A', 'Y', '2017-05-01', '635.40', NULL, '65.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(276, '2016110008', '20161', 1, 'A', 'Y', '2017-05-01', '652.70', NULL, '68.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(275, '2016110026', '20161', 1, 'A', 'Y', '2017-05-01', '680.50', NULL, '74.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(274, '2016110041', '20161', 1, 'A', 'Y', '2017-05-01', '579.30', NULL, '53.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(273, '201611022P', '20161', 1, 'A', 'Y', '2017-05-01', '584.00', NULL, '57.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(272, '2016110001', '20161', 1, 'A', 'Y', '2017-04-30', '689.20', NULL, '71.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, '2020-09-12', '2017-04-30', 'N', 'N'),
(271, '2016110029', '20161', 1, 'A', 'Y', '2017-05-01', '642.50', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(270, '2016110034', '20161', 1, 'A', 'Y', '2017-05-01', '643.40', NULL, '68.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(269, '2016110036', '20161', 1, 'A', 'Y', '2017-05-01', '620.80', NULL, '63.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(268, '2016110009', '20161', 1, 'A', 'Y', '2017-05-01', '190.00', NULL, '16.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(267, '201611045P', '20161', 1, 'A', 'Y', '2017-05-01', '579.10', NULL, '60.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(266, '2016110037', '20161', 1, 'A', 'Y', '2017-05-01', '649.70', NULL, '70.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(265, '2016110006', '20161', 1, 'A', 'Y', '2017-05-01', '576.80', NULL, '56.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(264, '2016110024', '20161', 1, 'A', 'Y', '2017-05-01', '625.10', NULL, '63.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(263, '2016110015', '20161', 1, 'A', 'Y', '2017-05-01', '607.90', NULL, '61.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(262, '201611042P', '20161', 1, 'A', 'Y', '2017-05-01', '572.10', NULL, '53.00', 20, 20, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(261, '2016110038', '20161', 1, 'A', 'Y', '2017-05-01', '649.70', NULL, '70.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(260, '2016120008', '20161', 1, 'A', 'Y', '2017-04-08', '600.50', NULL, '58.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(259, '2016120011', '20161', 1, 'A', 'Y', '2017-04-09', '625.80', NULL, '65.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(258, '2016120014', '20161', 1, 'A', 'Y', '2017-04-09', '618.40', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(257, '2016120023', '20161', 1, 'A', 'Y', '2017-04-09', '607.10', NULL, '56.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(256, '2016120020', '20161', 1, 'A', 'Y', '2017-04-09', '622.70', NULL, '63.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(255, '2016120017', '20161', 1, 'A', 'Y', '2017-04-09', '605.30', NULL, '58.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(254, '2016120018', '20161', 1, 'A', 'Y', '2017-04-09', '577.70', NULL, '54.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(253, '2016120022', '20161', 1, 'A', 'Y', '2017-04-09', '657.70', NULL, '70.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(252, '2016120006', '20161', 1, 'A', 'Y', '2017-04-08', '591.30', NULL, '58.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(251, '2016120013', '20161', 1, 'A', 'Y', '2017-04-09', '625.00', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(250, '2016120004', '20161', 1, 'A', 'Y', '2017-04-08', '0.00', NULL, '0.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(249, '2016120019', '20161', 1, 'A', 'Y', '2017-04-09', '618.30', NULL, '65.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(248, '2016120024', '20161', 1, 'A', 'Y', '2017-04-09', '627.90', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(247, '2016120009', '20161', 1, 'A', 'Y', '2017-04-08', '681.00', NULL, '74.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(246, '201612007P', '20161', 1, 'A', 'Y', '2017-04-08', '631.70', NULL, '64.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(245, '2016120010', '20161', 1, 'A', 'Y', '2017-04-08', '653.00', NULL, '66.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(244, '2016120012', '20161', 1, 'A', 'Y', '2017-04-09', '612.50', NULL, '63.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(243, '2016120015', '20161', 1, 'A', 'Y', '2017-04-09', '627.40', NULL, '65.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(242, '2016120005', '20161', 1, 'A', 'Y', '2017-04-08', '658.20', NULL, '71.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(241, '2016120021', '20161', 1, 'A', 'Y', '2017-04-09', '693.30', NULL, '77.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(240, '2016120016', '20161', 1, 'A', 'Y', '2017-04-09', '603.20', NULL, '62.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(239, '2016120003', '20161', 1, 'A', 'Y', '2017-04-08', '671.70', NULL, '72.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(238, '2016120002', '20161', 1, 'A', 'Y', '2017-04-08', '663.40', NULL, '71.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(237, '2016120001', '20161', 1, 'A', 'Y', '2017-04-08', '583.10', NULL, '53.00', 20, 20, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-30', 'N', 'N'),
(308, '2016120001', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(309, '2016120002', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(310, '2016120003', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(311, '2016120016', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(312, '2016120021', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(313, '2016120005', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(314, '2016120015', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(315, '2016120012', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(316, '2016120010', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(317, '201612007P', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(318, '2016120009', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(319, '2016120024', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(320, '2016120019', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(321, '2016120004', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 0, 24, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(322, '2016120013', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(323, '2016120006', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 21, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-12', 'N', 'N'),
(324, '2016120022', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(325, '2016120018', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 21, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-12', 'N', 'N'),
(326, '2016120017', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 21, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-12', 'N', 'N'),
(327, '2016120020', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(328, '2016120023', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 21, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-12', 'N', 'N'),
(329, '2016120014', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(330, '2016120011', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 24, 24, 9, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(331, '2016120008', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 21, 21, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-12', 'N', 'N'),
(332, '2016110038', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(333, '201611042P', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(334, '2016110015', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(335, '2016110024', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(336, '2016110006', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-18', 'N', 'N'),
(337, '2016110037', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(338, '201611045P', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(339, '2016110009', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 0, 0, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(340, '2016110036', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(341, '2016110034', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(342, '2016110029', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(343, '2016110001', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(344, '201611022P', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(345, '2016110041', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(346, '2016110026', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(347, '2016110008', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(348, '2016110023', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(349, '2016110013', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(350, '2016110039', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(351, '2016110047', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(352, '2016110010', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(353, '2016110021', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(354, '2016110002', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(355, '2016110011', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(356, '2016110031', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 0, 0, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(357, '2016110012', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(358, '2016110027', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(359, '2016110014', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 0, 0, 0, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(360, '2016110028', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(361, '201611046P', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(362, '2016110016', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(363, '2016110020', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(364, '2016110030', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(365, '2016110035', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(366, '2016110017', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(367, '2016110019', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(368, '2016110025', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 17, 18, 6, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(369, '2016110005', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(370, '2016110040', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(371, '2016110032', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 17, 18, 6, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(372, '2016110007', '20162', 2, 'A', 'Y', '2017-04-17', '0.00', NULL, '0.00', 20, 21, 7, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(373, '2016110044', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 17, 18, 6, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(374, '2016110043', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 17, 18, 6, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(375, '2016110004', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(376, '2016110018', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(377, '2016110003', '20162', 2, 'A', 'Y', '2017-04-11', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N'),
(378, '2016110033', '20162', 2, 'A', 'Y', '2017-04-18', '0.00', NULL, '0.00', 23, 24, 8, NULL, '0', 0, 0, 0, 0, NULL, '2017-04-11', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kodedosen`
--

CREATE TABLE `kodedosen` (
  `KodeDosen` char(1) NOT NULL DEFAULT '',
  `Nama` varchar(30) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kodedosen`
--

INSERT INTO `kodedosen` (`KodeDosen`, `Nama`, `NotActive`) VALUES
('A', 'Dosen Tetap', 'N'),
('B', 'Dosen PNS DPK', 'N'),
('C', 'Dosen Honorer PTN', 'N'),
('D', 'Dosen Honorer Non-PTN', 'N'),
('E', 'Kontrak/Tetap Kontrak', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `krs`
--

CREATE TABLE `krs` (
  `ID` int(11) NOT NULL,
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `SMT` varchar(5) NOT NULL DEFAULT '0',
  `Sesi` int(11) NOT NULL DEFAULT '0',
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `NamaMK` varchar(255) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `Status` varchar(5) DEFAULT NULL,
  `Program` varchar(10) DEFAULT NULL,
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `_dosen` varchar(10) DEFAULT '0',
  `unip` varchar(20) NOT NULL DEFAULT '001',
  `Tanggal` datetime DEFAULT NULL,
  `hr_1` char(2) DEFAULT NULL,
  `hr_2` char(2) DEFAULT NULL,
  `hr_3` char(2) DEFAULT NULL,
  `hr_4` char(2) DEFAULT NULL,
  `hr_5` char(2) DEFAULT NULL,
  `hr_6` char(2) DEFAULT NULL,
  `hr_7` char(2) DEFAULT NULL,
  `hr_8` char(2) DEFAULT NULL,
  `hr_9` char(2) DEFAULT NULL,
  `hr_10` char(2) DEFAULT NULL,
  `hr_11` char(2) DEFAULT NULL,
  `hr_12` char(2) DEFAULT NULL,
  `hr_13` char(2) DEFAULT NULL,
  `hr_14` char(2) DEFAULT NULL,
  `hr_15` char(2) DEFAULT NULL,
  `hr_16` char(2) DEFAULT NULL,
  `hr_17` char(2) DEFAULT NULL,
  `hr_18` char(2) DEFAULT NULL,
  `hr_19` char(2) DEFAULT NULL,
  `hr_20` char(2) DEFAULT NULL,
  `Hadir` decimal(5,2) DEFAULT '0.00',
  `Tugas1` decimal(5,2) DEFAULT '0.00',
  `Tugas2` decimal(5,2) DEFAULT '0.00',
  `Tugas3` decimal(5,2) DEFAULT '0.00',
  `Tugas4` decimal(5,2) DEFAULT '0.00',
  `Tugas5` decimal(5,2) DEFAULT '0.00',
  `NilaiMID` decimal(5,2) DEFAULT '0.00',
  `NilaiUjian` decimal(5,2) DEFAULT '0.00',
  `Nilai` decimal(5,2) DEFAULT '0.00',
  `GradeNilai` varchar(5) DEFAULT 'E',
  `Bobot` decimal(5,2) DEFAULT '0.00',
  `Tunda` enum('Y','N') DEFAULT 'N',
  `AlasanTunda` varchar(255) DEFAULT NULL,
  `Setara` enum('Y','N','A','B') NOT NULL DEFAULT 'N',
  `KodeSetara` varchar(10) NOT NULL DEFAULT '',
  `MKSetara` varchar(100) DEFAULT NULL,
  `SKSSetara` int(11) DEFAULT '0',
  `GradeSetara` varchar(5) DEFAULT NULL,
  `Konversi` int(11) DEFAULT '0',
  `Dispensasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglDispensasi` date DEFAULT NULL,
  `KetDispensasi` varchar(100) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `krs`
--

INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(1, '2016110001', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.30', '87.30', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(2, '2016110001', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '95.80', '95.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(3, '2016110001', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(4, '2016110001', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(5, '2016110001', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110001', '2017-04-30 10:55:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(6, '2016110001', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110001', '2017-04-30 10:55:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(7, '2016110001', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110001', '2017-04-30 10:56:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.60', '87.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(8, '2016110001', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110001', '2017-04-30 10:56:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(9, '2016110002', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110002', '2017-04-30 11:34:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.30', '65.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(10, '2016110002', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110002', '2017-04-30 11:34:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '57.80', '57.80', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(11, '2016110002', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110002', '2017-04-30 11:34:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(12, '2016110002', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110002', '2017-04-30 11:34:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.00', '81.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(13, '2016110002', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110002', '2017-04-30 11:34:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(14, '2016110002', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110002', '2017-04-30 11:34:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(15, '2016110002', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110002', '2017-04-30 11:35:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(16, '2016110002', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110002', '2017-04-30 11:35:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.00', '84.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(17, '2016110047', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110047', '2017-04-30 11:36:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(18, '2016110047', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110047', '2017-04-30 11:36:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(19, '2016110047', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110047', '2017-04-30 11:36:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(20, '2016110047', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110047', '2017-04-30 11:36:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(21, '2016110047', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110047', '2017-04-30 11:36:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.00', '81.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(22, '2016110047', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110047', '2017-04-30 11:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(23, '2016110003', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110003', '2017-04-30 11:37:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.60', '76.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(24, '2016110003', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110003', '2017-04-30 11:37:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.30', '87.30', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(25, '2016110003', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110003', '2017-04-30 11:37:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.00', '66.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(26, '2016110003', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110003', '2017-04-30 11:37:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(27, '2016110003', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110003', '2017-04-30 11:37:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.00', '64.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(28, '2016110047', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110047', '2017-04-30 11:37:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(29, '2016110003', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110003', '2017-04-30 11:37:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(30, '2016110003', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110003', '2017-04-30 11:37:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.10', '82.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(31, '2016110047', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110047', '2017-04-30 11:37:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.90', '76.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(32, '2016110003', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110003', '2017-04-30 11:38:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.00', '84.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(33, '2016110004', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110004', '2017-04-30 11:38:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(34, '2016110004', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110004', '2017-04-30 11:38:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '90.80', '90.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(35, '2016110004', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110004', '2017-04-30 11:38:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(36, '2016110004', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110004', '2017-04-30 11:38:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(37, '2016110004', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110004', '2017-04-30 11:38:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(38, '2016110004', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110004', '2017-04-30 11:38:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(39, '2016110004', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110004', '2017-04-30 11:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.10', '71.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(40, '2016110004', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110004', '2017-04-30 11:39:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(41, '201611046P', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '201611046P', '2017-04-30 11:39:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(42, '201611046P', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '201611046P', '2017-04-30 11:39:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(43, '201611046P', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '201611046P', '2017-04-30 11:39:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '65.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(44, '201611046P', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '201611046P', '2017-04-30 11:39:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(45, '201611046P', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '201611046P', '2017-04-30 11:39:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(46, '201611046P', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '201611046P', '2017-04-30 11:39:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(47, '2016110005', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110005', '2017-04-30 11:39:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.50', '74.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(48, '2016110005', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110005', '2017-04-30 11:39:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.50', '79.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(49, '2016110005', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110005', '2017-04-30 11:39:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(50, '2016110005', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110005', '2017-04-30 11:39:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(51, '2016110005', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110005', '2017-04-30 11:39:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(52, '201611046P', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '201611046P', '2017-04-30 11:39:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(53, '2016110005', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110005', '2017-04-30 11:39:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.50', '76.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(54, '2016110005', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110005', '2017-04-30 11:39:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.30', '85.30', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(55, '2016110005', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110005', '2017-04-30 11:40:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(56, '201611046P', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '201611046P', '2017-04-30 11:40:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(57, '2016110006', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110006', '2017-04-30 11:40:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.40', '60.40', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(58, '2016110006', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110006', '2017-04-30 11:40:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.80', '65.80', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(59, '2016110006', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110006', '2017-04-30 11:40:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(60, '2016110006', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110006', '2017-04-30 11:40:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(61, '2016110006', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110006', '2017-04-30 11:40:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(62, '2016110006', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110006', '2017-04-30 11:40:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.80', '72.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(63, '2016110006', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110006', '2017-04-30 11:41:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.80', '82.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(64, '201611045P', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '201611045P', '2017-04-30 11:41:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(65, '201611045P', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '201611045P', '2017-04-30 11:41:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '56.00', '56.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(66, '201611045P', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '201611045P', '2017-04-30 11:41:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(67, '201611045P', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '201611045P', '2017-04-30 11:42:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(68, '2016110006', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110006', '2017-04-30 11:42:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(69, '201611045P', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '201611045P', '2017-04-30 11:42:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.10', '70.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(70, '201611045P', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '201611045P', '2017-04-30 11:42:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(71, '201611045P', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '201611045P', '2017-04-30 11:42:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(72, '201611045P', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '201611045P', '2017-04-30 11:42:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(73, '2016110007', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110007', '2017-04-30 11:43:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '61.10', '61.10', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(74, '2016110007', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110007', '2017-04-30 11:43:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.80', '66.80', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(75, '2016110007', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110007', '2017-04-30 11:43:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.00', '66.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(76, '2016110007', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110007', '2017-04-30 11:43:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(77, '2016110007', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110007', '2017-04-30 11:43:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(78, '2016110007', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110007', '2017-04-30 11:43:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.50', '72.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(79, '2016110007', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110007', '2017-04-30 11:43:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.30', '76.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(80, '2016110007', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110007', '2017-04-30 11:43:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(81, '2016110008', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110008', '2017-04-30 11:44:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.70', '80.70', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(82, '2016110008', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110008', '2017-04-30 11:44:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.80', '81.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(83, '2016110008', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110008', '2017-04-30 11:44:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(84, '2016110008', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110008', '2017-04-30 11:44:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(85, '2016110008', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110008', '2017-04-30 11:44:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(86, '2016110008', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110008', '2017-04-30 11:45:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(87, '2016110008', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110008', '2017-04-30 11:45:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.20', '85.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(88, '2016110008', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110008', '2017-04-30 11:45:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(89, '2016110009', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110009', '2017-04-30 11:45:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(90, '2016110009', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110009', '2017-04-30 11:45:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(91, '2016110009', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110009', '2017-04-30 11:45:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(92, '2016110009', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110009', '2017-04-30 11:45:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(93, '2016110009', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110009', '2017-04-30 11:46:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(94, '2016110009', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110009', '2017-04-30 11:46:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(95, '2016110009', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110009', '2017-04-30 11:46:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(96, '2016110009', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110009', '2017-04-30 11:46:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(97, '2016110010', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110010', '2017-04-30 11:47:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.10', '79.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(98, '2016110010', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110010', '2017-04-30 11:47:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(99, '2016110010', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110010', '2017-04-30 11:47:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.00', '69.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(100, '2016110010', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110010', '2017-04-30 11:47:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(101, '2016110010', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110010', '2017-04-30 11:47:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(102, '2016110010', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110010', '2017-04-30 11:47:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(103, '2016110010', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110010', '2017-04-30 11:47:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.30', '82.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(104, '2016110010', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110010', '2017-04-30 11:47:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(105, '2016110011', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110011', '2017-04-30 11:47:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.60', '82.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(106, '2016110011', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110011', '2017-04-30 11:47:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(107, '2016110011', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110011', '2017-04-30 11:47:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(108, '2016110011', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110011', '2017-04-30 11:47:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(109, '2016110011', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110011', '2017-04-30 11:47:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(110, '2016110011', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110011', '2017-04-30 11:48:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(111, '2016110011', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110011', '2017-04-30 11:48:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.30', '85.30', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(112, '2016110011', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110011', '2017-04-30 11:48:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(113, '2016110012', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110012', '2017-04-30 11:48:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(114, '2016110012', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110012', '2017-04-30 11:48:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(115, '2016110012', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110012', '2017-04-30 11:48:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(116, '2016110012', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110012', '2017-04-30 11:48:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(117, '2016110012', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110012', '2017-04-30 11:48:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(118, '2016110012', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110012', '2017-04-30 11:49:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(119, '2016110012', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110012', '2017-04-30 11:49:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.30', '82.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(120, '2016110012', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110012', '2017-04-30 11:49:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(121, '2016110013', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110013', '2017-04-30 11:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.80', '80.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(122, '2016110013', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110013', '2017-04-30 11:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.00', '81.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(123, '2016110013', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110013', '2017-04-30 11:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(124, '2016110013', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110013', '2017-04-30 11:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.00', '89.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(125, '2016110013', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110013', '2017-04-30 11:50:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(126, '2016110013', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110013', '2017-04-30 11:50:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(127, '2016110013', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110013', '2017-04-30 11:50:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.20', '85.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(128, '2016110013', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110013', '2017-04-30 11:50:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(129, '2016110014', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110014', '2017-04-30 11:50:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(130, '2016110014', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110014', '2017-04-30 11:50:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(131, '2016110014', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110014', '2017-04-30 11:50:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(132, '2016110014', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110014', '2017-04-30 11:50:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(133, '2016110014', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110014', '2017-04-30 11:50:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(134, '2016110014', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110014', '2017-04-30 11:51:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(135, '2016110014', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110014', '2017-04-30 11:51:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(136, '2016110014', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110014', '2017-04-30 11:51:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(137, '2016110015', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110015', '2017-04-30 11:52:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '62.80', '62.80', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(138, '2016110015', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110015', '2017-04-30 11:52:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(139, '2016110015', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110015', '2017-04-30 11:52:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(140, '2016110015', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110015', '2017-04-30 11:52:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(141, '2016110015', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110015', '2017-04-30 11:52:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(142, '2016110015', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110015', '2017-04-30 11:53:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(143, '2016110015', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110015', '2017-04-30 11:53:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.80', '82.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(144, '2016110015', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110015', '2017-04-30 11:53:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(145, '2016110016', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110016', '2017-04-30 11:53:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.50', '79.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(146, '2016110016', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110016', '2017-04-30 11:53:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(147, '2016110016', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110016', '2017-04-30 11:53:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(148, '2016110016', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110016', '2017-04-30 11:53:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(149, '2016110016', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110016', '2017-04-30 11:53:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(150, '2016110016', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110016', '2017-04-30 11:54:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(151, '2016110016', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110016', '2017-04-30 11:54:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.60', '85.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(152, '2016110016', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110016', '2017-04-30 11:54:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '91.00', '91.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(153, '2016110044', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110044', '2017-04-30 12:06:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.60', '64.60', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(154, '2016110044', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110044', '2017-04-30 12:06:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '61.50', '61.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(155, '2016110044', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110044', '2017-04-30 12:06:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.00', '64.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(156, '2016110044', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110044', '2017-04-30 12:06:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(157, '2016110044', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110044', '2017-04-30 12:06:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(158, '2016110044', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110044', '2017-04-30 12:07:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(159, '2016110044', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110044', '2017-04-30 12:07:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '56.30', '56.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(160, '2016110044', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110044', '2017-04-30 12:07:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(161, '2016110043', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110043', '2017-04-30 12:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(162, '2016110043', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110043', '2017-04-30 12:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '57.50', '57.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(163, '2016110043', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110043', '2017-04-30 12:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '65.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(164, '2016110043', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110043', '2017-04-30 12:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(165, '2016110043', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110043', '2017-04-30 12:10:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(166, '2016110043', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110043', '2017-04-30 12:10:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '65.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(167, '2016110043', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110043', '2017-04-30 12:10:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.30', '71.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(168, '2016110043', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110043', '2017-04-30 12:11:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(169, '201611042P', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '201611042P', '2017-04-30 12:16:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(170, '201611042P', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '201611042P', '2017-04-30 12:16:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(171, '201611042P', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '201611042P', '2017-04-30 12:16:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.00', '69.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(172, '201611042P', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '201611042P', '2017-04-30 12:16:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(173, '201611042P', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '201611042P', '2017-04-30 12:16:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(174, '201611042P', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '201611042P', '2017-04-30 12:17:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.10', '68.10', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(175, '201611042P', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '201611042P', '2017-04-30 12:17:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(176, '201611042P', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '201611042P', '2017-04-30 12:17:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(177, '2016110041', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110041', '2017-04-30 12:20:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '59.30', '59.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(178, '2016110041', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110041', '2017-04-30 12:20:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.50', '64.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(179, '2016110041', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110041', '2017-04-30 12:20:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(180, '2016110041', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110041', '2017-04-30 12:20:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(181, '2016110041', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110041', '2017-04-30 12:20:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(182, '2016110041', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110041', '2017-04-30 12:21:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.50', '72.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(183, '2016110041', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110041', '2017-04-30 12:21:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(184, '2016110041', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110041', '2017-04-30 12:21:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(185, '2016110040', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110040', '2017-04-30 12:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(186, '2016110040', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110040', '2017-04-30 12:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(187, '2016110040', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110040', '2017-04-30 12:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(188, '2016110040', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110040', '2017-04-30 12:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(189, '2016110040', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110040', '2017-04-30 12:22:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(190, '2016110040', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110040', '2017-04-30 12:22:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(191, '2016110040', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110040', '2017-04-30 12:22:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.10', '82.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(192, '2016110040', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110040', '2017-04-30 12:22:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(193, '2016110039', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110039', '2017-04-30 12:26:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.80', '80.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(194, '2016110039', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110039', '2017-04-30 12:26:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.50', '73.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(195, '2016110039', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110039', '2017-04-30 12:26:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(196, '2016110039', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110039', '2017-04-30 12:26:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(197, '2016110039', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110039', '2017-04-30 12:26:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(198, '2016110039', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110039', '2017-04-30 12:26:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(199, '2016110039', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110039', '2017-04-30 12:27:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(200, '2016110039', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110039', '2017-04-30 12:27:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(201, '2016110038', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110038', '2017-04-30 12:28:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(202, '2016110038', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110038', '2017-04-30 12:28:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(203, '2016110038', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110038', '2017-04-30 12:28:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(204, '2016110038', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110038', '2017-04-30 12:28:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(205, '2016110038', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110038', '2017-04-30 12:28:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(206, '2016110038', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110038', '2017-04-30 12:28:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(207, '2016110038', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110038', '2017-04-30 12:29:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.20', '81.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(208, '2016110038', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110038', '2017-04-30 12:29:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(209, '2016110037', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110037', '2017-04-30 12:31:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.20', '74.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(210, '2016110037', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110037', '2017-04-30 12:31:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '90.50', '90.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(211, '2016110037', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110037', '2017-04-30 12:31:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(212, '2016110037', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110037', '2017-04-30 12:31:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(213, '2016110037', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110037', '2017-04-30 12:31:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(214, '2016110037', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110037', '2017-04-30 12:31:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(215, '2016110037', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110037', '2017-04-30 12:31:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(216, '2016110037', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110037', '2017-04-30 12:31:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(217, '2016110036', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110036', '2017-04-30 12:32:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.90', '71.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(218, '2016110036', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110036', '2017-04-30 12:32:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.50', '66.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(219, '2016110036', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110036', '2017-04-30 12:32:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(220, '2016110036', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110036', '2017-04-30 12:32:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(221, '2016110036', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110036', '2017-04-30 12:33:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(222, '2016110036', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110036', '2017-04-30 12:33:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(223, '2016110036', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110036', '2017-04-30 12:33:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.90', '83.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(224, '2016110036', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110036', '2017-04-30 12:34:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(225, '2016110035', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110035', '2017-04-30 12:35:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '67.20', '67.20', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(226, '2016110035', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110035', '2017-04-30 12:35:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(227, '2016110035', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110035', '2017-04-30 12:35:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(228, '2016110035', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110035', '2017-04-30 12:35:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(229, '2016110035', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110035', '2017-04-30 12:35:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '67.00', '67.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(230, '2016110035', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110035', '2017-04-30 12:35:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.50', '86.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(231, '2016110035', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110035', '2017-04-30 12:35:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(232, '2016110035', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110035', '2017-04-30 12:35:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(233, '2016110034', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110034', '2017-04-30 12:36:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.20', '76.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(234, '2016110034', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110034', '2017-04-30 12:36:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '95.00', '95.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(235, '2016110034', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110034', '2017-04-30 12:36:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(236, '2016110034', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110034', '2017-04-30 12:36:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(237, '2016110034', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110034', '2017-04-30 12:36:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(238, '2016110034', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110034', '2017-04-30 12:36:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.00', '88.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(239, '2016110034', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110034', '2017-04-30 12:36:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.20', '85.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(240, '2016110034', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110034', '2017-04-30 12:37:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(241, '2016110033', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110033', '2017-04-30 12:38:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.70', '68.70', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(242, '2016110033', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110033', '2017-04-30 12:38:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '91.50', '91.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(243, '2016110033', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110033', '2017-04-30 12:38:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(244, '2016110033', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110033', '2017-04-30 12:38:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(245, '2016110033', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110033', '2017-04-30 12:38:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '65.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(246, '2016110033', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110033', '2017-04-30 12:38:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.50', '76.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(247, '2016110033', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110033', '2017-04-30 12:39:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.20', '85.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(248, '2016110033', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110033', '2017-04-30 12:39:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(249, '2016110032', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110032', '2017-04-30 12:41:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '58.90', '58.90', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(250, '2016110032', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110032', '2017-04-30 12:41:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '57.50', '57.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(251, '2016110032', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110032', '2017-04-30 12:41:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(252, '2016110032', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110032', '2017-04-30 12:41:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(253, '2016110032', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110032', '2017-04-30 12:41:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(254, '2016110032', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110032', '2017-04-30 12:42:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.50', '70.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(255, '2016110032', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110032', '2017-04-30 12:42:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.60', '69.60', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(256, '2016110032', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110032', '2017-04-30 12:42:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(257, '2016110031', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110031', '2017-04-30 12:43:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '40.00', '40.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(258, '2016110031', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110031', '2017-04-30 12:43:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(259, '2016110031', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110031', '2017-04-30 12:43:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(260, '2016110031', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110031', '2017-04-30 12:43:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(261, '2016110031', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110031', '2017-04-30 12:43:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(262, '2016110031', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110031', '2017-04-30 12:44:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(263, '2016110031', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110031', '2017-04-30 12:44:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(264, '2016110031', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110031', '2017-04-30 12:44:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(265, '2016110029', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110029', '2017-04-30 12:46:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.10', '80.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(266, '2016110029', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110029', '2017-04-30 12:46:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '91.50', '91.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(267, '2016110029', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110029', '2017-04-30 12:46:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(268, '2016110029', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110029', '2017-04-30 12:46:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(269, '2016110029', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110029', '2017-04-30 12:46:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '67.00', '67.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(270, '2016110029', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110029', '2017-04-30 12:47:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(271, '2016110029', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110029', '2017-04-30 12:47:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.10', '85.10', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(272, '2016110029', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110029', '2017-04-30 12:47:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(273, '2016110028', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110028', '2017-04-30 12:48:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(274, '2016110028', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110028', '2017-04-30 12:48:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '90.00', '90.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(275, '2016110028', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110028', '2017-04-30 12:48:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(276, '2016110028', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110028', '2017-04-30 12:48:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(277, '2016110028', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110028', '2017-04-30 12:48:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(278, '2016110028', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110028', '2017-04-30 12:48:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(279, '2016110028', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110028', '2017-04-30 12:48:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.90', '83.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(280, '2016110028', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110028', '2017-04-30 12:49:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(281, '2016110027', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110027', '2017-04-30 12:49:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '91.00', '91.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(282, '2016110027', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110027', '2017-04-30 12:49:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '92.00', '92.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(283, '2016110027', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110027', '2017-04-30 12:49:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(284, '2016110027', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110027', '2017-04-30 12:49:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(285, '2016110027', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110027', '2017-04-30 12:50:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(286, '2016110027', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110027', '2017-04-30 12:50:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(287, '2016110027', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110027', '2017-04-30 12:50:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.90', '87.90', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(288, '2016110027', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110027', '2017-04-30 12:50:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.00', '84.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(289, '2016110026', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110026', '2017-04-30 12:51:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.50', '89.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(290, '2016110026', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110026', '2017-04-30 12:51:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(291, '2016110026', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110026', '2017-04-30 12:51:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(292, '2016110026', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110026', '2017-04-30 12:51:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(293, '2016110026', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110026', '2017-04-30 12:51:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(294, '2016110026', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110026', '2017-04-30 12:51:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(295, '2016110026', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110026', '2017-04-30 12:51:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.00', '88.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(296, '2016110026', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110026', '2017-04-30 12:52:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.00', '88.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(297, '2016110025', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110025', '2017-04-30 12:52:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(298, '2016110025', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110025', '2017-04-30 12:52:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.50', '76.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(299, '2016110025', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110025', '2017-04-30 12:52:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(300, '2016110025', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110025', '2017-04-30 12:52:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(301, '2016110025', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110025', '2017-04-30 12:52:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(302, '2016110025', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110025', '2017-04-30 12:53:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(303, '2016110025', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110025', '2017-04-30 12:53:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '35.00', '35.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(304, '2016110025', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110025', '2017-04-30 12:53:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(305, '2016110024', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110024', '2017-04-30 12:54:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.60', '76.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(306, '2016110024', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110024', '2017-04-30 12:54:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.50', '86.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(307, '2016110024', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110024', '2017-04-30 12:54:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.00', '69.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(308, '2016110024', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110024', '2017-04-30 12:54:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(309, '2016110024', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110024', '2017-04-30 12:54:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.60', '76.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(310, '2016110024', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110024', '2017-04-30 12:55:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(311, '2016110024', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110024', '2017-04-30 12:55:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(312, '2016110024', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110024', '2017-04-30 12:55:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(313, '2016110023', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110023', '2017-04-30 12:56:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.90', '73.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(314, '2016110023', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110023', '2017-04-30 12:56:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.30', '89.30', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(315, '2016110023', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110023', '2017-04-30 12:56:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(316, '2016110023', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110023', '2017-04-30 12:56:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(317, '2016110023', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110023', '2017-04-30 12:56:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(318, '2016110023', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110023', '2017-04-30 12:56:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.80', '78.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(319, '2016110023', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110023', '2017-04-30 12:56:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(320, '2016110023', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110023', '2017-04-30 12:57:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(321, '201611022P', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '201611022P', '2017-04-30 12:58:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(322, '201611022P', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '201611022P', '2017-04-30 12:58:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.50', '74.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(323, '201611022P', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '201611022P', '2017-04-30 12:58:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(324, '201611022P', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '201611022P', '2017-04-30 12:58:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(325, '201611022P', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '201611022P', '2017-04-30 12:58:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(326, '201611022P', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '201611022P', '2017-04-30 12:58:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.50', '70.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(327, '201611022P', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '201611022P', '2017-04-30 12:58:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(328, '201611022P', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '201611022P', '2017-04-30 12:58:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '60.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(329, '2016110021', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110021', '2017-04-30 12:59:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.10', '65.10', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(330, '2016110021', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110021', '2017-04-30 12:59:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(331, '2016110021', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110021', '2017-04-30 12:59:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(332, '2016110021', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110021', '2017-04-30 12:59:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(333, '2016110021', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110021', '2017-04-30 12:59:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(334, '2016110021', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110021', '2017-04-30 13:00:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(335, '2016110021', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110021', '2017-04-30 13:00:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.10', '82.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(336, '2016110021', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110021', '2017-04-30 13:00:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.00', '81.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(337, '2016110020', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110020', '2017-04-30 13:01:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.10', '78.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(338, '2016110020', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110020', '2017-04-30 13:01:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '90.00', '90.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(339, '2016110020', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110020', '2017-04-30 13:01:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(340, '2016110020', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110020', '2017-04-30 13:01:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(341, '2016110020', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110020', '2017-04-30 13:01:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(342, '2016110020', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110020', '2017-04-30 13:01:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(343, '2016110020', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110020', '2017-04-30 13:01:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(344, '2016110020', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110020', '2017-04-30 13:01:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(345, '2016110019', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110019', '2017-04-30 13:02:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(346, '2016110019', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110019', '2017-04-30 13:02:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(347, '2016110019', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110019', '2017-04-30 13:02:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.00', '64.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(348, '2016110019', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110019', '2017-04-30 13:02:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(349, '2016110019', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110019', '2017-04-30 13:02:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.00', '66.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(350, '2016110019', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110019', '2017-04-30 13:03:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(351, '2016110019', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110019', '2017-04-30 13:03:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(352, '2016110019', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110019', '2017-04-30 13:04:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(353, '2016110018', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110018', '2017-04-30 13:06:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.90', '82.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(354, '2016110018', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110018', '2017-04-30 13:06:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(355, '2016110018', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110018', '2017-04-30 13:06:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(356, '2016110018', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110018', '2017-04-30 13:06:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(357, '2016110018', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110018', '2017-04-30 13:06:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.00', '71.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(358, '2016110018', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110018', '2017-04-30 13:06:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(359, '2016110018', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110018', '2017-04-30 13:06:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.30', '82.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(360, '2016110018', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110018', '2017-04-30 13:07:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(361, '2016110017', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 8, '0', '2016110017', '2017-04-30 13:09:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(362, '2016110017', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', '2016110017', '2017-04-30 13:09:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(363, '2016110017', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', '2016110017', '2017-04-30 13:09:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(364, '2016110017', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', '2016110017', '2017-04-30 13:09:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(365, '2016110017', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', '2016110017', '2017-04-30 13:09:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(366, '2016110017', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', '2016110017', '2017-04-30 13:09:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.50', '86.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(367, '2016110017', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', '2016110017', '2017-04-30 13:09:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.10', '87.10', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(368, '2016110017', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', '2016110017', '2017-04-30 13:09:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(376, '2016110030', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 3, NULL, 'REG', 8, '0', 'admin', '2017-05-01 06:57:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.10', '60.10', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(370, '2016110030', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 8, '0', 'admin', '2017-05-01 06:44:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(371, '2016110030', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 8, '0', 'admin', '2017-05-01 06:44:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.00', '69.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(372, '2016110030', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 9, '0', 'admin', '2017-05-01 06:44:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(373, '2016110030', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 8, '0', 'admin', '2017-05-01 06:45:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '67.00', '67.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(374, '2016110030', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 3, '0', 'admin', '2017-05-01 06:45:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(375, '2016110030', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 16, '0', 'admin', '2017-05-01 06:45:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.20', '75.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(377, '2016110030', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 8, '0', 'admin', '2017-05-01 06:59:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(378, '2016120001', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:37:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.50', '66.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(379, '2016120001', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:37:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.50', '65.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(380, '2016120001', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:37:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(381, '2016120001', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:37:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(382, '2016120001', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:38:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.80', '81.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(383, '2016120001', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:39:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.50', '66.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(384, '2016120001', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:39:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.10', '78.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(385, '2016120001', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:40:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '70.00', '65.00', '85.00', '80.00', '75.00', '80.00', '71.20', '71.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(386, '2016120002', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:41:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(387, '2016120002', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:41:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.50', '89.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(388, '2016120002', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:41:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(389, '2016120002', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:41:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.20', '87.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(390, '2016120002', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:41:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.80', '88.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(391, '2016120002', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:43:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.50', '74.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(392, '2016120002', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:44:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(393, '2016120002', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:44:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '72.00', '78.00', '80.00', '67.00', '80.00', '75.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(394, '2016120003', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:44:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '91.00', '91.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(395, '2016120003', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:44:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.40', '82.40', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(396, '2016120003', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:44:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(397, '2016120003', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:44:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.10', '88.10', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(398, '2016120003', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:45:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.80', '88.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(399, '2016120003', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:45:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(400, '2016120003', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:45:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.40', '86.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(401, '2016120003', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:46:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '85.00', '67.00', '89.00', '76.00', '56.00', '78.00', '73.50', '73.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(402, '2016120004', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:51:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(403, '2016120004', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:51:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(404, '2016120004', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:51:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(405, '2016120004', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:51:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(406, '2016120004', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:51:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(407, '2016120004', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:52:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(408, '2016120004', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:52:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(409, '2016120004', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:52:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(410, '2016120005', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:53:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.50', '80.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(411, '2016120005', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:53:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.10', '79.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(412, '2016120005', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:53:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(413, '2016120005', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:53:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.70', '87.70', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(414, '2016120005', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:53:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(415, '2016120005', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:54:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(416, '2016120005', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:54:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.10', '72.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(417, '2016120005', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:54:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '85.00', '75.00', '88.00', '78.00', '80.00', '76.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(418, '2016120006', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 13:56:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '61.00', '61.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(419, '2016120006', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:56:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.20', '72.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(420, '2016120006', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 13:56:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(421, '2016120006', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 13:56:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.70', '83.70', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(422, '2016120006', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 13:56:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(423, '2016120006', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 13:57:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '64.00', '64.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(424, '2016120006', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:57:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.10', '70.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(425, '2016120006', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 13:58:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(426, '201612007p', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:01:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.50', '77.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(427, '201612007p', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:01:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(428, '201612007p', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:01:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(429, '201612007p', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:01:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(430, '201612007p', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:01:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.80', '81.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(431, '201612007p', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:02:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(432, '201612007p', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:04:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.90', '74.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(433, '201612007p', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:05:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(434, '2016120008', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:06:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.50', '73.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(435, '2016120008', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:06:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.60', '77.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(436, '2016120008', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:06:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(437, '2016120008', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:06:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.60', '79.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(438, '2016120008', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:10:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.80', '85.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(439, '2016120008', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:10:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(440, '2016120008', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:14:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '65.30', '65.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(441, '2016120008', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:16:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.70', '70.70', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(442, '2016120009', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:20:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '95.00', '95.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(443, '2016120009', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:20:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(444, '2016120009', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:20:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(445, '2016120009', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:20:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.80', '89.80', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(446, '2016120009', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:20:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(447, '2016120009', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:22:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(448, '2016120009', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:25:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(449, '2016120009', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:27:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.30', '73.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(450, '2016120010', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '93.00', '93.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(451, '2016120010', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(452, '2016120010', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(453, '2016120010', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.00', '89.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(454, '2016120010', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:28:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.80', '82.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(455, '2016120010', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:29:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(456, '2016120010', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.90', '85.90', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(457, '2016120010', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:34:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.30', '77.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(458, '2016120011', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:36:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(459, '2016120011', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:36:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(460, '2016120011', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:36:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(461, '2016120011', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:36:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.90', '85.90', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(462, '2016120011', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:36:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(463, '2016120011', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:37:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.50', '75.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(464, '2016120011', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:40:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.90', '75.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(465, '2016120011', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:42:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.50', '72.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(466, '2016120012', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:43:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.50', '77.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(467, '2016120012', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:43:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(468, '2016120012', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:43:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.00', '77.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(469, '2016120012', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:43:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.60', '81.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(470, '2016120012', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:43:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(471, '2016120012', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:45:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.50', '70.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(472, '2016120012', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:45:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.60', '79.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(473, '2016120012', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:45:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.30', '70.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(474, '2016120013', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:47:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(475, '2016120013', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:47:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.90', '74.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(476, '2016120013', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:47:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.00', '82.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(477, '2016120013', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:47:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.90', '83.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(478, '2016120013', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:47:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(479, '2016120013', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:48:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.30', '66.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(480, '2016120013', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:48:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.90', '72.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(481, '2016120013', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:48:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.50', '74.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(482, '2016120014', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:49:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.50', '77.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(483, '2016120014', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:49:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(484, '2016120014', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:49:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(485, '2016120014', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:49:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.50', '82.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(486, '2016120014', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:49:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.30', '76.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(487, '2016120014', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:50:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(488, '2016120014', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:50:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.60', '85.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(489, '2016120014', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:51:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.50', '72.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(490, '2016120015', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:51:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.50', '79.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(491, '2016120015', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:51:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '82.90', '82.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(492, '2016120015', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:51:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(493, '2016120015', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:51:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.60', '85.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(494, '2016120015', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:51:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(495, '2016120015', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:52:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(496, '2016120015', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:53:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.90', '72.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(497, '2016120015', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:53:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(498, '2016120016', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:53:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.50', '71.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(499, '2016120016', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:53:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.80', '79.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(500, '2016120016', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:53:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.00', '78.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(501, '2016120016', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:53:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.40', '85.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(502, '2016120016', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:54:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.30', '73.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(503, '2016120016', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:54:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.50', '71.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(504, '2016120016', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:54:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.90', '72.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(505, '2016120016', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:54:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.80', '70.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(506, '2016120017', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:55:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(507, '2016120017', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:55:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(508, '2016120017', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:55:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.00', '75.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(509, '2016120017', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:55:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.10', '84.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(510, '2016120017', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:55:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(511, '2016120017', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:56:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.00', '72.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(512, '2016120017', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:56:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.40', '74.40', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(513, '2016120017', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:57:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.30', '74.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(514, '2016120018', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 14:58:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.50', '70.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(515, '2016120018', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:58:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(516, '2016120018', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 14:58:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(517, '2016120018', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 14:58:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.80', '77.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(518, '2016120018', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 14:59:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.80', '72.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(519, '2016120018', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 14:59:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '69.50', '69.50', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(520, '2016120018', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 14:59:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.10', '70.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(521, '2016120018', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:00:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(522, '2016120019', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:01:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.50', '71.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(523, '2016120019', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:01:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '70.00', '70.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(524, '2016120019', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:01:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.00', '86.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(525, '2016120019', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:01:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.10', '84.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(526, '2016120019', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:01:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.80', '72.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(527, '2016120019', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:01:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '76.00', '76.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(528, '2016120019', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:02:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.60', '85.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(529, '2016120019', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:02:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.30', '72.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(530, '2016120020', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:02:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.50', '71.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(531, '2016120020', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:02:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '77.50', '77.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(532, '2016120020', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:02:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '95.00', '95.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(533, '2016120020', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:02:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.80', '80.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(534, '2016120020', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:03:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.80', '80.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(535, '2016120020', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:03:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.20', '71.20', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(536, '2016120020', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:03:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.90', '71.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(537, '2016120020', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:04:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(538, '2016120021', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:04:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(539, '2016120021', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:04:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '93.70', '93.70', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(540, '2016120021', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:04:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '88.00', '88.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(541, '2016120021', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:04:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '89.50', '89.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(542, '2016120021', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:04:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.00', '79.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(543, '2016120021', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:05:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(544, '2016120021', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:05:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.60', '85.60', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(545, '2016120021', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:06:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(546, '2016120022', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:06:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '79.50', '79.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(547, '2016120022', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:06:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.30', '78.30', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(548, '2016120022', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:06:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(549, '2016120022', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:06:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.20', '86.20', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(550, '2016120022', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.50', '85.50', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(551, '2016120022', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:06:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '81.00', '81.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(552, '2016120022', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:07:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '86.40', '86.40', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(553, '2016120022', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:07:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '75.80', '75.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(554, '2016120023', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:07:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '90.00', '90.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(555, '2016120023', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:07:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.00', '73.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(556, '2016120023', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:07:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(557, '2016120023', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:07:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '84.10', '84.10', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(558, '2016120023', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:07:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '72.80', '72.80', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(559, '2016120023', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:08:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '68.00', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(560, '2016120023', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:08:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '71.90', '71.90', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(561, '2016120023', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:08:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '67.30', '67.30', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(562, '2016120024', '20161', '0', 1, 18, 2, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-10 15:09:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '83.00', '83.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(563, '2016120024', '20161', '0', 1, 20, 1, 'MPK 101', 'PENDIDIKAN AGAMA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:09:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(564, '2016120024', '20161', '0', 1, 22, 24, 'MKK 219', 'MATEMATIKA EKONOMI', 3, NULL, 'REG', 22, '0', 'admin', '2017-04-10 15:09:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(565, '2016120024', '20161', '0', 1, 24, 5, 'MPK 105', 'BAHASA INDONESIA', 2, NULL, 'REG', 20, '0', 'admin', '2017-04-10 15:09:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.00', '87.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(566, '2016120024', '20161', '0', 1, 19, 9, 'MKK 203', 'PENGANTAR AKUNTANSI', 3, NULL, 'REG', 4, '0', 'admin', '2017-04-10 15:09:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '74.00', '74.00', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(567, '2016120024', '20161', '0', 1, 21, 6, 'MPK 106', 'BAHASA INGGRIS', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-10 15:10:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '85.00', '85.00', 'A', '4.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(568, '2016120024', '20161', '0', 1, 23, 3, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:10:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '73.60', '73.60', 'B', '3.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(569, '2016120024', '20161', '0', 1, 25, 7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-10 15:11:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '66.80', '66.80', 'C', '2.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(570, '2016120001', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-11 16:33:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(571, '2016120001', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-11 16:33:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(572, '2016120001', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-11 16:33:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(573, '2016120001', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-11 16:33:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(574, '2016120001', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-11 16:33:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(575, '2016120001', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-11 16:33:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(576, '2016120001', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-11 16:33:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(577, '2016120001', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-11 16:34:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(578, '2016120002', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-11 17:58:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(579, '2016120002', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-11 17:58:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(580, '2016120002', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-11 17:58:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(581, '2016120002', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-11 17:58:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(582, '2016120002', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-11 17:58:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(583, '2016120002', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-11 17:58:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(584, '2016120002', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-11 17:59:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(585, '2016120002', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-11 17:59:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(586, '2016120002', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-11 17:59:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(587, '2016120003', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-11 18:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(588, '2016120003', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-11 18:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(589, '2016120003', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-11 18:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(590, '2016120003', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-11 18:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(591, '2016120003', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-11 18:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(592, '2016120003', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-11 18:00:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(593, '2016120003', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-11 18:00:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(594, '2016120003', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-11 18:00:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(595, '2016120003', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-11 18:01:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(596, '2016120005', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(597, '2016120005', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(598, '2016120005', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(599, '2016120005', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(600, '2016120005', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(601, '2016120005', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 01:58:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(602, '2016120005', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 01:58:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(603, '2016120005', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 01:59:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(604, '2016120005', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 01:59:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(605, '2016120006', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:00:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(606, '2016120006', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:00:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(607, '2016120006', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:00:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(780, '2016110001', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:38:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(609, '2016120006', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:00:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(610, '2016120006', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:00:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(611, '2016120006', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:00:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(612, '2016120006', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:01:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(613, '2016120006', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:01:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(614, '201612007p', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:02:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(615, '201612007p', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:02:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(616, '201612007p', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:02:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(617, '201612007p', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:02:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(618, '201612007p', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:02:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(619, '201612007p', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:02:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(620, '201612007p', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:02:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(621, '201612007p', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:04:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(622, '201612007p', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:04:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(623, '2016120008', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:05:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(624, '2016120008', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:05:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(625, '2016120008', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:05:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(779, '2016110001', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(627, '2016120008', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:05:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(628, '2016120008', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:05:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(629, '2016120009', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(630, '2016120009', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(631, '2016120009', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(632, '2016120009', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(633, '2016120009', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:06:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(634, '2016120009', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:06:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(635, '2016120009', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:07:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(636, '2016120009', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:07:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(637, '2016120009', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:07:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(638, '2016120008', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:08:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(639, '2016120008', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:08:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(640, '2016120008', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:09:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(641, '2016120010', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:09:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(642, '2016120010', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:09:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(643, '2016120010', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:09:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(644, '2016120010', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:09:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(645, '2016120010', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:09:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(646, '2016120010', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:09:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(647, '2016120010', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:09:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(648, '2016120010', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:11:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(649, '2016120010', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:11:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(650, '2016120011', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(651, '2016120011', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(652, '2016120011', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(653, '2016120011', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(654, '2016120011', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(655, '2016120011', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:12:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(656, '2016120011', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:12:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(657, '2016120011', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:13:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(658, '2016120011', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:13:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(659, '2016120012', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:14:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(660, '2016120012', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:14:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(661, '2016120012', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:14:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(662, '2016120012', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:14:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(663, '2016120012', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:14:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(664, '2016120012', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:14:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(665, '2016120012', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:14:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(666, '2016120012', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:15:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(667, '2016120012', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:16:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(668, '2016120013', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(669, '2016120013', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(670, '2016120013', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(671, '2016120013', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(672, '2016120013', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(673, '2016120013', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:17:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(674, '2016120013', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(675, '2016120013', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:17:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(676, '2016120013', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:17:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(677, '2016120014', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(678, '2016120014', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(679, '2016120014', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(680, '2016120014', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(681, '2016120014', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:18:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(682, '2016120014', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:18:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(683, '2016120014', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:19:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(684, '2016120014', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:19:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(685, '2016120014', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:19:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(686, '2016120015', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:20:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(687, '2016120015', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:20:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(688, '2016120015', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:20:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(689, '2016120015', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:20:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(690, '2016120015', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:20:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(691, '2016120015', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(692, '2016120015', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:21:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(693, '2016120015', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:22:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(694, '2016120015', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:22:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(695, '2016120016', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:23:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(696, '2016120016', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:23:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(697, '2016120016', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:23:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(698, '2016120016', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:23:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(699, '2016120016', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:23:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(700, '2016120016', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:23:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(701, '2016120016', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:24:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(702, '2016120016', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:24:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(703, '2016120016', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:24:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(704, '2016120017', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:25:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(705, '2016120017', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:25:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(706, '2016120017', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:25:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(778, '2016110001', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(708, '2016120017', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:25:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(709, '2016120017', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:25:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(710, '2016120017', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:25:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(711, '2016120017', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:25:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(712, '2016120017', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:25:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(713, '2016120018', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:26:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(714, '2016120018', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:26:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(715, '2016120018', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:26:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(777, '2016110001', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(717, '2016120018', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:26:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(718, '2016120018', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:26:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(719, '2016120018', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:26:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(720, '2016120018', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:26:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(721, '2016120018', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:27:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(722, '2016120019', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:27:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(723, '2016120019', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:27:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(724, '2016120019', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:27:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(725, '2016120019', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:27:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(726, '2016120019', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:27:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(727, '2016120019', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:27:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(728, '2016120019', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:27:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(729, '2016120019', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:28:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(730, '2016120019', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:28:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(731, '2016120020', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(732, '2016120020', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(733, '2016120020', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(734, '2016120020', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(735, '2016120020', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:28:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(736, '2016120020', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:28:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(737, '2016120020', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:29:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(738, '2016120020', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:29:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(739, '2016120020', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:29:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(740, '2016120021', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:30:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(741, '2016120021', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:30:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(742, '2016120021', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:30:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(743, '2016120021', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:30:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(744, '2016120021', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:30:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(745, '2016120021', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:30:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(746, '2016120021', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:30:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(747, '2016120021', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:30:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(748, '2016120021', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:30:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(749, '2016120022', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(750, '2016120022', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(751, '2016120022', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(752, '2016120022', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(753, '2016120022', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(754, '2016120022', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:31:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(755, '2016120022', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:31:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(756, '2016120022', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:32:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(757, '2016120022', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:32:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(758, '2016120023', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:32:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(759, '2016120023', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:32:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(760, '2016120023', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:32:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(776, '2016110001', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(762, '2016120023', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:32:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(763, '2016120023', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:32:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(764, '2016120023', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:33:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(765, '2016120023', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:33:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(766, '2016120023', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:33:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(767, '2016120024', '20162', '0', 2, 26, 14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', 2, NULL, 'REG', 22, '0', 'admin', '2017-04-12 02:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(768, '2016120024', '20162', '0', 2, 28, 4, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(769, '2016120024', '20162', '0', 2, 30, 13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 14, '0', 'admin', '2017-04-12 02:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(770, '2016120024', '20162', '0', 2, 34, 45, 'MPB 502', 'ENGLISH FOR BUSSINES', 3, NULL, 'REG', 23, '0', 'admin', '2017-04-12 02:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(771, '2016120024', '20162', '0', 2, 32, 10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 5, '0', 'admin', '2017-04-12 02:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(772, '2016120024', '20162', '0', 2, 27, 26, 'MKK 221', 'STATISTIK', 3, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:33:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(773, '2016120024', '20162', '0', 2, 29, 12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', 2, NULL, 'REG', 12, '0', 'admin', '2017-04-12 02:34:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(774, '2016120024', '20162', '0', 2, 31, 8, 'MKK 202', 'PENGANTAR BISNIS', 3, NULL, 'REG', 11, '0', 'admin', '2017-04-12 02:34:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(775, '2016120024', '20162', '0', 2, 33, 11, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 15, '0', 'admin', '2017-04-12 02:34:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(781, '2016110001', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:38:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(782, '2016110001', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:38:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(783, '2016110001', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:38:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(784, '2016110002', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:38:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(785, '2016110002', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:38:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(786, '2016110002', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:38:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(787, '2016110002', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:38:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(788, '2016110002', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:38:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(789, '2016110002', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:38:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(790, '2016110002', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:38:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(791, '2016110003', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(792, '2016110003', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(793, '2016110003', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(794, '2016110003', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(795, '2016110003', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:39:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(796, '2016110003', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:39:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(797, '2016110003', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:39:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(798, '2016110003', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:39:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(799, '2016110004', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:39:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(800, '2016110004', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:39:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(801, '2016110004', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:39:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(802, '2016110004', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:39:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(803, '2016110004', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:39:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(804, '2016110004', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:39:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(805, '2016110004', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:39:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(806, '2016110004', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:39:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(807, '2016110005', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:40:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(808, '2016110005', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:40:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(809, '2016110005', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:40:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(810, '2016110005', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:40:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(811, '2016110005', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:40:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(812, '2016110005', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:40:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(813, '2016110005', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:40:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(814, '2016110005', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:40:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(815, '2016110006', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:40:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(816, '2016110006', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:40:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(817, '2016110006', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:40:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(818, '2016110006', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:40:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(819, '2016110006', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:40:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(820, '2016110006', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:40:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(822, '2016110006', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:41:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(823, '2016110007', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(824, '2016110007', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(825, '2016110007', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(826, '2016110007', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(827, '2016110007', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:42:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(828, '2016110007', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:42:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(829, '2016110007', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:42:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(830, '2016110008', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:42:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(831, '2016110008', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:42:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(832, '2016110008', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:42:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(833, '2016110008', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:42:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(834, '2016110008', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:42:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(835, '2016110008', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:42:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(836, '2016110008', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:42:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(837, '2016110008', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:42:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(838, '2016110010', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:43:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(839, '2016110010', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:43:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(840, '2016110010', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:43:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(841, '2016110010', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:43:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(842, '2016110010', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:43:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(843, '2016110010', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:43:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(844, '2016110010', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:43:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(845, '2016110010', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:43:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(846, '2016110011', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:43:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(847, '2016110011', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:43:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(848, '2016110011', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:43:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(849, '2016110011', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:43:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(850, '2016110011', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:43:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(851, '2016110011', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:43:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(852, '2016110011', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:43:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(853, '2016110011', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:43:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(854, '2016110012', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:44:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(855, '2016110012', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:44:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(856, '2016110012', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:44:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(857, '2016110012', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:44:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(858, '2016110012', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:44:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(859, '2016110012', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:44:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(860, '2016110012', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:44:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(861, '2016110012', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:44:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(862, '2016110013', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:44:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(863, '2016110013', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:44:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(864, '2016110013', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:44:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(865, '2016110013', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:44:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(866, '2016110013', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:44:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(867, '2016110013', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:44:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(868, '2016110013', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:44:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(869, '2016110013', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:44:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(870, '2016110015', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(871, '2016110015', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(872, '2016110015', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(873, '2016110015', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(874, '2016110015', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:45:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(875, '2016110015', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:45:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(876, '2016110015', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:45:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(877, '2016110015', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:45:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(878, '2016110016', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:45:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(879, '2016110016', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:45:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(880, '2016110016', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:45:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(881, '2016110016', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:45:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(882, '2016110016', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:45:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(883, '2016110016', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:46:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(884, '2016110016', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:46:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(885, '2016110016', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:46:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(886, '2016110017', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:46:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(887, '2016110017', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:46:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(888, '2016110017', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:46:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(889, '2016110017', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:46:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(890, '2016110017', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:46:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(891, '2016110017', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:46:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(892, '2016110017', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:46:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(893, '2016110017', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:46:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(894, '2016110018', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(895, '2016110018', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(896, '2016110018', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(897, '2016110018', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(898, '2016110018', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(899, '2016110018', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:46:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(900, '2016110018', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:46:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(901, '2016110018', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:46:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(902, '2016110019', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:48:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(903, '2016110019', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:48:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(904, '2016110019', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:48:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(905, '2016110019', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:48:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(906, '2016110019', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:48:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(907, '2016110019', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:48:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(908, '2016110019', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:48:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(909, '2016110020', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:49:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(910, '2016110020', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:49:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(911, '2016110020', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:49:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(912, '2016110020', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:49:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(913, '2016110020', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:49:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(914, '2016110020', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:49:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(915, '2016110020', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:49:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(916, '2016110020', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:49:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(917, '2016110021', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-18 14:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(918, '2016110021', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(919, '2016110021', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(920, '2016110021', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-18 14:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(921, '2016110021', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-18 14:50:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(922, '2016110021', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-18 14:50:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(923, '2016110021', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-18 14:50:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(924, '2016110021', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-18 14:50:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(925, '201611022p', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-22 14:06:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(926, '201611022p', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-22 14:06:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(927, '201611022p', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-22 14:06:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(928, '201611022p', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-22 14:06:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(929, '201611022p', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-22 14:06:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(930, '201611022p', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-22 14:06:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(931, '201611022p', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-22 14:06:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(932, '201611022p', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-22 14:06:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(933, '2016110023', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:48:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(934, '2016110023', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:48:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(935, '2016110023', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:48:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(936, '2016110023', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:48:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(937, '2016110023', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:48:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(938, '2016110023', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:48:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(939, '2016110023', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:48:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(940, '2016110023', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:48:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(941, '2016110024', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:49:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(942, '2016110024', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:49:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(943, '2016110024', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:49:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(944, '2016110024', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:49:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(945, '2016110024', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:49:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(946, '2016110024', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:49:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(947, '2016110024', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:49:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(948, '2016110024', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:49:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(949, '2016110025', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:50:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(950, '2016110025', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:50:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(951, '2016110025', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:50:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(952, '2016110025', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:50:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(953, '2016110025', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:50:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(954, '2016110025', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:50:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(955, '2016110026', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:51:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(956, '2016110026', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:51:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(957, '2016110026', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:51:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(958, '2016110026', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:51:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(959, '2016110026', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:51:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(960, '2016110026', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:51:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(961, '2016110026', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:51:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(962, '2016110026', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:51:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(963, '2016110027', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:51:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(964, '2016110027', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:51:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(965, '2016110027', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:51:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(966, '2016110027', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:51:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(967, '2016110027', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:51:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(968, '2016110027', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:51:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(969, '2016110027', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:51:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(970, '2016110027', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:52:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(971, '2016110028', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:53:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(972, '2016110028', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:53:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(973, '2016110028', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:53:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(974, '2016110028', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:53:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(975, '2016110028', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:53:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(976, '2016110028', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:53:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(977, '2016110028', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:53:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(978, '2016110028', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:53:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(979, '2016110029', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(980, '2016110029', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(981, '2016110029', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(982, '2016110029', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(983, '2016110029', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:55:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(984, '2016110029', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:55:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(985, '2016110029', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:55:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(986, '2016110029', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:55:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(987, '2016110030', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:55:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(988, '2016110030', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:55:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(989, '2016110030', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:55:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(990, '2016110030', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:55:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(991, '2016110030', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:55:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(992, '2016110030', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:55:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(993, '2016110030', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:55:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(994, '2016110032', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:56:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(995, '2016110032', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:56:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(996, '2016110032', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:56:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(997, '2016110032', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:56:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(998, '2016110032', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:56:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(999, '2016110032', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:56:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1000, '2016110033', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:57:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1001, '2016110033', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:57:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1002, '2016110033', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:57:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1003, '2016110033', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:57:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1004, '2016110033', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:57:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1005, '2016110033', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:57:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1006, '2016110033', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:57:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1007, '2016110033', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:57:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1008, '2016110034', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:57:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1009, '2016110034', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:57:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1010, '2016110034', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:57:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1011, '2016110034', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:57:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1012, '2016110034', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:57:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1013, '2016110034', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:58:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1014, '2016110034', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 12:58:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1015, '2016110034', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:58:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1016, '2016110035', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 12:58:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1017, '2016110035', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:58:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1018, '2016110035', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:58:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1019, '2016110035', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 12:58:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1020, '2016110035', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 12:58:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1021, '2016110035', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 12:58:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1022, '2016110035', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 12:58:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1023, '2016110036', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:03:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1024, '2016110036', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:03:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1025, '2016110036', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:03:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1026, '2016110036', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:03:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1027, '2016110036', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:03:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1028, '2016110036', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:03:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1029, '2016110036', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:03:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1030, '2016110036', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:03:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1031, '2016110037', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:04:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1032, '2016110037', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:04:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1033, '2016110037', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:04:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1034, '2016110037', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:04:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1035, '2016110037', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:04:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1036, '2016110037', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:04:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1037, '2016110037', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:04:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1038, '2016110037', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:04:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1039, '2016110038', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:04:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1040, '2016110038', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:04:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1041, '2016110038', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:04:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1042, '2016110038', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:04:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1043, '2016110038', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:04:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1044, '2016110038', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:04:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1045, '2016110038', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:04:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1046, '2016110038', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:04:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1047, '2016110039', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:05:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1048, '2016110039', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:05:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1049, '2016110039', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:05:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1050, '2016110039', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:05:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1051, '2016110039', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:05:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1052, '2016110039', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:05:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1053, '2016110039', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:05:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1054, '2016110039', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:05:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1055, '2016110040', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:06:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1056, '2016110040', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:06:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1057, '2016110040', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:06:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1058, '2016110040', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:06:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1059, '2016110040', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:06:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1060, '2016110040', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:06:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1061, '2016110040', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:06:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1062, '2016110040', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:06:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1063, '2016110041', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:07:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1064, '2016110041', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:07:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1065, '2016110041', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:07:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1066, '2016110041', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:07:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1067, '2016110041', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:07:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1068, '2016110041', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:07:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1069, '2016110041', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:07:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1070, '201611042p', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:07:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1071, '201611042p', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:07:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1072, '201611042p', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:07:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1073, '201611042p', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:07:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1074, '201611042p', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:08:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1075, '201611042p', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:08:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1076, '201611042p', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:08:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1077, '2016110043', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:08:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1078, '2016110043', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:08:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1079, '2016110043', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:08:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1080, '2016110043', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:08:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1081, '2016110043', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:08:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1082, '2016110043', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:08:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1083, '2016110044', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:08:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1084, '2016110044', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:08:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1085, '2016110044', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:08:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1086, '2016110044', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:08:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1087, '2016110044', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:09:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1088, '2016110044', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:09:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1089, '201611045p', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:09:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1090, '201611045p', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:09:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1091, '201611045p', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:09:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1092, '201611045p', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:09:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1093, '201611045p', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:09:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1094, '201611045p', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:09:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1095, '201611045p', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:09:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1096, '201611045p', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:09:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1097, '201611046p', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:10:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1098, '201611046p', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:10:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1099, '201611046p', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:10:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1100, '201611046p', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:10:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1101, '201611046p', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:10:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1102, '201611046p', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:10:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1103, '201611046p', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:10:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1104, '2016110047', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, NULL, 'REG', 8, '0', 'admin', '2017-04-23 13:10:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1105, '2016110047', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:10:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1106, '2016110047', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:10:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1107, '2016110047', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, NULL, 'REG', 19, '0', 'admin', '2017-04-23 13:10:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1108, '2016110047', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, NULL, 'REG', 21, '0', 'admin', '2017-04-23 13:10:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');
INSERT INTO `krs` (`ID`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `_dosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Konversi`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(1109, '2016110047', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, NULL, 'REG', 16, '0', 'admin', '2017-04-23 13:10:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1110, '2016110047', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, NULL, 'REG', 6, '0', 'admin', '2017-04-23 13:10:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N'),
(1111, '2016110047', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, NULL, 'REG', 10, '0', 'admin', '2017-04-23 13:10:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', NULL, 'N', '', NULL, 0, NULL, 0, 'N', NULL, NULL, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `krs1`
--

CREATE TABLE `krs1` (
  `ID` int(11) NOT NULL,
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `Sesi` int(11) NOT NULL DEFAULT '0',
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `NamaMK` varchar(255) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `Status` varchar(5) DEFAULT NULL,
  `Program` varchar(10) DEFAULT NULL,
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `unip` varchar(20) NOT NULL DEFAULT '',
  `Tanggal` datetime DEFAULT NULL,
  `hr_1` char(2) DEFAULT NULL,
  `hr_2` char(2) DEFAULT NULL,
  `hr_3` char(2) DEFAULT NULL,
  `hr_4` char(2) DEFAULT NULL,
  `hr_5` char(2) DEFAULT NULL,
  `hr_6` char(2) DEFAULT NULL,
  `hr_7` char(2) DEFAULT NULL,
  `hr_8` char(2) DEFAULT NULL,
  `hr_9` char(2) DEFAULT NULL,
  `hr_10` char(2) DEFAULT NULL,
  `hr_11` char(2) DEFAULT NULL,
  `hr_12` char(2) DEFAULT NULL,
  `hr_13` char(2) DEFAULT NULL,
  `hr_14` char(2) DEFAULT NULL,
  `hr_15` char(2) DEFAULT NULL,
  `hr_16` char(2) DEFAULT NULL,
  `hr_17` char(2) DEFAULT NULL,
  `hr_18` char(2) DEFAULT NULL,
  `hr_19` char(2) DEFAULT NULL,
  `hr_20` char(2) DEFAULT NULL,
  `Hadir` decimal(5,2) DEFAULT '0.00',
  `Tugas1` decimal(5,2) DEFAULT '0.00',
  `Tugas2` decimal(5,2) DEFAULT '0.00',
  `Tugas3` decimal(5,2) DEFAULT '0.00',
  `Tugas4` decimal(5,2) DEFAULT '0.00',
  `Tugas5` decimal(5,2) DEFAULT '0.00',
  `NilaiMID` decimal(5,2) DEFAULT '0.00',
  `NilaiUjian` decimal(5,2) DEFAULT '0.00',
  `Nilai` decimal(5,2) DEFAULT '0.00',
  `GradeNilai` varchar(5) DEFAULT NULL,
  `Bobot` decimal(5,2) DEFAULT '0.00',
  `Tunda` enum('Y','N') DEFAULT 'N',
  `AlasanTunda` varchar(255) DEFAULT NULL,
  `Setara` enum('Y','N') NOT NULL DEFAULT 'N',
  `MKSetara` varchar(100) DEFAULT NULL,
  `SKSSetara` int(11) DEFAULT '0',
  `GradeSetara` varchar(5) DEFAULT NULL,
  `Dispensasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglDispensasi` date DEFAULT NULL,
  `KetDispensasi` varchar(100) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `krskonv`
--

CREATE TABLE `krskonv` (
  `ID` int(11) NOT NULL,
  `krsID` int(11) DEFAULT '0',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `SMT` varchar(5) NOT NULL DEFAULT '0',
  `Sesi` int(11) NOT NULL DEFAULT '0',
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `NamaMK` varchar(255) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `Status` varchar(5) DEFAULT NULL,
  `Program` varchar(10) DEFAULT NULL,
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `unip` varchar(20) NOT NULL DEFAULT '001',
  `Tanggal` datetime DEFAULT NULL,
  `Hadir` decimal(5,2) DEFAULT '0.00',
  `Tugas1` decimal(5,2) DEFAULT '0.00',
  `Tugas2` decimal(5,2) DEFAULT '0.00',
  `Tugas3` decimal(5,2) DEFAULT '0.00',
  `Tugas4` decimal(5,2) DEFAULT '0.00',
  `Tugas5` decimal(5,2) DEFAULT '0.00',
  `NilaiMID` decimal(5,2) DEFAULT '0.00',
  `NilaiUjian` decimal(5,2) DEFAULT '0.00',
  `Nilai` decimal(5,2) DEFAULT '0.00',
  `GradeNilai` varchar(5) DEFAULT 'E',
  `Bobot` decimal(5,2) DEFAULT '0.00',
  `Tunda` enum('Y','N') DEFAULT 'N',
  `AlasanTunda` varchar(255) DEFAULT NULL,
  `Setara` enum('Y','N','A','B') NOT NULL DEFAULT 'N',
  `KodeSetara` varchar(10) NOT NULL DEFAULT '',
  `MKSetara` varchar(100) DEFAULT NULL,
  `SKSSetara` int(11) DEFAULT '0',
  `GradeSetara` varchar(5) DEFAULT NULL,
  `Dispensasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglDispensasi` date DEFAULT NULL,
  `KetDispensasi` varchar(100) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurikulum`
--

CREATE TABLE `kurikulum` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `Sesi` varchar(25) DEFAULT NULL,
  `JmlSesi` int(11) NOT NULL DEFAULT '0',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TglNA` datetime DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kurikulum`
--

INSERT INTO `kurikulum` (`ID`, `Nama`, `KodeJurusan`, `Tahun`, `Sesi`, `JmlSesi`, `Login`, `Tgl`, `TglNA`, `NotActive`) VALUES
(2014, 'Kurikulum 2014', '62201', '2017', 'KURIKULUM 2014-2015', 2, '', '2017-01-03 00:00:00', '2017-01-31 00:00:00', 'N'),
(2015, 'KURIKULUM 2000', '62201', '2017', 'KURIKULUM 2011-2013', 3, '', '0000-00-00 00:00:00', NULL, 'N'),
(2016, 'KURIKULUM 2016', '62201', '2016', 'KURIKULUM 2016-2019', 2, '', '0000-00-00 00:00:00', NULL, 'N'),
(2017, 'KURIKULUM 2016', '61201', '2016', 'KURIKULUM 2016-2019', 2, '', '0000-00-00 00:00:00', NULL, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `Level` smallint(6) NOT NULL DEFAULT '0',
  `Name` varchar(20) NOT NULL DEFAULT '',
  `usr` varchar(10) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`Level`, `Name`, `usr`, `NotActive`) VALUES
(1, 'Administrator', 'admin', 'N'),
(2, 'Karyawan', 'karyawan', 'N'),
(3, 'Dosen', 'dosen', 'N'),
(4, 'Mahasiswa', 'mhsw', 'N'),
(5, 'Umum', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matakuliah`
--

CREATE TABLE `matakuliah` (
  `ID` int(11) NOT NULL,
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama_Indonesia` varchar(100) NOT NULL DEFAULT '',
  `Nama_English` varchar(100) DEFAULT NULL,
  `KurikulumID` int(11) DEFAULT NULL,
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `SKSTatapMuka` int(11) NOT NULL DEFAULT '0',
  `SKSPraktikum` int(11) NOT NULL DEFAULT '0',
  `SKSPraktekLap` int(11) NOT NULL DEFAULT '0',
  `SKSMin` int(11) NOT NULL DEFAULT '0',
  `IPMin` decimal(5,2) NOT NULL DEFAULT '0.00',
  `GradeMin` varchar(5) DEFAULT NULL,
  `KodeJenisMK` varchar(10) NOT NULL DEFAULT '',
  `Wajib` enum('Y','N') NOT NULL DEFAULT 'N',
  `DoubleMajor` varchar(10) DEFAULT NULL,
  `Sesi` int(11) NOT NULL DEFAULT '1',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `matakuliah`
--

INSERT INTO `matakuliah` (`ID`, `Kode`, `Nama_Indonesia`, `Nama_English`, `KurikulumID`, `KodeFakultas`, `KodeJurusan`, `SKS`, `SKSTatapMuka`, `SKSPraktikum`, `SKSPraktekLap`, `SKSMin`, `IPMin`, `GradeMin`, `KodeJenisMK`, `Wajib`, `DoubleMajor`, `Sesi`, `Login`, `Tgl`, `NotActive`) VALUES
(1, 'MPK 101', 'PENDIDIKAN AGAMA', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 1, 'admin', '2017-03-03 11:26:48', 'N'),
(2, 'MPK 102', 'PENDIDIKAN PANCASILA', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 1, 'admin', '2017-03-03 11:28:33', 'N'),
(3, 'MPK 103', 'ILMU ALAMIAH DASAR', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 1, 'admin', '2017-03-03 11:30:46', 'N'),
(4, 'MPK 104', 'KEWARGANEGARAAN', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 2, 'admin', '2017-03-03 11:31:51', 'N'),
(5, 'MPK 105', 'BAHASA INDONESIA', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 1, 'admin', '2017-03-03 11:32:35', 'N'),
(6, 'MPK 106', 'BAHASA INGGRIS', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '62201', 1, 'admin', '2017-03-03 11:33:23', 'N'),
(7, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 1, 'admin', '2017-03-03 11:34:20', 'N'),
(8, 'MKK 202', 'PENGANTAR BISNIS', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:36:22', 'N'),
(9, 'MKK 203', 'PENGANTAR AKUNTANSI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 1, 'admin', '2017-03-03 11:37:08', 'N'),
(10, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:38:52', 'N'),
(11, 'MKK 205', 'PENGANTAR MANAJEMEN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:39:42', 'N'),
(12, 'MKK 206', 'PENGANTAR EKONOMI PEMBANGUNAN', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:40:27', 'N'),
(13, 'MKK 207', 'PENGANTAR APLIKASI KOMPUTER', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:41:08', 'N'),
(14, 'MKK 208', 'PRAKTIKUM PENGANTAR AKUNTANSI', '', 2016, 'STIE-P', '62201', 2, 0, 2, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:41:59', 'N'),
(15, 'MKK 209', 'PEREKONOMIAN INDONESIA', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 3, 'admin', '2017-03-03 11:42:50', 'N'),
(16, 'MKK 210', 'BANK DAN LEMBAGA KEUANGAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 4, 'admin', '2017-03-03 11:43:51', 'N'),
(17, 'MKK 211', 'AKUNTANSI PEMERINTAH', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 6, 'admin', '2017-03-03 11:44:49', 'N'),
(18, 'MKK 213', 'SISTEM INFORMASI MANAJEMEN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 5, 'admin', '2017-03-03 11:45:34', 'N'),
(19, 'MKK 214', 'PEMERIKSAAN AKUNTANSI I', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 5, 'admin', '2017-03-03 11:46:33', 'N'),
(20, 'MKK 215', 'PEMERIKSAAN AKUNTANSI II', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 6, 'admin', '2017-03-03 11:47:21', 'N'),
(21, 'MKK 216', 'PEMERIKSAAN MANAJEMEN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 7, 'admin', '2017-03-03 11:48:00', 'N'),
(22, 'MKK 217', 'MANAJEMEN KEUANGAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 3, 'admin', '2017-03-03 11:48:39', 'N'),
(23, 'MKK 218', 'MANAJEMEN PEMASARAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 3, 'admin', '2017-03-03 11:49:24', 'N'),
(24, 'MKK 219', 'MATEMATIKA EKONOMI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 1, 'admin', '2017-03-03 11:50:08', 'N'),
(25, 'MKK 220', 'METODELOGI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 5, 'admin', '2017-03-03 11:50:52', 'N'),
(26, 'MKK 221', 'STATISTIK', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '62201', 2, 'admin', '2017-03-03 11:51:41', 'N'),
(27, 'MKB 301', 'AKUNTANSI BIAYA', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 3, 'admin', '2017-03-03 11:52:27', 'N'),
(28, 'MKB 302', 'AKUNTANSI KEUANGAN MENENGAH I', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 3, 'admin', '2017-03-03 11:53:58', 'N'),
(29, 'MKB 303', 'HUKUM PAJAK', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 3, 'admin', '2017-03-03 11:54:36', 'N'),
(30, 'MKB 304', 'PRAKTIKUM KOMPUTER AKUNTANSI', '', 2016, 'STIE-P', '62201', 3, 0, 3, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 3, 'admin', '2017-03-03 11:55:20', 'N'),
(31, 'MKB 305', 'MANAJEMEN STRATEGI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 3, 'admin', '2017-03-03 11:56:08', 'N'),
(32, 'MKB 306', 'AKUNTANSI KEUANGAN MENENGAH II', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 4, 'admin', '2017-03-03 11:56:57', 'N'),
(33, 'MKB 307', 'MANAJEMEN OPERASIONAL', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 4, 'admin', '2017-03-03 11:57:45', 'N'),
(34, 'MKB 308', 'AKUNTANSI MANAJEMEN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 4, 'admin', '2017-03-03 11:58:29', 'N'),
(35, 'MKB 310', 'TEORI AKUNTANSI', '', 2016, 'STIE-P', '62201', 2, 2, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 4, 'admin', '2017-03-03 11:59:09', 'N'),
(36, 'MKB 311', 'SISTEM INFORMASI AKUNTANSI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 4, 'admin', '2017-03-03 11:59:54', 'N'),
(37, 'MKB 312', 'AKUNTANSI KEUANGAN LANJUT I', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 5, 'admin', '2017-03-03 12:00:46', 'N'),
(38, 'MKB 313', 'STUDY KELAYAKAN BISNIS', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 5, 'admin', '2017-03-03 12:01:26', 'N'),
(39, 'MKB 314', 'AKUNTANSI KEUANGAN LANJUT II', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 6, 'admin', '2017-03-03 12:02:08', 'N'),
(40, 'MKB 315', 'SEMINAR AKUNTANSI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '62201', 6, 'admin', '2017-03-03 12:02:39', 'N'),
(41, 'MBB 401', 'KULIAH KERJA LAPANGAN', '', 2016, 'STIE-P', '62201', 3, 0, 0, 3, 0, '0.00', NULL, 'MBB', 'Y', '62201', 6, 'admin', '2017-03-03 14:18:19', 'N'),
(42, 'MBB 402', 'SKRIPSI', '', 2016, 'STIE-P', '62201', 3, 0, 0, 0, 0, '0.00', NULL, 'MBB', 'Y', '62201', 7, 'admin', '2017-03-03 14:19:21', 'N'),
(43, 'MBB 403', 'UJIAN KOMPRESHIF', '', 2016, 'STIE-P', '62201', 3, 0, 0, 0, 0, '0.00', NULL, 'MBB', 'Y', '62201', 7, 'admin', '2017-03-03 14:20:12', 'N'),
(44, 'MPB 501', 'PERPAJAKAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '62201', 4, 'admin', '2017-03-03 14:21:14', 'N'),
(45, 'MPB 502', 'ENGLISH FOR BUSSINES', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '62201', 4, 'admin', '2017-03-03 14:22:15', 'N'),
(46, 'MPB 503', 'ETIKA DAN KOMUNIKASI BISNIS', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '62201', 4, 'admin', '2017-03-03 14:24:58', 'N'),
(47, 'MPB 504', 'KEWIRAUSAHAAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '62201', 6, 'admin', '2017-03-03 14:25:38', 'N'),
(48, 'MKP 601', 'PERILAKU ORGANISASI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:26:46', 'N'),
(49, 'MKP 602', 'ETIKA BISNIS DAN PROFESI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:27:38', 'N'),
(50, 'MKP 603', 'AKUNTANSI KEUANGAN PEMERINTAH', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 6, 'admin', '2017-03-03 14:28:29', 'N'),
(51, 'MKP 604', 'PENGAWASAN DAN AUDIT KEUANGAN PEMERINTAH', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:31:56', 'N'),
(52, 'MKP 605', 'MANAJEMEN KEUANGAN PEMERINTAH', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:38:21', 'N'),
(53, 'MKP 606', 'KEPEMIMPINAN', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:39:06', 'N'),
(54, 'MKP 608', 'TEORI EKONOMI', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 4, 'admin', '2017-03-03 14:39:53', 'N'),
(55, 'MKP 609', 'ENGLISH FOR ACCOUNTING', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 7, 'admin', '2017-03-03 14:40:42', 'N'),
(56, 'MKP 610', 'AKUNTANSI DESA', '', 2016, 'STIE-P', '62201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '62201', 5, 'admin', '2017-03-03 14:41:19', 'N'),
(57, 'MPK 101', 'PENDIDIKAN AGAMA', '', 2017, 'STIE-P', '61201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 1, 'admin', '2017-03-03 14:59:32', 'N'),
(58, 'MPK 102', 'PENDIDIKAN PANCASILA', '', 2017, 'STIE-P', '61201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 1, 'admin', '2017-03-03 15:02:17', 'N'),
(59, 'MPK 103', 'ILMU ALAMIAH DASAR', '', 2017, 'STIE-P', '61201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 1, 'admin', '2017-03-03 15:02:48', 'N'),
(60, 'MPK 104', 'KEWARGANEGARAAN', '', 2017, 'STIE-P', '61201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 2, 'admin', '2017-03-03 15:03:25', 'N'),
(61, 'MPK 105', 'BAHASA INDONESIA', '', 2017, 'STIE-P', '61201', 2, 2, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 1, 'admin', '2017-03-03 15:04:00', 'N'),
(62, 'MPK 106', 'BAHASA INGGRIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 1, 'admin', '2017-03-03 15:04:45', 'N'),
(63, 'MPK 107', 'KULIAH KERJA LAPANGAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPK', 'Y', '61201', 6, 'admin', '2017-03-03 15:05:20', 'N'),
(64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 1, 'admin', '2017-03-03 15:10:37', 'N'),
(65, 'MKK 202', 'PENGANTAR AKUNTANSI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 1, 'admin', '2017-03-03 15:11:20', 'N'),
(66, 'MKK 203', 'MATEMATIKA EKONOMI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 1, 'admin', '2017-03-03 15:16:21', 'N'),
(67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:17:16', 'N'),
(68, 'MKK 205', 'PENGANTAR MANAJEMEN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:18:02', 'N'),
(69, 'MKK 206', 'PENGANTAR BISNIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:18:42', 'N'),
(70, 'MKK 207', 'STATISTIK', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:19:17', 'N'),
(71, 'MKK 208', 'AKUNTANSI BIAYA', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 3, 'admin', '2017-03-03 15:19:48', 'N'),
(72, 'MKK 209', 'PEREKONOMIAN INDONESIA', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 3, 'admin', '2017-03-03 15:20:23', 'N'),
(73, 'MKK 210', 'METODOLOGI PENELITIAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 3, 'admin', '2017-03-03 15:21:12', 'N'),
(74, 'MKK 211', 'AKUNTANSI MANAJEMEN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 3, 'admin', '2017-03-03 15:22:53', 'N'),
(75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:23:53', 'N'),
(76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 2, 'admin', '2017-03-03 15:24:41', 'N'),
(77, 'MKK 214', 'SISTEM INFORMASI MANAJEMEN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKK', 'Y', '61201', 5, 'admin', '2017-03-03 15:28:45', 'N'),
(78, 'MPB 401', 'ENGLISH FOR BUSINESS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '61201', 2, 'admin', '2017-03-03 15:35:26', 'N'),
(79, 'MPB 402', 'HUKUM BISNIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '61201', 3, 'admin', '2017-03-03 15:37:04', 'N'),
(80, 'MPB 403', 'ETIKA DAN KOMUNIKASI BISNIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '61201', 6, 'admin', '2017-03-03 15:38:00', 'N'),
(81, 'MPB 404', 'KEWIRAUSAHAAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MPB', 'Y', '61201', 6, 'admin', '2017-03-03 15:38:40', 'N'),
(82, 'MBB 501', 'SKRIPSI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MBB', 'Y', '61201', 7, 'admin', '2017-03-03 15:39:53', 'N'),
(83, 'MBB 502', 'UJIAN KOMPRESHIF', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MBB', 'Y', '61201', 7, 'admin', '2017-03-03 15:40:29', 'N'),
(84, 'MKB 301', 'MANAJEMEN SUMBER DAYA MANUSIA', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 3, 'admin', '2017-03-03 15:41:21', 'N'),
(85, 'MKB 302', 'MANAJEMEN PEMASARAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 3, 'admin', '2017-03-03 15:42:06', 'N'),
(86, 'MKB 303', 'MANAJEMEN KEUANGAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 3, 'admin', '2017-03-03 15:43:10', 'N'),
(87, 'MKB 304', 'OPERATION RESEARCH', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:43:50', 'N'),
(88, 'MKB 305', 'EKONOMI MANAJERIAL', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:44:37', 'N'),
(89, 'MKB 306', 'EKONOMI INTERNASIONAL', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:45:16', 'N'),
(90, 'MKB 307', 'MANAJEMEN STRATEGI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:45:59', 'N'),
(91, 'MKB 308', 'MANAJEMEN OPERASIONAL', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:46:43', 'N'),
(92, 'MKB 309', 'MANAJEMEN KOPERASI DAN UKM', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:47:37', 'N'),
(93, 'MKB 310', 'MANAJEMEN PERBANKAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 4, 'admin', '2017-03-03 15:48:22', 'N'),
(94, 'MKB 311', 'MANAJEMEN MODAL KERJA', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:49:07', 'N'),
(95, 'MKB 312', 'MANAJEMEN PROYEK', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:49:49', 'N'),
(96, 'MKB 313', 'MANAJEMEN RESIKO', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:50:23', 'N'),
(97, 'MKB 314', 'PENGANGGARAN PERUSAHAAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:51:21', 'N'),
(98, 'MKB 315', 'TEORI PENGAMBILAN KEPUTUSAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:52:04', 'N'),
(99, 'MKB 316', 'TEKNIK PROYEKSI BISNIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 5, 'admin', '2017-03-03 15:52:59', 'N'),
(100, 'MKB 317', 'PERILAKU ORGANISASI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 6, 'admin', '2017-03-03 15:53:53', 'N'),
(101, 'MKB 318', 'STUDI KELAYAKAN BISNIS', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 6, 'admin', '2017-03-03 15:54:31', 'N'),
(102, 'MKB 319', 'PASAR UANG PASAR MODAL', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 6, 'admin', '2017-03-03 15:55:18', 'N'),
(103, 'MKB 320', 'BANK DAN LEMBAGA KEUANGAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKB', 'Y', '61201', 6, 'admin', '2017-03-03 15:56:07', 'N'),
(104, 'MKP 601', 'SEMINAR MANAJEMEN SDM', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 5, 'admin', '2017-03-03 16:02:26', 'N'),
(105, 'MKP 602', 'SEMINAR MANAJEMEN PEMASARAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 5, 'admin', '2017-03-03 16:03:07', 'N'),
(106, 'MKP 603', 'SEMINAR MANAJEMEN KEUANGAN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 6, 'admin', '2017-03-03 16:03:52', 'N'),
(107, 'MKP 604', 'PEMBINAAN DAN PEMBERDAYAAN SDM', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 5, 'admin', '2017-03-03 16:04:36', 'N'),
(108, 'MKP 605', 'PERILAKU KONSUMEN', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 6, 'admin', '2017-03-03 16:05:11', 'N'),
(109, 'MKP 606', 'MANAJEMEN INVESTASI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 6, 'admin', '2017-03-03 16:05:48', 'N'),
(110, 'MKP 607', 'MANAJEMEN KOMPENSASI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 5, 'admin', '2017-03-03 16:06:21', 'N'),
(111, 'MKP 608', 'PEMASARAN INTERNASIONAL', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 5, 'admin', '2017-03-03 16:06:58', 'N'),
(112, 'MKP 609', 'E-COMMERCE', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 3, 'admin', '2017-03-03 16:07:35', 'N'),
(113, 'MKP 610', 'EKONOMI MONETER', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 3, 'admin', '2017-03-03 16:08:06', 'N'),
(114, 'MKP 611', 'TEORI EKONOMI', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 3, 'admin', '2017-03-03 16:08:40', 'N'),
(115, 'MKP 612', 'SOSIOLOGI DAN POLITIK', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 3, 'admin', '2017-03-03 16:09:37', 'N'),
(116, 'MKP 613', 'EKONOMI SYARIAH', '', 2017, 'STIE-P', '61201', 3, 3, 0, 0, 0, '0.00', NULL, 'MKP', 'N', '61201', 3, 'admin', '2017-03-03 16:10:13', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `maxsks`
--

CREATE TABLE `maxsks` (
  `ID` int(11) NOT NULL,
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `IPSMin` decimal(4,2) NOT NULL DEFAULT '0.00',
  `IPSMax` decimal(4,2) NOT NULL DEFAULT '0.00',
  `SKSMax` int(11) NOT NULL DEFAULT '0',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `maxsks`
--

INSERT INTO `maxsks` (`ID`, `KodeJurusan`, `IPSMin`, `IPSMax`, `SKSMax`, `NotActive`) VALUES
(1, 'D3KB', '0.00', '1.49', 12, 'N'),
(2, 'D3M', '0.00', '1.49', 12, 'N'),
(3, 'D3M', '1.50', '1.99', 15, 'N'),
(4, 'D3M', '2.00', '2.49', 18, 'N'),
(5, 'D3M', '2.49', '2.99', 21, 'N'),
(6, 'D3M', '3.00', '4.00', 24, 'N'),
(7, '61201', '2.00', '2.50', 12, 'N'),
(8, '61201', '2.51', '2.90', 18, 'N'),
(9, '61201', '3.00', '4.00', 24, 'N'),
(10, '62201', '2.00', '2.50', 12, 'N'),
(11, '62201', '2.51', '2.90', 18, 'N'),
(12, '62201', '3.00', '4.00', 24, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mbrgetmbr`
--

CREATE TABLE `mbrgetmbr` (
  `ID` int(11) NOT NULL,
  `Tanggal` date NOT NULL DEFAULT '0000-00-00',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Email` varchar(100) DEFAULT NULL,
  `Telp` varchar(50) DEFAULT NULL,
  `HP` varchar(50) DEFAULT NULL,
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Bank` varchar(100) DEFAULT NULL,
  `NamaAkun` varchar(100) DEFAULT NULL,
  `NomerAkun` varchar(100) DEFAULT NULL,
  `Input` varchar(20) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mhsw`
--

CREATE TABLE `mhsw` (
  `ID` bigint(20) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `PMBID` varchar(20) DEFAULT NULL,
  `NIRM` varchar(20) DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Sex` char(1) NOT NULL DEFAULT 'L',
  `TempatLahir` varchar(50) DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `RT` varchar(4) DEFAULT NULL,
  `RW` varchar(4) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `KodePos` varchar(10) DEFAULT NULL,
  `KodeTelp` varchar(5) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `AgamaID` int(11) DEFAULT '0',
  `WargaNegara` varchar(30) DEFAULT NULL,
  `Grp` enum('Y','N') NOT NULL DEFAULT 'N',
  `SudahBekerja` enum('Y','N') NOT NULL DEFAULT 'N',
  `PusatKarir` enum('Y','N') NOT NULL DEFAULT 'N',
  `NamaPrsh` varchar(50) DEFAULT NULL,
  `Alamat1Prsh` varchar(100) DEFAULT NULL,
  `Alamat2Prsh` varchar(100) DEFAULT NULL,
  `KotaPrsh` varchar(50) DEFAULT NULL,
  `TelpPrsh` varchar(20) DEFAULT NULL,
  `FaxPrsh` varchar(20) DEFAULT NULL,
  `NamaOT` varchar(50) DEFAULT NULL,
  `PekerjaanOT` varchar(50) DEFAULT NULL,
  `AlamatOT1` varchar(100) DEFAULT NULL,
  `AlamatOT2` varchar(100) DEFAULT NULL,
  `RTOT` varchar(4) DEFAULT NULL,
  `RWOT` varchar(4) DEFAULT NULL,
  `KotaOT` varchar(50) DEFAULT NULL,
  `KodeTelpOT` varchar(4) DEFAULT NULL,
  `TelpOT` varchar(30) DEFAULT NULL,
  `EmailOT` varchar(50) DEFAULT NULL,
  `KodePosOT` varchar(10) DEFAULT NULL,
  `AsalSekolah` varchar(50) DEFAULT NULL,
  `PropSekolah` varchar(5) DEFAULT NULL,
  `JenisSekolah` varchar(10) DEFAULT NULL,
  `LulusSekolah` varchar(5) DEFAULT NULL,
  `IjazahSekolah` varchar(50) DEFAULT NULL,
  `NilaiSekolah` decimal(5,2) DEFAULT '0.00',
  `Pilihan1` varchar(100) DEFAULT NULL,
  `Pilihan2` varchar(100) DEFAULT NULL,
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `Status` varchar(5) DEFAULT NULL,
  `KodeProgram` varchar(10) DEFAULT NULL,
  `Kelas` smallint(6) NOT NULL DEFAULT '0',
  `StatusAwal` varchar(5) DEFAULT NULL,
  `StatusPotongan` varchar(5) DEFAULT NULL,
  `SPP_D` int(11) NOT NULL DEFAULT '0',
  `Semester` int(11) DEFAULT NULL,
  `TahunAkademik` varchar(5) DEFAULT NULL,
  `KodeBiaya` varchar(5) DEFAULT NULL,
  `Posting` char(1) DEFAULT NULL,
  `Lulus` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglLulus` date DEFAULT NULL,
  `TahunLulus` varchar(5) DEFAULT NULL,
  `PredikatLulus` varchar(50) DEFAULT NULL,
  `WaktuKuliah` varchar(10) DEFAULT NULL,
  `Keterangan` varchar(100) DEFAULT NULL,
  `Pinjaman` int(11) DEFAULT NULL,
  `KTahun` date DEFAULT NULL,
  `K_Dosen` varchar(10) DEFAULT NULL,
  `DosenID` int(11) NOT NULL DEFAULT '0',
  `Ranking` int(11) DEFAULT NULL,
  `mGroup` char(1) DEFAULT NULL,
  `Target` int(11) DEFAULT NULL,
  `Prop` varchar(10) DEFAULT NULL,
  `Masuk` date DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `TestScore` int(11) DEFAULT '0',
  `TA` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglTA` date DEFAULT NULL,
  `TotalSKS` int(11) NOT NULL DEFAULT '0',
  `IPK` decimal(5,2) NOT NULL DEFAULT '0.00',
  `JudulTA` varchar(255) DEFAULT NULL,
  `PembimbingTA` int(11) DEFAULT NULL,
  `CatatanTA` text,
  `PMBSyarat` varchar(100) DEFAULT NULL,
  `PMBKurang` varchar(100) DEFAULT NULL,
  `NomerIjazah` varchar(255) DEFAULT NULL,
  `MGM` enum('Y','N') NOT NULL DEFAULT 'N',
  `MGMOleh` int(11) DEFAULT NULL,
  `MGMHonor` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mhsw`
--

INSERT INTO `mhsw` (`ID`, `Login`, `Password`, `Description`, `NIM`, `PMBID`, `NIRM`, `Tanggal`, `Name`, `Email`, `Sex`, `TempatLahir`, `TglLahir`, `Alamat1`, `Alamat2`, `RT`, `RW`, `Kota`, `KodePos`, `KodeTelp`, `Phone`, `Agama`, `AgamaID`, `WargaNegara`, `Grp`, `SudahBekerja`, `PusatKarir`, `NamaPrsh`, `Alamat1Prsh`, `Alamat2Prsh`, `KotaPrsh`, `TelpPrsh`, `FaxPrsh`, `NamaOT`, `PekerjaanOT`, `AlamatOT1`, `AlamatOT2`, `RTOT`, `RWOT`, `KotaOT`, `KodeTelpOT`, `TelpOT`, `EmailOT`, `KodePosOT`, `AsalSekolah`, `PropSekolah`, `JenisSekolah`, `LulusSekolah`, `IjazahSekolah`, `NilaiSekolah`, `Pilihan1`, `Pilihan2`, `KodeFakultas`, `KodeJurusan`, `Status`, `KodeProgram`, `Kelas`, `StatusAwal`, `StatusPotongan`, `SPP_D`, `Semester`, `TahunAkademik`, `KodeBiaya`, `Posting`, `Lulus`, `TglLulus`, `TahunLulus`, `PredikatLulus`, `WaktuKuliah`, `Keterangan`, `Pinjaman`, `KTahun`, `K_Dosen`, `DosenID`, `Ranking`, `mGroup`, `Target`, `Prop`, `Masuk`, `NotActive`, `TestScore`, `TA`, `TglTA`, `TotalSKS`, `IPK`, `JudulTA`, `PembimbingTA`, `CatatanTA`, `PMBSyarat`, `PMBKurang`, `NomerIjazah`, `MGM`, `MGMOleh`, `MGMHonor`) VALUES
(1, '2016120001', '*84AAC12F5', '', '2016120001', '2004101', '', '2017-04-20', 'YELI NOVELINA', '', 'P', 'Kemang', '1997-11-27', 'Desa Kemang Kec. Lembak Kab. Muara Enim', '', '', '', '', '31171', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Saptono', 'Petani', 'Desa Kemang Kec. Lembak Kab. Muara Enim', '', '', '', '', NULL, '', '', '31171', 'SMK Negeri 1 Prabumulih', '11', 'SMK', 'N', 'DN 11 Mk 0010803', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, 'N', 0, 'N', NULL, 41, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(2, '2016120002', '*84AAC12F5', '', '2016120002', '2004102', '', '2017-04-20', 'PIKY BRUCKY MANDASARI', '', 'P', 'Prabumulih ', '1996-09-11', 'Jalan Sindang Lura ', '', '2', '3', '', '31116', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sarminto S', 'Wiraswasta', 'Jalan Sindang Lura ', '', '2', '3', '', NULL, '', '', '31116', 'SMA Negeri 2 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0009839', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(3, '2016120003', '*84AAC12F5', '', '2016120003', '2004103', '', '2017-04-20', 'PEBTHY INDRIANI SIDIEK', '', 'P', 'Prabumulih', '1998-02-05', 'Jalan Sindang Lura ', '', '2', '3', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'A.	Sidik', 'Wiraswasta', 'Jalan Sindang Lura', '', '2', '3', 'Prabumulih', NULL, '', '', '31116', 'SMA Negeri 2 Prabumulih', 'Prabu', 'SMA', 'N', '', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, 'N', 56, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(4, '2016110038', '*84AAC12F5', '', '2016110038', '2004162', '', '2017-04-20', 'ADIL LASTARI', '', 'L', 'Palembang ', '1997-01-07', 'Jalan Hikmah Rt. 03 Rw. 02 Kel. Cambai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Wahyu Sapto Hadi', 'Wiraswasta', 'Jalan Hikmah Rt. 03 Rw. 02 Kel. Cambai Kec. Cambai Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 22 Palembang', 'Palem', 'SMA', 'N', 'DN 11 Ma 0008173', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 87, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(5, '201611042P', '*84AAC12F5', '', '201611042P', '2004166', '', '2017-04-20', 'AJENG PUSPITA LARASATI', '', 'P', 'Prabumulih ', '1995-10-07', 'Jalan Pakjo No. 40 Rt. 003 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Indriya Herlina', 'Wiraswasta', 'Jalan Pakjo No. 40 Rt. 003 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 3 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0010580', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(6, '2016110015', '*84AAC12F5', '', '2016110015', '2004139', '', '2017-04-20', 'AMI SRIRATNA NINGSI', '', 'P', 'Purun ', '1998-01-24', 'Jalan Arimbi Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Awab Asno', 'Buruh Harian Lepas', 'Jalan Arimbi Kel. Prabujaya Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(7, '2016110024', '*84AAC12F5', '', '2016110024', '2004148', '', '2017-04-20', 'ANITA ASARI', '', 'P', 'Prabumulih ', '1998-11-22', 'Jalan Bangau No. 113 Rt. 02 Rw. 03 Kel. Karang Raja II Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Junaidi', 'Wiraswasta', 'Jalan Bangau No. 113 Rt. 02 Rw. 03 Kel. Karang Raja II Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'Madrasah Aliyah Negeri Prabumulih', 'Prabu', 'MA', 'N', 'MA 062000490', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(8, '2016110006', '*84AAC12F5', '', '2016110006', '2004130', '', '2017-04-20', 'ANJAS MEIDHERWAN', '', 'L', 'Tebat Agung ', '1999-05-01', 'Dusun Tebat Agung Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Suhadi Riyatno', 'Wiraswasta', 'Dusun Tebat Agung Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Rambang Dangku', 'Ramba', 'SMA', 'N', 'DN 11 Ma/06 0010623', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(9, '2016110037', '*84AAC12F5', '', '2016110037', '2004161', '', '2017-04-20', 'ARNISA', '', 'P', 'Jemenang ', '1999-03-12', 'Dusun II Desa Jemenang Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ison Susemi', 'Buruh Harian Lepas', 'Dusun II Desa Jemenang Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Rambang Dangku', 'Ramba', 'SMA', 'N', 'DN 11 Ma/06 0010527', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 84, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(10, '201611045P', '*84AAC12F5', '', '201611045P', '2004169', NULL, '2017-04-20', 'BASUKI RAHMAT ', '', 'L', 'Prabumulih ', '1985-11-11', 'Jalan A. Yani No. 082 Rt. 001 Rw. 003 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'H. Basyaruddin', 'Pensiunan', 'Jalan A. Yani No. 082 Rt. 001 Rw. 003 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMU MUHAMMADIYAH 1 PRABUMULIH', 'Prabu', 'SMU', 'N', '11 Mu 0425184', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'P', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2003', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(11, '2016110009', '*84AAC12F5', '', '2016110009', '2004133', NULL, '2017-04-20', 'DEWI NUR AFNI K', '', 'P', 'Bandung ', '1978-05-18', 'Jalan Perum Griya Sejahtera Rt. 006 Rw. 005 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'O. Komarudin', 'Wiraswasta', 'Jalan Perum Griya Sejahtera Rt. 006 Rw. 005 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMEA Muslimin Bandung', 'Bandu', 'Lain-lain', 'N', 'No. 02 OB om 0215408', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '1996', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 87, 'N', NULL, 20, '0.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(12, '2016110036', '*84AAC12F5', '', '2016110036', '2004160', '', '2017-04-20', 'DINNI SARI DEVI', '', 'P', 'Prabumulih ', '1998-08-17', 'PPKR Dusun Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Dal Hadi', 'Buruh Harian Lepas', 'PPKR Dusun Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/06 0006369', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(13, '2016110034', '*84AAC12F5', '', '2016110034', '2004158', '', '2017-04-20', 'DISKA UTAMI', '', 'P', 'Raja ', '1997-11-19', 'Kel. Raja Kec. Tanah Abang Kab. Pali', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Darmadi', 'Petani', 'Kel. Raja Kec. Tanah Abang Kab. Pali', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Tanah Abang', 'Tanah', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 82, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(14, '2016110029', '*84AAC12F5', '', '2016110029', '2004153', '', '2017-04-20', 'EKA ARI CRUSITA WIJAYANTI', '', 'P', 'Jakarta ', '1997-08-04', 'Asmil Yonkav 5 Rt. 014 Rw. 003 Kel. Karang Endah Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ahmad Mansyur', 'TNI AD', 'Asmil Yonkav 5 Rt. 014 Rw. 003 Kel. Karang Endah Kec. Gelumbang Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Gelumbang', 'Gelum', 'SMA', 'N', 'DN 11 Ma 0015949', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(15, '2016110001', '*84AAC12F5', '', '2016110001', '2004125', '', '2017-04-20', 'EKO HERYANI', '', 'P', 'Gunung Ibul', '1980-02-17', 'Jalan Nias No.123 Rt. 005 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Herman Z', 'PNS', 'Jalan Nias No.123 Rt. 005 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMU Negeri 02 Prabumulih', 'Prabu', 'SMU', 'N', 'No. 11 Mu 102 026764', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '1998', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 43, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(16, '201611022P', '*84AAC12F5', '', '201611022P', '2004146', '', '2017-04-20', 'ERA HUSTRI', '', 'P', 'Tanah Abang ', '1977-12-03', 'Jalan Nusa I.A No. 02 Rt. 003 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sukarman', 'Wiraswasta', 'Jalan Nusa I.A No. 02 Rt. 003 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'Akademi Keuangan dan Perbankan Mulia Darma Pratama', 'Palem', 'Akademi', 'N', '001/IV/D3/KP/2002', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'P', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2002', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(17, '2016110041', '*84AAC12F5', '', '2016110041', '2004165', '', '2017-04-20', 'FADILLA SRI PAJAR SARI', '', 'P', 'Pagar Agung', '1996-09-05', 'Jalan Sugihwaras Kel. Pagar Agung Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sastri Daryadi', 'Petani', 'Jalan Sugihwaras Kel. Pagar Agung Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0006301', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(18, '2016110026', '*84AAC12F5', '', '2016110026', '2004150', '', '2017-04-20', 'HESTI SASKIA', '', 'P', 'Muara Dua ', '1999-01-15', 'Jalan Patih Jaya Rt. 03 Rw. 03 Kel. Muara Dua Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Asmuni', 'Petani', 'Jalan Patih Jaya Rt. 03 Rw. 03 Kel. Muara Dua Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMK Negeri 1 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk/13 0004080', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(19, '2016110008', '*84AAC12F5', '', '2016110008', '2004132', '', '2017-04-20', 'INDRI RIZKI LESTARI', '', 'P', 'Prabumulih ', '1997-06-07', 'Jalan Andalas No. 698 Rt. 01 Rw. 01 Kel. Mangga Besar Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Irawan', 'Wiraswasta', 'Jalan Andalas No. 698 Rt. 01 Rw. 01 Kel. Mangga Besar Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0009714', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 84, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(20, '2016110023', '*84AAC12F5', '', '2016110023', '2004147', '', '2017-04-20', 'KARMILA YANTI ', '', 'P', 'Suban Jeriji ', '1998-08-06', 'Jalan Bima Taman Baka Kel. Prabujaya Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Cik Agus', 'Karyawan Swasta', 'Jalan Bima Taman Baka Kel. Prabujaya Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 2 Prabumulih', 'SMA N', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 83, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(21, '2016110013', '*84AAC12F5', '', '2016110013', '2004137', '', '2017-04-20', 'M. KHOIRUL KHITAMI', '', 'L', 'Prabumulih ', '1998-03-07', 'Jalan K.H Ahmad Dahlan Kec. Prabu Jaya Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'H. Sobri', 'Wiraswasta', 'Jalan K.H Ahmad Dahlan Kec. Prabu Jaya Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Muhamadiyah  1 Prabumulih', 'Prabu', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(22, '2016110039', '*84AAC12F5', '', '2016110039', '2004163', '', '2017-04-20', 'MARIA ALVIONITTA', '', 'P', 'Prabumulih ', '1998-05-02', 'Jalan Jend. Sudirman Gang Bersama Rt. 001 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabum', '', '', '', 'Prabumulih', '', NULL, '', NULL, 1, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Antonius Suharta', 'Karyawan Swasta', 'Jalan Jend. Sudirman Gang Bersama Rt. 001 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabum', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0006287', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 88, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(23, '2016110047', '*84AAC12F5', '', '2016110047', '2004171', '', '2017-04-20', 'MAWADDAH WAROHMAH', '', 'P', 'Prabumulih ', '1998-01-23', 'Jalan Arimbi Rt. 01 Rw. 05 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Drs. H. Zulkarnain', 'Wiraswasta', 'Jalan Arimbi Rt. 01 Rw. 05 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 1 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0004055', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(24, '2016110010', '*84AAC12F5', '', '2016110010', '2004134', '', '2017-04-20', 'MEGA YUNITA', '', 'P', 'Prabumulih ', '1998-03-11', 'Jalan Bangau Rt. 003 Rw. 002 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Abdul Khanan', 'Wiraswasta', 'Jalan Bangau Rt. 003 Rw. 002 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/06 0006680', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '|2|3|4|5|6|11|8|10|12|13|14|', '', NULL, 'N', 0, 0),
(25, '2016110021', '*84AAC12F5', '', '2016110021', '2004145', '', '2017-04-20', 'MERI SUSANTI', '', 'P', 'Prabumulih ', '1984-08-23', 'Jalan Kenari No. 04 Rt. 028 Rw. 010 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Harison', 'Buruh Harian Lepas', 'Jalan Kenari No. 04 Rt. 028 Rw. 010 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMU PGRI Prabumulih', 'Prabu', 'SMU', 'N', 'No 11 Mu 0424554', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2003', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 43, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(26, '2016110002', '*84AAC12F5', '', '2016110002', '2004126', NULL, '2017-04-20', 'MERIANA', '', 'P', 'Gunung Ibul', '1987-04-12', 'Jalan Padat Karya Rt. 05 Rw. 01 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'H. Tarman', 'Wiraswasta', 'Jalan Padat Karya Rt. 05 Rw. 01 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA PGRI Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0464437', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2005', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 83, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(27, '2016110011', '*84AAC12F5', '', '2016110011', '2004135', '', '2017-04-20', 'MOLFI ARIZKI', '', 'P', 'Prabumulih ', '1997-02-24', 'Jalan Sugih waras Rt. 02 Rw. 02 Kel. Tanjung Raya Kec. Rambang Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'M. Yasip', 'Petani', 'Jalan Sugih waras Rt. 02 Rw. 02 Kel. Tanjung Raya Kec. Rambang Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 2 Muara Enim', 'Muara', 'SMA', 'N', 'DN 11 Ma 0015218', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(28, '2016110031', '*84AAC12F5', '', '2016110031', '2004155', '', '2017-04-20', 'MUHAMMAD RAMDHAN CHEBA', '', 'L', 'Jakarta ', '1998-01-27', 'Jalan Gang Masjid Ar-Rahman Rt. 001 Rw. 002 Kel. Pasar Prabumulih 2 Kec. Prabumulih Utara Kota Prabu', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Khairul', 'Karyawan BUMN', 'Jalan Gang Masjid Ar-Rahman Rt. 001 Rw. 002 Kel. Pasar Prabumulih 2 Kec. Prabumulih Utara Kota Prabu', '', '', '', '', NULL, '', '', '', 'SMK YPS Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk/06 0006395', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '0.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(29, '2016110012', '*84AAC12F5', '', '2016110012', '2004136', '', '2017-04-20', 'NAAFIRA WALIYA ARISTA', '', 'P', 'Prabumulih ', '1999-07-06', 'Jalan Vina Sejahtra II Blok BB No. 07 Rt. 03 Rw. 09 Kel. Prabumulih Kec. Prabumulih Timur Kota Prabu', '', '', '', 'Prabumulih ', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sutrisno', '', 'Jalan Vina Sejahtra II Blok BB No. 07 Rt. 03 Rw. 09 Kel. Prabumulih Kec. Prabumulih Timur Kota Prabu', '', '', '', 'Prabumulih', NULL, '', '', '', 'Madrasah Aliyah Negeri Prabumulih', 'Prabu', 'MA', 'N', 'MA 062000556', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 82, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(30, '2016110027', '*84AAC12F5', '', '2016110027', '2004151', NULL, '2017-04-20', 'NEKI YAN PURNAMA', '', 'P', 'Prabumulih ', '1987-01-11', 'Jalan Jati Baru No. 09 Rt. 4 Rw. 2 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Jak Idin Rambang', 'Wiraswasta', 'Jalan Jati Baru No. 09 Rt. 4 Rw. 2 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA PGRI Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Mu 0300060', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2004', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(31, '2016110014', '*84AAC12F5', '', '2016110014', '2004138', '', '2017-04-20', 'NINI OKTAVIANI', '', 'P', 'Raja ', '1997-10-09', 'Dusun I Raja Tanah Abang Kel. Tanah Abang Kab. Pali', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Hendrianto', 'Petani', 'Dusun I Raja Tanah Abang Kel. Tanah Abang Kab. Pali', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Tanah Abang', 'Tanah', 'SMA', 'N', 'DN 11 Ma/06 0023711', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '0.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(32, '2016110028', '*84AAC12F5', '', '2016110028', '2004152', '', '2017-04-20', 'NITA SURYANTI', '', 'P', 'Palembang ', '1996-11-12', 'Jalan Pipa Talang Salim SP 4 Rt. 007 Rw. 002 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Joni', 'Sopir', 'Jalan Pipa Talang Salim SP 4 Rt. 007 Rw. 002 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK Negeri 2 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk 0011041', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 89, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(33, '201611046P', '*84AAC12F5', '', '201611046P', '2004170', '', '2017-04-20', 'NUR ANISATUS SOLIHAH', '', 'P', 'Sumber Rahayu', '1997-01-25', 'Sumber Rahayu Rt. 001 Rw. 001 Desa Sumber Rahayu Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Mustofa Hadi', 'Petani', 'Sumber Rahayu Rt. 001 Rw. 001 Desa Sumber Rahayu Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', NULL, '', '', '', 'Madrasah Aliyah Salafiyah Syafi&#39;iyah', 'Prabu', 'MA', 'N', 'Ma 160058592', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'P', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(34, '2016110016', '*84AAC12F5', '', '2016110016', '2004140', '', '2017-04-20', 'OKTA SARI', '', 'P', 'Sukaraja ', '1998-10-06', 'Jalan Basuki Rahmad No. 175 Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Bohori', 'Petani', 'Jalan Basuki Rahmad No. 175 Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 2 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/13 0002835', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2017', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(35, '2016110020', '*84AAC12F5', '', '2016110020', '2004144', '', '2017-04-20', 'OVIYANI', '', 'P', 'Gunung Ibul', '1997-10-03', 'Jalan Padat Karya Gang. Cempedak Rt. 006 Rw. 001 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Mat Nasim', 'Petani', 'Jalan Padat Karya Gang. Cempedak Rt. 006 Rw. 001 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumu', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0010654', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 76, 'N', NULL, 43, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(36, '2016110030', '*84AAC12F5', '', '2016110030', '2004154', '', '2017-04-20', 'PIPIN ARDIANSYAH', '', 'L', 'Tambangan Kelekar ', '1998-03-10', 'Jalan Tambangan Kelekar Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Tazmi', 'Petani', 'Jalan Tambangan Kelekar Kec. Gelumbang Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Gelumbang', 'Gelum', 'SMA', 'N', 'DN 11 Ma/06 0010069', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 21, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(37, '2016110035', '*84AAC12F5', '', '2016110035', '2004159', '', '2017-04-20', 'PUTRA PERMANA', '', 'L', 'Tambangan Kelekar ', '1998-08-09', 'Jalan Mentawai Rt. 01 Rw. 02 Kel. Gunung Ibul Barat Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Riza Pahlevi', 'PNS', 'Jalan Mentawai Rt. 01 Rw. 02 Kel. Gunung Ibul Barat Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 5 Bandar Lampung', 'Banda', 'SMA', 'N', 'DN 12 Ma/06 0002408', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(38, '2016110017', '*84AAC12F5', '', '2016110017', '2004141', '', '2017-04-20', 'PUTRI AYU AFRILIANY', '', 'P', 'Prabumulih ', '1998-04-26', 'Jalan Tromol Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Muhamad Rozie', 'Buruh Harian Lepas', 'Jalan Tromol Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 2 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/13 0002841', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 87, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(39, '2016110019', '*84AAC12F5', '', '2016110019', '2004143', '', '2017-04-20', 'RESTY JULIANTI ROSALINDA SOPANDI', '', 'P', 'Bandung ', '1999-07-11', 'Jalan Srikandi No. 49 Rt. 05 Rw. 03 Kel. Muntang Tapus Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Tetet Sopandi', 'Pensiunan', 'Jalan Srikandi No. 49 Rt. 05 Rw. 03 Kel. Muntang Tapus Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(40, '2016110025', '*84AAC12F5', '', '2016110025', '2004149', '', '2017-04-20', 'RINA HAYATUN NUFUS', '', 'P', 'Tebing Tenggi ', '1999-02-08', 'Tanjung Makmur Kec. Tebing Tinggi', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Tusdi Kamril', 'PNS', 'Tanjung Makmur Kec. Tebing Tinggi', '', '', '', '', NULL, '', '', '', 'SMA Negeri 6 Tebing Tinggi', 'Tebin', 'SMA', 'N', '', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(41, '2016110005', '*84AAC12F5', '', '2016110005', '2004129', '', '2017-04-20', 'RINI NOVRIANTI', '', 'P', 'Prabumulih ', '1998-11-14', 'Jalan Gotong Royong Karang Raja III Rt. 02 Rw. 04 Kel Karang Raja Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ria Musiarso', 'Wiraswasta', 'Jalan Gotong Royong Karang Raja III Rt. 02 Rw. 04 Kel Karang Raja Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/06 0006658', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(42, '2016110040', '*84AAC12F5', '', '2016110040', '2004164', '', '2017-04-20', 'RISKA ANDINI', '', 'P', 'Prabumulih ', '1995-11-09', 'Jalan Bukit Barisan No. 49 Rt. 02 Rw. 03 Kel. Majasari Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Rizal Effendi', 'Wiraswasta', 'Jalan Bukit Barisan No. 49 Rt. 02 Rw. 03 Kel. Majasari Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0010344', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, 'N', 72, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(43, '2016110032', '*84AAC12F5', '', '2016110032', '2004156', '', '2017-04-20', 'RIZKY FAIZ', '', 'L', 'Prabumulih ', '1995-06-25', 'Jalan Arjuna 1 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'M. Iriansyah', 'Wiraswasta', 'Jalan Arjuna 1 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Yayasan Bakti Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0011135', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 76, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(44, '2016110007', '*84AAC12F5', '', '2016110007', '2004131', '', '2017-04-20', 'SHINTIA CITRA FRANSISKA', '', 'P', 'Prabumulih ', '1998-01-29', 'Jalan Pakjo Rt. 03 Rw. 03 Kel. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Muhammad Fadhli', 'Wiraswasta', 'Jalan Pakjo Rt. 03 Rw. 03 Kel. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMK Negeri 1 Prabumulih', 'Prabu', 'SMK', 'N', 'DN-11 Mk 0010928', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 82, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(45, '2016110044', '*84AAC12F5', '', '2016110044', '2004168', NULL, '2017-04-20', 'SUGIYANTO', '', 'L', 'Trenggalek', '1987-01-02', 'Jalan Sugriwa Rt. 004 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sari', 'Karyawan Swasta', 'Jalan Sugriwa Rt. 004 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK Karya Dharma I Trenggalek Jatim', 'Jatim', 'SMK', 'N', 'DN 05 Mk 0412815', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2005', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(46, '2016110043', '*84AAC12F5', '', '2016110043', '2004167', '', '2017-04-20', 'SULAIMAN', '', 'L', 'Prabumulih ', '1991-01-11', 'Jalan Ade Irma Nasution Rt. 11 Rw. 05 Kel. Mangga Besar Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ruslan', 'Buruh Harian Lepas', 'Jalan Ade Irma Nasution Rt. 11 Rw. 05 Kel. Mangga Besar Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK PGRI 2 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk 0011087', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2009', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 20, '2.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(47, '2016110004', '*84AAC12F5', '', '2016110004', '2004128', '', '2017-04-20', 'TEDI RAHMAT NUGRAHA', '', 'P', 'Prabumulih ', '1994-07-13', 'Jalan Perum Prabu Indah Rt. 007 Rw. 004 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Junaidi', 'Wiraswasta', 'Jalan Perum Prabu Indah Rt. 007 Rw. 004 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Yayasan Bakti Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0011169', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(48, '2016110018', '*84AAC12F5', '', '2016110018', '2004142', '', '2017-04-20', 'WINDA NOPIYANI', '', 'P', 'Prabumulih ', '1998-11-03', 'Jalan Suban Mas No. 29 Rt. 04 Rw. 02 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sudianto', 'Buruh Harian Lepas', 'Jalan Suban Mas No. 29 Rt. 04 Rw. 02 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMK Negeri 2 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk/06 0006711', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(49, '2016110003', '*84AAC12F5', '', '2016110003', '2004127', '', '2017-04-20', 'YOLAN SAFITRI', '', 'P', 'Prabumulih ', '1995-11-21', 'Jalan M. Yusuf Wahid No. 38 Kel. Bumi Sako Damai Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ahmad Syaripudin', 'Wiraswasta', 'Jalan M. Yusuf Wahid No. 38 Kel. Bumi Sako Damai Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMK Negeri 5 Palembang', 'Palem', 'SMK', 'N', 'DN 11 Mk 0002008', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 'N', 76, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(50, '2016110033', '*84AAC12F5', '', '2016110033', '2004157', '', '2017-04-20', 'YULI APRIANI', '', 'P', 'Suka Maju ', '1998-07-04', 'Jalan Sukamaju Kec. Talang Ubi', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Bambang Irawan', 'Petani', 'Jalan Sukamaju Kec. Talang Ubi', '', '', '', '', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/06 0006316', '0.00', '61201', '', 'STIE-P', '61201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(51, '2016120016', '*84AAC12F5', '', '2016120016', '2004116', '', '2017-04-20', 'AISYAH GARDENA', '', 'P', 'Prabumulih ', '1997-07-03', 'Jalan Ade Irma Rt. 11 Rw. 05 Kel. Mangga Besar Kec. Prabumulih Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'H. Sunarto, S.Pd', 'PNS', 'Jalan Ade Irma Rt. 11 Rw. 05 Kel. Mangga Besar Kec. Prabumulih Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'MA Maâ€™arif NU Kepung', 'Kepun', 'MA', 'N', 'MA 160012676', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, NULL, 'N', 79, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(52, '2016120021', '*84AAC12F5', '', '2016120021', '2004121', '', '2017-04-20', 'AMALIYA ATMAWIJAYA', '', 'P', 'Wonogiri ', '1998-04-21', 'Jalan Jambangan Rt. 004 Rw. 003 Kel. Balepanjang Kec. Jatipurno Kab. Wonogiri Provinsi Jawa Tengah', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Suparno', 'Petani', 'Jalan Jambangan Rt. 004 Rw. 003 Kel. Balepanjang Kec. Jatipurno Kab. Wonogiri Provinsi Jawa Tengah', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Jatisrono', 'Jatis', 'SMA', 'N', 'DN 03 MA/13 0019408', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'N', 85, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(53, '2016120005', '*84AAC12F5', '', '2016120005', '2004105', '', '2017-04-20', 'ANDRE SAPUTRA', '', 'P', 'Sira Pulau Padang ', '1998-08-03', 'Gunung Ibul', '', '1', '1', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Irawan', 'Karyawan Swasta', 'Gunung Ibul', '', '1', '1', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 4 Bengkulu', 'Bengk', 'SMA', 'N', 'DN 26 Ma/13 0100202', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(54, '2016120015', '*84AAC12F5', '', '2016120015', '2004115', '', '2017-04-20', 'ANGRAINI PRATIWI', '', 'P', 'Prabumulih ', '1996-07-20', 'Jalan Urip Sumoharjo No. 168 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sukiman', '', 'Jalan Urip Sumoharjo No. 168 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK YPS Prabumulih', 'Prabu', 'SMK', 'N', 'DN-11 Mk 0013682', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(55, '2016120012', '*84AAC12F5', '', '2016120012', '2004112', '', '2017-04-20', 'ANJANI', '', 'P', 'Muara Sungai ', '1997-03-07', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'M. Ridwan', 'Petani', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'Madrasah Aliyah Negeri Prabumulih', 'Prabu', 'MA', 'N', 'MA 062000491', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, 'N', 87, 'N', NULL, 38, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(56, '2016120010', '*84AAC12F5', '', '2016120010', '2004110', '', '2017-04-20', 'ANNES CARERA', '', 'P', 'Talang Bulang ', '1998-04-14', 'Jalan Raya Pendopo Desa Talang Bulang Kec. Talang Ubi Kab. Pali', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Muhamad', 'Petani', 'Jalan Raya Pendopo Desa Talang Bulang Kec. Talang Ubi Kab. Pali', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Belimbing', 'Belim', 'SMA', 'N', '', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, 'N', 75, 'N', NULL, 38, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(57, '201612007P', '*84AAC12F5', '', '201612007P', '2004107', '', '2017-04-20', 'DENTI LIYANA', '', 'P', 'Baru Rambang ', '1995-02-03', 'Jalan Dusun II Rt. 02 Kel. Baru Rambang Kec. Rambang Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sukron', 'Petani', 'Jalan Dusun II Rt. 02 Kel. Baru Rambang Kec. Rambang Kab. Muara Enim', '', '', '', '', NULL, '', '085357828523 ', '', 'Politeknik Negeri Sriwijaya', 'Palem', 'Akademi', 'N', '0000063-251-2015', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'P', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 38, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(58, '2016120009', '*84AAC12F5', '', '2016120009', '2004109', '', '2017-04-20', 'FEBSA ZUMEL', '', 'P', 'Sukamerindu ', '1994-02-22', 'Jalan Lintas Baturaja Kel. Sukamerindu Kec. Lubai Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Zumroh Taslim', 'Petani', 'Jalan Lintas Baturaja Kel. Sukamerindu Kec. Lubai Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Lubai', 'Lubai', 'SMA', 'N', 'DN 11 Ma 0013769', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2012', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(59, '2016120024', '*84AAC12F5', '', '2016120024', '2004124', '', '2017-04-20', 'INDA OKTRIANI', '', 'P', 'Prabumulih ', '1998-10-05', 'Jalan Gotong Royong Rt. 002 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Andi Setiawan', 'Karyawan Swasta', 'Jalan Gotong Royong Rt. 002 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma/06 0006506', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, 'N', 85, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(60, '2016120019', '*84AAC12F5', '', '2016120019', '2004119', '', '2017-04-20', 'MARGAWAN KELANA', '', 'L', 'Mesuji', '1996-11-28', 'Dusun IV Kel. Lembak Kec. Lembak Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Marzuki', 'Wiraswasta', 'Dusun IV Kel. Lembak Kec. Lembak Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Lembak', 'Lemba', 'SMA', 'N', 'DN 11 Ma 0016168', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, 'N', 76, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(61, '2016120004', '*84AAC12F5', '', '2016120004', '2004104', '', '2017-04-20', 'MAYA PANGASTUTI', '', 'P', 'Kencana Mulia ', '1996-04-14', 'Kencana Mulia Kec. Rambang Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Tentrem', 'Wiraswasta', 'Kencana Mulia Kec. Rambang Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'Madrasah Aliyah Negeri Prabumulih', 'Prabu', 'MA', 'N', '', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL, 'N', 86, 'N', NULL, 20, '0.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(62, '2016120013', '*84AAC12F5', '', '2016120013', '2004113', '', '2017-04-20', 'MEILANTIKA SARI', '', 'P', 'Muara Sungai ', '1997-07-13', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Sarimin', 'PNS', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 6 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0010156', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(63, '2016120006', '*84AAC12F5', '', '2016120006', '2004106', '', '2017-04-20', 'MUNZIR', '', 'L', 'Lubuk Semantung', '1994-09-02', 'Karang Raja', '', '3', '5', 'Prabumulih ', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Fahrurozi', 'PNS', 'Karang Raja Rt. 03 Rw. 05 Kel. Karang Raja Kec. Prabumulih Timur ', '', '', '', 'Prabumulih ', NULL, '', '', '', 'Madrasah Aliyah Babul Falah Tanjung Bunut', 'Tanju', 'MA', 'N', 'MA 060007751', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2017', NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL, 'N', 76, 'N', NULL, 41, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0);
INSERT INTO `mhsw` (`ID`, `Login`, `Password`, `Description`, `NIM`, `PMBID`, `NIRM`, `Tanggal`, `Name`, `Email`, `Sex`, `TempatLahir`, `TglLahir`, `Alamat1`, `Alamat2`, `RT`, `RW`, `Kota`, `KodePos`, `KodeTelp`, `Phone`, `Agama`, `AgamaID`, `WargaNegara`, `Grp`, `SudahBekerja`, `PusatKarir`, `NamaPrsh`, `Alamat1Prsh`, `Alamat2Prsh`, `KotaPrsh`, `TelpPrsh`, `FaxPrsh`, `NamaOT`, `PekerjaanOT`, `AlamatOT1`, `AlamatOT2`, `RTOT`, `RWOT`, `KotaOT`, `KodeTelpOT`, `TelpOT`, `EmailOT`, `KodePosOT`, `AsalSekolah`, `PropSekolah`, `JenisSekolah`, `LulusSekolah`, `IjazahSekolah`, `NilaiSekolah`, `Pilihan1`, `Pilihan2`, `KodeFakultas`, `KodeJurusan`, `Status`, `KodeProgram`, `Kelas`, `StatusAwal`, `StatusPotongan`, `SPP_D`, `Semester`, `TahunAkademik`, `KodeBiaya`, `Posting`, `Lulus`, `TglLulus`, `TahunLulus`, `PredikatLulus`, `WaktuKuliah`, `Keterangan`, `Pinjaman`, `KTahun`, `K_Dosen`, `DosenID`, `Ranking`, `mGroup`, `Target`, `Prop`, `Masuk`, `NotActive`, `TestScore`, `TA`, `TglTA`, `TotalSKS`, `IPK`, `JudulTA`, `PembimbingTA`, `CatatanTA`, `PMBSyarat`, `PMBKurang`, `NomerIjazah`, `MGM`, `MGMOleh`, `MGMHonor`) VALUES
(64, '2016120022', '*84AAC12F5', '', '2016120022', '2004122', '', '2017-04-20', 'OKTARIA', '', 'P', 'Modong ', '1997-10-31', 'Dusun Modong Kec. Sungai Rotan Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Feri Irawan', 'Petani', 'Dusun Modong Kec. Sungai Rotan Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMK Negeri 1 Kotabumi', 'Kotab', 'SMK', 'N', '', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'N', 78, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(65, '2016120018', '*84AAC12F5', '', '2016120018', '2004118', '', '2017-04-20', 'POPI ANGRAINI', '', 'P', 'Prabumulih ', '1999-01-30', 'Jalan Krisna Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Bahrul Hadi', 'Buruh Harian Lepas', 'Jalan Krisna Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK Negeri 1 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk 13 0004037', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, 'N', 89, 'N', NULL, 41, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(66, '2016120017', '*84AAC12F5', '', '2016120017', '2004117', '', '2017-04-20', 'ROSSA AYU LESTARI PRAYOGIE', '', 'P', 'Palembang ', '1998-05-20', 'Jalan Surip Gang Rambang Rt. 04 Rw. 04 Kel. Pasar II Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Nur Samin Prayogie', 'Buruh Harian Lepas', 'Jalan Surip Gang Rambang Rt. 04 Rw. 04 Kel. Pasar II Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK Negeri 1 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 Mk 13 0004041', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 44, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(67, '2016120020', '*84AAC12F5', '', '2016120020', '2004120', '', '2017-04-20', 'RYAN RESTI', '', 'P', 'Belimbing Jaya ', '1996-07-07', 'Desa Belimbing Kab. Muara Enim', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Kardiman', 'Petani', 'Desa Belimbing Kab. Muara Enim', '', '', '', '', NULL, '', '', '', 'SMA Negeri 1 Gunung Megang', 'Gunun', 'SMA', 'N', 'DN 11 Ma/06 0010181', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'N', 70, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(68, '2016120023', '*84AAC12F5', '', '2016120023', '2004123', '', '2017-04-20', 'SASTRI YANI', '', 'P', 'Sugihan ', '1995-10-16', 'Jalan Perumnas Griya Medang Permai Rt. 03 Rw. 09 Kel. Sungai Medang Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Nur Hidayat', 'Petani', 'Jalan Perumnas Griya Medang Permai Rt. 03 Rw. 09 Kel. Sungai Medang Kec. Cambai Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMK PGRI 2 Prabumulih', 'Prabu', 'SMK', 'N', 'DN 11 MK 0013796', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, 'N', 90, 'N', NULL, 41, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(69, '2016120014', '*84AAC12F5', '', '2016120014', '2004114', '', '2017-04-20', 'WULAN DARI', '', 'P', 'Tanjung Raman ', '1996-03-15', 'Jalan Basuki Rahmad Rt. 004 Rw. 003 Kel. Tanjung Raman Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Jhon Kenedy', 'Sopir', 'Jalan Basuki Rahmad Rt. 004 Rw. 003 Kel. Tanjung Raman Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMA Negeri 4 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 0010487', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, 'N', 88, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(70, '2016120011', '*84AAC12F5', '', '2016120011', '2004111', '', '2017-04-20', 'WULAN SEPTIANA', '', 'P', 'Prabumulih ', '1997-09-17', 'Jl. Prabujaya Rt. 003 Rw. 002 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Anang Satio', 'Karyawan Swasta', 'Jl. Prabujaya Rt. 003 Rw. 002 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', NULL, '', '', '', 'SMK Negeri 1 Prabumulih', 'Prabu', 'SMK', 'N', 'DN-11 Mk 0010867', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, 'N', 80, 'N', NULL, 20, '3.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(71, '2016120008', '*84AAC12F5', '', '2016120008', '2004108', '', '2017-04-20', 'YATI OKTAVIANI', '', 'P', 'Muara Niru ', '1998-06-12', 'Jalan Toman Rt. 03 Rw. 02 Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', NULL, '', NULL, 3, 'Indonesia', 'N', 'N', 'N', '', '', '', '', '', '', 'Ibnu Alamsyah', 'Petani', 'Jalan Toman Rt. 03 Rw. 02 Kel. Prabujaya Kota Prabumulih', '', '', '', '', NULL, '', '', '', 'SMA Negeri 7 Prabumulih', 'Prabu', 'SMA', 'N', 'DN 11 Ma 06 0006664', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, 'B', '', 0, 0, '20161', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'N', 89, 'N', NULL, 41, '1.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0),
(72, '201612008', '01011940', NULL, '201612008', '2004174', NULL, '2017-04-22', 'RANTI YULININGSIH', 'stieprabumulihypp@gmail.com', 'L', 'Prabumulih', '1940-01-01', 'Prabumulih', '', '', '', '', '31116', NULL, '', NULL, 3, '', 'N', 'N', 'N', '', '', '', '', '', '', 'Sudarto', '', '', '', '', '', '', NULL, '', '', '', 'SMA Negeri 2Prabumulih', 'Prabu', 'SMA', 'N', '', '0.00', '62201', '', 'STIE-P', '62201', 'A', 'REG', 0, '', '', 0, 0, '20162', '0', NULL, 'N', NULL, '2016', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'N', 0, 'N', NULL, 0, '0.00', NULL, NULL, NULL, '', '', NULL, 'N', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `modul`
--

CREATE TABLE `modul` (
  `ModulID` int(11) NOT NULL,
  `GroupModul` varchar(20) NOT NULL DEFAULT '',
  `Modul` varchar(100) NOT NULL DEFAULT '',
  `Author` varchar(255) DEFAULT NULL,
  `EmailAuthor` varchar(100) DEFAULT NULL,
  `Description` varchar(100) NOT NULL DEFAULT '',
  `InMenu` enum('Y','N') NOT NULL DEFAULT 'N',
  `web` enum('Y','N') NOT NULL DEFAULT 'N',
  `cs` enum('Y','N') NOT NULL DEFAULT 'N',
  `Baris` smallint(6) NOT NULL DEFAULT '1',
  `Link` varchar(100) DEFAULT NULL,
  `ImgLink` varchar(100) NOT NULL DEFAULT '',
  `OnlyAdmin` enum('Y','N') NOT NULL DEFAULT 'N',
  `Help` varchar(255) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Level` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `modul`
--

INSERT INTO `modul` (`ModulID`, `GroupModul`, `Modul`, `Author`, `EmailAuthor`, `Description`, `InMenu`, `web`, `cs`, `Baris`, `Link`, `ImgLink`, `OnlyAdmin`, `Help`, `NotActive`, `Level`) VALUES
(1, 'PMB', '03 Daftar Mahasiswa Baru', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Lihat daftar mahasiswa baru', 'Y', 'Y', 'N', 1, 'listofnewstudent', 'sysfo/image/usr01.gif', 'N', '', 'N', '12345'),
(2, 'PMB', '02 Pendaftaran Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pendaftaran Mahasiswa Baru', 'Y', 'Y', 'N', 1, 'pmbform&agree=1', 'sysfo/image/usr01.gif', 'N', '', 'N', '125'),
(3, 'Master', '06 Master Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Mahasiswa', 'Y', 'Y', 'N', 1, 'mhsw', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(4, 'AdmDosen', '00 Master Dosen', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Dosen', 'Y', 'Y', 'N', 1, 'dosen&usr=dosen', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(5, 'Master', '01 Kampus', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Kampus', 'Y', 'Y', 'N', 1, 'listofcampus', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(6, 'Master', '03 Program/Kelas', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Program atau Kelas', 'Y', 'Y', 'N', 1, 'listofprogram', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(7, 'Sistem', '1. Admin User Admin', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Login Administrator', 'Y', 'Y', 'N', 1, 'useraccess&usr=admin', 'sysfo/image/computer.gif', 'Y', '', 'N', '1'),
(8, 'Sistem', '3. Admin User Dosen', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Login Dosen', 'Y', 'Y', 'N', 1, 'useraccess&usr=dosen', 'sysfo/image/computer.gif', 'Y', '', 'N', '1'),
(9, 'PMB', 'st1 Stat Pendaft per Prg', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Menampilkan statistik jumlah pendaftar per program.', 'Y', 'Y', 'N', 1, 'pmbstat', 'sysfo/image/usr01.gif', 'N', '', 'N', '123'),
(10, 'PMB', '00 Prefix Aktif PMB', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Mengganti prefix yg aktif utk no. ID PMB', 'Y', 'Y', 'N', 1, 'pmbprefix', 'sysfo/image/usr01.gif', 'N', '', 'N', '12'),
(11, 'PMB', 'st2 Stat Pendaft per Asal Sklh', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Statistik pendaftar per asal sekolah', 'Y', 'Y', 'N', 1, 'pmbstatasal', 'sysfo/image/usr01.gif', 'N', '', 'N', '123'),
(12, 'PMB', '01 Biaya Pendaftaran', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Setup biaya pendaftaran per Program', 'Y', 'Y', 'N', 1, 'pmbprice', 'sysfo/image/usr01.gif', 'N', '', 'N', '12'),
(18, 'Master', '02 Fakultas & Jurusan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Edit fakultas-Jurusan', 'Y', 'Y', 'N', 1, 'fakjur', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(16, 'Sistem', '0. Administrasi Modul', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Modul Admin.', 'Y', 'Y', 'N', 1, 'modmin', 'sysfo/image/computer.gif', 'Y', '', 'N', '1'),
(17, 'Sistem', '2. Admin User Karyawan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Login Karyawan', 'Y', 'Y', 'N', 1, 'useraccess&usr=karyawan', 'sysfo/image/computer.gif', 'N', '', 'N', '1'),
(19, 'Master', '04 Ruang Kelas', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Ruang Kuliah', 'Y', 'Y', 'N', 1, 'listofruang', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(20, 'KaAkademik', '01 Kurikulum', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Kurikulum', 'Y', 'Y', 'N', 1, 'kurikulum', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(21, 'KaAkademik', '03 Mata Kuliah per Semester', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar Mata Kuliah per Semester', 'Y', 'Y', 'N', 1, 'matakuliah', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(22, 'Master', '07 Jenis Mata Kuliah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengelolaan Jenis Mata Kuliah', 'Y', 'Y', 'N', 1, 'jenismatakuliah', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(23, 'KaAkademik', '04 Mata Kuliah per Jenis', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar Mata Kuliah per Jenis MK', 'Y', 'Y', 'N', 1, 'matakuliah&st=1', 'icon/folderclose.gif', 'N', '', 'N', '1'),
(62, 'Keuangan', '03 Balance Keuangan Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Total keuangan mhsw', 'Y', 'Y', 'N', 1, 'mhswkeu', '', 'N', '', 'N', '1'),
(30, 'Master', '00 Master Nilai', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Range Nilai', 'Y', 'Y', 'N', 1, 'nilai', '', 'N', '', 'N', '1'),
(25, 'Master', '00 Identitas Perguruan Tinggi', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Identitas perguruan tinggi', 'Y', 'Y', 'N', 1, 'identity', '', 'N', '', 'N', '1'),
(26, 'PMB', '6. Kapasitas Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Kapasitas Mahasiswa', 'N', 'Y', 'N', 1, 'pmbkap', '', 'N', NULL, 'N', '1'),
(27, 'KaAkademik', '02 Tahun Akademik', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Tahun Akademik.', 'Y', 'Y', 'N', 1, 'thnakd', '', 'N', '', 'N', '1'),
(28, 'Akademik', '01 Penjadwalan Kuliah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Penjadwalan Kuliah', 'Y', 'Y', 'N', 1, 'jdwlkuliah', '', 'N', '', 'N', '12'),
(29, 'Akademik', '02. Jadwal Ruang', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jadwal Ruang Kelas', 'N', 'Y', 'N', 1, 'jdwlrg', '', 'N', '', 'N', '1'),
(31, 'Sistem', '4. Admin User Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Admin akses mahasiswa', 'Y', 'Y', 'N', 1, 'useraccess&usr=mhsw', '', 'N', '', 'N', '1'),
(32, 'Mahasiswa', '02 Kartu Rencana Studi (KRS)', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Pengisian KRS oleh mahasiswa', 'Y', 'Y', 'N', 1, 'mhswkrs', '', 'N', '', 'N', '14'),
(33, 'Akademik', '0. Kalendar Akademik', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Kalendar Akademik', 'Y', 'Y', 'N', 1, 'bataskrs', '', 'N', '', 'N', '1'),
(34, 'Akademik', '03 Absensi', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Untuk absensi dosen & mahasiswa', 'Y', 'Y', 'N', 1, 'absensi', '', 'N', '', 'N', '12'),
(35, 'Dosen', '01 Nilai Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Input nilai mahasiswa', 'Y', 'Y', 'N', 1, 'mhswnilai', '', 'N', '', 'N', '13'),
(38, 'Dosen', '00 Jadwal Mengajar', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jadwal mengajar dosen.', 'Y', 'Y', 'N', 1, 'jdwldosen', '', 'N', '', 'N', '13'),
(36, 'Mahasiswa', '00 Jadwal Kuliah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar mata kuliah yang ditawarkan.', 'Y', 'Y', 'N', 1, 'jmk01', '', 'N', '', 'N', '14'),
(37, 'Mahasiswa', '03 Kartu Hasil Studi (KHS)', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Lihat nilai mahasiswa', 'Y', 'Y', 'N', 1, 'mhswkhs', '', 'N', '', 'N', '14'),
(39, 'Akademik', '04 Tunda Mata Kuliah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Tunda penilaian MK', 'Y', 'Y', 'N', 1, 'tundamk', '', 'N', '', 'N', '1'),
(40, 'Dosen', '02 Tunda Nilai MK Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Tunda nilai mata kuliah mahasiswa', 'Y', 'Y', 'N', 1, 'tundamhsw', '', 'N', '', 'N', '13'),
(41, 'Mahasiswa', '06 Daft MK yg tlh Diambil', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar Mata Kuliah yg Telah Diambil.', 'Y', 'Y', 'N', 1, 'mhswmk', '', 'N', '', 'N', '14'),
(42, 'KaAkademik', '05 Prasyarat Mata Kuliah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Prasyarat suatu mata kuliah', 'Y', 'Y', 'N', 1, 'prasyaratmk', '', 'N', '', 'N', '1'),
(43, 'Mahasiswa', '05 Index Prestasi Kumulatif', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Index Prestasi Kumulatif', 'Y', 'Y', 'N', 1, 'mhswipk', '', 'N', '', 'N', '14'),
(44, 'Dosen', '03 Perwalian', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Perwalian', 'Y', 'Y', 'N', 1, 'dosenwali', '', 'N', '', 'N', '13'),
(45, 'Master', '08 Setup Prefix NIM', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Set prefix tiap jurusan', 'Y', 'Y', 'N', 1, 'prefixnim', '', 'N', '', 'N', '1'),
(58, 'Dosen', 'r0 Jadwal Jaga Ujian', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jadwal jaga ujian', 'Y', 'Y', 'N', 1, 'dosenjaga', '', 'N', NULL, 'N', '13'),
(46, 'Akademik', '0. Status Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Set status mahasiswa', 'Y', 'Y', 'N', 1, 'statusmhsw', '', 'N', NULL, 'N', '12'),
(47, 'Akademik', '05 Jadwal Ujian', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jadwal Ujian Tengah Semester', 'Y', 'Y', 'N', 1, 'jdwlujian&ujn=', '', 'N', '', 'N', '1'),
(51, 'Akademik', 'r- Cetak Formulir Absensi', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Absensi Kuliah', 'Y', 'Y', 'N', 1, 'cetakabsen&ujn=', '', 'N', '', 'N', '1'),
(49, 'Mahasiswa', '04 Jadwal Ujian', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jadwal Ujian: UAS, UTS atau Susulan', 'Y', 'Y', 'N', 1, 'jdwlujian&ujn=', '', 'N', '', 'N', '14'),
(60, 'Keuangan', '00 Master Biaya', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Master Biaya', 'Y', 'Y', 'N', 1, 'masterbiaya', '', 'N', '', 'N', '1'),
(57, 'Dosen', 'r1 Cetak Nilai Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak nilai2 mahasiswa', 'Y', 'Y', 'N', 1, 'cetaknilai&nil=', '', 'N', NULL, 'N', '13'),
(56, 'Akademik', 'r- Cetak Nilai Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Mencetak nilai2 mahasiswa', 'Y', 'Y', 'N', 1, 'cetaknilai&nil=', '', 'N', '', 'N', '12'),
(61, 'Keuangan', '01 Master Keuangan Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Keuangan Mahasiswa', 'Y', 'Y', 'N', 1, 'mhswmasterkeu', '', 'N', '', 'N', '1'),
(59, 'Akademik', 'r- Cetak Pengawas Ujian', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Pengawas Ujian', 'Y', 'Y', 'N', 1, 'cetakjagaujian', '', 'N', '', 'N', '12'),
(63, 'Keuangan', '+ Setup Master BPP Pokok', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Setup biaya BPP Pokok', 'Y', 'Y', 'N', 1, 'setupbppp', '', 'N', '', 'N', '1'),
(64, 'Mahasiswa', '07 Keuangan Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Keuangan Mahasiswa', 'Y', 'Y', 'N', 1, 'mhswkeu', '', 'N', '', 'N', '14'),
(65, 'PMB', '06 Masukkan Nilai Tes Masuk', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Memasukkan nilai test masuk.', 'Y', 'Y', 'N', 1, 'pmbnilai', '', 'N', '', 'N', '1'),
(66, 'PMB', '07 Proses Penerimaan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Proses Penerimaan Mahasiswa Baru.', 'Y', 'Y', 'N', 1, 'pmbterima', '', 'N', '', 'N', '1'),
(67, 'Sistem', '0. Group Modul', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Administrasi group modul', 'Y', 'Y', 'N', 1, 'modgroup', '', 'N', '', 'N', '1'),
(68, 'Keuangan', 'rX Laporan Penerimaan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Laporan penerimaan pembayaran', 'N', 'Y', 'N', 1, 'lappterima', '', 'N', '', 'N', '1'),
(69, 'Keuangan', 'r1 Laporan Mhsw Belum Lunas', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Laporan mhsw belum lunas', 'Y', 'Y', 'N', 1, 'lapblmlns', '', 'N', '', 'N', '1'),
(70, 'Master', '00 Struktur Organisasi', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Struktur Organisasi', 'Y', 'Y', 'N', 1, 'struorg', '', 'N', '', 'N', '1'),
(71, 'Akademik', '06 Tugas Akhir', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Administrasi Tugas Akhir', 'Y', 'Y', 'N', 1, 'tgsakhir', '', 'N', '', 'N', '1'),
(72, 'Dosen', '04 Bimbingan Tugas Akhir', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Bimbingan Tugas Akhir', 'Y', 'Y', 'N', 1, 'dosenta', '', 'N', '', 'N', '13'),
(73, 'Master', '09 Jenis Pembayaran', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Jenis Pembayaran', 'Y', 'Y', 'N', 1, 'jnsbyr', '', 'N', '', 'N', '1'),
(75, 'Sistem', '0. Daftar Modul Standar', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar Modul terinstal', 'Y', 'Y', 'N', 1, 'daftarmodul', '', 'N', '', 'N', '1'),
(76, 'Sistem', '0. Daftar Modul Tambahan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Daftar Modul Tambahan', 'Y', 'Y', 'N', 1, 'daftarmodul&md=1', '', 'N', '', 'N', '1'),
(77, 'PMB', 'r0 Laporan Pembayaran PMB', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Pembayaran PMB', 'Y', 'Y', 'N', 1, 'cetakbyrpmb', '', 'N', '', 'N', '1'),
(78, 'PMB', 'r1 Laporan Belum Bayar PMB', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak yg belum bayar PMB', 'Y', 'Y', 'N', 1, 'cetakbyrpmb&md=-1', '', 'N', '', 'N', '1'),
(79, 'PMB', '00 Kosongkan Tabel PMB', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Mengosongkan tabel PMB', 'Y', 'Y', 'N', 1, 'pmbempty', '', 'N', '', 'N', '1'),
(80, 'Master', '00 Master-master', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Edit beberapa tabel master', 'Y', 'Y', 'N', 1, 'mastermaster', '', 'N', '', 'N', '1'),
(81, 'PMB', '00 Check List Persyaratan Penerimaan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Master check list prasyarat PMB', 'Y', 'Y', 'N', 1, 'pmbsyarat', '', 'N', '', 'N', '1'),
(82, 'Mahasiswa', '00 Kalendar Akademik', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Kalendar Akademik', 'Y', 'Y', 'N', 1, 'kalakd', '', 'N', '', 'N', '14'),
(100, 'Keuangan', 'r2 Laporan Kewajiban Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Laporan Kewajiban Mahasiswa', 'Y', 'Y', 'N', 1, 'rptcall&rpt=kewajibanmhsw&param=1', '', 'N', '', 'N', '1'),
(85, 'Master', '10 Setup Ijazah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Setup Ijazah tiap jurusan', 'Y', 'Y', 'N', 1, 'setupijazah', '', 'N', '', 'N', '1'),
(86, 'KaAkademik', '07 Kelulusan Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Kelulusan Mahasiswa', 'Y', 'Y', 'N', 1, 'mhswlulus', '', 'N', '', 'N', '1'),
(87, 'Akademik', 'r- Cetak Ijazah', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Mencetak ijazah berdasarkan template', 'Y', 'Y', 'N', 1, 'cetakijazah', '', 'N', '', 'N', '1'),
(88, 'KaAkademik', '08 Edit MK Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Edit nilai mahasiswa manual', 'Y', 'Y', 'N', 1, 'editnilaimhsw', '', 'N', '', 'N', '1'),
(89, 'Sistem', '- Migrasi Kurikulum (1)', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Lihat: Kurikulum.txt', 'N', 'Y', 'N', 1, 'migrasi/migrasikurikulum', '', 'N', '', 'N', '1'),
(90, 'Akademik', '09 Buat File Nilai utk Dosen', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Buat file nilai utk dosen', 'Y', 'Y', 'N', 1, 'filenilai', '', 'N', '', 'N', '1'),
(91, 'Sistem', '- Migrasi Kurikulum ID (2)', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'MIGRASI', 'N', 'Y', 'N', 1, 'migrasi/migrasikurikulumid', '', 'N', '', 'N', '1'),
(92, 'Sistem', '- Migrasi Nilai Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'MIGRASI', 'N', 'Y', 'N', 1, 'migrasi/migrasinilaimhsw', '', 'N', '', 'N', '1'),
(93, 'Akademik', 'r- Cetak Kartu Hasil Studi', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Kartu Hasil Studi', 'Y', 'Y', 'N', 1, 'rptcall&rpt=cetakkhs', '', 'N', '', 'N', '1'),
(94, 'Sistem', '5. Report Manager', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Print Manager', 'Y', 'Y', 'N', 1, 'rptmgr', '', 'N', '', 'N', '1'),
(95, 'PMB', '04 Member: Get Member', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Program Member Get Member', 'Y', 'Y', 'N', 1, 'mbrgetmbr', '', 'N', '', 'N', '1'),
(96, 'PMB', 'r2 Laporan Mhsw Get Member', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Laporan Mhsw Member Get Member', 'Y', 'Y', 'N', 1, 'rpt/cetakmgm', '', 'N', '', 'N', '1'),
(97, 'PMB', 'r3 Honor Program MGM', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Laporan Program Member Get Member', 'Y', 'Y', 'N', 1, 'rpt/cetakprgmgm', '', 'N', '', 'N', '1'),
(98, 'PMB', '05 Honor Member Get Member', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Honor program Member Get Member', 'Y', 'Y', 'N', 1, 'mbrgetmbrhonor', '', 'N', '', 'N', '1'),
(99, 'Keuangan', '+ Setup Lap. Kewajiban', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Setup laporan kewajiban mahasiswa', 'Y', 'Y', 'N', 1, 'setupkeuwajib&Kali=1', '', 'N', '', 'N', '1'),
(101, 'Akademik', 'r- Cetak Transkrip Nilai', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak transkrip nilai', 'Y', 'Y', 'N', 1, 'rptcall&rpt=cetaktranskrip', '', 'N', '', 'N', '1'),
(102, 'Keuangan', '+ Setup Program BPP Pokok', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Setup Program BPP Pokok', 'N', 'N', 'N', 1, 'prgbppp', '', 'N', '', 'N', '1'),
(103, 'Akademik', 'r- Cetak Transkrip Sementara', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Transkrip Sementara', 'Y', 'Y', 'N', 1, 'rptcall&rpt=transkripsementara', '', 'N', '', 'N', '1'),
(104, 'Master', '11 Master Tanda Tangan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Master tanda tangan', 'Y', 'Y', 'N', 1, 'tandatangan', '', 'N', '', 'N', '1'),
(105, 'Akademik', 'r- Cetak Kartu Penyetaraan', 'E. Setio Dewo', 'setio_dewo@telkom.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=laporanpenyetaraan', '', 'N', '', 'N', '1'),
(106, 'KaAkademik', 'r- Surat Keputusan Mengajar', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Surat Keputusan Mengajar', 'Y', 'Y', 'N', 1, 'suratmengajar', '', 'N', '', 'N', '1'),
(107, 'Akademik', 'r- Kartu Realisasi Perkuliahan', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Kartu Realisasi Perkuliahan', 'Y', 'Y', 'N', 1, 'karturealisasiperkuliahan', '', 'N', '', 'N', '1'),
(108, 'Akademik', 'r- Cetak Kartu Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Cetak Kartu Mahsiswa', 'Y', 'Y', 'N', 1, 'cetakkartumhsw', '', 'N', '', 'N', '1'),
(109, 'Akademik', 'r- Lap. Kehadiran Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', '', 'Y', 'Y', 'N', 1, 'cetakkehadiranmhsw', '', 'N', '', 'N', '1'),
(110, 'Sistem', '- Migrasi Prasyarat MK', 'E. Setio Dewo', 'setio_dewo@telkom.net', '', 'N', 'Y', 'N', 1, 'migrasi/migrasiprasyaratmk', '', 'N', '', 'N', '1'),
(111, 'Akademik', '02 KRS Mahasiswa', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'KRS Mahasiswa', 'Y', 'Y', 'N', 1, 'mhswkrs', '', 'N', '', 'N', '1'),
(112, 'KaAkademik', '06 Maksimum SKS', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Maksimum SKS yg dpt diambil', 'Y', 'Y', 'N', 1, 'maxsks', '', 'N', '', 'N', '1'),
(113, 'KaAkademik', '07 Dispensasi KRS', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Dispensasi KRS', 'Y', 'Y', 'N', 1, 'dispensasikrs', '', 'N', '', 'N', '1'),
(114, 'KaAkademik', '09 Password Dosen untuk File Nilai', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Set password dosen utk file nilai', 'Y', 'Y', 'N', 1, 'dosenpwd', '', 'N', '', 'N', '1'),
(115, 'Sistem', 'x. Hapus File2 Temporary', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Hapus File2 Temporary', 'Y', 'Y', 'N', 1, 'hapustemp', '', 'N', '', 'N', '1'),
(116, 'KaAkademik', '10 Master Penyetaraan', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Master Penyetaraan', 'Y', 'Y', 'N', 1, 'setaramaster', '', 'N', '', 'N', '1'),
(117, 'KaAkademik', '11 Rekap Data Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Rekap Data Mahasiswa', 'N', 'Y', 'N', 1, 'mhswrekap', '', 'N', '', 'N', '1'),
(118, 'Keuangan', 'r3 Laporan Penerimaan', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Laporan Penerimaan Pembayaran', 'Y', 'Y', 'N', 1, 'rptcall&rpt=laptrmbyr', '', 'N', '', 'N', '1'),
(119, 'KaAkademik', '12 Monitor IPK/IPS Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Untuk melihat IPK dan IPS Mahasiswa', 'Y', 'Y', 'N', 1, 'mhswipkips', '', 'N', '', 'N', '1'),
(120, 'AdmDosen', '04 Evaluasi Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Evaluasi Dosen', 'Y', 'Y', 'N', 1, 'doseneval', '', 'N', '', 'N', '1'),
(121, 'AdmDosen', '00 Jabatan Organisasi', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Jabatan Organisasi', 'Y', 'Y', 'N', 1, 'jbtnorg', '', 'N', '', 'N', '1'),
(122, 'AdmDosen', '00 Setup Honor  per Program', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Honor Dosen per Program', 'Y', 'Y', 'N', 1, 'jurhonor', '', 'N', '', 'N', '1'),
(123, 'AdmDosen', '01 Honor Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Honor Dosen', 'Y', 'Y', 'N', 1, 'dosenhonor', '', 'N', '', 'N', '1'),
(124, 'AdmDosen', 'r- Perincian Dosen Pembimbing', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Perincian Dosen Pembimbing', 'Y', 'Y', 'N', 1, 'rptcall&rpt=dsnbimbing', '', 'N', '', 'N', '1'),
(127, 'Keuangan', '02 Proses Keuangan', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Proses Keuangan', 'Y', 'Y', 'N', 1, 'proseskeu', '', 'N', '', 'N', '1'),
(126, 'AdmDosen', '02 Rekapitulasi Honor Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Rekapitulasi Honor Dosen', 'Y', 'Y', 'N', 1, 'rptcall&rpt=rekaphonordosen', '', 'N', '', 'N', '1'),
(128, 'AdmDosen', '03 Rekap Kehadiran Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=rekaphadirdosen', '', 'N', '', 'N', '1'),
(129, 'Alumni', '00 Pengelolaan Alumni', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Pengelolaan Alumni', 'Y', 'Y', 'N', 1, 'alumni', '', 'N', '', 'N', '1'),
(130, 'Alumni', '01 Statistik Alumni', 'E Setio Dewo', 'setio_dewo@sisfokampus.net', 'Statistik Alumni yang Sudah Bekerja vs Belum', 'Y', 'Y', 'N', 1, 'alumnistatistik', '', 'N', '', 'N', '1'),
(131, 'Mahasiswa', '01 Registrasi Ulang Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Registrasi Ulang', 'Y', 'Y', 'N', 1, 'mhswherreg', '', 'N', '', 'N', '14'),
(132, 'PMB', 'r- Kelengkapan Prasyarat', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Data Penerimaan Mahasiswa', 'Y', 'Y', 'N', 1, 'pmbintake', '', 'N', '', 'N', '1'),
(133, 'PMB', '08 Pembatalan Penerimaan', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Pembatalan Penerimaan Mahasiswa Baru', 'Y', 'Y', 'N', 1, 'pmbbatal', '', 'N', '', 'N', '1'),
(134, 'Keuangan', '+ Setup Lap. Potongan Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Potongan', 'Y', 'Y', 'N', 1, 'setupkeuwajib&Kali=-1', '', 'N', '', 'N', '1'),
(135, 'Keuangan', 'r2 Laporan Potongan Mhsw', 'E. Setio Dewo', 'setio_dewo@telkom.net', 'Potongan', 'Y', 'Y', 'N', 1, 'rptcall&rpt=potonganmhsw&param=-1', '', 'N', '', 'N', '1'),
(136, 'KaAkademik', '10 Penyetaraan', 'E. Setio Dewo', 'setio_dewo@telkom.net', '', 'Y', 'Y', 'N', 1, 'setara', '', 'N', '', 'N', '1'),
(137, 'KaAkademik', 'r- Lap IPK Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=cetakipkmhsw', '', 'N', '', 'N', '1'),
(138, 'KaAkademik', 'r- Lap Nilai per Mata Kuliah', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'cetaknilaimk', '', 'N', '', 'N', '1'),
(139, 'KaAkademik', 'r- Lap Pengambilan KRS', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'cetaksksmhsw', '', 'N', '', 'N', '1'),
(140, 'KaAkademik', 'r- Lap Status Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'cetakstatusmhsw', '', 'N', '', 'N', '1'),
(141, 'AdmDosen', 'r- Cetak Kwitansi Honor Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'dosenkwitansi', '', 'N', '', 'N', '1'),
(142, 'KaAkademik', 'r- Evaluasi 4 Semester', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=evaluasi4smt', '', 'N', '', 'N', '1'),
(143, 'PMB', 'r- Data Mahasiswa PMB', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'pmbmhsw', '', 'N', '', 'N', '1'),
(144, 'Keuangan', 'w- Kasir', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'N', 'N', 'Y', 1, 'kasir', '', 'N', '', 'N', '1'),
(145, 'Akademik', 'r- Rincian Transkrip Nilai', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=transkriprincian', '', 'N', '', 'N', '1'),
(146, 'Master', '00 Currency', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'currency', '', 'N', '', 'N', '1'),
(147, 'PMB', 'r- Data Calon Mahasiswa', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'pmbcalon', '', 'N', '', 'N', '1'),
(148, 'PMB', 'r- Ranking PMB', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'pmbranking', '', 'N', '', 'N', '1'),
(149, 'AdmDosen', 'r- Laporan Data Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'dosendata', '', 'N', '', 'N', '1'),
(150, 'AdmDosen', '05 Kewajiban Mengajar Dosen', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'kewajibanmengajar', '', 'N', '', 'N', '1'),
(151, 'Master', '00 Master Predikat', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'predikat', '', 'N', '', 'N', '1'),
(152, 'Keuangan', 'r- Laporan Pembayaran Mhsw', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Laporan Pembayaran Mhsw', 'Y', 'Y', 'N', 1, 'rptcall&rpt=lapbyrmhsw', '', 'N', '', 'N', '1'),
(153, 'Keuangan', 'r- Rekap Pembayaran Mhsw', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Rekap Pembayaran Mhsw', 'Y', 'Y', 'N', 1, 'rptcall&rpt=lapbyrmhswrekap', '', 'N', '', 'N', '1'),
(154, 'Keuangan', 'r- Lap Harian Pembayaran', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=lapbyrmhswharian', '', 'N', '', 'N', '1'),
(155, 'Keuangan', 'r- Rincian Pembayaran Lain', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'rptcall&rpt=lapbyrlain', '', 'N', '', 'N', '1'),
(156, 'Akademik', '07 Ubah NIM & Jurusan Mhsw', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'mhswubahnim', '', 'N', '', 'N', '1'),
(157, 'KaAkademik', '13. Konversi Kurikulum Mhsw', 'E. Setio Dewo', 'setio_dewo@sisfokampus.net', 'Untuk mengkonversi kurikulum Mhsw', 'Y', 'Y', 'N', 1, 'mhswkonversikurikulum', '', 'N', '', 'N', '1'),
(158, 'AdmDosen', '06 Setup Tanda Tangan Honor', 'Emanuel Setio Dewo', 'setio_dewo@sisfokampus.net', '', 'Y', 'Y', 'N', 1, 'dosenhonortt', '', 'N', '', 'N', '1'),
(159, 'Master', '12 Survey', 'Sgufakto', 'sgufakto@gmail.com', 'CRUD Survey', 'Y', 'Y', 'N', 1, 'survey', '', 'N', '', 'N', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `news`
--

CREATE TABLE `news` (
  `NewsID` int(20) NOT NULL,
  `NewsDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Language` varchar(20) NOT NULL DEFAULT '',
  `Title` varchar(100) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Category` varchar(25) NOT NULL DEFAULT '',
  `Level` smallint(6) NOT NULL DEFAULT '4',
  `unip` varchar(20) NOT NULL DEFAULT '',
  `Author` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `Charge` varchar(20) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `DateExpired` datetime DEFAULT '0000-00-00 00:00:00',
  `Content` text,
  `ReadCount` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `news`
--

INSERT INTO `news` (`NewsID`, `NewsDate`, `Language`, `Title`, `Description`, `Category`, `Level`, `unip`, `Author`, `Email`, `Location`, `Charge`, `NotActive`, `DateExpired`, `Content`, `ReadCount`) VALUES
(1, '2005-03-21 00:33:21', 'Indonesia', 'Sistem Informasi Akademik YPPrabumulih', 'Selamat datang di situs resmi pengembang sistem informasi manajemen perguruan tinggi open source: <b>Sisfo Kampus</b>', 'FrontPage', 5, 'admin', 'Administrator', '', NULL, NULL, 'N', '0000-00-00 00:00:00', '<p>\r\nSistem Informasi Akademik adalah Sistem Informasi Manajemen Akademik YPPrabumulih. Tercermin bahwa Sistem ini ditujukan untuk mengelola Sistem Informasi Akademik, dalam hal ini adalah perguruan tinggi di Indonesia.</p>\r\n</p>', 4),
(2, '2005-03-21 01:10:48', 'Indonesia', 'Sisfo Informasi Akademik YPPrabumulih', 'Jika Anda dapat membaca pesan ini, berarti Anda telah berhasil menjalankan Sisfo Kampus 2oo5. Untuk login, Anda dapat menggunakan user name: admin, level: Administrator, password: admin.', 'Pengumuman', 5, 'admin', 'Administrator', '', NULL, NULL, 'Y', '0000-00-00 00:00:00', '<p>Keterangan lebih lanjut mengenai Sisfo Informasi Akademik dapat Anda peroleh dengan menghubungi tim ICT kami</p>\r\n\r\n<p>Selain itu terdapat forum untuk kesulitan-kesulitan dalam implementasi Sistem Informasi Akademik. Diharapkan dengan forum ini, semua masalah & juga usulan pengembangan dapat didiskusikan bersama.</p>', 62),
(3, '2017-04-22 15:08:04', 'Indonesia', '', '', 'Visi dan Misi', 5, 'admin', 'Administrator', '', NULL, NULL, 'N', '0000-00-00 00:00:00', 'Visi STIE Prabumulih\r\n\r\n\"Menjadi sekolah tinggi yang terkemuka dalam bidang ilmu ekonomi dengan menghasilkan lulusan yang berkualitas, ilmiah, dan berdaya saing ditingkat nasional tahun 2025\"\r\n\r\nMisi STIE Prabumulih\r\n\r\nMenghasilkan lulusan yang menguasai ilmu ekonomi yang berjiwa bisnis dan kewirausahaan yang profesional; Meningkatkan kompetensi dosen untuk menunjang tridharma perguruan tinggi di bidang ilmu ekonomi; Menerapkan ilmu dan teknologi di bidang manajemen dan akuntansi; Mengembangkan jaringan kerjasama dengan pemerintah, industri dan  kemitraan lainnya  dalam bidang ekonomi untuk merespon daya saing global.', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `newscategory`
--

CREATE TABLE `newscategory` (
  `Category` varchar(20) NOT NULL DEFAULT '',
  `Language` varchar(20) NOT NULL DEFAULT '',
  `Level` smallint(6) NOT NULL DEFAULT '0',
  `EditLevel` smallint(6) NOT NULL DEFAULT '1',
  `Listed` enum('Y','N') NOT NULL DEFAULT 'N',
  `Description` varchar(100) NOT NULL DEFAULT '',
  `ImgLink` varchar(100) DEFAULT NULL,
  `Comment` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `newscategory`
--

INSERT INTO `newscategory` (`Category`, `Language`, `Level`, `EditLevel`, `Listed`, `Description`, `ImgLink`, `Comment`, `NotActive`) VALUES
('Sambutan', 'Indonesia', 5, 2, 'Y', 'Sambutan-sambutan', NULL, 'N', 'N'),
('Kelas Internasional', 'Indonesia', 5, 3, 'Y', 'Berita utk kelas Internasional', NULL, 'N', 'N'),
('Jadwal Kuliah', 'Indonesia', 4, 3, 'Y', '', NULL, 'N', 'N'),
('Pengumuman', 'Indonesia', 5, 3, 'Y', '', NULL, 'N', 'N'),
('Opini', 'Indonesia', 5, 4, 'Y', 'Anda punya opini? Silakan posting!', NULL, 'N', 'N'),
('Artikel', 'Indonesia', 5, 3, 'Y', '', NULL, 'N', 'N'),
('Announcement', 'English', 5, 1, 'Y', '', NULL, 'N', 'N'),
('Admin', 'Indonesia', 1, 1, 'Y', '', NULL, 'N', 'N'),
('Visi dan Misi', 'Indonesia', 5, 2, 'N', 'Visi', NULL, 'N', 'N'),
('Profile', 'Indonesia', 5, 2, 'N', 'Profile', NULL, 'N', 'N'),
('Lowongan Pekerjaan', 'Indonesia', 5, 3, 'Y', '', NULL, 'N', 'N'),
('Kolom Alumni', 'Indonesia', 5, 4, 'Y', '', NULL, 'N', 'N'),
('Pelatihan', 'Indonesia', 5, 3, 'Y', 'Pelatihan utk Mahasiswa.', NULL, 'N', 'N'),
('Seminar', 'Indonesia', 5, 3, 'Y', 'Daftar Seminar', NULL, 'N', 'N'),
('Kegiatan', 'Indonesia', 5, 4, 'Y', 'Kegiatan internal & eksternal kampus', NULL, 'N', 'N'),
('Jadwal PMB', 'Indonesia', 5, 2, 'Y', '', NULL, 'N', 'N'),
('FrontPage', 'Indonesia', 5, 1, 'N', 'Gambar di halaman depan', NULL, 'N', 'N'),
('DaftarDosen', 'Indonesia', 5, 1, 'N', '', NULL, 'N', 'N'),
('PelatihanAbout', 'Indonesia', 5, 3, 'N', 'Tentang Pelatihan', NULL, 'N', 'N'),
('PelatihanKonsultan', 'Indonesia', 5, 3, 'N', 'Konsultan Pelatihan', NULL, 'N', 'N'),
('a', 'Indonesia', 1, 1, 'Y', '', NULL, 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `ID` int(11) NOT NULL,
  `Kode` varchar(25) DEFAULT NULL,
  `Nilai` varchar(10) NOT NULL DEFAULT '',
  `BatasAtas` decimal(5,2) NOT NULL DEFAULT '0.00',
  `BatasBawah` decimal(5,2) NOT NULL DEFAULT '0.00',
  `Bobot` decimal(5,2) NOT NULL DEFAULT '0.00',
  `Keterangan` varchar(50) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`ID`, `Kode`, `Nilai`, `BatasAtas`, `BatasBawah`, `Bobot`, `Keterangan`, `NotActive`) VALUES
(1, 'Umum', 'A', '100.00', '85.00', '4.00', 'Sangat Baik', 'N'),
(2, 'Umum', 'B', '84.99', '70.00', '3.00', 'Baik', 'N'),
(3, 'Umum', 'C', '69.99', '50.00', '2.00', 'Cukup', 'N'),
(4, 'Umum', 'D', '49.99', '45.00', '1.00', 'Kurang', 'N'),
(5, 'Umum', 'E', '44.99', '0.00', '0.00', 'Gagal', 'N'),
(19, 'S2', 'A', '100.00', '80.00', '4.00', '', 'N'),
(17, 'S2', 'E', '44.00', '0.00', '0.00', NULL, 'N'),
(8, 'S2', 'D', '49.00', '45.00', '1.00', NULL, 'N'),
(10, 'S2', 'B+', '79.00', '74.00', '3.30', '', 'N'),
(11, 'S2', 'B', '73.00', '68.00', '3.00', '', 'N'),
(12, 'S2', 'B-', '67.00', '62.00', '2.70', '', 'N'),
(14, 'S2', 'C', '61.00', '50.00', '2.00', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `org`
--

CREATE TABLE `org` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(255) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `org`
--

INSERT INTO `org` (`ID`, `Nama`, `NotActive`) VALUES
(1, 'Yayasan', 'N'),
(2, 'Universitas', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orgmbr`
--

CREATE TABLE `orgmbr` (
  `ID` int(11) NOT NULL,
  `OrgID` int(11) NOT NULL DEFAULT '0',
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Jabatan` varchar(100) NOT NULL DEFAULT '',
  `Nama` varchar(255) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orgmbr`
--

INSERT INTO `orgmbr` (`ID`, `OrgID`, `Rank`, `Jabatan`, `Nama`, `NotActive`) VALUES
(5, 1, 2, 'Ketua STIE Prabumulih', 'Yudi Tusri, SE., M.Si', 'N'),
(6, 1, 3, 'Pembantu Ketua I ', 'Ajabar, S.IP., M.Si', 'N'),
(3, 1, 1, 'Ketua Yayasan Pendidikan Prabumulih', 'H. T. Kosim Cikming, S.Ip., M.Si', 'N'),
(7, 1, 4, 'Pembantu Ketua II', 'Meirani Betriana, SE., M.Si', 'N'),
(8, 1, 5, 'Pembantu Ketua III', 'Susianti, SE., M.M', 'N'),
(9, 1, 6, 'Ketua Prodi Akuntansi', 'Citra Etika, SE., M.Si', 'N'),
(10, 1, 7, 'Ketua Prodi Manajemen', 'Zakaria Harahap, S.EI., M.HI', 'N'),
(11, 1, 8, 'Ketua LP2M', 'Rona Anggrainie, SP., M.Si', 'N'),
(12, 1, 9, 'Ketua UPM', 'Siska Alfiati, SP., M.Si', 'N'),
(13, 1, 10, 'Ketua BAAK/BAU', 'Hj. Sri Suparni, SS., M.Si', 'N'),
(14, 1, 11, 'Ketua UPT Laboratorium', 'Khana Wijaya, S.Kom., M.Kom', 'N'),
(15, 1, 12, 'Ketua UPT Lembaga Bahasa', 'Hephny Samosir, S.pd., M.Pd', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengawasujian`
--

CREATE TABLE `pengawasujian` (
  `ID` int(11) NOT NULL,
  `Level` smallint(6) NOT NULL DEFAULT '0',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `UJN` varchar(5) DEFAULT NULL,
  `Honor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengawasujian`
--

INSERT INTO `pengawasujian` (`ID`, `Level`, `Login`, `IDJadwal`, `UJN`, `Honor`) VALUES
(1, 3, 'harahap', 9, 'UAS', 0),
(2, 3, 'ajabar', 9, 'UAS', 0),
(3, 3, 'resi', 9, 'UAS', 0),
(4, 3, 'endrekson', 9, 'UAS', 0),
(5, 3, 'yuditusri', 1, 'UAS', 0),
(6, 3, 'citra', 1, 'UAS', 0),
(7, 3, 'susianti', 1, 'UAS', 0),
(8, 3, 'suparni', 1, 'UAS', 0),
(9, 3, 'lingga', 2, 'UAS', 0),
(10, 3, 'sukmawati', 2, 'UAS', 0),
(11, 3, 'suparni', 2, 'UAS', 0),
(12, 3, 'susianti', 2, 'UAS', 0),
(13, 3, 'ajabar', 3, 'UAS', 0),
(14, 3, 'harahap', 3, 'UAS', 0),
(15, 3, 'yelli', 3, 'UAS', 0),
(16, 3, 'resi', 3, 'UAS', 0),
(17, 3, 'endrekson', 4, 'UAS', 0),
(18, 3, 'sukmawati', 4, 'UAS', 0),
(19, 3, 'resi', 4, 'UAS', 0),
(20, 3, 'yelli', 4, 'UAS', 0),
(21, 3, 'citra', 5, 'UAS', 0),
(22, 3, 'yuditusri', 5, 'UAS', 0),
(23, 3, 'suparni', 5, 'UAS', 0),
(24, 3, 'sukmawati', 5, 'UAS', 0),
(25, 3, 'susianti', 6, 'UAS', 0),
(26, 3, 'lingga', 6, 'UAS', 0),
(27, 3, 'sukmawati', 6, 'UAS', 0),
(28, 3, 'suparni', 6, 'UAS', 0),
(29, 3, 'yelli', 7, 'UAS', 0),
(30, 3, 'endrekson', 7, 'UAS', 0),
(31, 3, 'harahap', 7, 'UAS', 0),
(32, 3, 'resi', 7, 'UAS', 0),
(33, 3, 'citra', 10, 'UAS', 0),
(34, 3, 'suparni', 10, 'UAS', 0),
(35, 3, 'susianti', 10, 'UAS', 0),
(36, 3, 'yuditusri', 10, 'UAS', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perguruantinggi`
--

CREATE TABLE `perguruantinggi` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Kota` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `perguruantinggi`
--

INSERT INTO `perguruantinggi` (`Kode`, `Nama`, `Kota`, `NotActive`) VALUES
('001001', 'UNIVERSITAS GAJAH MADA', 'YOGYAKARTA\r', 'N'),
('001002', 'UNIVERSITAS INDONESIA', 'JAKARTA\r', 'N'),
('001003', 'UNIVERSITAS SUMATERA UTARA', 'MEDAN\r', 'N'),
('001004', 'UNIVERSITAS AIRLANGGA', 'SURABAYA\r', 'N'),
('001005', 'UNIVERSITAS HASANUDIN', 'MAKASSAR\r', 'N'),
('001006', 'UNIVERSITAS ANDALAS', 'PADANG\r', 'N'),
('001007', 'UNIVERSITAS PADJADJARAN', 'BANDUNG\r', 'N'),
('001008', 'UNIVERSITAS DIPONEGORO', 'SEMARANG\r', 'N'),
('001009', 'UNIVERSITAS SRIWIJAYA', 'PALEMBANG\r', 'N'),
('001010', 'UNIVERSITAS LAMBUNG MANGKURAT', 'BANJARMASIN\r', 'N'),
('001011', 'UNIVERSITAS SYIAH KUALA', 'BANDA ACEH\r', 'N'),
('001012', 'UNIVERSITAS SAM RATULANGI', 'MANADO\r', 'N'),
('001013', 'UNIVERSITAS UDAYANA', 'DENPASAR\r', 'N'),
('001014', 'UNIVERSITAS NUSA CENDANA', 'KUPANG\r', 'N'),
('001015', 'UNIVERSITAS MULAWARMAN', 'SAMARINDA\r', 'N'),
('001016', 'UNIVERSITAS MATARAM', 'MATARAM\r', 'N'),
('001017', 'UNIVERSITAS RIAU', 'PEKANBARU\r', 'N'),
('001018', 'UNIVERSITAS CENDRAWASIH', 'JAYAPURA\r', 'N'),
('001019', 'UNIVERSITAS BRAWIJAYA', 'MALANG\r', 'N'),
('001020', 'UNIVERSITAS JAMBI', 'JAMBI\r', 'N'),
('001021', 'UNIVERSITAS PATTIMURA', 'AMBON\r', 'N'),
('001022', 'UNIVERSITAS TANJUNGPURA', 'PONTIANAK\r', 'N'),
('001023', 'UNIVERSITAS JENDERAL SOEDIRMAN', 'PURWOKERTO\r', 'N'),
('001024', 'UNIVERSITAS PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('001025', 'UNIVERSITAS JEMBER', 'JEMBER\r', 'N'),
('001026', 'UNIVERSITAS LAMPUNG', 'BANDAR LAMPUNG\r', 'N'),
('001027', 'UNIVERSITAS NEGERI SEBELAS MARET', 'SURAKARTA\r', 'N'),
('001028', 'UNIVERSITAS TADULAKO', 'PALU\r', 'N'),
('001029', 'UNIVERSITAS HALUOLEO', 'KENDARI\r', 'N'),
('001030', 'UNIVERSITAS BENGKULU', 'BENGKULU\r', 'N'),
('001031', 'UNIVERSITAS TERBUKA', 'JAKARTA\r', 'N'),
('001032', 'UNIVERSITAS NEGERI PADANG', 'PADANG\r', 'N'),
('001033', 'UNIVERSITAS NEGERI MALANG', 'MALANG\r', 'N'),
('001034', 'UNIVERSITAS PENDIDIKAN INDONESIA', 'BANDUNG\r', 'N'),
('001035', 'UNIVERSITAS NEGERI MANADO', 'MANADO\r', 'N'),
('001036', 'UNIVERSITAS NEGERI MAKASAR', 'MAKASSAR\r', 'N'),
('001037', 'UNIVERSITAS NEGERI JAKARTA', 'JAKARTA\r', 'N'),
('001038', 'UNIVERSITAS NEGERI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('001039', 'UNIVERSITAS NEGERI SURABAYA', 'SURABAYA\r', 'N'),
('001040', 'UNIVERSITAS NEGERI MEDAN', 'MEDAN\r', 'N'),
('001041', 'UNIVERSITAS NEGERI SEMARANG', 'SEMARANG\r', 'N'),
('001042', 'UNIVERSITAS SULTAN AGENG TIRTAYASA', 'SERANG\r', 'N'),
('001043', 'UNIVERSITAS TRUNOJOYO MADURA', 'BANGKALAN\r', 'N'),
('001044', 'UNIVERSITAS KHAIRUN TERNATE', 'TERNATE\r', 'N'),
('001045', 'UNIVERSITAS NEGERI PAPUA MANOKWARI', 'MANOKWARI\r', 'N'),
('001046', 'UNIVERSITAS MALIKUSSALEH', 'LHOKSEUMAWE\r', 'N'),
('002001', 'INSTITUT TEKNOLOGI BANDUNG', 'BANDUNG\r', 'N'),
('002002', 'INSTITUT TEKNOLOGI SEPULUH NOVEMBER', 'SURABAYA\r', 'N'),
('002003', 'INSTITUT PERTANIAN BOGOR', 'BOGOR\r', 'N'),
('002004', 'INSTITUT KEGURUAN DAN ILMU PENDIDIKAN GORONTALO', 'GORONTALO\r', 'N'),
('002005', 'INSTITUT SENI INDONESIA YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('002006', 'INSTITUT KEGURUAN DAN ILMU PENDIDIKAN SINGARAJA', 'SINGARAJA\r', 'N'),
('003001', 'SEKOLAH TINGGI SENI INDONESIA SURAKARTA', 'SURAKARTA\r', 'N'),
('003002', 'SEKOLAH TINGGI SENI INDONESIA PADANG PANJANG', 'PADANG PANJANG\r', 'N'),
('003003', 'SEKOLAH TINGGI SENI INDONESIA DENPASAR', 'DENPASAR\r', 'N'),
('003004', 'SEKOLAH TINGGI SENI INDONESIA BANDUNG', 'BANDUNG\r', 'N'),
('005001', 'POLITEKNIK MANUFAKTUR BANDUNG', 'BANDUNG\r', 'N'),
('005002', 'POLITEKNIK NEGERI JAKARTA', 'JAKARTA\r', 'N'),
('005003', 'POLITEKNIK NEGERI MEDAN', 'MEDAN\r', 'N'),
('005004', 'POLITEKNIK NEGERI BANDUNG', 'BANDUNG\r', 'N'),
('005005', 'POLITEKNIK NEGERI SEMARANG', 'SEMARANG\r', 'N'),
('005006', 'POLITEKNIK NEGERI SRIWIJAYA', 'PALEMBANG\r', 'N'),
('005007', 'POLITEKNIK PERTANIAN NEGERI LAMPUNG', 'BANDAR LAMPUNG\r', 'N'),
('005008', 'POLITEKNIK NEGERI AMBON', 'AMBON\r', 'N'),
('005009', 'POLITEKNIK NEGERI PADANG', 'PADANG\r', 'N'),
('005010', 'POLITEKNIK NEGERI BALI', 'DENPASAR\r', 'N'),
('005011', 'POLITEKNIK NEGERI PONTIANAK', 'PONTIANAK\r', 'N'),
('005012', 'POLITEKNIK NEGERI MAKASSAR', 'MAKASSAR\r', 'N'),
('005013', 'POLITEKNIK NEGERI MANADO', 'MANADO\r', 'N'),
('005014', 'POLITEKNIK PERKAPALAN NEGERI SURABAYA', 'SURABAYA\r', 'N'),
('005015', 'POLITEKNIK NEGERI BANJARMASIN', 'BANJARMASIN\r', 'N'),
('005016', 'POLITEKNIK NEGERI LHOSEUMAWE', 'LHOK SEUMAWE\r', 'N'),
('005017', 'POLITEKNIK NEGERI KUPANG', 'KUPANG\r', 'N'),
('005018', 'POLITEKNIK ELEKTRONIK NEGERI SURABAYA', 'SURABAYA\r', 'N'),
('005019', 'POLITEKNIK NEGERI JEMBER', 'JEMBER\r', 'N'),
('005020', 'POLITEKNIK NEGERI PANGKAJENE KEPULAUAN', 'PANGKEP\r', 'N'),
('005021', 'POLITEKNIK PERTANIAN NEGERI KUPANG', 'KUPANG\r', 'N'),
('005022', 'POLITEKNIK PERIKANAN NEGERI TUAL', 'TUAL\r', 'N'),
('005023', 'POLITEKNIK NEGERI MALANG', 'MALANG\r', 'N'),
('005024', 'POLITEKNIK PERTANIAN NEGERI SAMARINDA', 'SAMARINDA\r', 'N'),
('005025', 'POLITEKNIK PERTANIAN NEGERI PAYAKUMBUH', 'PAYAKUMBUH\r', 'N'),
('005026', 'POLITEKNIK NEGERI SAMARINDA', 'SAMARINDA\r', 'N'),
('011001', 'UNIVERSITAS ISLAM SUMATERA UTARA', 'MEDAN\r', 'N'),
('011002', 'UNIVERSITAS HKBP NOMMENSEN', 'MEDAN\r', 'N'),
('011003', 'UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA', 'MEDAN\r', 'N'),
('011004', 'UNIVERSITAS PEMBANGUNAN PANCA BUDI', 'MEDAN\r', 'N'),
('011005', 'UNIVERSITAS SIMALUNGUN', 'PEMATANG SIANTAR\r', 'N'),
('011006', 'UNIVERSITAS METHODIST INDONESIA', 'MEDAN\r', 'N'),
('011007', 'UNIVERSITAS DARMA AGUNG', 'MEDAN\r', 'N'),
('011008', 'UNIVERSITAS MEDAN AREA', 'MEDAN\r', 'N'),
('011009', 'UNIVERSITAS KATOLIK SANTO THOMAS', 'MEDAN\r', 'N'),
('011010', 'UNIVERSITAS AMIR HAMZAH', 'MEDAN\r', 'N'),
('011011', 'UNIVERSITAS SISINGAMANGARAJA XII', 'MEDAN\r', 'N'),
('011012', 'UNIVERSITAS DHARMA WANGSA', 'MEDAN\r', 'N'),
('011013', 'UNIVERSITAS KARO', 'KABANJAHE\r', 'N'),
('011014', 'UNIVERSITAS MUHAMMADIYAH TAPANULI SELATAN', 'PADANG SIDEMPUAN\r', 'N'),
('011015', 'UNIVERSITAS GRAHA NUSANTARA', 'PADANG SIDEMPUAN\r', 'N'),
('011016', 'UNIVERSITAS ASAHAN', 'KISARAN\r', 'N'),
('011017', 'UNIVERSITAS ALWASHLIYAH', 'MEDAN\r', 'N'),
('011018', 'UNIVERSITAS SISINGAMANGARAJA XII', 'SIBORONG\r', 'N'),
('011019', 'UNIVERSITAS SAMUDRA', 'LANGSA\r', 'N'),
('011020', 'UNIVERSITAS MALIKUSSALEH', 'LHOKSEUMAWE\r', 'N'),
('011021', 'UNIVERSITAS JABAL GAFUR', 'SIGLI\r', 'N'),
('011022', 'UNIVERSITAS ISKANDAR  MUDA', 'BANDA ACEH\r', 'N'),
('011023', 'UNIVERSITAS ABULYATAMA', 'BANDA ACEH\r', 'N'),
('011024', 'UNIVERSITAS MUHAMMADIYAH BANDA ACEH', 'BANDA ACEH\r', 'N'),
('011025', 'UNIVERSITAS PEMBANGUNAN MASYARAKAT INDONESIA', 'MEDAN\r', 'N'),
('011026', 'UNIVERSITAS AL- AZHAR', 'MEDAN\r', 'N'),
('011027', 'UNIVERSITAS MUSLIM NUSANTARA', 'MEDAN\r', 'N'),
('011028', 'UNIVERSITAS ISLAM LABUHAN BATU', 'RANTAU PRAPAT\r', 'N'),
('011029', 'UNIVERSITAS CUT NYAK DHIEN', 'MEDAN\r', 'N'),
('012001', 'I.K.I.P. GUNUNG SITO', 'GUNUNG SITOLI\r', 'N'),
('012002', 'I.K.I.P. ALWASYLIYAH     *', 'MEDAN\r', 'N'),
('012003', 'INSTITUT TEKNOLOGI MEDAN', 'MEDAN\r', 'N'),
('012004', 'INSTITUT SAINS & TEKNOLOGI TD PARDEDE', 'MEDAN\r', 'N'),
('013001', 'SEKOLAH TINGGI HUKUM YNI', 'PEMATANG SIANTAR\r', 'N'),
('013002', 'S.T.K.I.P. RIAMA', 'MEDAN\r', 'N'),
('013003', 'SEKOLAH TINGGI ILMU EKONOMI SWADAYA', 'MEDAN\r', 'N'),
('013004', 'SEKOLAH TINGGI BAHASA ASING SWADAYA', 'MEDAN\r', 'N'),
('013005', 'SEKOLAH TINGGI ILMU HUKUM SWADAYA', 'MEDAN\r', 'N'),
('013006', 'SEKOLAH TINGGI ILMU EKONOMI CUT NYAK DHIEN', 'MEDAN\r', 'N'),
('013007', 'SEKOLAH TINGGI POLITIH CUT NYAK DHIEN   *', 'MEDAN\r', 'N'),
('013008', 'SEKOLAH TINGGI ILMU HUKUM CUT NYAK DHIEN   *', 'MEDAN\r', 'N'),
('013009', 'SEKOLAH TINGGI ILMU EKONOMI HARAPAN', 'MEDAN\r', 'N'),
('013010', 'SEKOLAH TINGGI BAHASA ASING HARAPAN', 'MEDAN\r', 'N'),
('013011', 'SEKOLAH TIGGI TEKNIK HARAPAN', 'MEDAN\r', 'N'),
('013012', 'S.T.K.I.P. TELADAN', 'MEDAN\r', 'N'),
('013013', 'SEKOLAH TINGGI POLITIK SURYA NUSANTARA', 'TEBING TINGGI\r', 'N'),
('013014', 'SEKOLAH TINGGI ILMU EKONOMI DUTA NUSANTARA   *', 'MEDAN\r', 'N'),
('013015', 'SEKOLAH TINGGI ILMU KOMUNIKASI MEDAN', 'MEDAN\r', 'N'),
('013016', 'S.T.K.I.P. SERAMBI MEKAH', 'BANDA ACEH\r', 'N'),
('013017', 'SEKOLAH TINGGI TEKNOLOGI INDUSTRI SERAMBI MEKAH', 'BANDA ACEH\r', 'N'),
('013018', 'SEKOLAH TINGGI TEKNOLOGI PERTANIAN SERAMBI MEKAH', 'BANDA ACEH\r', 'N'),
('013019', 'SEKOLAH TINGGI ILMU EKONOMI INDONESIA', 'BANDA ACEH\r', 'N'),
('013020', 'SEKOLAH TINGGI ILMU HUKUM B HURABA', 'PADANG SIDEMPUAN\r', 'N'),
('013021', 'SEKOLAH TINGGI ILMU EKONOMI BENTENG HURABA', 'PADANG SIDEMPUAN\r', 'N'),
('013022', 'SEKOLAH TINGGI ILMU EKONOMI NUSA BANGSA', 'MEDAN\r', 'N'),
('013023', 'SEKOLAH TINGGI ILMU EKONOMI TRI KARYA', 'MEDAN\r', 'N'),
('013024', 'SEKOLAH TINGGI OLAHRAGA & KESEHATAN BINAGUNA', 'MEDAN\r', 'N'),
('013025', 'SEKOLAH TINGGI ILMU EKONOMI SABANG', 'BANDA ACEH\r', 'N'),
('013026', 'SEKOLAH TINGGI THEOLOGI  H K B P', 'PEMATANG SIANTAR\r', 'N'),
('013027', 'SEKOLAH TINGGI ILMU EKONOMI PELITA  BANGSA', 'BINJAI\r', 'N'),
('013028', 'S.T.K.I.P. PELITA BANGSA', 'BINJAI\r', 'N'),
('013029', 'S.T.K.I.P. BUDI DAYA', 'BINJAI\r', 'N'),
('013030', 'SEKOLAH TINGGI ILMU EKONOMI AL-WASHLIYAH', 'SIBOLGA\r', 'N'),
('013031', 'S.T.K.I.P. TAPANULI SELATAN', 'PADANG SIDEMPUAN\r', 'N'),
('013032', 'SEKOLAH TINGGI PERTANIAN GAJAH PUTIH', 'TAKENGON\r', 'N'),
('013033', 'SEKOLAH TINGGI PERTANIAN AL-MUSLIM', 'BIREUEN\r', 'N'),
('013034', 'S.T.M.I.K. ABULYATAMA', 'BANDA ACEH\r', 'N'),
('013035', 'SEKOLAH TINGGI ILMU KEHUTANAN PANTE KULU', 'BANDA ACEH\r', 'N'),
('013036', 'SEKOLAH TINGGI PERTANIAN BENTENG HURABA', 'PADANG SIDEMPUAN\r', 'N'),
('013037', 'SEKOLAH TINGGI ILMU MANAJEMEN MEDAN', 'MEDAN\r', 'N'),
('013038', 'SEKOLAH TINGGI ILMU ADMINISTRASI ISKANDAR THANI', 'BANDA ACEH\r', 'N'),
('013039', 'SEKOLAH TINGGI TEKNIK ISKANDAR THANI', 'BANDA ACEH\r', 'N'),
('013040', 'SEKOLAH TINGGI TEKNIK INDUSTRI GLUGUR', 'MEDAN\r', 'N'),
('013041', 'SEKOLAH TINGGI TEKNOLOGI IMMANUEL', 'MEDAN\r', 'N'),
('013042', 'SEKOLAH TINGGI ILMU KOMUNIKASI PEMBANGUNAN', 'MEDAN\r', 'N'),
('013043', 'SEKOLAH TINGGI ILMU EKONOMI MUHAMMADIYAH ASAHAN', 'KISARAN\r', 'N'),
('013044', 'SEKOLAH TINGGI PERTANIAN NAMO RAYA    **', 'TEBING TINGGI\r', 'N'),
('013045', 'SEKOLAH TINGGI ILMU EKONOMI JENDERAL SUDIRMAN  *', 'MEDAN\r', 'N'),
('013046', 'SEKOLAH TINGGI TEKNIK PELITA BANGSA   **', 'BINJAI\r', 'N'),
('013047', 'SEKOLAH TINGGI ILMU EKONOMI M A R S', 'PEMATANG SIANTAR\r', 'N'),
('013048', 'SEKOLAH TINGGI ILMU EKONOMI KHALSA      **', 'MEDAN\r', 'N'),
('013049', 'SEKOLAH TINGGI TEKNIK DUTA NUSANTARA   **', 'MEDAN\r', 'N'),
('013050', 'SEKOLAH TINGGI PERTANIAN DUTA NUSANTARA  **', 'MEDAN\r', 'N'),
('013051', 'SEKOLAH TINGGI ILMU EKONOMI SURYA NUSANTARA', 'PEMATANG SIANTAR\r', 'N'),
('013052', 'SEKOLAH TINGGI ILMU MANAJEMEN MASYARAKAT PASE', 'LANGSA\r', 'N'),
('013053', 'SEKOLAH TINGGI PERTANIAN GUNUNG LEUSER', 'KUTA CANE\r', 'N'),
('013054', 'SEKOLAH TINGGI PERTANIAN MEULABOH', 'MEULABOH\r', 'N'),
('013055', 'SEKOLAH TINGGI ILMU MANAJEMEN BANDA ACEH', 'BANDA ACEH\r', 'N'),
('013056', 'SEKOLAH TINGGI ILMU EKONOMI Y H B', 'BANDA ACEH\r', 'N'),
('013057', 'SEKOLAH TINGGI EKONOMI TELADAN', 'MEDAN\r', 'N'),
('013058', 'SEKOLAH TINGGI TEKNIK BINA CENDIKIA', 'BANDA ACEH\r', 'N'),
('013059', 'SEKOLAH TINGGI ILMU HUKUM MUHAMMADIYAH TAKENGON', 'TAKENGON\r', 'N'),
('013060', 'SEKOLAH TINGGI ILMU HUKUM MUHAMMADIYAH KISARAN', 'KISARAN\r', 'N'),
('013061', 'S.T.M.I.K. BUDI DARMA', 'MEDAN\r', 'N'),
('013062', 'SEKOLAH TINGGI ILMU EKONOMI TAMAN HARAPAN', 'MEDAN\r', 'N'),
('013063', 'SEKOLAH TINGGI MANAJEMEN & ILMU KOMPUTER MIKROSKIL', 'MEDAN\r', 'N'),
('013064', 'SEKOLAH TINGGI ILMU EKONOMI TRICOM', 'MEDAN\r', 'N'),
('013065', 'SEKOLAH TINGGI ILMU EKONOMI GAJAH PUTIH', 'TAKENGON\r', 'N'),
('013066', 'SEKOLAH TINGGI ILMU EKONOMI AL-HIKMAH', 'MEDAN\r', 'N'),
('013067', 'SEKOLAH TINGGI ILMU EKONOMI EKA PRASETYA', 'MEDAN\r', 'N'),
('013068', 'SEKOLAH TINGGI ILMU EKONOMI GRAHA KIRANA', 'MEDAN\r', 'N'),
('013069', 'SEKOLAH TINGGI ILMU HUKUM GRAHA KIRANA', 'MEDAN\r', 'N'),
('013070', 'SEKOLAH TINGGI ILMU HUKUM AL-HIKMAH', 'MEDAN\r', 'N'),
('013071', 'SEKOLAH TINGGI ILMU EKONOMI ATMATERA', 'MEDAN\r', 'N'),
('013072', 'S.T.M.I.K. SISINGAMANGARAJA XII', 'MEDAN\r', 'N'),
('013073', 'SEKOLAH TINGGI ILMU EKONOMI LABUHAN BATU', 'RANTAU PRAPAT\r', 'N'),
('013074', 'SEKOLAH TINGGI ILMU EKONOMI IBBI', 'MEDAN\r', 'N'),
('013075', 'SEKOLAH TINGGI ILMU EKONOMI SOPOSURUNG', 'BALIGE\r', 'N'),
('013076', 'SEKOLAH TINGGI ILMU EKONOMI Y P KAMPUS', 'PADANG SIDEMPUAN\r', 'N'),
('013077', 'SEKOLAH TINGGI ILMU EKONOMI L M IMMANUEL INDONESIA', 'MEDAN\r', 'N'),
('013078', 'SEKOLAH TINGGI ILMU EKONOMI SERAMBI MEKAH', 'BANDA ACEH\r', 'N'),
('013079', 'SEKOLAH TINGGI TEKNIK GRAHA KIRANA', 'MEDAN\r', 'N'),
('013080', 'SEKOLAH TINGGI TEKNIK CUT NYAK DHIEN   *', 'MEDAN\r', 'N'),
('013081', 'SEKOLAH TINGGI ILMU EKONOMI RIAMA', 'MEDAN\r', 'N'),
('013082', 'S.T.K.I.P. LABUHAN  BATU', 'RANTAU PRAPAT\r', 'N'),
('013083', 'SEKOLAH TINGGI ILMU PERTANIAN LABUHAN BATU', 'RANTAU PRAPAT\r', 'N'),
('013084', 'SEKOLAH TINGGI ILMU HUKUM LABUHAN BATU', 'RANTAU PRAPAT\r', 'N'),
('013085', 'SEKOLAH TINGGI ILMU MANAJEMEN SUKMA', 'MEDAN\r', 'N'),
('013086', 'S.T.M.I.K. BINA BANGSA', 'LHOKSEUMAWE\r', 'N'),
('013087', 'S.T.K.I.P. ALMUSLIM - BIREUEN', 'BIREUEN\r', 'N'),
('013088', 'SEKOLAH TINGGI ILMU EKONOMI SULTAN AGUNG', 'PEMATANG SIANTAR\r', 'N'),
('013089', 'SEKOLAH TINGGI ILMU EKONOMI PEMBANGUNAN', 'GUNUNG SITOLI\r', 'N'),
('013090', 'SEKOLAH TINGGI FILSAFAT THEOLOGI S NUSANTARA', 'PEMATANG SIANTAR\r', 'N'),
('013091', 'SEKOLAH TINGGI ILMU EKONOMI ITMI', 'MEDANG\r', 'N'),
('013092', 'SEKOLAH TINGGI ILMU EKONOMI GUNUNG LEUSER', 'KUTACANE\r', 'N'),
('013093', 'SEKOLAH TINGGI ILMU KESEHATAN MUTIARA INDONESIA', 'MEDAN\r', 'N'),
('013094', 'S.T.K.I.P. GUNUNG LEUSER', 'KUTACANE\r', 'N'),
('013095', 'SEKOLAH TINGGI PERIKANAN SIBOLGA', 'SIBOLGA\r', 'N'),
('013096', 'SEKOLAH TINGGI MANAJEMEN & ILMU KOMPUTER MEULABOH', 'MEULABOH\r', 'N'),
('013097', 'SEKOLAH TINGGI ILMU EKONOMI BINA KARYA', 'TEBING TINGGI\r', 'N'),
('013098', 'SEKOLAH TINGGI ILMU BAHASA ASING ITMI', 'MEDAN\r', 'N'),
('013099', 'SEKOLAH TINGGI ILMU EKONOMI IBMI', 'MEDAN\r', 'N'),
('013100', 'SEKOLAH TINGGI KELAUTAN DAN PERIKANAN INDONESIA', 'MEDAN\r', 'N'),
('013101', 'SEKOLAH TINGGI ILMU KESEHATAN HELVETIA', 'MEDAN\r', 'N'),
('013102', 'SEKOLAH TINGGI ILMU KESEHATAN MEDISTRA', 'LUBUK PAKAM\r', 'N'),
('013103', 'SEKOLAH TINGGI ILMU PSIKOLOGI HARAPAN BANGSA', 'BANDA ACEH\r', 'N'),
('013104', 'SEKOLAH TINGGI ILMU KESEHATAN DELI HUSADA', 'DELITUA\r', 'N'),
('013105', 'SEKOLAH TINGGI MANAJEMEN & ILMU KOMPUTER TIME', 'MEDAN\r', 'N'),
('013106', 'SEKOLAH TINGGI ILMU ADMINISTRASI NASIONAL', 'LHOMSEUMAWE\r', 'N'),
('013107', 'S.T.K.I.P. AL-WASHLIYAH', 'BANDA ACEH\r', 'N'),
('013108', 'SEKOLAH TINGGI ILMU KESEHATAN TAKASIMA', 'KABANJAHE\r', 'N'),
('013109', 'SEKOLAH TINGGI MANAJEMEN & ILMU KOMPUTER LOGIKA', 'MEDAN\r', 'N'),
('013110', 'SEKOLAH TINGGI ILMU KESEHATAN PRIMA HUSADA', 'MEDAN\r', 'N'),
('014001', 'AKADEMI PIMPINAN PERUSAHAAN APIPSU', 'MEDAN\r', 'N'),
('014002', 'AKADEMI KEUANGAN PERBANKAN SWADAYA', 'MEDAN\r', 'N'),
('014003', 'AKADEMI PERTANIAN GUNUNG SITOLI', 'GUNUNG SITOLI\r', 'N'),
('014004', 'AKADEMI MANAJEMEN BANDA ACEH   **', 'BANDA ACEH\r', 'N'),
('014005', 'AKADEMI MARITIM INDONESIA', 'MEDAN\r', 'N'),
('014006', 'AKADEMI AKUNTANSI Y P K', 'MEDAN\r', 'N'),
('014007', 'AKADEMI TEKNIK ISKANDAR MUDA', 'BANDA ACEH\r', 'N'),
('014008', 'AKADEMI PARIWISATA DARMA AGUNG', 'MEDAN\r', 'N'),
('014009', 'AKADEMI SEKRETARI MANAJEMEN KALSA **', 'MEDAN\r', 'N'),
('014010', 'AKADEMI SEKRETARI MANAJEMEN MARIA GORETI', 'SIANTAR\r', 'N'),
('014011', 'A.M.I.K. JENDERAL SUDIRMAN  *', 'MEDAN\r', 'N'),
('014012', 'AKADEMI PERTANIAN ISKANDAR MUDA   **', 'BANDA ACEH\r', 'N'),
('014013', 'AKADEMI TEKNIK INDONESIA CUT MEUTIA', 'BANDA ACEH\r', 'N'),
('014014', 'AKADEMI MANAJEMEN & ILMU KOMPUTER KESATRIA', 'MEDAN\r', 'N'),
('014015', 'AKADEMI MANAJEMEN GUNUNG LEUSER', 'KUTACANE\r', 'N'),
('014016', 'AKADEMI SEKRETARI MANAJEMEN TANAH RENCONG', 'LHOKSEUMAWE\r', 'N'),
('014017', 'AKADEMI SEKRETARI MANAJEMEN NUSANTARA', 'BANDA ACEH\r', 'N'),
('014018', 'AKADEMI M.I.P.A. ALWASHLIYAH *', 'MEDAN\r', 'N'),
('014019', 'AKADEMI TEKNIK INDUSTRI PERMINYAKAN **', 'PANGKALAN BRANDAN\r', 'N'),
('014020', 'AKADEMI MARITIM BELAWAN', 'BELAWAN\r', 'N'),
('014021', 'AKADEMI TEKNOLOGI LORENA', 'MEDAN\r', 'N'),
('014022', 'AKADEMI MANAJEMEN GUNUNG SITOLI  *', 'GUNUNG SITOLI\r', 'N'),
('014023', 'AKADEMI MANAJEMEN & ILMU KOMPUTER INDONESIA', 'BANDA ACEH\r', 'N'),
('014024', 'AKADEMI MANAJEMEN & ILMU KOMPUTER AL-MUSLIM', 'BANDA ACEH\r', 'N'),
('014025', 'AKADEMI PARIWISATA MUHAMMADIYAH', 'BANDA ACEH\r', 'N'),
('014026', 'AKADEMI AKUNTANSI MEDAN', 'MEDAN\r', 'N'),
('014027', 'AKADEMI SEKRETARI MANAJEMEN CENDANA', 'MEDAN\r', 'N'),
('014028', 'AKADEMI SEKRETARI MANAJEMEN LANCANG KUNING', 'MEDAN\r', 'N'),
('014029', '\"AKADEMI PARIWISATA \"\"TAMPAN\"\"\"', 'MEDAN\r', 'N'),
('014030', 'AKADEMI TEKNIK INDUSTRI IMANUEL', 'MEDAN\r', 'N'),
('014031', 'AKADEMI KEUANGAN PERBANKAN NUSANTARA', 'BANDA ACEH\r', 'N'),
('014032', 'AKADEMI MANAJEMEN & ILMU KOMPUTER JABAL GHAFUR', 'SIGLI\r', 'N'),
('014033', '\"POLITEKNIK \"\"TAMPAN\"\" (GANTI KODE PT)\"', 'MEDAN\r', 'N'),
('014034', 'AKADEMI KEUANGAN PERBANKAN GETSEMPENA', 'BANDA ACEH\r', 'N'),
('014035', 'AKADEMI KEUANGAN PERBANKAN NASIONAL LHOKSEUMAWE', 'LHOKSEUMAWE\r', 'N'),
('014036', 'AKADEMI MANAJEMEN & ILMU KOMPUTER M.B. POLYTEKNIK', 'MEDAN\r', 'N'),
('014037', 'AKADEMI MANAJEMEN & ILMU KOMPUTER I T M I', 'MEDAN\r', 'N'),
('014038', 'AKADEMI MARITIM NUSANTARA MALAHAYATI', 'BANDA ACEH\r', 'N'),
('014039', 'AKADEMI PARIWISATA NUSANTARA', 'MEDAN\r', 'N'),
('014040', 'A.M.I.K. PARBINA NUSANTARA', 'PEMATANG SIANTAR\r', 'N'),
('014041', 'AKADEMI FARMASI INDAH DELI SERDANG', 'MEDAN\r', 'N'),
('014042', 'AKADEMI KEBIDANAN NUSANTARA 2000', 'MEDAN\r', 'N'),
('014043', 'AKADEMI MANAJEMEN & ILMU KOMPUTER UNIVERSAL', 'MEDAN\r', 'N'),
('014044', 'AKADEMI MANAJEMEN & ILMU KOMPUTER WIDYA LOKA', 'MEDAN\r', 'N'),
('014045', 'AKADEMI MANAJEMEN & ILMU KOMPUTER LABUHAN BATU', 'RANTAU PRAPAT\r', 'N'),
('014046', 'AKADEMI MANAJEMEN & ILMU KOMPUTER LOGIKA', 'MEDAN\r', 'N'),
('014047', 'AKADEMI BIDAN SARI MUTIARA', 'MEDAN\r', 'N'),
('014048', 'AKADEMI BIDAN R.S. SANTA ELISABETH', 'MEDAN\r', 'N'),
('014049', 'AKADEMI PERAWAT PRIMA MEDAN', 'MEDAN\r', 'N'),
('014050', 'AKADEMI BIDAN PRIMA MEDAN', 'MEDAN\r', 'N'),
('014051', 'AKADEMI FARMASI INDAH DELI SERDANG', 'MEDAN\r', 'N'),
('014052', 'AKADEMI TEKNIK DELI SERDANG', 'LUBUK PAKAM\r', 'N'),
('014053', 'AKADEMI BIDAN HELFETIA', 'MEDAN\r', 'N'),
('014054', 'AKADEMI KHOLISATUR RAHMI', 'BINJAI\r', 'N'),
('014055', 'AKADEMI BIDAN FLORA', 'MEDAN\r', 'N'),
('014056', 'AKADEMI BIDAN IMELDA', 'MEDAN\r', 'N'),
('014057', 'AKADEMI BIDAN BAKTI INANG PERSADA', 'MEDAN\r', 'N'),
('014058', 'A.M.I.K. POLIBISNIS MEDAN', 'MEDAN\r', 'N'),
('014059', 'AKADEMI BIDAN BINA HUSADA', 'TEBING TINGGI\r', 'N'),
('014060', 'AKADEMI BIDAN HENDERSEN', 'PEMATANG SIANTAR\r', 'N'),
('014061', 'A.M.I.K. STIEKOM SUMATERA UTARA', 'MEDAN\r', 'N'),
('014062', 'AKADEMI BIDAN DARMO', 'MEDAN\r', 'N'),
('014063', 'AKADEMI BAHASA ASING MEDAN BUSINESS POLYTECHNIC', 'MEDAN\r', 'N'),
('015001', '\"POLITEKNIK \"\"TAMPAN\"\"\"', 'MEDAN\r', 'N'),
('015002', 'POLITEKNIK INFORMATIKA DEL TOBA', 'SAMOSIR\r', 'N'),
('015003', 'POLITEKNIK UNGGUL LP3I MEDAN', 'MEDAN\r', 'N'),
('015004', 'POLITEKNIK GANESHA NUSANTARA', 'MEDAN\r', 'N'),
('015005', 'POLITEKNIK MANDIRI BINA PRESTASI', 'MEDAN\r', 'N'),
('021001', 'UNIVERSITAS MUHAMMADIYAH PALEMBANG', '\r', 'N'),
('021002', 'UNIVERSITAS MUHAMMADIYAH LAMPUNG', '\r', 'N'),
('021003', 'UNIVERSITAS SABURAI BANDAR LAMPUNG', '\r', 'N'),
('021004', 'UNIVERSITAS MUHAMMADIYAH METRO', '\r', 'N'),
('021005', 'UNIVERSITAS PROF.DR. HAZAIRIN, SH', '\r', 'N'),
('021006', 'UNIVERSITAS PALEMBANG', '\r', 'N'),
('021007', 'UNIVERSITAS IBA PALEMBANG', '\r', 'N'),
('021008', 'UNIVERSITAS TRIDINANTI PALEMBANG', '\r', 'N'),
('021009', 'UNIVERSITAS MALAHAYATI BANDAR LAMPUNG', '\r', 'N'),
('021010', 'UNIVERSITAS MUHAMMADIYAH BENGKULU', '\r', 'N'),
('021011', 'UNIVERSITAS TAMANSISWA PALEMBANG', '\r', 'N'),
('021012', 'UNIVERSITAS BANDAR LAMPUNG', '\r', 'N'),
('021013', 'UNIVERSITAS SJAKHYAKIRTI PALEMBANG', '\r', 'N'),
('021014', 'UNIVERSITAS TULANG BAWANG B. LAMPUNG', '\r', 'N'),
('021015', 'UNIVERSITAS BATURAJA', '\r', 'N'),
('021016', 'UNIVERSITAS PGRI PALEMBANG', '\r', 'N'),
('021017', 'UNIVERSITAS KADER BANGSA PALEMBANG', '\r', 'N'),
('021018', 'UNIVERSITAS RATU SAMBAN ARGA MAKMUR BENGKULU', '\r', 'N'),
('021019', 'UNIVERSITAS BINA DARMA PALEMBANG', '\r', 'N'),
('023001', 'STIE MUHAMMADIYAH PRINGSEWU', '\r', 'N'),
('023002', 'STKIP PGRI LUBUK LINGGAU', '\r', 'N'),
('023003', 'STIPER SURYA DHARMA BANDAR LAMPUNG', '\r', 'N'),
('023004', 'STIA BENGKULU', '\r', 'N'),
('023005', 'STISIPOL CHANDRADIMUKA PALEMBANG', '\r', 'N'),
('023006', 'STIPER DHARMA WACANA METRO', '\r', 'N'),
('023007', 'STKIP PGRI KOTABUMI     *', '\r', 'N'),
('023008', 'STKIP PGRI METRO', '\r', 'N'),
('023009', 'STIE PERTIBA PANGKAL PINANG', '\r', 'N'),
('023010', 'STKIP MUHAMMADIYAH KOTABUMI', '\r', 'N'),
('023011', 'STKIP MUHAMMADIYAH PRINGSEWU', '\r', 'N'),
('023012', 'STIH PERTIBA PANGKAL PINANG', '\r', 'N'),
('023013', 'STKIP PGRI BANDAR LAMPUNG', '\r', 'N'),
('023014', 'STT MUSI PALEMBANG', '\r', 'N'),
('023015', 'STIE SULTAN MAHMUD BADARUDDIN PALEMBANG', '\r', 'N'),
('023016', 'STIE RAGAM TUNAS KOTABUMI', '\r', 'N'),
('023017', 'STIPER SRIWIGAMA PALEMBANG', '\r', 'N'),
('023018', 'STPER BUMI SILAMPARI LUBUK LINGGAU', '\r', 'N'),
('023019', 'STIE APRIN PALEMBANG', '\r', 'N'),
('023020', 'STIM AMKOP PALEMBANG', '\r', 'N'),
('023021', 'STISIPOL DHARMA WACANA METRO', '\r', 'N'),
('023022', 'STIBUN LAMPUNG', '\r', 'N'),
('023023', 'STIE SERASAN MUARA ENIM', '\r', 'N'),
('023024', 'STIH SUMPAH PEMUDA PALEMBANG', '\r', 'N'),
('023025', 'STIE SERELO LAHAT', '\r', 'N'),
('023026', 'STT NUSANTARA BANDAR LAMPUNG', '\r', 'N'),
('023027', 'STIE TRISNA NEGARA BELITANG', '\r', 'N'),
('023028', 'STIE BINA NUSANTARA LAMPUNG', '\r', 'N'),
('023029', 'STMIK DARMAJAYA BANDAR LAMPUNG', '\r', 'N'),
('023030', 'STKIP DHARMA WACANA METRO', '\r', 'N'),
('023031', 'STBA YUNISLA BANDAR LAMPUNG', '\r', 'N'),
('023032', 'STTP PALEMBANG', '\r', 'N'),
('023033', 'STIE MUHAMMADIYAH KALIANDA', '\r', 'N'),
('023034', 'STIE DEHASEN BENGKULU', '\r', 'N'),
('023035', 'STMIK DEHASEN BENGKULU', '\r', 'N'),
('023036', 'STMIK TUNAS BANGSA BANDAR LAMPUNG', '\r', 'N'),
('023037', 'STIH SERASAN MUARA ENIM', '\r', 'N'),
('023038', 'STIT SERASAN MUARA ENIM', '\r', 'N'),
('023039', 'STIE LAMPUNG', '\r', 'N'),
('023040', 'STMIK IGM PALEMBANG', '\r', 'N'),
('023041', 'STIPSI WIDYA DHARMA PALEMBANG', '\r', 'N'),
('023042', 'STIE MULIA DARMA PALEMBANG', '\r', 'N'),
('023043', 'STIE DWI SAKTI BATURAJA', '\r', 'N'),
('023044', 'STIE RAHMANIYAH SEKAYU', '\r', 'N'),
('023045', 'STIE MUSI RAWAS LUBUK LINGGAU', '\r', 'N'),
('023046', 'STIE SATU NUSA BANDAR LAMPUNG', '\r', 'N'),
('023047', 'STBA METHODIST PALEMBANG', '\r', 'N'),
('023048', 'STIE MUSI PALEMBANG', '\r', 'N'),
('023049', 'STBA TEKNOKRAT BANDAR LAMPUNG', '\r', 'N'),
('023050', 'STIH MUHAMMADIYAH KOTABUMI', '\r', 'N'),
('023051', 'STIKES WIDYA DHARMA PALEMBANG', '\r', 'N'),
('023052', 'STIA SATYA NEGARA PALEMBANG', '\r', 'N'),
('023053', 'STBA WIDYA DHARMA PALEMBANG', '\r', 'N'),
('023054', 'STIKES KADER BANGSA PALEMBANG', '\r', 'N'),
('023055', 'STIH RAHMANIYAH SEKAYU', '\r', 'N'),
('023056', 'STIE DARMAJAYA BANDAR LAMPUNG', '\r', 'N'),
('023057', 'STIKES BINA HUSADA PALEMBANG', '\r', 'N'),
('023058', 'STIE MITRA ADIGUNA PALEMBANG', '\r', 'N'),
('023059', 'STIE PRABUMULIH', '\r', 'N'),
('023060', 'STIE IBEK PANGKAL PINANG', '\r', 'N'),
('023061', 'STKIP MUHAMMADIYAH PAGARALAM', '\r', 'N'),
('023062', 'STIE LEMBAH DEMPO PAGARALAM', '\r', 'N'),
('023063', 'STMIK SURYA INTAN KOTABUMI', '\r', 'N'),
('023064', 'STMIK DHARMA WACANA METRO', '\r', 'N'),
('023065', 'STIKES NUSANTARA PALEMBANG', '\r', 'N'),
('023066', 'STMIK PRABUMULIH', '\r', 'N'),
('023067', 'STIT PRABUMULIH', '\r', 'N'),
('023068', 'STIE ABDI NUSA PALEMBANG', '\r', 'N'),
('023069', 'STIKES ABDI NUSA PALEMBANG', '\r', 'N'),
('023070', 'STIPER BANGKA SUNGAILIAT', '\r', 'N'),
('023071', 'STIH MUHAMMADIYAH KALIANDA', '\r', 'N'),
('023072', 'STIE BINA WARGA PALEMBANG', '\r', 'N'),
('023073', 'STIE MITRA BANDAR LAMPUNG', '\r', 'N'),
('023074', 'STIPER BELITANG', '\r', 'N'),
('023075', 'STMIK MITRA LAMPUNG', '\r', 'N'),
('023076', 'STIKES MITRA LAMPUNG', '\r', 'N'),
('023077', 'STIKES TRI MANDIRI SAKTI BENGKULU', '\r', 'N'),
('023078', 'STIPSI ABDI NUSA PALEMBANG', '\r', 'N'),
('023079', 'STIKES ABDI NUSA PANGKAL PINANG', '\r', 'N'),
('023080', 'STIKES AL MA\'ARIF BATURAJA', '\r', 'N'),
('023081', 'STIM KARTIKA JAYA BANDAR LAMPUNG', '\r', 'N'),
('023082', 'STMIK SIGMA PALEMBANG', '\r', 'N'),
('023083', 'STIFI BHAKTI PERTIWI PALEMBANG', '\r', 'N'),
('023084', '\"STMIK GLOBAL INFORMATIKA \"\"MDP\"\" PALEMBANG\"', '\r', 'N'),
('023085', 'STKIP SERA LAHAT', '\r', 'N'),
('023086', 'STMIK TEKNOKRAT BANDAR LAMPUNG', '\r', 'N'),
('024001', 'AKADEMI PERTANIAN DHARMA WACANA METRO   *', '\r', 'N'),
('024002', 'AKADEMI PERTANIAN RAGAM TUNAS KOTABUMI  *', '\r', 'N'),
('024003', 'AKADEMI AKUNTANSI SJAKHYAKIRTI PALEMBANG', '\r', 'N'),
('024004', 'AMK PRIMA BANDAR LAMPUNG     *', '\r', 'N'),
('024005', 'ASM BANDAR LAMPUNG', '\r', 'N'),
('024006', 'ASM SRIWIJAYA PALEMBANG', '\r', 'N'),
('024007', 'AMIK SIGMA PALEMBANG', '\r', 'N'),
('024008', 'AKADEMI PARIWISATA SATU NUSA LAMPUNG', '\r', 'N'),
('024009', 'AMIK STARTECK PRINGSEWU', '\r', 'N'),
('024010', 'AKADEMI AKUNTANSI LAMPUNG', '\r', 'N'),
('024011', 'AAP DHARMA WACANA METRO    *', '\r', 'N'),
('024012', 'AMIK MITRA BANDAR LAMPUNG', '\r', 'N'),
('024013', 'AMIK ADIGUNA BANDAR LAMPUNG', '\r', 'N'),
('024014', 'AKADEMI AKUNTANSI UNGGULAN SMB PALEMBANG', '\r', 'N'),
('024015', 'AKADEMI PERIKANAN BIMA SAKTI WAY JEPARA', '\r', 'N'),
('024016', 'AMKOP MANNA BENGKULU      *', '\r', 'N'),
('024017', 'ABA DCC LAMPUNG', '\r', 'N'),
('024018', 'AKADEMI AKUNTANSI BHAKTI', '\r', 'N'),
('024019', 'AKPER SITI CHODIJAH PALEMBANG', '\r', 'N'),
('024020', 'AKPER MITRA ADIGUNA PALEMBANG', '\r', 'N'),
('024021', 'AKPER FITRAH ALDAR LUBUK LINGGAU', '\r', 'N'),
('024022', 'AKPER PERDHAKI CHARITAS PALEMBANG', '\r', 'N'),
('024023', 'AKADEMI FISIOTERAPI MUHAMMADIYAH PALEMBANG', '\r', 'N'),
('024024', 'AKADEMI KESEHATAN LINGKUNGAN MUH. PLG', '\r', 'N'),
('024025', 'AKPER AL-MA\'ARIF BATURAJA', '\r', 'N'),
('024026', 'AKPER MUHAMMADIYAH PALEMBANG', '\r', 'N'),
('024027', 'AKADEMI PARIWISATA WIDYA DHARMA PALEMBANG', '\r', 'N'),
('024028', 'ATP DEHASEN BENGKULU', '\r', 'N'),
('024029', 'AKADEMI AKUNTANSI DAN MANAJEMEN MITRA LPG', '\r', 'N'),
('024030', 'AMIK MASTER LAMPUNG', '\r', 'N'),
('024031', 'AMIK NUSANTARA BENGKULU', '\r', 'N'),
('024032', 'AKADEMI ANALISIS KESEHATAN WIDYA DHARMA', '\r', 'N'),
('024033', 'AMIK DCC BANDAR LAMPUNG', '\r', 'N'),
('024034', 'APIKES WIDYA DHARMA PALEMBANG', '\r', 'N'),
('024035', 'AKUBANK MULIA DARMA PRATAMA PALEMBANG', '\r', 'N'),
('024036', 'AMIK LAMPUNG', '\r', 'N'),
('024037', 'A2M PEMBANGUNAN PALEMBANG', '\r', 'N'),
('024038', 'AKADEMI SAINS DAN TEKNOLOGI PEMBANGUNAN PLG', '\r', 'N'),
('024039', 'AMIK TEKNOKRAT LAMPUNG', '\r', 'N'),
('024040', 'ATRO WIDYA DHARMA  PALEMBANG', '\r', 'N'),
('024041', 'ATRO KADER BANGSA PALEMBANG', '\r', 'N'),
('024042', 'AKADEMI MANAJEMEN BELITUNG', '\r', 'N'),
('024043', 'AMIK LEMBAH DEMPO', '\r', 'N'),
('024044', 'AMIK MDP PALEMBANG', '\r', 'N'),
('024045', 'ATEM KADER BANGSA PALEMBANG', '\r', 'N'),
('024046', 'AKADEMI AKUNTANSI INDO SAIN PALEMBANG', '\r', 'N'),
('024047', 'AMIK BINA WARGA PALEMBANG', '\r', 'N'),
('024048', 'AKPER AISYIYAH PALEMBANG', '\r', 'N'),
('024049', 'ASM INDO SAIN PALEMBANG', '\r', 'N'),
('024050', 'ABA BINA INSANI INDONESIA PALEMBANG', '\r', 'N'),
('024051', 'AKPER PEMBINA PALEMBANG', '\r', 'N'),
('024052', 'AKADEMI KESEHATAN LINGKUNGAN PEMBINA PLG', '\r', 'N'),
('024053', 'AKADEMI PERIKANAN WAHYUNI MANDIRA OKI', '\r', 'N'),
('024054', 'AMIK ATMA LUHUR PANGKAL PINANG', '\r', 'N'),
('024056', 'AKADEMI FARMASI KADER BANGSA', '\r', 'N'),
('024057', 'AKADEMI ANALISIS KESEHATAN KADER BANGSA PLG', '\r', 'N'),
('024058', 'AKADEMI REFRAKSI OPTISI KADER BANGSA PLG', '\r', 'N'),
('024059', 'AKPER KADER BANGSA PALEMBANG', '\r', 'N'),
('024060', 'AKBID MITRA ADIGUNA PALEMBANG', '\r', 'N'),
('024061', 'AKPER PPNI PALEMBANG', '\r', 'N'),
('024062', 'AKPER PEMDA TK.II BENGKULU', '\r', 'N'),
('024063', 'AMIK BATURAJA', '\r', 'N'),
('024064', 'AKBID KADER BANGSA PALEMBANG', '\r', 'N'),
('024065', 'AKADEMI FARMASI PUTRA INDONESIA LAMPUNG', '\r', 'N'),
('024066', 'AKADEMI KEBIDANAN BENGKULU', '\r', 'N'),
('024067', 'APIKES HARAPAN PALEMBANG', '\r', 'N'),
('024068', 'AKPER BAITUL HIKMAH BANDAR LAMPUNG', '\r', 'N'),
('024069', 'AKPER BUNDA DELIMA LAMPUNG', '\r', 'N'),
('024071', 'AKPER DHARMA WACANA METRO', '\r', 'N'),
('024073', 'AKPER KARYA HUSADA BENGKULU', '\r', 'N'),
('024074', 'AKPER KESDAM II SRIWIJAYA PALEMBANG', '\r', 'N'),
('024075', 'AKPER MALAHAYATI LAMPUNG', '\r', 'N'),
('024076', 'AKPER MUHAMMADIYAH PRINGSEWU', '\r', 'N'),
('024077', 'AKPER PANCA BHAKTI LAMPUNG', '\r', 'N'),
('024078', 'AKPER PANGKAL PINANG', '\r', 'N'),
('024079', 'AKPER PEMDA ARGA MAKMUR BENGKULU', '\r', 'N'),
('024080', 'AKPER RSUD BENGKULU', '\r', 'N'),
('024081', 'AKPER SAPTA BHAKTI BENGKULU', '\r', 'N'),
('024082', 'AKADEMI FARMASI HARAPAN PALEMBANG', '\r', 'N'),
('024083', 'AKADEMI PERPAJAKAN TRIDARMA B.LAMPUNG', '\r', 'N'),
('025001', 'POLITEKNIK MANUFAKTUR TIMAH', '\r', 'N'),
('025002', 'POLITEKNIK ANIKA PALEMBANG', '\r', 'N'),
('025003', 'POLITEKNIK NUSANTARA PALEMBANG', '\r', 'N'),
('025004', 'POLITEKNIK DARUSALAM PALEMBANG', '\r', 'N'),
('025005', 'POLITEKNIK MAHAMERU PALEMBANG', '\r', 'N'),
('025006', 'POLITEKNIK YPPB BELITANG', '\r', 'N'),
('025007', 'POLITEKNIK RAFLESIA CURUP', '\r', 'N'),
('031001', 'UNIVERSITAS IBNU CHALDUN', 'JAKARTA\r', 'N'),
('031003', 'UNIVERSITAS ISLAM JAKARTA', 'JAKARTA\r', 'N'),
('031004', 'UNIVERSITAS ISLAM SYEKH YUSUF', 'JAKARTA\r', 'N'),
('031005', 'UNIVERSITAS JAKARTA', 'JAKARTA\r', 'N'),
('031006', 'UNIVERSITAS JAYABAYA', 'JAKARTA\r', 'N'),
('031007', 'UNIVERSITAS KATOLIK INDONESIA ATMA JAYA', 'JAKARTA\r', 'N'),
('031008', 'UNIVERSITAS KRISNADWIPAYANA', 'JAKARTA\r', 'N'),
('031009', 'UNIVERSITAS KRISTEN INDONESIA', 'JAKARTA\r', 'N'),
('031010', 'UNIVERSITAS KRISTEN KRIDA WACANA', '\r', 'N'),
('031011', 'UNIVERSITAS MUHAMMADIYAH', '\r', 'N'),
('031012', 'UNIVERSITAS NASIONAL', '\r', 'N'),
('031013', 'UNIVERSITAS PANCASILA', 'JAKARTA\r', 'N'),
('031014', 'UNIVERSITAS PROF. DR. MOESTOPO (BERAGAMA)', '\r', 'N'),
('031015', 'UNIVERSITAS TARUMANAGARA', 'JAKARTA\r', 'N'),
('031016', 'UNIVERSITAS TRISAKTI', 'JAKARTA\r', 'N'),
('031017', 'UNIVERSITAS 17 AGUSTUS 1945', 'JAKARTA\r', 'N'),
('031018', 'UNIVERSITAS BOROBUDUR', 'JAKARTA\r', 'N'),
('031019', 'UNIVERSITAS MERCU BUANA', 'JAKARTA\r', 'N'),
('031020', 'UNIVERSITAS PERSADA INDONESIA YAI', 'JAKARTA\r', 'N'),
('031021', 'UNIVERSITAS ISLAM AS-SYAFIIYAH', '\r', 'N'),
('031022', 'UNIVERSITAS WIRASWASTA INDONESIA', 'JAKARTA\r', 'N'),
('031023', 'UNIVERSITAS DARMA PERSADA', '\r', 'N'),
('031024', 'UNIVERSITAS MPU TANTULAR', 'JAKARTA\r', 'N'),
('031025', 'UNIVERSITAS SATYA NEGARA INDONESIA', 'JAKARTA\r', 'N'),
('031026', 'UNIVERSITAS YARSI', 'JAKARTA\r', 'N'),
('031027', 'UNIVERSITAS RESPATI INDONESIA', '\r', 'N'),
('031028', 'UNIVERSITAS KERTANEGARA', 'JAKARTA\r', 'N'),
('031029', 'UNIVERSITAS SURAPATI', 'JAKARTA\r', 'N'),
('031030', 'UNIVERSITAS SAHID', '\r', 'N'),
('031031', 'UNIVERSITAS SATYAGAMA', 'JAKARTA\r', 'N'),
('031032', 'UNIVERSITAS ISLAM ATTAHIRIYAH', '\r', 'N'),
('031033', 'UNIVERSITAS INDONUSA ESA UNGGUL', '\r', 'N'),
('031034', 'UNIVERSITAS PELITA HARAPAN', '\r', 'N'),
('031035', 'UNIVERSITAS PEMBANGUNAN NASIONAL VETERAN JAKARTA', 'JAKARTA\r', 'N'),
('031036', 'UNIVERSITAS BHAYANGKARA JAYA', 'JAKARTA\r', 'N'),
('031037', 'UNIVERSITAS GUNADARMA', '\r', 'N'),
('031038', 'UNIVERSITAS BINA NUSANTARA', 'JAKARTA\r', 'N'),
('031039', 'UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA', '\r', 'N'),
('031040', 'UNIVERSITAS AZZAHRA', '\r', 'N'),
('031041', 'UNIVERSITAS PARAMADINA MULYA', '\r', 'N'),
('031042', 'UNIVERSITAS BUNG KARNO', 'JAKARTA\r', 'N'),
('031043', 'UNIVERSITAS SURYADARMA', 'JAKARTA\r', 'N'),
('031044', 'UNIVERSITAS AL-AZHAR INDONESIA', 'JAKARTA\r', 'N'),
('031045', 'UNIVERSITAS BUDI LUHUR', 'JAKARTA\r', 'N'),
('032002', 'INSTITUT KESENIAN JAKARTA - LPKJ', '\r', 'N'),
('032004', 'INSTITUT SAINS DAN TEKNOLOGI NASIONAL', 'JAKARTA\r', 'N'),
('032005', 'INSTITUT ILMU SOSIAL & ILMU POLITIK JAKARTA', 'JAKARTA\r', 'N'),
('032006', 'INSTITUT TEKNOLOGI INDONESIA', '\r', 'N'),
('032007', 'INSTITUT TEKNOLOGI BUDI UTOMO', 'JAKARTA\r', 'N'),
('032008', 'INSTITUT SAINS DAN TEKNOLOGI AL KAMAL', '\r', 'N'),
('033001', '\"SEKOLAH TINGGI FILSAFAT \"\"DRIYARKARA\"\"\"', 'JAKARTA\r', 'N'),
('033003', '\"ST. KIP \"\"WIJAYA BAKTI\"\"\"', 'JAKARTA\r', 'N'),
('033004', 'ST.I.SOSIAL & I.POLITIK WIDURI', '\r', 'N'),
('033006', 'ST. TEKNIK JAKARTA', 'JAKARTA\r', 'N'),
('033009', 'ST. FILSAFAT (THEOLOGI)', 'JAKARTA\r', 'N'),
('033011', '\"ST. KIP \"\"PURNAMA\"\"\"', '\r', 'N'),
('033012', 'ST. ILMU EKONOMI INDONESIA', 'JAKARTA\r', 'N'),
('033013', '\"ST. KIP \"\"PGRI\"\" JAKARTA\"', '\r', 'N'),
('033014', '\"ST. KIP \"\"KUSUMANEGARA\"\"\"', 'JAKARTA\r', 'N'),
('033015', '\"ST. ILMU EKONOMI \"\"SWADAYA\"\"\"', 'JAKARTA\r', 'N'),
('033016', '\"ST. ILMU EKONOMI \"\"PERBANAS\"\"\"', 'JAKARTA\r', 'N'),
('033019', '\"ST. ILMU ADM. \"\"YAPANN\"\"\"', 'JAKARTA\r', 'N'),
('033020', 'ST. KETATALAKS.PELAYARAN NIAGA', '\r', 'N'),
('033021', 'ST. MANAJ.INFO.&KOMP.GUNADARMA', '\r', 'N'),
('033022', 'ST. MANAJ.INF. & KOMP. YILKOM', 'JAKARTA\r', 'N'),
('033024', '\"ST. ILMU EKONOMI \"\"YAI\"\"\"', 'JAKARTA\r', 'N'),
('033025', 'ST. ILMU EKONOMI KAMPUS UNGU', 'JAKARTA\r', 'N'),
('033028', 'ST. ILMU ADMINISTRASI YPIAMI', 'JAKARTA\r', 'N'),
('033029', 'ST.MANAJEMEN TRANSPOR TRISAKTI', 'JAKARTA\r', 'N'),
('033031', 'ST.MANAJ.INF.&TEKNIK KOMP.INDO', 'JAKARTA\r', 'N'),
('033032', 'ST. ILMU EKONOMI KUSUMANAGARA', 'JAKARTA\r', 'N'),
('033033', 'STMIK BINA NUSANTARA', '\r', 'N'),
('033034', '\"ST. MANAJ.INF.& KOMP. \"\"KUWERA\"\"\"', 'JAKARTA\r', 'N'),
('033035', '\"STMIK \"\"BUDI LUHUR\"\"\"', 'JAKARTA\r', 'N'),
('033036', 'STIE KEU.& PERBANKAN INDONESIA', '\r', 'N'),
('033037', 'STIA MANDALA INDONESIA', 'JAKARTA\r', 'N'),
('033038', '\"STIE \"\"BHAKTI PEMBANGUNAN\"\"\"', '\r', 'N'),
('033039', 'STIE-IBEK', 'JAKARTA\r', 'N'),
('033040', 'STIE TRI DHARMA WIDYA', 'JAKARTA\r', 'N'),
('033041', 'ST.KEU.NIAGA & NGR.PEMBANGUNAN', 'JAKARTA\r', 'N'),
('033042', 'ST. TEKNOLOGI DIRGANTARA', '\r', 'N'),
('033043', 'ST. ILMU EKONOMI JAGAKARSA', 'JAKARTA\r', 'N'),
('033044', 'STIE. NASIONAL INDONESIA', 'JAKARTA\r', 'N'),
('033045', 'STIE. JAKARTA RAYA', '\r', 'N'),
('033046', 'ST. TEKNOLOGI INDONESIA', 'JAKARTA\r', 'N'),
('033047', 'ST. MANAJ. INDUSTRI INDONESIA', 'JAKARTA\r', 'N'),
('033048', 'ST. ILMU EKONOMI SARI BAKTI', '\r', 'N'),
('033049', 'ST. TEKNO. KELAUTAN HATAWANA', 'JAKARTA\r', 'N'),
('033050', 'STIA. KAWULA INDONESIA', 'JAKARTA\r', 'N'),
('033051', 'ST. ILMU EKONOMI NUSANTARA', 'JAKARTA\r', 'N'),
('033052', 'STISIP PUSAKA NUSANTARA JKT.', '\r', 'N'),
('033053', 'ST. ILMU EKONOMI GUNADARMA', '\r', 'N'),
('033054', 'ST. ILMU MANAJEMEN KOSGORO', 'JAKARTA\r', 'N'),
('033055', 'STIM JAKARTA', 'JAKARTA\r', 'N'),
('033056', 'ST. ILMU HUKUM JAGAKARSA', 'JAKARTA\r', 'N'),
('033057', 'ST. ILMU EKONOMI DWI PUTRA', 'JAKARTA\r', 'N'),
('033058', 'ST. ILMU EKONOMI CORPATARIN', 'JAKARTA\r', 'N'),
('033059', 'ST. ILMU EKONOMI JAYAKARTA', 'JAKARTA\r', 'N'),
('033060', 'ST. ILMU EKO. BISNIS INDONESIA', 'JAKARTA\r', 'N'),
('033061', 'SEKOLAH TINGGI HUKUM INDONESIA', '\r', 'N'),
('033062', '\"STIE \"\"GOTONG ROYONG\"\"\"', '\r', 'N'),
('033063', 'ST.MANJ.INF. & KOMP. SWADHARMA', '\r', 'N'),
('033064', 'STM LABORA', '\r', 'N'),
('033065', 'STIE IBII', 'JAKARTA\r', 'N'),
('033066', 'ST. ILMU EKONOMI TRISAKTI', 'JAKARTA\r', 'N'),
('033067', 'STM PRASETIYA MULYA', '\r', 'N'),
('033068', 'SEKOLAH TINGGI MANAJEMEN IPMI', '\r', 'N'),
('033069', 'ST. MANAJEMEN PPM', '\r', 'N'),
('033070', 'ST. PENERBANGAN AVIASI', 'JAKARTA\r', 'N'),
('033071', 'ST. MAN. INF. & KOMP. PERBANAS', '\r', 'N'),
('033072', '\"ST. ILMU EKONOMI \"\"IGI\"\"\"', '\r', 'N'),
('033073', '\"STIH \"\"LPIHM - IBLM\"\"\"', '\r', 'N'),
('033074', 'STIE IPWI', '\r', 'N'),
('033075', 'SEKOLAH TINGGI MANAJEMEN IMNI', '\r', 'N'),
('033076', 'STIE GANESHA', '\r', 'N'),
('033077', '\"ST. MANAJEMEN \"\"IMMI\"\"\"', '\r', 'N'),
('033078', '\"ST. ILMU EKONOMI \"\"KALBE\"\"\"', '\r', 'N'),
('033079', '\"ST. ILMU EKONOMI \"\"IBMI\"\"\"', '\r', 'N'),
('033080', '\"STMIK \"\"JAYAKARTA\"\"\"', '\r', 'N'),
('033082', '\"ST. ILMU EKONOMI \"\"SETIA BUDI\"\"\"', 'JAKARTA\r', 'N'),
('033083', 'STTI. BENARIF INDONESIA', '\r', 'N'),
('033084', 'STIE WIDYA JAYAKARTA', '\r', 'N'),
('033085', 'ST. ILMU MANAJEMEN LPMI', '\r', 'N'),
('033086', 'STIE TAMAN SISWA', 'JAKARTA\r', 'N'),
('033087', 'STIE TRIANANDRA', 'JAKARTA\r', 'N'),
('033088', 'STMIK MUHAMMADIYAH', 'JAKARTA\r', 'N'),
('033089', 'STMIK DARMA BAKTI', '\r', 'N'),
('033090', 'STT. JAGAKARSA', 'JAKARTA\r', 'N'),
('033091', '\"STMIK \"\"BUNDA MULIA\"\"\"', 'JAKARTA\r', 'N'),
('033092', '\"ST. KIP  \"\"ALBANA\"\"\"', '\r', 'N'),
('033093', 'STIE MOH. HUSNI THAMRIN', 'JAKARTA\r', 'N'),
('033094', 'ST. PERPAJAKAN INDONESIA', '\r', 'N'),
('033095', 'ST. ILMU EKONOMI DWIPA WACANA', 'JAKARTA\r', 'N'),
('033096', 'STIE MUHAMMADIYAH JAKARTA', '\r', 'N'),
('033097', 'STIE BUDI LUHUR', '\r', 'N'),
('033098', 'STIE BUNDA MULIA', 'JAKARTA\r', 'N'),
('033099', 'STIE DR. MOECHTAR TALIB', '\r', 'N'),
('033101', 'STMIK SATYAGAMA', 'JAKARTA\r', 'N'),
('033102', 'STISIP BUDI LUHUR', '\r', 'N'),
('033103', 'STIE AHMAD DAHLAN JAKARTA', '\r', 'N'),
('033104', 'SEKOLAH TINGGI TEKNIK \'YPLN\'', 'JAKARTA\r', 'N'),
('033105', 'ST. ILMU EKONOMI TRIGUNA', 'JAKARTA\r', 'N'),
('033106', 'ST.ILMU KOMUNIKASI INTER STUDI', '\r', 'N'),
('033107', 'STIE TUNAS NUSANTARA', 'JAKARTA\r', 'N'),
('033108', 'STIE DHARMA WACANA', '\r', 'N'),
('033109', '\"STIE \"\"ISM\"\"\"', 'JAKARTA\r', 'N'),
('033110', 'STMIK BINA MULYA', 'JAKARTA\r', 'N'),
('033111', 'ST. BAHASA ASING LIA', 'JAKARTA\r', 'N'),
('033112', 'ST. ILMU KOMUNIKASI (LSPR)', 'JAKARTA\r', 'N'),
('033113', 'ST. TEKNIK BUDI LUHUR', '\r', 'N'),
('033114', 'ST. PARIWISATA TRISAKTI', 'JAKARTA\r', 'N'),
('033115', 'STIE PENGEMB. BISNIS DAN MAN.', '\r', 'N'),
('033116', '\"STBA. \"\"PERTIWI INDONESIA\"\"\"', '\r', 'N'),
('033117', 'SEKOLAH TINGGI ILMU KEPERAWATAN SINT CAROLUS', 'JAKARTA\r', 'N'),
('033118', 'STIE ABDUL LATIEF', 'JAKARTA\r', 'N'),
('033119', 'STIE JAYAKUSUMA', '\r', 'N'),
('033120', 'STIE KASIH BANGSA', 'JAKARTA\r', 'N'),
('033121', 'STIE DHARMA BUMI PUTRA', 'JAKARTA\r', 'N'),
('033122', '\"ST.ILMU ADM.& SEKRETARI \"\"ASMI\"\"\"', 'JAKARTA\r', 'N'),
('033123', 'SEKOLAH TINGGI MANAJEMEN YAKSI', '\r', 'N'),
('033124', 'STIE SAILENDRA', 'JAKARTA\r', 'N'),
('033125', 'STIE MAIJI', 'JAKARTA\r', 'N'),
('033126', 'STIE WIYATAMANDALA', 'JAKARTA\r', 'N'),
('033127', 'STMIK M. H. THAMRIN', '\r', 'N'),
('033128', 'STIE YPBI', 'JAKARTA\r', 'N'),
('033129', 'ST. ILMU KESEHATAN IND. MAJU', 'JAKARTA\r', 'N'),
('033130', 'STMIK VERITAS', 'JAKARTA\r', 'N'),
('033131', 'STIE WIRA BAKTI', 'JAKARTA\r', 'N'),
('033132', 'STIE PARIWISATA INTERNASIONAL', 'JAKARTA\r', 'N'),
('033133', 'STIA DAN SEKR.BUNDA HATI KUDUS', 'JAKARTA\r', 'N'),
('033134', 'ST. PARIWISATA SAHID', 'JAKARTA\r', 'N'),
('033135', 'ST. ILMU KESEHATAN BINAWAN', 'JAKARTA\r', 'N'),
('033136', 'ST. TEKNOLOGI 10 NOVEMBER', 'JAKARTA\r', 'N'),
('033137', 'ST. BAHASA ASING \'IEC\'', 'JAKARTA\r', 'N'),
('033138', 'ST. ILMU MARITIM AMI', 'JAKARTA\r', 'N'),
('033139', '\"ST.ILMU KESEHATAN \"\"PAMENTAS\"\"\"', '\r', 'N'),
('033140', 'STMIK WIDURI', '\r', 'N'),
('033141', 'ST.ILMU KESEHATAN MEDISTRA IND', '\r', 'N'),
('033142', 'STMIK INDONESIA ERA PRESTASI', '\r', 'N'),
('033143', 'ST. MANAJ. TRANSPO. MAHALAYATI', '\r', 'N'),
('033144', 'ST. OLAH TRAMPIL CENDEKIA JKT', '\r', 'N'),
('033145', 'ST. TEKNIK JAYA MAHADIKA', '\r', 'N'),
('033146', 'STISIP DHARMA PARAMITA', '\r', 'N'),
('033147', 'ST. ILMU KES. ISTARA NUSANTARA', '\r', 'N'),
('033148', 'STIE CAKRA BUANA', '\r', 'N'),
('033149', 'STMIK JIBES', '\r', 'N'),
('033150', 'STIKOM CIPTA KARYA INDONESIA', '\r', 'N'),
('033151', '\"ST.ILMU KOMUNIKASI \"\"YTKP\"\"\"', '\r', 'N'),
('033152', 'ST.MANAJEMEN ASURANSI TRISAKTI', 'JAKARTA\r', 'N'),
('034002', 'AKD. ADM. PEMBANGUN INDONESIA', '\r', 'N'),
('034004', '\"AKADEMI AKUNTANSI \"\"BOROBUDUR\"\"\"', 'JAKARTA\r', 'N'),
('034006', 'AKD.AKUN.ARTAWIYATA INDO - LPI', '\r', 'N'),
('034008', 'AKADEMI AKUNTANSI JAYABAYA', '\r', 'N'),
('034009', 'AKADEMI AKUNTANSI NASIONAL', '\r', 'N'),
('034011', '\"AKADEMI AKUNTANSI \"\"YAI\"\"\"', '\r', 'N'),
('034013', '\"AKD. BAHASA ASING \"\"BOROBUDUR\"\"\"', '\r', 'N'),
('034014', '\"AKADEMI BAHASA ASING  \"\"LPI\"\"\"', '\r', 'N'),
('034016', 'AKD. KEU. & PERBANKAN PRATISIA', 'JAKARTA\r', 'N'),
('034017', 'AKD. KEU.& PERBANKAN YAPENANTA', '\r', 'N'),
('034018', '\"AKD. KEU. DAN PERBANKAN \"\"YPK\"\"\"', 'JAKARTA\r', 'N'),
('034019', '\"AKD. KEU. DAN PERBANKAN \"\"LPI\"\"\"', '\r', 'N'),
('034020', 'AKD. KEU. DAN PERBANKAN INTER.', '\r', 'N'),
('034021', 'AKD.KEU.& PERBAN. MUHAMMADIYAH', '\r', 'N'),
('034024', 'AKD. TEKNOLOGI GRAFIKA INDONE.', 'JAKARTA\r', 'N'),
('034025', 'AKADEMI HUBUNGAN INTERNASIONAL', '\r', 'N'),
('034027', 'AKD.SEKRETARI DAN MANAJ. INDO.', '\r', 'N'),
('034028', '\"AKADEMI MANAJEMEN \"\"YPPI\"\"\"', 'JAKARTA\r', 'N'),
('034029', 'AKADEMI MARITIM INDONESIA', '\r', 'N'),
('034030', 'AKADEMI MARITIM NASIONAL JAYA', '\r', 'N'),
('034031', '\"AKD. PARIWISATA \"\"BUANA WISATA\"\"\"', 'JAKARTA\r', 'N'),
('034032', 'AKD. MANAJ. PERUSAHAN JAYABAYA', '\r', 'N'),
('034033', 'AKD.KEU. & PERBANKAN BOROBUDUR', '\r', 'N'),
('034034', 'AKD. ADM. KEUANGAN JAKARTA', 'JAKARTA\r', 'N'),
('034035', '\"AKADEMI MANAJEMEN \"\"YAPK\"\"\"', '\r', 'N'),
('034036', 'AKD. PERAWATAN RS DGI CIKINI', 'JAKARTA\r', 'N'),
('034037', 'AKADEMI PERAWATAN ST. CAROLUS', '\r', 'N'),
('034039', 'AKADEMI PARIWISATA TRISAKTI', '\r', 'N'),
('034040', 'AKADEMI MANAJEMEN INDONESIA', '\r', 'N'),
('034042', 'AKD. SEKRETARIS LPK TARAKANITA', 'JAKARTA\r', 'N'),
('034044', 'AKADEMI TEKNIK INDONESIA', '\r', 'N'),
('034045', '\"AKADEMI BAHASA ASING \"\"LPN\"\"\"', '\r', 'N'),
('034047', 'AKD. AKUNT. DR. MOECTAR THALIB', '\r', 'N'),
('034050', 'AKADEMI BAHASA ASING NASIONAL', '\r', 'N'),
('034051', '\"AKADEMI BAHASA ASING \"\"YAB\"\"\"', '\r', 'N'),
('034053', '\"ABA \"\"PERTIWI INDONESIA\"\"\"', '\r', 'N'),
('034054', '\"ABA \"\"KERTANEGEGARA\"\"\"', '\r', 'N'),
('034056', '\"AKD ADM. \"\"KERTANEGARA\"\"\"', '\r', 'N'),
('034058', '\"AKADEMI MARITIM  \"\"DJADAJAT\"\"\"', '\r', 'N'),
('034060', 'AKD.SEKRETARIS & MANAJ PURNAMA', 'JAKARTA\r', 'N'),
('034061', 'AKADEMI PARIWISATA NASIONAL', '\r', 'N'),
('034062', 'AKADEMI PARIWISATA INDONESIA', '\r', 'N'),
('034064', 'AKADEMI AKUNTANSI VETERAN', '\r', 'N'),
('034065', 'AKADEMI AKUNTANSI MUHAMMADIYAH', '\r', 'N'),
('034066', 'AKADEMI TEKNIK VETERAN', '\r', 'N'),
('034067', 'AKADEMI PARIWISATA SAHID', '\r', 'N'),
('034068', 'AMIK DARMA KARYA', '\r', 'N'),
('034069', '\"AKD. MANAJ. KEUANGAN \"\"YP-IPPI\"\"\"', 'JAKARTA\r', 'N'),
('034071', 'AKD. TEKNOLOGI SAPTA TARUNA', 'JAKARTA\r', 'N'),
('034072', 'AMIK BUNDA MULYA', '\r', 'N'),
('034073', '\"AMIK \"\"PRO PATRIA\"\"\"', 'JAKARTA\r', 'N'),
('034075', 'AKD. TEKNO. GRAFIKA TRISAKTI', 'JAKARTA\r', 'N'),
('034076', 'AKD. ADM. KEUANGAN INDONESIA', '\r', 'N'),
('034077', 'AKADEMI TEKNIK DESAIN INTERIOR', 'JAKARTA\r', 'N'),
('034079', 'AKD SEKR.DAN MANAJ. SAINT MARY', '\r', 'N'),
('034080', 'AKD. MANAJ.PERDAGANGAN TRIGUNA', '\r', 'N'),
('034081', 'AKD.LITIGASI INDONESIA TRIGUNA', 'JAKARTA\r', 'N'),
('034083', 'AKD. SENI RUPA DAN DESAIN ISWI', '\r', 'N'),
('034084', 'AKD. PARIWISATA TRIDAYA WISATA', '\r', 'N'),
('034085', 'AA. JENDERAL GATOT SUBROTO', '\r', 'N'),
('034086', 'AA. SYAFA\'AT INDONESIA', '\r', 'N'),
('034087', 'AKD. SEKRE. REGINA CONFESSORUM', '\r', 'N'),
('034088', 'AKAD. PERTAMANAN PRO PATRIA', 'JAKARTA\r', 'N'),
('034089', '\"AKAD. SEKRETARIS \"\"ISWI\"\"\"', '\r', 'N'),
('034090', 'AMIK ANDALAN JAKARTA', '\r', 'N'),
('034091', '\"AKD. BAHASA ASING \"\"SAINT MARY\"\"\"', '\r', 'N'),
('034092', 'AKD. MAN.INF.& KOMP. JAYAKARTA', '\r', 'N'),
('034093', 'AKD. PERBANKAN YUKI', 'JAKARTA\r', 'N'),
('034094', 'AKD.MAN.& INF. M.HUSNI THAMRIN', '\r', 'N'),
('034095', 'AKD. KIMIA ANALIS CARAKA NUS.', '\r', 'N'),
('034096', 'AMIK PUSAKA NUSANTARA', '\r', 'N'),
('034097', 'AKD.MANJ.& INF.KOMPUTER WIDURI', '\r', 'N'),
('034098', 'AKD. SEKR. & MANJ.SANTA URSULA', '\r', 'N'),
('034099', 'AKD. BAHASA ASING SANTA URSULA', '\r', 'N'),
('034101', 'AKD. AKUNTANSI SANTA URSULA', '\r', 'N'),
('034102', 'AKD. PARIWISATA NUSANTARA JAYA', '\r', 'N'),
('034103', 'AKD. PARIWISATA PARAMITHA', '\r', 'N'),
('034104', 'AKD. SEKRETARI BUDI LUHUR', 'JAKARTA\r', 'N'),
('034105', 'AKD. SEKR. & MANJ. DON BOSCO', '\r', 'N'),
('034106', 'AKPAR JKT INTERNATIONAL HOTELS', '\r', 'N'),
('034107', 'AKD. PARIWISATA PELITA HARAPAN', '\r', 'N'),
('034108', '\"AKD. ILMU PERPUSTAKAAN \"\"YARSI\"\"\"', '\r', 'N'),
('034109', 'AKD. PARIWISATA PATRIA INDO.', '\r', 'N'),
('034110', 'AMIK BINA SARANA INFORMATIKA', 'JAKARTA\r', 'N'),
('034111', 'AKD. OLAH RAGA INDONESIA', '\r', 'N'),
('034112', 'AKD. KOMUNIK. MEDIA RADIO & TV', 'JAKARTA\r', 'N'),
('034113', 'AKD. SEKRETARI JAYABAYA', '\r', 'N'),
('034114', 'AMIK JAYABAYA', '\r', 'N'),
('034115', 'AKADEMI TEKNIK FEDERAL', '\r', 'N'),
('034116', 'AKADEMI AKUNTANSI PARAMITA', 'JAKARTA\r', 'N'),
('034117', 'AKD. AKUNT. BENTARA INDONESIA', '\r', 'N'),
('034118', 'AKD.TEKNO. KOMUNIKASI DAN INF.', 'JAKARTA\r', 'N'),
('034119', 'AKD. TELEKOMUNIK.IND.GEMILANG', '\r', 'N'),
('034120', 'AKADEMI PARIWISATA JAKARTA', '\r', 'N'),
('034121', 'AKADEMI MARITIM PEMBANGUNAN', '\r', 'N'),
('034122', 'AKD. PARIWISATA CITRA NUSA', '\r', 'N'),
('034123', 'AKD. PELAYARAN NIAGA INDONESIA', 'JAKARTA\r', 'N'),
('034124', 'AKD. PARIWISATA PERTIWI', 'JAKARTA\r', 'N'),
('034125', 'ABA PRAWIRA MARTA', 'JAKARTA\r', 'N'),
('034126', 'AKD. ASURANSI INDONESIA', 'JAKARTA\r', 'N'),
('034127', 'AKD. KOMUNIKASI BINA EKATAMA', '\r', 'N'),
('034128', 'AKD. PARIWISATA MATOA', 'JAKARTA\r', 'N'),
('034129', 'AMIK MPU TANTULAR', '\r', 'N'),
('034130', 'AKD.PARIWIS. GSP INTERNASIONAL', 'JAKARTA\r', 'N'),
('034131', 'AMIK YAPRI', 'JAKARTA\r', 'N'),
('034132', 'AKD. KEU. DAN PERBANKAN INDO.', '\r', 'N'),
('034133', 'AKD.MAN.PELAYANA.RS.MH.THAMRIN', 'JAKARTA\r', 'N'),
('034134', 'AKD. PARIWISATA SAINT MARY', 'JAKARTA\r', 'N'),
('034135', 'AKADEMI SEKRETARI INTERSTUDI', 'JAKARTA\r', 'N'),
('034136', 'AKSEMA DHARMA BUDHI BHAKTI', 'JAKARTA\r', 'N'),
('034137', 'AKD SEKRETARIS & MANAJEMEN BSI', 'JAKARTA\r', 'N'),
('034138', 'AKD.SEKRETARI & MANAJ.PITALOKA', 'JAKARTA\r', 'N'),
('034139', 'AKD. KEBIDANAN KERIS HUSADA', '\r', 'N'),
('034140', 'AKD. KEPERAWATAN KERIS HUSADA', '\r', 'N'),
('034141', 'AMIK LAKSI-31', 'JAKARTA\r', 'N'),
('034142', 'AKD. KEBIDANAN AL-FATHONAH', '\r', 'N'),
('034143', 'AKD. KEBIDANAN MH. THAMRIN', '\r', 'N'),
('034144', 'AKD. KEPERAWATAN SYAFAAT IND.', '\r', 'N'),
('034145', 'AKD. SEKRETARI SAINT THERESIA', '\r', 'N'),
('034146', 'AMIK WAHANA MANDIRI', '\r', 'N'),
('034147', 'ATI. TUNAS BANGSA', '\r', 'N'),
('035001', 'POLITEKNIK BISNIS INDONESIA', '\r', 'N'),
('035002', 'POLITEKNIK SWADHARMA', '\r', 'N'),
('035003', 'POLITEKNIK MANUFAKTUR ASTRA', 'JAKARTA\r', 'N'),
('035004', 'POLITEKNIK BUNDA KANDUNG', '\r', 'N'),
('035005', 'POLITEKNIK GUNAKARYA INDONESIA', '\r', 'N'),
('039006', 'POLITEKNIK TUGU', '\r', 'N'),
('041001', 'UNIVERSITAS IBNU KHALDUN', '\r', 'N'),
('041002', 'UNIVERSITAS ISLAM BANDUNG', '\r', 'N'),
('041003', 'UNIVERSITAS ISLAM NUSANTARA', '\r', 'N'),
('041004', 'UNIVERSITAS PAKUAN', '\r', 'N'),
('041005', 'UNIVERSITAS ISLAM SYEKH YUSUF', '\r', 'N'),
('041006', 'UNIVERSITAS KATHOLIK PARAHYANGAN', '\r', 'N'),
('041007', 'UNIVERSITAS KRISTEN MARANATHA', '\r', 'N'),
('041008', 'UNIVERSITAS PASUNDAN', '\r', 'N'),
('041009', 'UNIVERSITAS SWADAYA GUNUNG DJATI', '\r', 'N'),
('041010', 'UNIVERSITAS 17 AGUSTUS 1945 CIREBON', '\r', 'N'),
('041011', 'UNIVERSITAS ADVENT INDONESIA', '\r', 'N'),
('041012', 'UNIVERSITAS SILIWANGI', '\r', 'N'),
('041013', 'UNIVERSITAS TIRTAYASA', '\r', 'N'),
('041014', 'UNIVERSITAS WIRALODRA', '\r', 'N'),
('041015', 'UNIVERSITAS LANGLANGBUANA', '\r', 'N'),
('041016', 'UNIVERSITAS BANDUNG RAYA', '\r', 'N'),
('041017', 'UNIVERSITAS SINGAPERBANGSA', '\r', 'N'),
('041018', 'UNIVERSITAS ISLAM \'45', '\r', 'N'),
('041019', 'UNIVERSITAS DJUANDA', '\r', 'N'),
('041020', 'UNIVERSITAS NUSA BANGSA', '\r', 'N'),
('041021', 'UNIVERSITAS JENDERAL ACHMAD YANI', '\r', 'N'),
('041022', 'UNIVERSITAS WINAYA MUKTI', '\r', 'N'),
('041023', 'UNIVERSITAS GALUH CIAMIS', '\r', 'N'),
('041024', 'UNIVERSITAS GARUT', '\r', 'N'),
('041025', 'UNIVERSITAS NURTANIO', '\r', 'N'),
('041026', 'UNIVERSITAS SWISS GERMAN', '\r', 'N'),
('041027', 'UNIVERSITAS KOMPUTER INDONESIA', '\r', 'N'),
('041028', 'UNIVERSITAS ARS INTERNASIONAL', '\r', 'N'),
('041029', 'UNIVERSITAS MUHAMMADIYAH', '\r', 'N'),
('042001', 'INSTITUT MANAJEMEN KOPERASI INDONESIA', '\r', 'N'),
('042002', 'INSTITUT TEKNOLOGI NASIONAL', '\r', 'N'),
('042003', 'INSTITUT TEKNOLOGI ADITYAWARMAN', '\r', 'N'),
('042004', 'INSTITUT TEKNOLOGI SWISS TANGERANG', '\r', 'N'),
('043001', 'STH BANDUNG', '\r', 'N'),
('043002', 'STH GALUNGGUNG', '\r', 'N'),
('043003', 'STH PASUNDAN', '\r', 'N'),
('043004', 'SEKOLAH TINGGI ILMU BAHASA BANDUNG', '\r', 'N'),
('043005', 'STH PURNAWARMAN', '\r', 'N'),
('043006', 'STH SURYAKANCANA', '\r', 'N'),
('043007', 'STIE BANDUNG', '\r', 'N'),
('043008', 'STISIP GARUT', '\r', 'N'),
('043009', 'STIA ANGKASA', '\r', 'N'),
('043010', 'STIA M YUSUF', '\r', 'N'),
('043011', 'STKIP MAJALENGKA', '\r', 'N'),
('043012', 'STT PRAMITA', '\r', 'N'),
('043013', 'STMIK PMBI', '\r', 'N'),
('043014', 'STIA TASIKMALAYA', '\r', 'N'),
('043015', 'STIA KUTAWARINGIN', '\r', 'N'),
('043016', 'STKIP KUNINGAN', '\r', 'N'),
('043017', 'STKIP SURYAKANCANA', '\r', 'N'),
('043018', 'STIE TRI DHARMA', '\r', 'N'),
('043019', 'STKIP UNSAP', '\r', 'N'),
('043020', 'STIE YPKP', '\r', 'N'),
('043021', 'STIE INABA', '\r', 'N'),
('043022', 'STIE P SAKTI', '\r', 'N'),
('043023', 'STT KUTA WARINGIN SUBANG', '\r', 'N'),
('043024', 'STIE PERTIWI BEKASI', '\r', 'N'),
('043025', 'STIA YPPM', '\r', 'N'),
('043026', 'STIE STEMBI', '\r', 'N'),
('043027', 'STMIK MARDIRA', '\r', 'N'),
('043028', 'STT YUPPENTEK', '\r', 'N'),
('043029', 'STH GARUT', '\r', 'N'),
('043030', 'STKIP GARUT', '\r', 'N'),
('043031', 'STKIP BALE BANDUNG', '\r', 'N'),
('043032', 'STIPER BALE BANDUNG', '\r', 'N'),
('043033', 'STIA MENARA SISWA', '\r', 'N'),
('043034', 'STKIP PASUNDAN', '\r', 'N'),
('043035', 'STBA YAPARI', '\r', 'N'),
('043036', 'STKIP PGRI', '\r', 'N'),
('043037', 'STBA INDONESIA INTERNASIONAL', '\r', 'N'),
('043038', 'SEKOLAH TINGGI SAINS DAN TEKNOLOGI INDON', '\r', 'N'),
('043039', 'SEKOLAH TINGGI FARMASI GARUT', '\r', 'N'),
('043040', 'SEKOLAH TINGGI TEKNOLOGI MANDALA', '\r', 'N'),
('043041', 'STKIP SILIWANGI', '\r', 'N'),
('043042', 'SEKOLAH TINGGI ILMU ADMINISTRASI BAGASASI', '\r', 'N'),
('043043', 'SEKOLAH TINGGI ILMU EKONOMI PASUNDAN', '\r', 'N'),
('043044', 'SEKOLAH TINGGI TEKNOLOGI BINA TUNGGAL', '\r', 'N');
INSERT INTO `perguruantinggi` (`Kode`, `Nama`, `Kota`, `NotActive`) VALUES
('043045', 'SEKOLAH TINGGI ILMU KOMUNIKASI BANDUNG', '\r', 'N'),
('043046', 'SEKOLAH TINGGI PERTANIAN JAWA BARAT', '\r', 'N'),
('043047', 'SEKOLAH TINGGI TEKNOLOGI INDONESIA', '\r', 'N'),
('043048', 'SEKOLAH TINGGI PERTANIAN GILANG KENCANA', '\r', 'N'),
('043049', 'SEKOLAH TINGGI TEKNOLOGI MINERAL INDONES', '\r', 'N'),
('043050', 'SEKOLAH TINGGI TEKNOLOGI CIREBON', '\r', 'N'),
('043051', 'SEKOLAH TINGGI TEKNOLOGI GARUT', '\r', 'N'),
('043052', 'SEKOLAH TINGGI TEKNOLOGI TELEKOM', '\r', 'N'),
('043053', 'SEKOLAH TINGGI SENIRUPA & DESAIN INDONES', '\r', 'N'),
('043054', 'SEKOLAH TINGGI BAHASA ASING NASIONAL', '\r', 'N'),
('043055', 'STBA AL JAWAMI', '\r', 'N'),
('043056', 'SEKOLAH TINGGI ILMU EKONOMI PARIWISATA Y', '\r', 'N'),
('043057', 'SEKOLAH TINGGI MATEMATIKA & ILMU PENG. A', '\r', 'N'),
('043058', 'STIE GARUT', '\r', 'N'),
('043059', 'STT BANDUNG', '\r', 'N'),
('043060', 'STIE NASIONAL', '\r', 'N'),
('043061', 'STIE BINA NIAGA', '\r', 'N'),
('043062', 'STIA KOSGORO', '\r', 'N'),
('043063', 'STMIK BANDUNG', '\r', 'N'),
('043064', 'STIE LATANSA MASHIRO', '\r', 'N'),
('043065', 'SEKOLAH TINGGI MANAJEMEN BANDUNG', '\r', 'N'),
('043066', 'STIE INDONESIA INTERNASIONAL', '\r', 'N'),
('043067', 'STIE INDONESIA ESA UTAMA', '\r', 'N'),
('043068', 'STIE MUHAMADDIYAH', '\r', 'N'),
('043069', 'SEKOLAH TINGGI TEKNOLOGI SFB', '\r', 'N'),
('043070', 'STIE I/BMI', '\r', 'N'),
('043071', 'STMIK LIKMI', '\r', 'N'),
('043072', 'SEKOLAH TINGGI ILMU BAHASA BANTEN RAYA', '\r', 'N'),
('043073', 'STMIK IM', '\r', 'N'),
('043074', 'STIE YASA ANGGANA', '\r', 'N'),
('043075', 'STIE TRIGUNA', '\r', 'N'),
('043076', 'STIE PRAMITA', '\r', 'N'),
('043077', 'STIE KUNINGAN', '\r', 'N'),
('043078', 'SEKOLAH TINGGI MANAJEMEN PARIWISATA BAND', '\r', 'N'),
('043079', 'SEKOLAH TINGGI DESAIN INDONESIA BANDUNG', '\r', 'N'),
('043080', 'STT YPKP', '\r', 'N'),
('043081', 'STIE 11 APRIL', '\r', 'N'),
('043082', 'STIE PUJI SUKABUMI', '\r', 'N'),
('043083', 'STIE BUDDHI', '\r', 'N'),
('043084', 'STIE PGRI', '\r', 'N'),
('043085', 'STISIP YUPPENTEK', '\r', 'N'),
('043086', 'STMIK JABAR', '\r', 'N'),
('043087', 'STIE INDONESIA EMAS', '\r', 'N'),
('043088', 'STIE MIFTAFUL HUDA', '\r', 'N'),
('043089', 'STIE MULIA PRATAMA', '\r', 'N'),
('043090', 'STIA 11 APRIL', '\r', 'N'),
('043091', 'STT MUTU MUHAMADIYAH', '\r', 'N'),
('043092', 'STIE MATHLA\'UL ANWAR', '\r', 'N'),
('043093', 'STIE YP KARYA', '\r', 'N'),
('043094', 'STIE CIREBON', '\r', 'N'),
('043095', 'SEKOLAH TINGGI TEKNOLOGI WIDYATAMA', '\r', 'N'),
('043096', 'STIE TRI BHAKTI', '\r', 'N'),
('043097', 'STIE KESATUAN', '\r', 'N'),
('043098', 'STIE PASIM', '\r', 'N'),
('043099', 'STIE YPPI', '\r', 'N'),
('043100', 'STT CIPASUNG', '\r', 'N'),
('043101', 'STMIK SERANG', '\r', 'N'),
('043102', 'STMIK BUDHI', '\r', 'N'),
('043103', 'STISIP W MANDIRI', '\r', 'N'),
('043104', 'STMIK T CENDEKIA', '\r', 'N'),
('043105', 'STMIK MIKAR', '\r', 'N'),
('043106', 'STIH GUNUNG JATI', '\r', 'N'),
('043107', 'STT TEXMACO', '\r', 'N'),
('043108', 'STMIK BANI SALEH', '\r', 'N'),
('043109', 'STMIK LPMIK', '\r', 'N'),
('043110', 'STISIP TASIK', '\r', 'N'),
('043111', 'STKIP ARRAHMANIYAH', '\r', 'N'),
('043112', 'STMIK BAJA', '\r', 'N'),
('043113', 'SEKOLAH TINGGI TEKNOLOGI JABAR', '\r', 'N'),
('043114', 'STIE YASMI', '\r', 'N'),
('043115', 'SEKOLAH TINGGI FARMASI BANDUNG', '\r', 'N'),
('043116', 'STIE IGI', '\r', 'N'),
('043117', 'STMIK IGI', '\r', 'N'),
('043118', 'STMIK DCI', '\r', 'N'),
('043119', 'STIE SUTAATMADJA', '\r', 'N'),
('043200', 'STMIK CIC', '\r', 'N'),
('043201', 'STMIK CILEGON', '\r', 'N'),
('043202', 'STMIK SUBANG', '\r', 'N'),
('043203', 'STKIP SUBANG', '\r', 'N'),
('043204', 'SEKOLAH TINGGI TEKNOLOGI YPPI', '\r', 'N'),
('043205', 'STBA YPPI', '\r', 'N'),
('043206', 'STIE MAJALENGKA', '\r', 'N'),
('043207', 'STIE FAJAR', '\r', 'N'),
('043208', 'STIE GEMA WIDYA BANGSA', '\r', 'N'),
('043209', 'STIE PELITA NUSANTARA', '\r', 'N'),
('043210', 'STIE CIPASUNG', '\r', 'N'),
('043211', 'STBA JIA', '\r', 'N'),
('043212', 'SEKOLAH TINGGI FARMASI BOGOR', '\r', 'N'),
('043213', 'SEKOLAH TINGGI ILMU PERTANIAN YPPM', '\r', 'N'),
('043214', 'STIE YPN', '\r', 'N'),
('043215', 'STIE MUHAMMADIYAH BANDUNG', '\r', 'N'),
('043216', 'STIE LEMBAGA MANAJEMEN', '\r', 'N'),
('043217', 'STMIK AMIK BDG', '\r', 'N'),
('043218', 'STMIK TRISAKA', '\r', 'N'),
('043219', 'STMIK ROSMA', '\r', 'N'),
('043220', 'STKIP YASIKA', '\r', 'N'),
('043221', 'STIA BANDUNG', '\r', 'N'),
('043222', 'STMIK SUMEDANG', '\r', 'N'),
('043223', 'STIE NUSA DHARMA', '\r', 'N'),
('043224', 'STMIK PAMITRAN', '\r', 'N'),
('043225', 'STMIK KHARISMA', '\r', 'N'),
('043226', 'STMIK PASIM', '\r', 'N'),
('043227', 'STIE MERCU SUAR', '\r', 'N'),
('043228', 'STISIP SYAMSUL ULUN', '\r', 'N'),
('043229', 'STIE PANDU MADANIA', '\r', 'N'),
('043230', 'STIE YPPN   BEKASI', '\r', 'N'),
('043231', 'STIE AKPI', '\r', 'N'),
('043232', 'SEKOLAH TINGGI DESAIN KOMUNIKASI VISUAL', '\r', 'N'),
('043233', 'STIE STAN-IM', '\r', 'N'),
('043234', 'STIE ADHY NIAGA', '\r', 'N'),
('043235', 'STIE MAJALENGKA', '\r', 'N'),
('043236', 'STIE EMINEN', '\r', 'N'),
('044001', 'AMIK YASIKA', '\r', 'N'),
('044002', 'AKADEMI AKUNTANSI BANDUNG', '\r', 'N'),
('044003', 'AKADEMI AKUNTANSI BINA INSANI', '\r', 'N'),
('044004', 'AKADEMI SEKRETARI DAN MANAJEMEN BINA INS', '\r', 'N'),
('044005', 'AKADEMI KEUANGAN DAN PERBANKAN MERDEKA', '\r', 'N'),
('044006', 'AKADEMI SEKRETARI & MANAJEMEN BHAKTI', '\r', 'N'),
('044007', 'AMIK MULYA MITRA CILEGON', '\r', 'N'),
('044008', 'AKADEMI BAHASA ASING YPKK TANGERANG', '\r', 'N'),
('044009', 'AMIK WIRA NUSANTARA   PANDEGLANG', '\r', 'N'),
('044010', 'AKADEMI KESEJAHTERAAN SOSIAL', '\r', 'N'),
('044011', 'AKADEMI MANAJEMEN INDUSTRI DAN NIAGA', '\r', 'N'),
('044013', 'AKADEMI PARIWISATA YAPARI', '\r', 'N'),
('044015', 'AKADEMI KEUANGAN DAN PERBANKAN INDONESIA', '\r', 'N'),
('044019', 'AKADEMI PERTANIAN TANJUNGSARI', '\r', 'N'),
('044021', 'AKADEMI SEKRETARI DAN MANAJEMEN BANDUNG', '\r', 'N'),
('044022', 'AKADEMI SEKRETARI & MANAJEMEN TARUNA BAK', '\r', 'N'),
('044024', 'ATPU', '\r', 'N'),
('044028', 'AKADEMI MANAJEMEN KESATUAN', '\r', 'N'),
('044031', 'AMIK BANDUNG', '\r', 'N'),
('044032', 'AKADEMI TEKNOLOGI PEMBANGUNAN', '\r', 'N'),
('044033', 'AMIK HASS', '\r', 'N'),
('044034', 'AKADEMI PARIWISATA TADIKA PURI', '\r', 'N'),
('044035', 'AKADEMI MARITIM SUAKA BAHARI', '\r', 'N'),
('044036', 'AKADEMI KIMIA ANALIS', '\r', 'N'),
('044037', 'AMIK BANI SALEH', '\r', 'N'),
('044038', 'AKADEMI SEKRETARI & MANAJEMEN INDUSTRI Y', '\r', 'N'),
('044039', 'AKADEMI BAHASA ASING YPPI', '\r', 'N'),
('044040', 'AKADEMI AERONATIKA DIRGANTARA BANDUNG', '\r', 'N'),
('044041', 'AKADEMI PARIWISATA NASIONAL', '\r', 'N'),
('044042', 'AKADEMI BAHASA ASING 11 APRIL', '\r', 'N'),
('044043', 'AMIK BINA NIAGA', '\r', 'N'),
('044044', 'AAMIK SUKAPURA', '\r', 'N'),
('044045', 'AMIK BUMI NUSANTARA', '\r', 'N'),
('044046', 'AMIK YPPI   CIANJUR', '\r', 'N'),
('044047', 'AKADEMI MEDIS VETERINER BANDUNG', '\r', 'N'),
('044048', 'AKADEMI SEKRETARIS MANAJEMEN BUMI NUSANT', '\r', 'N'),
('044049', 'AKADEMI SEKRETARIS MANAJEMEN KENCANA', '\r', 'N'),
('044050', 'AKADEMI PARIWISATA PANCA DHARMA BHAKTI', '\r', 'N'),
('044050', 'AMIK PERTIWI', '\r', 'N'),
('044051', 'AKADEMI PERDAGANGAN BANDUNG', '\r', 'N'),
('044052', 'AKADEMI MARITIM CIREBON', '\r', 'N'),
('044053', 'AMIK MUHAMMADIYAH   SERANG', '\r', 'N'),
('044054', 'AKADEMI KESENIAN BOGOR', '\r', 'N'),
('044055', 'AKADEMI TEKNIK PATRIOT', '\r', 'N'),
('044056', 'AKADEMI PARIWISATA YASMI', '\r', 'N'),
('044057', 'AMIK CIREBON', '\r', 'N'),
('044058', 'AKADEMI BANK YASMI', '\r', 'N'),
('044059', 'AKADEMI PERDAGANGAN WIDYA DHARMA', '\r', 'N'),
('044060', 'AMIK SERANG', '\r', 'N'),
('044061', 'ASM AL MA\'SOEM', '\r', 'N'),
('044062', 'ASMI TUNAS HARAPAN', '\r', 'N'),
('044063', 'AMIK KHARISMA', '\r', 'N'),
('044064', 'AMIK GARUT', '\r', 'N'),
('044065', 'AKADEMI PARIWISATA SILIWANGI', '\r', 'N'),
('044066', 'AKADEMI BAHASA ASING BUDHI', '\r', 'N'),
('044067', 'AKADEMI AKUNTANSI BUDHI', '\r', 'N'),
('044068', 'AMIK SULTAN AGUNG', '\r', 'N'),
('044069', 'ASM TRIGUNA', '\r', 'N'),
('044070', 'ASM LEPISI', '\r', 'N'),
('044071', 'AKADEMI SEKRETARI & MANAJEMEN ARIYANTI', '\r', 'N'),
('044072', 'AKADEMI TATABOGA BANDUNG', '\r', 'N'),
('044073', 'AKADEDMI TEKSTIL PRIMA', '\r', 'N'),
('044074', 'AKADEMI TEKNOLOGI BANDUNG', '\r', 'N'),
('044075', 'AKADEMI INDUSTRI TEKSTIL BANDUNG', '\r', 'N'),
('044076', 'AMIK CIKARANG', '\r', 'N'),
('044077', 'AKADEMI KOMUNIKASI RADIO & TV HUTAMA', '\r', 'N'),
('044078', 'AKADEMI TEKNOLOGI BOGOR', '\r', 'N'),
('044079', 'AMIK MIKAR', '\r', 'N'),
('044080', 'AKADEMI AKUNTANSI PERDANA MANDIRI', '\r', 'N'),
('044081', 'AMIK AL MA\'SOEM', '\r', 'N'),
('044082', 'AKADEMI PARIWISATA SANDY PUTRA', '\r', 'N'),
('044083', 'ASM PRIMA GRAHA', '\r', 'N'),
('044084', 'ASM ARS INTERNASIONAL', '\r', 'N'),
('044085', 'AMIK ARS INTERNASIONAL', '\r', 'N'),
('044086', 'AKADEMI AKUNTANSI & PERBANKAN ARS INTERN', '\r', 'N'),
('044087', 'AKADEMI AKUNTANSI ERA 2020', '\r', 'N'),
('044088', 'AKADEMI OLAH RAGA INDONESIA', '\r', 'N'),
('044089', 'AMIK YPAT', '\r', 'N'),
('044090', 'AMIK RAHARJA', '\r', 'N'),
('044091', 'AMIK MASA DEPAN', '\r', 'N'),
('044092', 'AKADEMI TEKNOLOGI AERONAUTIKA SILIWANGI', '\r', 'N'),
('044093', 'AKADEMI PARIWISATA NASIONAL INDONESIA', '\r', 'N'),
('044094', 'AKADEMI AKUNTANSI KEUANGAN & PERBANKAN I', '\r', 'N'),
('044095', 'AKADEMI SEKRETARI & MANAJEMEN ADVENT BAN', '\r', 'N'),
('044096', 'AMIK WIDYA SARANA', '\r', 'N'),
('044097', 'AMIK MIC', '\r', 'N'),
('044098', 'AKADEMI SEKRETARI & MANAJEMEN TRI BAKTI', '\r', 'N'),
('044099', 'AKADEMI AKUNTANSI BANDUNG', '\r', 'N'),
('044416', 'AKADEMI KOMUNIKASI BANDUNG', '\r', 'N'),
('045001', 'POLITEKNIK INDUSTRI & NIAGA BANDUNG', '\r', 'N'),
('045002', 'POLITEKNIK INSAN CINTA BANGSA', '\r', 'N'),
('045003', 'POLITEKNIK GAJAH TUNGGAL', '\r', 'N'),
('045004', 'POLITEKNIK BINA BUDAYA CIPTA', '\r', 'N'),
('045005', 'POLITEKNIK WINAYA KARYA BHAKTI', '\r', 'N'),
('045006', 'POLITEKNIK KRAKATAU', '\r', 'N'),
('045007', 'POLITEKNIK GANESHA', '\r', 'N'),
('045008', 'POLITEKNIK KOMPUTER NIAGA', '\r', 'N'),
('051001', 'UNIVERSITAS ISLAM INDONESIA', 'YOGYAKARTA\r', 'N'),
('051002', 'UNIVERSITAS SARJANAWIYATA TAMANSISWA', 'YOGYAKARTA\r', 'N'),
('051003', 'UNIVERSITAS JANABADRA', 'YOGYAKARTA\r', 'N'),
('051004', 'UNIVERSITAS PROKLAMASI \'45', 'YOGYAKARTA\r', 'N'),
('051005', 'UNIVERSITAS ATMA JAYA YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('051006', 'UNIVERSITAS COKROAMINOTO', 'YOGYAKARTA\r', 'N'),
('051007', 'UNIVERSITAS MUHAMMADIYAH YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('051008', 'UNIVERSITAS WIDYA MATARAM', 'YOGYAKARTA\r', 'N'),
('051009', 'UNIVERSITAS WANGSA MANGGALA', 'YOGYAKARTA\r', 'N'),
('051010', 'UNIVERSITAS KRISTEN IMMANUEL', 'YOGYAKARTA\r', 'N'),
('051011', 'UNIVERSITAS KRISTEN DUTA WACANA', 'YOGYAKARTA\r', 'N'),
('051012', 'UNIVERSITAS SANATA DHARMA', 'YOGYAKARTA\r', 'N'),
('051013', 'UNIVERSITAS AHMAD DAHLAN', 'YOGYAKARTA\r', 'N'),
('051014', '\"UNIVERSITAS PEMBANGUNAN NASIONAL \"\"VETERAN\"\"\"', 'YOGYAKARTA\r', 'N'),
('051015', 'UNIVERSITAS PGRI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('051016', 'UNIVERSITAS DIRGANTARA INDONESIA', 'YOGYAKARTA\r', 'N'),
('051017', 'UNIVERSITAS GUNUNG KIDUL', 'GUNUNG KIDUL\r', 'N'),
('052000', 'IKIP VETERAN (PHASING OUT)', 'YOGYAKARTA\r', 'N'),
('052001', 'IKIP PGRI WATES', 'YOGYAKARTA\r', 'N'),
('052002', 'INSTITUT PERTANIAN (INTAN)', 'YOGYAKARTA\r', 'N'),
('052003', 'INSTITUT SAINS & TEKNOLOGI AKPRIND', 'YOGYAKARTA\r', 'N'),
('052004', '\"INSTITUT PERTANIAN \"\"STIPER\"\"\"', 'YOGYAKARTA\r', 'N'),
('053001', 'STIKIP CATUR SAKTI', 'YOGYAKARTA\r', 'N'),
('053002', 'SEKOLAH TINGGI TEKNOLOGI NASIONAL', 'YOGYAKARTA\r', 'N'),
('053003', 'SEKOLAH TINGGI ILMU EKONOMI YKPN', 'YOGYAKARTA\r', 'N'),
('053004', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA WIWAHA', 'YOGYAKARTA\r', 'N'),
('053005', 'SEKOLAH TINGGI ILMU EKONOMI NUSA MEGAR KENCANA', 'YOGYAKARTA\r', 'N'),
('053006', '\"STISIPOL \"\"KARTIKA BANGSA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053007', 'SEKOLAH TINGGI TEKNIK LINGKUNGAN (STTL)', 'YOGYAKARTA\r', 'N'),
('053008', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"KERJA SAMA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053009', '\"SEKOLAH TINGGI PEMBANGUNAN MASYARAKAT DESA \"\"APMD\"\"\"', 'YOGYAKARTA\r', 'N'),
('053010', 'SEKOLAH TINGGI MANAJEMEN INF. & KOMPUTER AKAKOM', 'YOGYAKARTA\r', 'N'),
('053011', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"YOGYAKARTA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053012', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"SBI\"\"\"', 'YOGYAKARTA\r', 'N'),
('053013', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"MITRA INDONESIA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053014', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"BBANK\"\"\"', 'YOGYAKARTA\r', 'N'),
('053015', 'SEKOLAH TINGGI ILMU EKONOMI \'YKP\'', 'YOGYAKARTA\r', 'N'),
('053016', 'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DHARMA BANGSA', 'YOGYAKARTA\r', 'N'),
('053017', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"ISTI EKATANA UPAWEDA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053018', 'SEKOLAH TINGGI TEKNOLOGI KEDIRGANTARAAN', 'YOGYAKARTA\r', 'N'),
('053019', '\"SEKOLAH TINGGI ILMU ADMINISTRASI \"\"AAN\"\"\"', 'YOGYAKARTA\r', 'N'),
('053020', '\"SEKOLAH TINGGI PARIWISATA \"\"AMPTA\"\" YOGYAKARTA\"', 'YOGYAKARTA\r', 'N'),
('053021', '\"SEK.TINGGI MANAJ INFORMATIKA & KOMPUTER \"\"AMIKOM\"\"\"', 'YOGYAKARTA\r', 'N'),
('053022', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"PARIWISATA API\"\"\"', 'YOGYAKARTA\r', 'N'),
('053023', '\"SEKOLAH TINGGI BAHASA ASING \"\"LIA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053024', 'SEKOLAH TINGGI TEKNOLOGI ADISUTJIPTO', 'YOGYAKARTA\r', 'N'),
('053025', '\"STMIK \"\"EL RAHMA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053026', 'STMIK PROACTIVE', 'YOGYAKARTA\r', 'N'),
('053027', '\"SEK. TINGGI ILMU KESEHATAN (STIKES) \"\"WIRA HUSADA\"\"\"', 'YOGYAKARTA\r', 'N'),
('053028', 'SEKOLAH TINGGI PSIKOLOGI PRIMAGAMA', 'YOGYAKARTA\r', 'N'),
('054001', 'AKADEMI AKUNTANSI YKPN', 'YOGYAKARTA\r', 'N'),
('054002', '\"AKADEMI KESEJAHTERAAN SOSIAL \"\"TARAKANITA\"\"\"', 'YOGYAKARTA\r', 'N'),
('054003', 'AKADEMI BAHASA ASING YIPK', 'YOGYAKARTA\r', 'N'),
('054004', 'AKADEMI KEUANGAN DAN PERBANKAN YIPK', 'YOGYAKARTA\r', 'N'),
('054004', 'AKADEMI BAHASA ASING YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054005', 'AKADEMI TEKNIK YKPN', 'YOGYAKARTA\r', 'N'),
('054006', 'AKADEMI MANAJEMEN PUTRA JAYA', 'YOGYAKARTA\r', 'N'),
('054007', 'AKADEMI KESEJAHTERAAN SOSIAL AKK', 'YOGYAKARTA\r', 'N'),
('054008', 'AKADEMI MANAJEMEN PERUSAHAAN YKPN', 'YOGYAKARTA\r', 'N'),
('054009', 'AKADEMI SEKRETARI DAN MANAJEMEN ISTHIKAYANA', 'YOGYAKARTA\r', 'N'),
('054010', 'AKADEMI PARIWISATA BUANA WISATA', 'YOGYAKARTA\r', 'N'),
('054011', 'AKADEMI PETERNAKAN BRAHMA PUTRA', 'YOGYAKARTA\r', 'N'),
('054012', 'AKADEMI MARITIM YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054013', 'AKADEMI KOMUNIKASI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054014', 'AKADEMI KETATALAKSANAAN PELAYARAN NIAGA BAHTERA', 'YOGYAKARTA\r', 'N'),
('054015', 'AKADEMI PERTANIAN YOGYAKARTA (APTA)', 'YOGYAKARTA\r', 'N'),
('054016', 'AKADEMI SEKRETARI DAN MANAJEMEN SANTA MARIA', 'YOGYAKARTA\r', 'N'),
('054017', 'AKADEMI PERIKANAN YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054018', '\"AKADEMI MANAJEMEN INFORMATIKA DAN KOMPUTER \"\"ASTER\"\"\"', 'YOGYAKARTA\r', 'N'),
('054019', '\"AKADEMI KESEJAHTERAAN SOSIAL \"\"ATPS\"\" YOGYAKARTA\"', 'YOGYAKARTA\r', 'N'),
('054020', 'AKADEMI PARIWISATA INDRAPHRASTA', 'YOGYAKARTA\r', 'N'),
('054021', 'AMIK WIRA SETYA MULYA.', 'YOGYAKARTA\r', 'N'),
('054022', 'AKADEMI SEKRETARI DAN MANAJEMEN INDONESIA DESANTA', 'YOGYAKARTA\r', 'N'),
('054023', '\"AKADEMI TEKNIK \"\"PIRI\"\"\"', 'YOGYAKARTA\r', 'N'),
('054024', 'AKADEMI TELEKOMUNIKASI INDONESIA', 'YOGYAKARTA\r', 'N'),
('054025', 'AKADEMI AKUNTANSI SAPTA WIDYA TAMA', 'YOGYAKARTA\r', 'N'),
('054026', '\"AKADEMI PARIWISATA \"\"YOGYAKARTA\"\" (AKPARYO)\"', 'YOGYAKARTA\r', 'N'),
('054027', 'AKAD. KOM. INDONESIA YAYASAN PENDIDIKAN KOMUNIKASI', 'YOGYAKARTA\r', 'N'),
('054028', 'AKADEMI PARIWISATA DHARMA NUSANTARA SAKTI', 'YOGYAKARTA\r', 'N'),
('054029', 'AKADEMI TEKNOLOGI OTOMOTIF NASIONAL ( ATONAL )', 'YOGYAKARTA\r', 'N'),
('054030', '\"AKADEMI PARIWISATA \"\"STIPARY\"\"\"', 'YOGYAKARTA\r', 'N'),
('054031', 'AKADEMI MARITIM GANESHA YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054032', 'AMIK \'KARTIKA YANI\' YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054033', 'AKADEMI DESAIN VISI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054034', '\"AKADEMI PARIWISATA \"\"AMPARI GARUDA\"\" YOGYAKARTA\"', 'YOGYAKARTA\r', 'N'),
('054035', 'AKADEMI KOMUNIKASI RADYA BINATAMA', 'YOGYAKARTA\r', 'N'),
('054036', '\"AKADEMI SENI RUPA DAN DESAIN \"\"AKSERI\"\"\"', 'YOGYAKARTA\r', 'N'),
('054037', 'AMIK PROACTIVE', 'YOGYAKARTA\r', 'N'),
('054038', 'AKADEMI MANAJEMEN INFORMATIKA & KOMPUTER YAPPINDO', 'YOGYAKARTA\r', 'N'),
('054039', 'AKADEMI MANAJEMEN ADMINISTRASI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054040', '\"AKADEMI BAHASA ASING \"\"YAPPINDO\"\"\"', 'YOGYAKARTA\r', 'N'),
('054041', '\"AKADEMI SENI RUPA DAN DESAIN \"\"MSD\"\"\"', 'YOGYAKARTA\r', 'N'),
('054042', 'AKADEMI PARIWISATA AMBARUKMO YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('054043', '\"AKADEMI MANAJEMEN ADMINISTRASI \"\"YPK\"\" YOGYAKARTA\"', 'YOGYAKARTA\r', 'N'),
('054044', '\"AKADEMI FISIOTERAPI \"\"YAB\"\" YOGYAKARTA\"', 'YOGYAKARTA\r', 'N'),
('054045', 'AKADEMI BAHASA ASING PRIMAGAMA', 'YOGYAKARTA\r', 'N'),
('055001', '\"POLITEKNIK \"\"API\"\"\"', 'YOGYAKARTA\r', 'N'),
('055002', '\"POLITEKNIK \"\"LPP\"\"\"', 'YOGYAKARTA\r', 'N'),
('055003', '\"POLITEKNIK \"\"PPKP\"\"\"', 'YOGYAKARTA\r', 'N'),
('055004', '\"POLITEKNIK \"\"YDHI\"\"\"', 'YOGYAKARTA\r', 'N'),
('055005', '\"POLITEKNIK \"\"STENKO\"\"\"', 'YOGYAKARTA\r', 'N'),
('055006', 'POLITEKNIK SENI YOGYAKARTA', 'YOGYAKARTA\r', 'N'),
('061001', 'UNIVERSITAS KRISTEN SATYA WACANA', 'SALATIGA\r', 'N'),
('061002', 'UNIVERSITAS ISLAM SULTAN AGUNG', 'SEMARANG\r', 'N'),
('061003', 'UNIVERSITAS 17 AGUSTUS 1945', 'SEMARANG\r', 'N'),
('061004', 'UNIVERSITAS MUHAMMADIYAH MAGELANG', 'MAGELANG\r', 'N'),
('061005', 'UNIVERSITAS TIDAR MAGELANG', 'MAGELANG\r', 'N'),
('061006', 'UNIVERSITAS SLAMET RIYADI', 'SURAKARTA\r', 'N'),
('061007', 'UNIVERSITAS WIJAYA KUSUMA', 'PURWOKERTO\r', 'N'),
('061008', 'UNIVERSITAS MUHAMMADIYAH SURAKARTA', 'SUKOHARJO\r', 'N'),
('061009', 'UNIVERSITAS MURIA KUDUS', 'KUDUS\r', 'N'),
('061010', 'UNIVERSITAS TUNAS PEMBANGUNAN', 'SURAKARTA\r', 'N'),
('061011', 'UNIVERSITAS PEKALONGAN', 'PEKALONGAN\r', 'N'),
('061012', 'UNIVERSITAS KATOLIK SOEGIJAPRANATA', 'SEMARANG\r', 'N'),
('061013', 'UNIVERSITAS PANCASAKTI', 'TEGAL\r', 'N'),
('061014', 'UNIVERSITAS DARUL ULUM ISLAMIC CENTRE', 'UNGARAN\r', 'N'),
('061015', 'UNIVERSITAS ISLAM BATIK', 'SURAKARTA\r', 'N'),
('061016', 'UNIVERSITAS VETERAN BANGUN NUSANTARA', 'SUKOHARJO\r', 'N'),
('061017', 'UNIVERSITAS SEMARANG', 'SEMARANG\r', 'N'),
('061018', 'UNIVERSITAS WIDYA DHARMA', 'KLATEN\r', 'N'),
('061019', 'UNIVERSITAS MUHAMMADIYAH PURWOKERTO', 'PURWOKERTO\r', 'N'),
('061020', 'UNIVERSITAS KRISTEN SURAKARTA', 'SURAKARTA\r', 'N'),
('061021', 'UNIVERSITAS PANDANARAN', 'SEMARANG\r', 'N'),
('061022', 'UNIVERSITAS SETIA BUDI SURAKARTA', 'SURAKARTA\r', 'N'),
('061023', 'UNIVERSITAS BAWEN', 'BAWEN\r', 'N'),
('061024', 'UNIVERSITAS SURAKARTA', 'SURAKARTA\r', 'N'),
('061025', 'UNIVERSITAS MUHAMMADIYAH PURWOREJO', 'PURWOREJO\r', 'N'),
('061026', 'UNIVERSITAS MUHAMMADIYAH SEMARANG', 'SEMARANG\r', 'N'),
('061027', 'UNIVERSITAS WAHID HASYIM', 'SEMARANG\r', 'N'),
('061028', 'UNIVERSITAS NAHDLATUL ULAMA', 'SURAKARTA\r', 'N'),
('061029', 'UNIVERSITAS STIKUBANK', 'SEMARANG\r', 'N'),
('061030', 'UNIVERSITAS SAINS ALQUR\'AN', 'WONOSOBO\r', 'N'),
('061031', 'UNIVERSITAS DIAN NUSWANTORO', 'SEMARANG\r', 'N'),
('062001', 'IKIP VETERAN JAWA TENGAH', 'SEMARANG\r', 'N'),
('062002', 'IKIP PGRI SEMARANG', 'SEMARANG\r', 'N'),
('063001', 'SEKOLAH TINGGI ILMU KOMUNIKASI', 'SEMARANG\r', 'N'),
('063002', 'SEKOLAH TINGGI ILMU EKONOMI STIKUBANK', 'SEMARANG\r', 'N'),
('063003', 'SEKOLAH TINGGI ILMU EKONOMI ANINDYAGUNA', 'SEMARANG\r', 'N'),
('063004', 'SEKOLAH TINGGI ILMU EKONOMI SURAKARTA', 'SURAKARTA\r', 'N'),
('063005', 'SEKOLAH TINGGI ILMU EKONOMI SATRIA', 'PURWOKERTO\r', 'N'),
('063006', 'SEKOLAH TINGGI ILMU EKONOMI ATMA BHAKTI', 'SURAKARTA\r', 'N'),
('063007', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA MANGGALA', 'SEMARANG\r', 'N'),
('063008', 'SEKOLAH TINGGI ILMU EKONOMI DHARMA PUTRA', 'SEMARANG\r', 'N'),
('063009', 'SEKOLAH TINGGI ILMU PERTANIAN FARMING', 'SEMARANG\r', 'N'),
('063010', 'SEKOLAH TINGGI ILMU EKONOMI PARIWISATA INDONESIA', 'SEMARANG\r', 'N'),
('063011', 'SEKOLAH TINGGI ILMU EKONOMI CENDEKIA KARYA UTAMA', 'SEMARANG\r', 'N'),
('063012', 'SEKOLAH TINGGI ILMU EKONOMI ST PIGNATELLI', 'SURAKARTA\r', 'N'),
('063013', 'SEKOLAH TINGGI ILMU EKONOMI BANK BPD JAWA TENGAH', 'SEMARANG\r', 'N'),
('063014', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA MANGGALIA', 'BREBES\r', 'N'),
('063015', 'SEKOLAH TINGGI ILMU EKONOMI NAHDLATUL ULAMA', 'JEPARA\r', 'N'),
('063016', 'SEKOLAH TINGGI ILMU EKONOMI TRIANANDRA', 'KARTASURA\r', 'N'),
('063017', 'SEKOLAH TINGGI BAHASA ASING SATYA WACANA', 'SALATIGA\r', 'N'),
('063018', 'SEKOLAH TINGGI ILMU EKONOMI SWASTA MANDIRI', 'SURAKARTA\r', 'N'),
('063019', 'SEKOLAH TINGGI BAHASA ASING 17 AGUSTUS 1945', 'SEMARANG\r', 'N'),
('063020', 'STMIK WIDYA UTAMA', 'PURWOKERTO\r', 'N'),
('063021', 'STMIK BINA PATRIA', 'MAGELANG\r', 'N'),
('063022', 'SEKOLAH TINGGI ILMU EKONOMI AMA', 'SALATIGA\r', 'N'),
('063023', '\"SEKOLAH TINGGI MANAJEMEN TRANSPOR \"\"AMNI\"\"\"', 'SEMARANG\r', 'N'),
('063024', 'SEKOLAH TINGGI ILMU EKONOMI ADI UNGGUL BHIRAWA', 'SURAKARTA\r', 'N'),
('063025', 'SEKOLAH TINGGI ILMU EKONOMI SEMARANG', 'SEMARANG\r', 'N'),
('063026', 'SEKOLAH TINGGI ILMU EKONOMI MANGUNKARSA', 'SEMARANG\r', 'N'),
('063027', 'SEKOLAH TINGGI ILMU EKONOMI WIJAYA MULYA', 'SURAKARTA\r', 'N'),
('063028', 'SEKOLAH TINGGI ILMU EKONOMI PELITA NUSANTARA', 'SEMARANG\r', 'N'),
('063029', 'SEKOLAH TINGGI MANGKUBUMI', 'SURAKARTA\r', 'N'),
('063030', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"ASSHOLEH\"\"\"', 'PEMALANG\r', 'N'),
('063031', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"YPPI\"\"\"', 'REMBANG\r', 'N'),
('063032', '\"SEKOLAH TINGGI ILMU FARMASI \"\"YAYASAN PHARMASI\"\"\"', 'SEMARANG\r', 'N'),
('063033', 'SEKOLAH TINGGI ILMU EKONOMI AKI', 'SEMARANG\r', 'N'),
('063034', 'SEKOLAH TINGGI BAHASA ASING AKI', 'SEMARANG\r', 'N'),
('063035', 'STMIK AKI', 'SEMARANG\r', 'N'),
('063036', 'SEKOLAH TINGGI ILMU KESEHATAN NGUDI WALUYO', 'SEMARANG\r', 'N'),
('063037', 'SEKOLAH TINGGI ILMU EKONOMI PUTRA BANGSA', 'KEBUMEN\r', 'N'),
('063038', 'SEKOLAH TINGGI ILMU PERIKANAN KALINYAMAT', 'JEPARA\r', 'N'),
('063039', 'SEKOLAH TINGGI ILMU EKONOMI RAJAWALI', 'PUWOREJO\r', 'N'),
('063040', 'STMIK SINAR NUSANTARA', 'SURAKARTA\r', 'N'),
('063041', 'STIMIK BANJARNEGARA', 'BANJARNEGAR\r', 'N'),
('063042', 'STMIK AUB', 'SURAKARTA\r', 'N'),
('063043', 'STMIK WIDYA PRATAMA', 'PEKALONGAN\r', 'N'),
('063044', 'SEKOLAH TINGGI TEKNIK WIWOROTOMO', 'PURWOKERTO\r', 'N'),
('063045', 'SEKOLAH TINGGI ILMU ADMINISTRASI MADANI', 'KLATEN\r', 'N'),
('063046', 'SEKOLAH TINGGI ILMU KESEHATAN HARAPAN BANGSA', 'PURWOKERTO\r', 'N'),
('064001', 'AKADEMI MARITIM NASIONAL INDONESIA', 'SEMARANG\r', 'N'),
('064002', 'AKADEMI BAHASA 17 AGUSTUS 1945 SEMARANG', 'SEMARANG\r', 'N'),
('064003', 'AKADEMI TEKNIK MESIN INDUSTRI SURAKARTA', 'SURAKARTA\r', 'N'),
('064004', 'AKADEMI PELAYARAN NIAGA INDONESIA', 'SEMARANG\r', 'N'),
('064005', 'AKADEMI KEUANGAN DAN AKUNTANSI WIKA J', 'SEMARANG\r', 'N'),
('064006', 'AKADEMI TEKNOLOGI SEMARANG', 'SEMARANG\r', 'N'),
('064007', 'AKADEMI SEKRETARI MARSUDIRINI ST MARIA', 'SEMARANG\r', 'N'),
('064008', 'AKADEMI SEKRETARI DAN MANAJEMEN INDONESIA', 'SURAKARTA\r', 'N'),
('064009', 'AKADEMI BAHASA ASING IEC SEMARANG', 'SEMARANG\r', 'N'),
('064010', 'AKADEMI KESEJAHTERAAN SOSIAL IBU KARTINI', 'SEMARANG\r', 'N'),
('064011', 'AKADEMI PERTANIAN HKTI BANYUMAS', 'PURWOKERTO\r', 'N'),
('064012', 'AKADEMI PETERNAKAN KARANGANYAR', 'KARANGANYAR\r', 'N'),
('064013', 'AKADEMI BAHASA ASING ST. PIGNATELLI', 'SURAKARTA\r', 'N'),
('064014', 'AKADEMI KIMIA INDUSTRI SANTO PAULUS SEMARANG', 'SEMARANG\r', 'N'),
('064015', 'AKADEMI TEKNOLOGI RONGGOLAWE CEPU', 'CEPU\r', 'N'),
('064016', 'AKADEMI PERTANIAN MUHAMMADIYAH PEMALANG', 'PEMALANG\r', 'N'),
('064017', 'AKADEMI PERTANIAN PANDANARAN BOYOLALI', 'BOYOLALI\r', 'N'),
('064018', 'AKADEMI MARITIM NUSANTARA CILACAP', 'CILACAP\r', 'N'),
('064019', 'AKADEMI PERDAGANGAN TJENDEKIA PURUHITA', 'SEMARANG\r', 'N'),
('064020', 'AKADEMI MANAJEMEN INDUSTRI SURAKARTA', 'SURAKARTA\r', 'N'),
('064021', 'AMIK VETERAN PURWOKERTO', 'PURWOKERTO\r', 'N'),
('064022', 'AKADEMI PERTANIAN PRAGOLAPATI', 'PATI\r', 'N'),
('064023', 'AKADEMI TEKNOLOGI INDUSTRI VETERAN SEMARANG', 'SEMARANG\r', 'N'),
('064024', 'AMIK HARAPAN BANGSA SURAKARTA', 'SURAKARTA\r', 'N'),
('064025', 'AKADEMI STATISTIKA MUHAMMADIYAH SEMAR', 'SEMARANG\r', 'N'),
('064026', 'AKADEMI TEKNOLOGI WARGA SURAKARTA', 'SURAKARTA\r', 'N'),
('064027', 'AKADEMI PARIWISATA WIDYA NUSANTARA SURAKARTA', 'SURAKARTA\r', 'N'),
('064028', 'AMIK YMI TEGAL', 'TEGAL\r', 'N'),
('064029', 'AKADEMI PERIKANAN KALINYAMAT JEPARA', 'JEPARA\r', 'N'),
('064030', 'AKADEMI TEKNOLOGI INDUSTRI KAYU JEPARA', 'JEPARA\r', 'N'),
('064031', 'AMIK ABDI KARYA INDONESIA', 'PATI\r', 'N'),
('064032', 'AKADEMI AKUNTANSI MUHAMMADIYAH CILACAP', 'CILACAP\r', 'N'),
('064033', 'AKADEMI PERTANIAN PGRI WONOSOBO', 'WONOSOBO\r', 'N'),
('064034', 'AKADEMI AKUNTANSI DIAN KARTIKA', 'SEMARANG\r', 'N'),
('064035', 'AKADEMI TEKNOLOGI INDUSTRI PGRI BLORA', 'BLORA\r', 'N'),
('064036', 'AKADEMI TEKNIK PRIMA AGUS TEKNIK', 'SEMARANG\r', 'N'),
('064037', 'AMIK PGRI KEBUMEN', 'KEBUMEN\r', 'N'),
('064038', 'AKADEMI TEKNIK ADIYASA', 'SURAKARTA\r', 'N'),
('064039', 'AKADEMI TEKNIK PERKAPALAN VETERAN', 'SEMARANG\r', 'N'),
('064040', 'AKADEMI PARIWISATA MANDALA BHAKTI', 'SUKOHARJO\r', 'N'),
('064041', 'AMIK WIDYA PRATAMA', 'PEKALONGAN\r', 'N'),
('064042', 'AKADEMI TEKNIK FAJAR INDONESIA SURAKARTA', 'SUKOHARJO\r', 'N'),
('064043', 'AKADEMI AKUNTANSI DAN PERPAJAKAN BENT', 'SURAKARTA\r', 'N'),
('064044', 'AKADEMI TEKNOLOGI ARIE LASUT', 'SEMARANG\r', 'N'),
('064045', 'AKADEMI PARIWISATA EKA SAKTI', 'PURWOKERTO\r', 'N'),
('064046', 'AKADEMI AKUNTANSI MUHAMMADIYAH KLATEN', 'KLATEN\r', 'N'),
('064047', 'AKADEMI AKUNTANSI MUHAMMADIYAH PEKALONGAN', 'PEKALONGAN\r', 'N'),
('064048', 'AKADEMI AKUNTANSI BUDI LUHUR SEMARANG', 'SEMARANG\r', 'N'),
('064049', 'AMIK CIPTA DARMA', 'SURAKARTA\r', 'N'),
('064050', 'AKADEMI DESAIN SURAKARTA', 'SURAKARTA\r', 'N'),
('064051', 'AKADEMI TEKNIK WACANA MANUNGGAL SEMARANG', 'SEMARANG\r', 'N'),
('064052', 'AKADEMI PERIKANAN DAN KELAUTAN SRI MU', 'CILACAP\r', 'N'),
('064053', 'AKADEMI MANAJEMEN INDONESIA SEMARANG', 'SEMARANG\r', 'N'),
('064054', 'AKADEMI PARIWISATA SAHID', 'SURAKARTA\r', 'N'),
('064055', 'AKADEMI AKUNTANSI EFFENDIHARAHAP', 'SEMARANG\r', 'N'),
('064056', 'AKADEMI PARIWISATA STIKUBANK', 'SEMARANG\r', 'N'),
('064057', 'AKADEMI SENI DAN DESAIN INDONESIA', 'SURAKARTA\r', 'N'),
('064058', 'AKADEMI SEKRETASI DAN MANAJEMEN WIDYA PRATAMA', 'PEKALONGAN\r', 'N'),
('064059', 'AKADEMI SEKRETARI DAN MANAJEMEN SANTA', 'BOYOLALI\r', 'N'),
('064060', '\"AKADEMI TEKNIK \"\"PRASETYA 28\"\"\"', 'UNGARAN\r', 'N'),
('064061', 'AKADEMI TEKNIK TIRTA WIYATA', 'MAGELANG\r', 'N'),
('064062', 'AKADEMI ANALIS KESEHATAN DHARMA MULIA', 'PEKALONGAN\r', 'N'),
('064063', 'AKADEMI BAHASA ASING HARAPAN BANGSA', 'SURAKARTA\r', 'N'),
('064064', '\"AKADEMI PERIKANAN \"\"BARUNA\"\"\"', 'SLAWI\r', 'N'),
('064065', 'AKADEMI KEBIDANAN MAMBA\'UL ULUM SURAKARTA', 'SURAKARTA\r', 'N'),
('064066', 'AMIK JAKARTA TEKNOLOGI CIPTA', 'SEMARANG\r', 'N'),
('064067', 'AKADEMI KEBIDANAN KUSUMA HUSADA', 'SURAKARTA\r', 'N'),
('064068', 'AKADEMI KEPERAWATAN KUSUMA HUSADA', 'SURAKARTA\r', 'N'),
('064069', 'AKADEMI KEPERAWATAN BHAKTI MULIA', 'SUKOHARJO\r', 'N'),
('064070', 'AKADEMI FARMASI BHAKTI MULIA', 'SUKOHARJO\r', 'N'),
('064071', 'AKADEMI KEBIDANAN ESTU UTOMO', 'BOYOLALI\r', 'N'),
('064072', 'AKADEMI KEBIDANAN NGUDI WALUYO', 'SEMARANG\r', 'N'),
('064073', 'AKADEMI BAHASA ASING PRAWIRA MARTHA', 'SUKOHARJO\r', 'N'),
('064074', 'AKADEMI KEBIDANAN AN-NUR', 'PURWODADI\r', 'N'),
('064075', 'AKADEMI KEBIDANAN GIRI SATRIA HUSADA', 'WONOGIRI\r', 'N'),
('064076', 'AKADEMI TEKNOLOGI AUB', 'SURAKARTA\r', 'N'),
('064077', 'AKADEMI BAHASA ASING IEC PUTRA BANGSA', 'TEGAL\r', 'N'),
('064078', 'AKADEMI TEKNIK TELEKOMUNIKASI', 'PURWOKERTO\r', 'N'),
('064079', 'AKADEMI KEBIDANAN YLPP', 'PURWOKERTO\r', 'N'),
('064080', '\"AKADEMI BAHASA ASING \"\"RA.KARTINI\"\"\"', 'SURAKARTA\r', 'N'),
('064081', 'AKADEMI KEPERAWATAN MUHAMMADIYAH KUDUS', 'KUDUS\r', 'N'),
('064082', 'AKADEMI KEPERAWATAN KARYA HUSADA', 'SEMARANG\r', 'N'),
('065001', 'POLITEKNIK JAWA DWIPA', 'UNGARAN\r', 'N'),
('065002', 'POLITEKNIK PRATAMA MULIA', 'SURAKARTA\r', 'N'),
('065003', 'POLITEKNIK MUHAMMADIYAH KARANGANYAR', 'KARANGANYAR\r', 'N'),
('065004', 'POLITEKNIK MUHAMMADIYAH MAGELANG', 'MAGELANG\r', 'N'),
('065005', 'POLITEKNIK SURAKARTA', 'SURAKARTA\r', 'N'),
('065006', 'POLITEKNIK PRATAMA', 'SURAKARTA\r', 'N'),
('065007', 'POLITEKNIK MA\'ARIF', 'PURWOKERTO\r', 'N'),
('065008', 'POLITEKNIK SAWUNGGALIH AJI', 'KUTOARJO\r', 'N'),
('065009', 'POLITEKNIK PEMALANG', 'PEMALANG\r', 'N'),
('065010', 'POLITEKNIK DHARMA PATRIA', 'KEBUMEN\r', 'N'),
('065011', '\"POLITEKNIK \"\"MEGATEK\"\"\"', 'PURWOREJO\r', 'N'),
('065012', 'POLITEKNIK HARAPAN BERSAMA', 'TEGAL\r', 'N'),
('065013', 'POLITEKNIK INDONUSA', 'SURAKARTA\r', 'N'),
('065014', 'POLITEKNIK PUSMANU', 'PEKALONGAN\r', 'N'),
('071001', 'UNIVERSITAS 17 AGUSTUS 1945 SURABAYA', 'SURABAYA\r', 'N'),
('071002', 'UNIVERSITAS KRISTEN PETRA', 'SURABAYA\r', 'N'),
('071003', 'UNIVERSITAS KATOLIK WIDYA MANDALA SURABAYA', 'SURABAYA\r', 'N'),
('071004', 'UNIVERSITAS SURABAYA', 'SURABAYA\r', 'N'),
('071005', 'UNIVERSITAS DR. SOETOMO', 'SURABAYA\r', 'N'),
('071006', 'UNIVERSITAS MERDEKA SURABAYA', 'SURABAYA\r', 'N'),
('071007', 'UNIVERSITAS SUNAN GIRI', 'SIDOARJO\r', 'N'),
('071008', 'UNIVERSITAS NAROTAMA', 'SURABAYA\r', 'N'),
('071009', 'UNIVERSITAS WIJAYA KUSUMA', 'SURABAYA\r', 'N'),
('071010', 'UNIVERSITAS BHAYANGKARA', 'SURABAYA\r', 'N'),
('071011', 'UNIVERSITAS WIJAYA PUTRA', 'SURABAYA\r', 'N'),
('071012', 'UNIVERSITAS MUHAMMADIYAH SURABAYA', 'SURABAYA\r', 'N'),
('071013', 'UNIVERSITAS YOS SUDARSO', 'SURABAYA\r', 'N'),
('071014', 'UNIVERSITAS W R SUPRATMAN', 'SURABAYA\r', 'N'),
('071015', 'UNIVERSITAS PUTRA BANGSA', 'SURABAYA\r', 'N'),
('071016', 'UNIVERSITAS 45', 'SURABAYA\r', 'N'),
('071017', 'UNIVERSITAS WIDYA KARTIKA', 'SURABAYA\r', 'N'),
('071018', 'UNIVERSITAS KARTINI', 'SURABAYA\r', 'N'),
('071019', 'UNIVERISTAS KATOLIK DARMA CENDIKA', 'SURABAYA\r', 'N'),
('071020', 'UNIVERSITAS GRESIK', 'GRESIK\r', 'N'),
('071021', 'UNIVERSITAS JENGGALA', 'SIDOARJO\r', 'N'),
('071022', 'UNIVERSITAS MAYJEN SUNGKONO', 'MOJOKERTO\r', 'N'),
('071023', 'UNIVERSITAS DARUL \'ULUM', 'JOMBANG\r', 'N'),
('071024', 'UNIVERSITAS MUHAMMADIYAH MALANG', 'MALANG\r', 'N'),
('071025', 'UNIVERSITAS MERDEKA MALANG', 'MALANG\r', 'N'),
('071026', 'UNIVERSITAS KATOLIK WIDYA KARYA', 'MALANG\r', 'N'),
('071027', 'UNIVERSITAS ISLAM MALANG', 'MALANG\r', 'N'),
('071028', 'UNIVERSITAS WISNUWARDHANA', 'MALANG\r', 'N'),
('071029', 'UNIVERSITAS KRISTEN CIPTA WACANA', 'MALANG\r', 'N'),
('071030', 'UNIVERSITAS WIDYA GAMA', 'MALANG\r', 'N'),
('071031', 'UNIVERSITAS MERDEKA PASURUAN', 'PASURUAN\r', 'N'),
('071032', 'UNIVERSITAS MUHAMMADIYAH JEMBER', 'JEMBER\r', 'N'),
('071033', 'UNIVERSITAS MOCHAMMAD SROEDJI', 'JEMBER\r', 'N'),
('071034', 'UNIVERSITAS ISLAM JEMBER', 'JEMBER\r', 'N'),
('071035', 'UNIVERSITAS 17 AGUSTUS 1945 BANYUWANGI', 'BANYUWANGI\r', 'N'),
('071036', 'UNIVERSITAS ABDURACHMAN SALEH', 'SITUBONDO\r', 'N'),
('071037', 'UNIVERSITAS BONDOWOSO', 'BONDOWOSO\r', 'N'),
('071038', 'UNIVERSITAS PANCA MARGA', 'PROBOLINGGO\r', 'N'),
('071039', 'UNIVERSITAS KADIRI', 'KEDIRI\r', 'N'),
('071040', 'UNIVERSITAS ISLAM KADIRI', 'KEDIRI\r', 'N'),
('071041', 'UNIVERSITAS TULUNGAGUNG', 'TULUNGAGUNG\r', 'N'),
('071042', 'UNIVERSITAS MERDEKA MADIUN', 'MADIUN\r', 'N'),
('071043', 'UNIVERSITAS MERDEKA PONOROGO', 'PONOROGO\r', 'N'),
('071044', 'UNIVERSITAS MUHAMMADIYAH PONOROGO', 'PONOROGO\r', 'N'),
('071045', 'UNIVERSITAS SOERJO', 'NGAWI\r', 'N'),
('071046', 'UNIVERSITAS BOJONEGORO', 'BOJONEGORO\r', 'N'),
('071047', 'UNIVERSITAS SUNAN BONANG', 'TUBAN\r', 'N'),
('071048', 'UNIVERSITAS MADURA', 'PAMEKASAN\r', 'N'),
('071049', 'UNIVERSITAS PGRI ADI BUANA', 'SURABAYA\r', 'N'),
('071050', 'UNIVERSITAS KANJURUHAN', 'MALANG\r', 'N'),
('071051', 'UNIVERSITAS GAJAYANA', 'MALANG\r', 'N'),
('071052', 'UNIVERSITAS PAWYATAN DAHA', 'KEDIRI\r', 'N'),
('071053', 'UNIVERSITAS KATOLIK WIDYA MANDALA MADIUN', 'MADIUN\r', 'N'),
('071054', 'UNIVERSITAS TRI TUNGGAL', 'SURABAYA\r', 'N'),
('071055', 'UNIVERSITAS LUMAJANG', 'LUMAJANG\r', 'N'),
('071056', 'UNIVERSITAS HANG TUAH', 'SURABAYA\r', 'N'),
('071057', 'UNIVERSITAS BAPTIS', 'SURABAYA\r', 'N'),
('071058', 'UNIVERSITAS WIRARAJA', 'SUMENEP\r', 'N'),
('071059', 'UNIVERSITAS MUHAMMADIYAH GRESIK', 'GRESIK\r', 'N'),
('071060', 'UNIVERSITAS MUHAMMADIYAH SIDOARJO', 'SIDOARJO\r', 'N'),
('071061', 'UNIVERSITAS TRIBHUWANA TUNGGA DEWI', 'MALANG\r', 'N'),
('071062', 'UNIVERSITAS ISLAM DARUL \'ULUM', 'LAMONGAN\r', 'N'),
('071063', 'UNIVERSITAS AL-FALAH', 'SURABAYA\r', 'N'),
('071064', 'UPN VETERAN JAWA TIMUR', 'SURABAYA\r', 'N'),
('071065', 'UNIVERSITAS PESANTREN TINGGI DARUL \'ULUM', 'JOMBANG\r', 'N'),
('071066', 'UNIVERSITAS ISLAM MAJAPAHIT', 'MOJOKERTO\r', 'N'),
('071067', 'UNIVERSITAS ISLAM SUNAN GIRI', 'LAMONGAN\r', 'N'),
('071068', 'UNIVERSITAS ISLAM MADURA', 'PAMEKASAN\r', 'N'),
('071069', 'UNIVERSITAS YUDHARTA PASURUAN', 'PASURUAN\r', 'N'),
('072001', 'INSTITUT TEKNOLOGI PEMBANGUNAN SURABAYA', 'SURABAYA\r', 'N'),
('072002', 'INSTITUT TEKNOLOGI ADI TAMA SURABAYA', 'SURABAYA\r', 'N'),
('072003', 'IKIP WIDYA DARMA', 'SURABAYA\r', 'N'),
('072004', 'INSTITUT TEKNOLOGI NASIONAL', 'MALANG\r', 'N'),
('072005', 'IKIP BUDI UTOMO', 'MALANG\r', 'N'),
('072006', 'INSTITUT PERTANIAN MALANG', 'MALANG\r', 'N'),
('072007', 'IKIP PGRI JEMBER', 'JEMBER\r', 'N'),
('072008', 'IKIP PGRI BANYUWANGI', 'BANYUWANGI\r', 'N'),
('072009', 'IKIP PGRI KEDIRI', 'KEDIRI\r', 'N'),
('072010', 'IKIP PGRI MADIUN', 'MADIUN\r', 'N'),
('072011', 'IKIP PGRI BOJONEGORO', 'BOJONEGORO\r', 'N'),
('072012', 'IKIP PGRI TUBAN', 'TUBAN\r', 'N'),
('072013', 'INSTITUT SAINS & TEKNOLOGI PALAPA', 'MALANG\r', 'N'),
('073001', 'SEKOLAH TINGGI ILMU EKONOMI INDONESIA (STIESIA)', 'SURABAYA\r', 'N'),
('073002', 'SEKOLAH TINGGI ILMU EKONOMI PERBANAS SURABAYA', 'SURABAYA\r', 'N'),
('073003', 'SEKOLAH TINGGI KESENIAN WILWATIKTA', 'SURABAYA\r', 'N'),
('073004', 'SEKOLAH TINGGI ILMU EKONOMI MAHARDHIKA', 'SURABAYA\r', 'N'),
('073005', 'SEKOLAH TINGGI TEKNIK SURABAYA', 'SURABAYA\r', 'N'),
('073006', 'SEKOLAH TINGGI ILMU EKONOMI URIP SUMOHARDJO', 'SURABAYA\r', 'N'),
('073007', 'SEKOLAH TINGGI ILMU EKONOMI ARTHA BODHI ISWARA', 'SURABAYA\r', 'N'),
('073008', 'SEKOLAH TINGGI ILMU EKONOMI PARIWISATA SATYA WIDYA', 'SURABAYA\r', 'N'),
('073009', 'STKIP YBPK', 'SURABAYA\r', 'N'),
('073010', 'STIKOSA AWS PRAPANCA', 'SURABAYA\r', 'N'),
('073011', 'SEKOLAH TINGGI BAHASA ASING SATYA WIDYA', 'SURABAYA\r', 'N'),
('073012', 'STKIP TRI BHUWANA', 'SURABAYA\r', 'N'),
('073013', 'SEKOLAH TINGGI TEKNIK RAJASA', 'SURABAYA\r', 'N'),
('073014', 'SEKOLAH TINGGI MANAJEMEN INFORMATIKA DAN KOMPUTER', 'SURABAYA\r', 'N'),
('073015', 'SEKOLAH TINGGI TEKNIK RADEN WIJAYA', 'MOJOKERTO\r', 'N'),
('073016', 'STKIP PGRI MOJOKERTO', 'MOJOKERTO\r', 'N'),
('073017', 'STKIP PGRI JOMBANG', 'JOMBANG\r', 'N'),
('073018', 'SEKOLAH TINGGI ILMU EKONOMI JAYA NEGARA', 'MALANG\r', 'N'),
('073019', 'SEKOLAH TINGGI ILMU EKONOMI MALANGKUCECWARA', 'MALANG\r', 'N'),
('073020', 'SEKOLAH TINGGI FILSAFAT THEOLOGIA WIDYA SASANA', 'MALANG\r', 'N'),
('073021', 'SEKOLAH TINGGI ILMU HUKUM SUNAN GIRI', 'MALANG\r', 'N'),
('073022', 'SEKOLAH TINGGI ILMU EKONOMI KERTANEGARA', 'MALANG\r', 'N'),
('073023', 'SEKOLAH TINGGI ILMU ADMINISTRASI MALANG', 'MALANG\r', 'N'),
('073024', 'SEKOLAH TINGGI ILMU EKONOMI INDONESIA (STIMI)', 'MALANG\r', 'N'),
('073025', 'STMIK INDONESIA', 'MALANG\r', 'N'),
('073026', 'SEKOLAH TINGGI SOSIAL POLITIK WASKITA DARMA', 'MALANG\r', 'N'),
('073027', 'STKIP PGRI PASURUAN', 'PASURUAN\r', 'N'),
('073028', 'SEKOLAH TINGGI ILMU EKONOMI DHARMA NASIONAL', 'JEMBER\r', 'N'),
('073029', 'SEKOLAH TINGGI ILMU PERTANIAN', 'JEMBER\r', 'N'),
('073030', 'SEKOLAH TINGGI ILMU EKONOMI MANDALA', 'JEMBER\r', 'N'),
('073031', 'SEKOLAH TINGGI ILMU EKONOMI KOSGORO', 'JEMBER\r', 'N'),
('073032', 'STKIP PGRI SITUBONDO', 'SITUBONDO\r', 'N'),
('073033', 'STKIP PGRI LUMAJANG', 'LUMAJANG\r', 'N'),
('073034', 'SEKOLAH TINGGI ILMU HUKUM JENDERAL SUDIRMAN', 'LUMAJANG\r', 'N'),
('073035', 'STKIP MUHAMMADIYAH', 'LUMAJANG\r', 'N'),
('073036', 'STKIP PGRI PROBOLINGGO', 'PROBOLINGGO\r', 'N'),
('073037', 'SEKOLAH TINGGI ILMU HUKUM ZAINUL HASAN', 'PROBOLINGGO\r', 'N'),
('073038', 'SEKOLAH TINGGI ILMU ADMINISTRASI BAYUANGGA', 'PROBOLINGGO\r', 'N'),
('073039', 'STKIP PGRI BLITAR', 'BLITAR\r', 'N'),
('073040', 'STKIP PGRI TULUNGAGUNG', 'TULUNGAGUNG\r', 'N'),
('073041', 'STKIP PGRI NGANJUK', 'NGANJUK\r', 'N'),
('073042', 'STKIP WIDYA YUWANA', 'MADIUN\r', 'N'),
('073043', 'STISIP MUHAMMADIYAH MADIUN', 'MADIUN\r', 'N'),
('073044', 'STKIP PGRI PONOROGO', 'PONOROGO\r', 'N'),
('073045', 'STKIP PGRI TRENGGALEK', 'TRENGGALEK\r', 'N'),
('073046', 'STKIP PGRI NGAWI', 'NGAWI\r', 'N'),
('073047', 'STKIP PGRI MAGETAN', 'MAGETAN\r', 'N'),
('073048', 'STKIP PGRI LAMONGAN', 'LAMONGAN\r', 'N'),
('073049', 'STKIP PRGI BANGKALAN', 'BANGKALAN\r', 'N'),
('073050', 'STKIP PGRI SAMPANG', 'SAMPANG\r', 'N'),
('073051', 'STKIP PGRI SUMENEP', 'SUMENEP\r', 'N'),
('073052', 'SEKOLAH TINGGI ILMU ADMINISTRASI PANGLIMA SUDIRMAN', 'SURABAYA\r', 'N'),
('073053', 'SEKOLAH TINGGI PARIWISATA SATYA WIDYA', 'SURABAYA\r', 'N'),
('073054', 'SEKOLAH TINGGI ILMU EKONOMI WILWATIKTA', 'SURABAYA\r', 'N'),
('073055', 'SEKOLAH TINGGI BAHASA ASING', 'MALANG\r', 'N'),
('073056', 'SEKOLAH TINGGI ILMU EKONOMI KOPERASI', 'MALANG\r', 'N'),
('073057', 'SEKOLAH TINGGI TEKNIK MALANG', 'MALANG\r', 'N'),
('073058', 'SEKOLAH TINGGI ILMU ADMINISTRASI PEMBANGUNAN', 'JEMBER\r', 'N'),
('073059', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA ARTHA', 'SURABAYA\r', 'N'),
('073060', 'SEKOLAH TINGGI TEKNIK INDUSTRI TUREN', 'MALANG\r', 'N'),
('073061', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA GAMA', 'LUMAJANG\r', 'N'),
('073062', 'SEKOLAH TINGGI TEKNOLOGI & KEJURUAN', 'SURABAYA\r', 'N'),
('073063', 'SEKOLAH TINGGI ILMU EKONOMI KUSUMANEGARA', 'BLITAR\r', 'N'),
('073064', 'STKIP PGRI SIDOARJO', 'SIDOARJO\r', 'N'),
('073065', 'STKIP PGRI PACITAN', 'PACITAN\r', 'N'),
('073066', 'STIMIK YADIKA BANGIL', 'PASURUAN\r', 'N'),
('073067', 'SEKOLAH TINGGI ILMU PERIKANAN', 'MALANG\r', 'N'),
('073068', 'SEKOLAH TINGGI ILMU EKONOMI BHAKTI PERTIWI', 'JEMBER\r', 'N'),
('073069', 'SEKOLAH TINGGI ILMU EKONOMI IEU', 'SURABAYA\r', 'N'),
('073070', 'SEKOLAH TINGGI TEKNIK BUDI UTOMO', 'MALANG\r', 'N'),
('073071', 'SEKOLAH TINGGI ILMU EKONOMI KEDIRI', 'KEDIRI\r', 'N'),
('073072', 'SEKOLAH TINGGI ILMU KOMPUTER PGRI BANYUWANGI', 'BANYUWANGI\r', 'N'),
('073073', 'SEKOLAH TINGGI TEKNIK CAKRAWALA', 'MALANG\r', 'N'),
('073074', 'SEKOLAH TINGGI ILMU EKONOMI CAKRAWALA', 'MALANG\r', 'N'),
('073075', 'SEKOLAH TINGGI ILMU EKONOMI IBMT', 'SURABAYA\r', 'N'),
('073076', 'SEKOLAH TINGGI ILMU EKONOMI NGANJUK', 'NGANJUK\r', 'N'),
('073077', 'SEKOLAH TINGGI ILMU TEOLOGI WIDYA DARMA', 'SURABAYA\r', 'N'),
('073078', 'SEKOLAH TINGGI TEKNIK NGANJUK', 'NGANJUK\r', 'N'),
('073079', 'SEKOLAH TINGGI ILMU EKONOMI WIDYA DARMA', 'SURABAYA\r', 'N'),
('073080', 'SEKOLAH TINGGI ILMU EKONOMI DEWANTARA', 'JOMBANG\r', 'N'),
('073081', 'SEKOLAH TINGGI TEKNIK CAHAYA SURYA', 'KEDIRI\r', 'N'),
('073082', 'SEKOLAH TINGGI ILMU EKONOMI BUDI UTOMO', 'MALANG\r', 'N'),
('073083', 'SEKOLAH TINGGI PERIKANAN BANYUWANGI', 'BANYUWANGI\r', 'N'),
('073084', 'SEKOLAH TINGGI ILMU EKONOMI DHARMA ISWARA', 'MADIUN\r', 'N'),
('073085', 'SEKOLAH TINGGI TEKNIK DHARMA ISWARA', 'MADIUN\r', 'N'),
('073086', 'SEKOLAH TINGGI ILMU EKONOMI FATAHILLAH', 'SURABAYA\r', 'N'),
('073087', 'SEKOLAH TINGGI ILMU EKONOMI MUHAMMADIYAH LAMONGAN', 'LAMONGAN\r', 'N'),
('073088', 'STIAMAK BARUNAWATI', 'SURABAYA\r', 'N'),
('073089', 'SEKOLAH TINGGI ILMU EKONOMI CENDEKIA', 'BOJONEGORO\r', 'N'),
('073090', 'SEKOLAH TINGGI ILMU EKONOMI KH AHMAD DAHLAN', 'LAMONGAN\r', 'N'),
('073091', 'SEKOLAH TINGGI TEKNIK PGRI KEDIRI', 'KEDIRI\r', 'N'),
('073092', '\"SEKOLAH TINGGI TEKNIK \"\"YPM\"\" SEPANJANG\"', 'SIDOARJO\r', 'N'),
('073093', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"YPM\"\" SEPANJANG\"', 'SIDOARJO\r', 'N'),
('073094', 'SEKOLAH TINGGI ILMU EKONOMI AL-ANWAR', 'MOJOKERTO\r', 'N'),
('073095', 'SEKOLAH TINGGI ILMU EKONOMI WAHIDIYAH', 'KEDIRI\r', 'N'),
('073096', 'SEKOLAH TINGGI TEKNIK STIKMA INTERNASIONAL', 'MALANG\r', 'N'),
('073097', 'SEKOLAH TINGGI TEKNIK NURUL JADID', 'PROBOLINGGO\r', 'N'),
('073098', 'SEKOLAH TINGGI ILMU EKONOMI NU TRATE', 'GRESIK\r', 'N'),
('073099', 'SEKOLAH TINGGI ILMU EKONOMI CANDA BHIRAWA', 'KEDIRI\r', 'N'),
('073100', 'SEKOLAH TINGGI TEKNIK ATLAS NUSANTARA', 'MALANG\r', 'N'),
('073101', 'SEKOLAH TINGGI ILMU EKONOMI PRIMA VISI', 'SURABAYA\r', 'N'),
('073102', 'SEKOLAH TINGGI ILMU EKONOMI PEMNAS INDONESIA', 'MALANG\r', 'N'),
('073103', 'SEKOLAH TINGGI ILMU EKONOMI PRAKARTI MULYA', 'SURABAYA\r', 'N'),
('073104', 'STIMIK PPKIA PRADNYA PARAMITA', 'MALANG\r', 'N'),
('073105', 'SEKOLAH TINGGI TEKNIK GEMPOL', 'PASURUAN\r', 'N'),
('073106', 'SEKOLAH TINGGI ILMU EKONOMI GEMPOL', 'PASURUAN\r', 'N'),
('073107', 'SEKOLAH TINGGI ILMU EKONOMI MUHAMMADIYAH TUBAN', 'TUBAN\r', 'N'),
('073108', 'SEKOLAH TINGGI ILMU EKONOMI PEMUDA', 'SURABAYA\r', 'N'),
('073109', 'SEKOLAH TINGGI ILMU HUKUM YPM SEPANJANG', 'SIDOARJO\r', 'N'),
('073110', 'SEKOLAH TINGGI ILMU EKONOMI ASIA', 'MALANG\r', 'N'),
('073111', 'SEKOLAH TINGGI ILMU EKONOMI INDOCAKTI', 'MALANG\r', 'N'),
('073112', 'STMIK KADIRI', 'KEDIRI\r', 'N'),
('073113', 'SEKOLAH TINGGI TEKNIK POMOSDA', 'NGANJUK\r', 'N'),
('073114', 'SEKOLAH TINGGI ILMU EKONOMI YAPAN', 'SURABAYA\r', 'N'),
('073115', 'SEKOLAH TINGGI ILMU EKONOMI YADIKA BANGIL', 'PASURUAN\r', 'N'),
('073116', 'STMIK ASIA MALANG', 'MALANG\r', 'N'),
('073117', 'STMIK CAHAYA SURYA', 'KEDIRI\r', 'N'),
('073118', 'SEKOLAH TINGGI BAHASA ASING CAHAYA SURYA', 'KEDIRI\r', 'N'),
('073119', 'STMIK BAHRUL \'ULUM JOMBANG', 'JOMBANG\r', 'N'),
('074001', 'AKADEMI TEKNIK INDUSTRI TEKSTIL SURABAYA', 'SURABAYA\r', 'N'),
('074002', 'AKADEMI SEKRETARI DAN MANAJEMEN INDONESIA', 'SURABAYA\r', 'N'),
('074003', 'AKADEMI TEKNOLOGI INDUSTRI', 'SURABAYA\r', 'N'),
('074004', 'AKADEMI ILMU HUKUM DAN KEPENGACARAAN', 'SURABAYA\r', 'N'),
('074005', 'AKADEMI TEKNIK NASIONAL', 'SIDOARJO\r', 'N'),
('074006', 'AKADEMI BAHASA ASING WEBB', 'SURABAYA\r', 'N'),
('074007', 'AKADEMI MANJEMEN KOPERASI SURYA', 'SURABAYA\r', 'N'),
('074008', 'AMIK PRACTICA', 'SURABAYA\r', 'N'),
('074009', 'AKADEMI BAHASA ASING BINA BUDAYA', 'MALANG\r', 'N'),
('074010', 'AKADEMI SEKRETARI DAN MANAJEMEN ARDJUNA', 'MALANG\r', 'N'),
('074011', 'AKADEMI BAHASA ASING BHAKTI PERTIWI', 'JEMBER\r', 'N'),
('074012', 'AKADEMI MANAJEMEN MUHAMMADIYAH', 'BANYUWANGI\r', 'N'),
('074013', 'AKADEMI MANAJEMEN KOPERASI', 'KEDIRI\r', 'N'),
('074014', 'AKADEMI MANAJEMEN KOPERASI TANTULAR', 'MADIUN\r', 'N'),
('074015', 'AKADEMI ADMINISTRASI MAGETAN (AAM)', 'MAGETAN\r', 'N'),
('074016', 'AKADEMI ADMINISTRASI PEMBANGUNAN', 'BANGKALAN\r', 'N'),
('074017', 'AKADEMI PARIWISATA PRAPANCA', 'SURABAYA\r', 'N'),
('074018', 'AKADEMI MANAJEMEN PERPAJAKAN INDONESIA', 'BLITAR\r', 'N'),
('074019', 'AKADEMI SEKRETARI DAN MANAJEMEN SATYA CENDIKA', 'JEMBER\r', 'N'),
('074020', 'AKADEMI PERIKANAN QOMARUDDIN', 'GRESIK\r', 'N'),
('074021', 'AKADEMI PERIKANAN PGRI', 'TUBAN\r', 'N'),
('074022', 'AKADEMI TEKNIK KARTANEGARA', 'KEDIRI\r', 'N'),
('074023', 'AKADEMI MANAJEMEN INFORMATIKA DAN KOMPUTER JOMBANG', 'JOMBANG\r', 'N'),
('074024', 'AKADEMI KEUANGAN DAN PERBANKAN NITRO NITRO', 'MALANG\r', 'N'),
('074025', 'AKADEMI PETERNAKAN PGRI', 'JEMBER\r', 'N'),
('074026', 'AKADEMI AKUNTANSI PGRI', 'JEMBER\r', 'N'),
('074027', 'AKADEMI PARIWISATA 17 AGUSTUS 1945 SURABAYA', 'SURABAYA\r', 'N'),
('074028', 'AKADEMI PARIWISATA ARDJUNA', 'MALANG\r', 'N'),
('074029', 'AKADEMI PARIWISATA BHAKTI WIYATA', 'KEDIRI\r', 'N'),
('074030', 'AKADEMI PARIWISATA MUHAMMADIYAH', 'JEMBER\r', 'N'),
('074031', 'AKADEMI SEKRETARI WIDYA MANDALA SURABAYA', 'SURABAYA\r', 'N'),
('074032', 'AKADEMI PARIWISATA MAJAPAHIT', 'MOJOKERTO\r', 'N'),
('074033', 'AKADEMI MANAJEMEN INFORMATIKA DAN KOMPUTER TARUNA', 'PROBOLINGGO\r', 'N'),
('074034', 'AMIK AJI JAYA BAYA', 'KEDIRI\r', 'N'),
('074035', 'AKADEMI MANAJEMEN INFORMATIKA & KOMPUTER IBRAHIMY', 'SITUBONDO\r', 'N'),
('074036', 'AKADEMI PERIKANAN IBRAHIMY', 'SITUBONDO\r', 'N'),
('074037', 'AKADEMI ANALIS KESEHATAN UNMUH SURABAYA', 'SURABAYA\r', 'N'),
('074038', 'AKPER BAHRUL \'ULUM JOMBANG', 'JOMBANG\r', 'N'),
('074039', 'AKADEMI SEKRETARI WIDYA MANDALA MADIUN', 'MADIUN\r', 'N'),
('074040', 'AKADEMI KEPERAWATAN DIAN HUSADA', 'MOJOKERTO\r', 'N'),
('074041', 'AKADEMI KEBIDANAN DARUL \'ULUM JOMBANG', 'JOMBANG\r', 'N'),
('074042', 'AKADEMI KEBIDANAN HUSADA JOMBANG', 'JOMBANG\r', 'N'),
('075001', 'POLITEKNIK CAHAYA SURYA', 'KEDIRI\r', 'N'),
('075002', 'POLITEKNIK NSC', 'SURABAYA\r', 'N'),
('075003', 'POLITEKNIK UBAYA', 'SURABAYA\r', 'N'),
('075004', 'POLITEKNIK SURABAYA', 'SURABAYA\r', 'N'),
('075005', 'POLITEKNIK UNISMA MALANG', 'MALANG\r', 'N'),
('075006', 'POLITEKNIK KESEHATAN MAJAPAHIT', 'MOJOKERTO\r', 'N'),
('081001', 'UNIVERSITAS MAHENDRADATA DENPASAR', '\r', 'N'),
('081002', 'UNIVERSITAS NGURAH RAI DENPASAR', '\r', 'N'),
('081003', 'UNIVERSITAS MAHASRASWATI DENPASAR', '\r', 'N'),
('081004', 'UNIVERSITAS PENDIDIKAN NASIONAL DPS.', '\r', 'N'),
('081005', 'UNIVERSITAS DWIJENDRA DENPASAR', '\r', 'N'),
('081006', 'UNIVERSITAS TABANAN', '\r', 'N'),
('081007', 'UNIVERSITAS WARMADEWA DENPASAR', '\r', 'N'),
('081008', 'UNIVERSITAS PANJI SAKTI SINGARAJA', '\r', 'N'),
('081009', 'UNIVERSITAS HINDU INDONESIA', '\r', 'N'),
('081010', 'UNIVERSITAS MUHAMMADIYAH MATARAM', '\r', 'N'),
('081011', 'UNIVERSITAS MAHASASRASWATI MATARAM', '\r', 'N'),
('081012', 'UNIVERSITAS ISLAM AL-AZHAR MATARAM', '\r', 'N'),
('081013', 'UNIVERSITAS 45 MATARAM', '\r', 'N'),
('081014', 'UNIVERSITAS NAHDLATUL WATHAN MATARAM', '\r', 'N'),
('081015', 'UNIVERSITAS GUNUNG RINJANI MATARAM', '\r', 'N'),
('081016', 'UNIVERSITAS SAMAWA SUMBAWA', '\r', 'N'),
('081017', 'UNIVERSITAS NUSA TENGGARA BARAT', '\r', 'N'),
('081018', 'UNIV. KATOLIK WIDYA MANDIRA KUPANG', '\r', 'N'),
('081019', 'UNIVERSITAS FLORES', '\r', 'N'),
('081020', 'UNIV.KRISTEN ARTHA WACANA KUPANG', '\r', 'N'),
('081021', 'UNIVERSITAS MUHAMMADIYAH KUPANG', '\r', 'N'),
('081022', 'UNIVERSITAS PGRI KUPANG', '\r', 'N'),
('081023', 'UNIVERSITAS TIMOR', '\r', 'N'),
('081024', 'UNIVERSITAS NUSA LONTAR ROTE', '\r', 'N'),
('082001', 'IKIP SARASWATI TABANAN', '\r', 'N'),
('082002', 'IKIP PGRI BALI', '\r', 'N'),
('082003', 'IKIP  MATARAM', '\r', 'N'),
('083001', 'SEKOLAH TINGGI ILMU ADMINISTRASI', '\r', 'N'),
('083002', 'HANDAYANI DENPASAR', '\r', 'N'),
('083003', 'POLITIK WIRA BHAKTI DENPASAR', '\r', 'N'),
('083004', 'PENDIDIKAN AGAMA HINDU SINGARAJA', '\r', 'N'),
('083005', 'POLITIK MARGARANA TABANAN', '\r', 'N'),
('083006', 'PENDIDIKAN AGAMA HINDU AMLAPURA', '\r', 'N'),
('083007', '\"PENDIDIKAN 45\"\" DENPASAR\"', '\r', 'N'),
('083008', 'DHARMA SINGARAJA', '\r', 'N'),
('083009', 'SEKOLAH TINGGI EKONOMI TRIATMA MULYA', '\r', 'N'),
('083010', '\"SEKOLAH TINGGI ILMU EKONOMI \"\"BALI INTER-\"', '\r', 'N'),
('083011', 'SEKOLAH TINGGI ILMU MANAJEMEN (STIM)', '\r', 'N'),
('083012', 'SEKOLAH TINGGI KEGURUAN DAN ILMU', '\r', 'N'),
('083013', 'SEKOLAH TINGGI KEGURUAN DAN ILMU', '\r', 'N'),
('083014', 'SEKOLAH TINGGI ADMINISTRASI', '\r', 'N'),
('083015', 'SEKOLAH TINGGI ILMU POLITIK MBOJO BIMA', '\r', 'N'),
('083016', 'SEKOLAH TINGGI ILMU EKONOMI  45 MATARM', '\r', 'N'),
('083017', 'SEKOLAH TINGGI ILMU HUKUM', '\r', 'N'),
('083018', 'SEKOLAH TINGGI ILMU ADMINISTRASI MATARM', '\r', 'N'),
('083019', 'SEKOLAH TINGGI MANAJEMEN INFORMATIKA', '\r', 'N'),
('083020', 'SEKOLAH TINGGI ILMU EKONOMI NASIONAL', '\r', 'N'),
('083021', 'SEKOLAH TINGGI ILMU EKONOMI YAPIS', '\r', 'N'),
('083022', 'SEKOLAH TINGGI KESEHATAN MATARAM', '\r', 'N'),
('083023', 'SEKOLAH TINGGI FILSAFAT KATOLIK LEDALERO', '\r', 'N'),
('083024', 'SEKOLAH TINGGI KEGURUAN DAN ILMU', '\r', 'N'),
('083025', 'SEKOLAH TINGGI ILMU MANAJEMEN KUPANG', '\r', 'N'),
('083026', 'SEKOLAH TINGGI ILMU EKONOMI WIRA', '\r', 'N'),
('083027', 'SEKOLAH TINGGI ILMU KOMPUTER UYELINDO', '\r', 'N'),
('083028', 'SEKOLAH TINGGI ILMU EKONOMI (STIE)', '\r', 'N'),
('083029', 'SEKOLAH TINGGI PEMBANGUNAN MASYARAKAT', '\r', 'N'),
('084001', 'AKADEMI AKUNTANSI DENPASAR', '\r', 'N'),
('084002', 'AKADEMI PARIWISATA DENPASAR', '\r', 'N'),
('084003', 'AKADEMI KEUANGAN DAN PERBANKAN', '\r', 'N'),
('084004', 'AKADEMI MANAJEMEN INFORMATIKA DAN', '\r', 'N'),
('084005', 'AKADEMI BAHASA ASING SARASWATI', '\r', 'N'),
('084006', 'AKADEMI BAHASA ASING SINGAMANDAWA', '\r', 'N'),
('084007', 'AKADEMI PARIWISATA TRIATMAJAYA', '\r', 'N'),
('084008', 'AKADEMI MANAJEMEN MATARAM', '\r', 'N'),
('084009', 'AKADEMI MANAJEMEN BIMA', '\r', 'N'),
('084010', 'AKADEMI PARIWISATA MATARAM', '\r', 'N'),
('084011', 'AKADEMI SEKRETARI DAN MANAJEMEN', '\r', 'N'),
('084012', 'ABA BUMI GORA MATARAM', '\r', 'N'),
('084013', 'AKADEMI TEKNIK BIMA', '\r', 'N'),
('084014', 'AKADEMI TEKNIK KUPANG', '\r', 'N'),
('084015', 'AKADEMI ADMINISTRASI PEMBANGUNAN', '\r', 'N'),
('084016', 'AKADEMI BAHASA ASING MENTARI KUPANG', '\r', 'N'),
('084017', 'AKADEMI BAHASA ASING SANTA MARIA', '\r', 'N'),
('084018', 'AKADEMI KEUANGAN DAN PERBANKAN', '\r', 'N'),
('084019', 'AKADEMI PEKERJAAN SOSIAL KUPANG', '\r', 'N'),
('084020', 'AKADEMI PARIWISATA KUPANG', '\r', 'N'),
('084021', 'AKADEMI KOPERASI INDONESIA (AKOPIN)', '\r', 'N'),
('085001', 'POLITEKNIK NASIONAL DENPASAR', '\r', 'N'),
('091001', 'UNIVERSITAS VETERAN  RI', 'MAKASSAR\r', 'N'),
('091002', 'UNIVERSITAS MUSLIM INDONESIA', 'MAKASSAR\r', 'N'),
('091003', 'UNIVERSITAS KRISTEN INDONESIA PAULUS', 'MAKASSAR\r', 'N'),
('091004', 'UNIVERSITAS MUHAMMADIYAH MAKASAR', 'MAKASSAR\r', 'N'),
('091005', 'UNIVERSITAS KRISTEN INDONESIA TOMOHON', 'TOMOHON\r', 'N'),
('091006', 'UNIVERSITAS DAYANU IKHSANUDDIN', 'BAU-BAU\r', 'N'),
('091007', 'UNIVERSITAS KLABAT', 'KLABAT\r', 'N'),
('091008', 'UNIVERSITAS PEPABRI MAKASSAR', 'MAKASSAR\r', 'N'),
('091009', 'UNIVERSITAS ATMA JAYA MAKASSAR', 'MAKASSAR\r', 'N'),
('091010', '\"UNIVERSITAS \"\"45\"\" MAKASSAR\"', 'MAKASSAR\r', 'N'),
('091011', 'UNIVERSITAS MUHAMMADIYAH PALU', 'PALU\r', 'N'),
('091012', 'UNIVERSITAS SINTUWU MAROSO POSO', 'POSO\r', 'N'),
('091013', 'UNIVERSITAS SAWERIGADING MAKASSAR', 'MAKASSAR\r', 'N'),
('091014', 'UNIVERSITAS DUMOGA BONE KOTA MUBAGU', 'MUBAGU\r', 'N'),
('091015', 'UNIVERSITAS SULAWESI TENGGARA', 'KENDARI\r', 'N'),
('091016', 'UNIVERSITAS SATRIA MAKASSAR', 'MAKASSAR\r', 'N'),
('091017', 'UNIVERSITAS PANCASAKTI', 'MAKASSAR\r', 'N'),
('091018', 'UNIVERSITAS AL KHAERAT', 'PALU\r', 'N'),
('091019', 'UNIVERSITAS COKROMINOTO', 'MAKASSAR\r', 'N'),
('091020', 'UNIVERSITAS KRISTEN INDONESIA TORAJA', 'TORAJA\r', 'N'),
('091021', 'UNIVERSITAS LAKIDENDE UNAHAA', 'KENDARI\r', 'N'),
('091022', 'UNIVERSITAS NUSANTARA MANADO', 'MANADO\r', 'N'),
('091023', 'UNIVERSITAS ANDI JEMMA PALOPO', 'PALOPO\r', 'N'),
('091024', 'UNIVERSITAS MUHAMMADIYAH PARE-PARE', 'PARE-PARE\r', 'N'),
('091025', 'UNIVERSITAS MUHAMMADIYAH LUWUK BANGGAI', 'LUWUH BANGGAI\r', 'N'),
('091026', 'UNIVERSITAS TOMPOTIKA LUWUK BANGGAI', 'LUWUH BANGGAI\r', 'N'),
('091027', 'UNIVERSITAS SARI PUTRA TOMOHON', 'TOMOHON\r', 'N'),
('091028', 'UNIVERSITAS ISLAM AL-GAZALI', 'MAKASSAR\r', 'N');
INSERT INTO `perguruantinggi` (`Kode`, `Nama`, `Kota`, `NotActive`) VALUES
('091029', 'UNIVERSITAS KATHOLIK DE LA SALLE', 'MANADO\r', 'N'),
('091030', 'UNIVERSITAS PEMBANGUNAN INDONESIA', 'MANADO\r', 'N'),
('091031', 'UNIVERSITAS GORONTALO LIMBOTO', 'LIMBOTO\r', 'N'),
('091032', 'UNIVERSITAS MUHAMMADIYAH BUTON', 'BUTON\r', 'N'),
('091033', 'UNIVERSITAS ICHSAN GORONTALO', 'GORONTALO\r', 'N'),
('091034', 'UNIVERSITAS INDONESIA TIMUR', 'MAKASSAR\r', 'N'),
('091035', 'UNIVERSITAS MADAKO TOLI-TOLI', 'TOLI-TOLI\r', 'N'),
('091036', 'UNIVERSITAS MUHAMMADIYAH KENDARI', 'MAKASSAR\r', 'N'),
('092001', 'INSTITUT SAINSTEKNOLOGI PEMB. INDONESIA', 'MAKASSAR\r', 'N'),
('092002', 'INSTITUT TEKNOLOGI MINAESA', 'MINAHASA\r', 'N'),
('093001', 'STKIP MUHAMMADIYAH BONE', 'BONE\r', 'N'),
('093002', 'STKIP MUHAMMADIYAH BULUKUMBA', 'BULUKUMBA\r', 'N'),
('093003', 'STKIP MUHAMMADIYAH ENREKANG', 'ENREKANG\r', 'N'),
('093004', 'STKIP MUHAMMADIYAH SIDRAP', 'SIDRAP\r', 'N'),
('093005', 'STIA AL. BAZALI BARRU', 'BARRU\r', 'N'),
('093006', 'STISIPOL 17-8-1945 MAKASSAR', 'MAKASSAR\r', 'N'),
('093007', 'STKIP COKROMINOTO PALOPO', 'PALOPO\r', 'N'),
('093008', 'STKIP COKROMINOTO PINRANG', 'PINRANG\r', 'N'),
('093009', 'STKIP YPPUP MAKASSAR', 'MAKASSAR\r', 'N'),
('093010', 'STKIP MUHAMMADIYAH BARRU', 'BARRU\r', 'N'),
('093011', 'STIA AL. BAZALI SOPPENG', 'SOPPENG\r', 'N'),
('093012', 'STIH AL. GAZALI SOPPENG', 'SOPPENG\r', 'N'),
('093013', 'STKIP DDI POLMAS', 'POLMAS\r', 'N'),
('093014', 'STIKS BONGAYA MAKASSAR', 'MAKASSAR\r', 'N'),
('093015', 'ST. FILSAFAT JAFFRAU MAKASSAR', 'MAKASSAR\r', 'N'),
('093016', 'STIE INDONESIA MAKASSAR', 'MAKASSAR\r', 'N'),
('093017', 'STISIPOL VETERAN PALOPO', 'PALOPO\r', 'N'),
('093018', 'STKIP VETERAN SIDRAP', 'SIDRAP\r', 'N'),
('093019', 'STIE YPPUP MAKASSAR', 'MAKASSAR\r', 'N'),
('093020', 'STIA YAPPI MAKASSAR', 'MAKASSAR\r', 'N'),
('093021', 'STISIPOL MERDEKA MANADO', 'MANADO\r', 'N'),
('093022', 'STIP YAPI PINRANG', 'PINRANG\r', 'N'),
('093023', 'STITEK DHARMA YADI MAKASSAR', 'MAKASSAR\r', 'N'),
('093024', 'STIE HARAPAN KASIH MANADO', 'MANADO\r', 'N'),
('093025', 'STIA PUANGIMAGGALATUNG (PRIMA)', 'SENGKANG\r', 'N'),
('093026', 'STKIP PUANGIMAGGALATUNG (PRIMA)', 'SENGKANG\r', 'N'),
('093027', 'STIEM BONGAYA YPBUP MAKASSAR', 'MAKASSAR\r', 'N'),
('093028', 'STIA PANCAMARGA PALU', 'PALU\r', 'N'),
('093029', 'STISIPPOL PANCA BHAKTI PALU', 'PALU\r', 'N'),
('093030', 'STIE PANCA BHAKTI PALU', 'PALU\r', 'N'),
('093031', 'STIE MUHAMMADIYAH PALOPO', 'PALOPO\r', 'N'),
('093032', 'STF SEMINARI PINELENG', 'PINELENG\r', 'N'),
('093033', 'STIE AMSIR PARE-PARE', 'PARE-PARE\r', 'N'),
('093034', 'STKIP YAPTI JENEPONTO', 'JENEPONTO\r', 'N'),
('093035', 'STIKS MANADO', 'MANADO\r', 'N'),
('093036', 'STIKOM MANADO', 'MANADO\r', 'N'),
('093037', 'STKIP PEMBANGUNAN INDONESIA', 'MAKASSAR\r', 'N'),
('093038', 'STIE PEMBANGUNAN INDONESIA', 'MAKASSAR\r', 'N'),
('093039', 'STIE SULAWESI UTARA', 'MANADO\r', 'N'),
('093040', 'STIA MAJENE (PO)', '\r', 'N'),
('093041', 'STKIP YAPIM MAROS', 'MAROS\r', 'N'),
('093042', 'STKIP 19 NOPEMBER KOLAKA', 'KOLAKA\r', 'N'),
('093043', 'STIE BAJIMINASA MAKASSAR', 'MAKASSAR\r', 'N'),
('093044', 'STIKS TAMALANREA MAKASSAR', 'MAKASSAR\r', 'N'),
('093045', 'STIM LPI MAKASSAR', 'MAKASSAR\r', 'N'),
('093046', 'STIMI YAPMI MAKASSAR', 'MAKASSAR\r', 'N'),
('093047', 'STIE BUDI UTOMO MANADO', 'MANADO\r', 'N'),
('093048', 'STIE LPI MAKASSAR', 'MAKASSAR\r', 'N'),
('093049', 'STIP WUNA RAHA', '\r', 'N'),
('093050', 'STIE YAPMAN MAJENE', 'MAJENE\r', 'N'),
('093051', 'STIE EBEN HAEZER MANADO', 'MANADO\r', 'N'),
('093052', 'STMIK DIPANEGARA MAKASSAR', 'MAKASSAR\r', 'N'),
('093053', 'STIE NUSANTARA MAKASSAR', 'MAKASSAR\r', 'N'),
('093054', 'STIE EL FATAH MANADO', 'MANADO\r', 'N'),
('093055', 'STIP MUJAHIDIN TOLI-TOLI', 'TOLI-TOLI\r', 'N'),
('093056', 'STMIK MATUARI', 'MANADO\r', 'N'),
('093057', 'STKIP PGRI MANADO', 'MANADO\r', 'N'),
('093058', 'STISIPOL PETTA BARINGENG SOPPENG', 'SOPPENG\r', 'N'),
('093059', 'STIE DHARMA BARATA KENDARI', 'KENDARI\r', 'N'),
('093060', 'STIE ENAM ENAM KENDARI', 'KENDARI\r', 'N'),
('093061', 'STIE YAPTI JENEPONTO', 'JENEPONTO\r', 'N'),
('093062', 'STIH DAMARICA PALOPO', 'PALOPO\r', 'N'),
('093063', 'STIP KENDARI', 'KENDARI\r', 'N'),
('093064', 'STIE AMKOP MAKASSAR', 'MAKASSAR\r', 'N'),
('093065', 'STIM NITRO MAKASSAR', 'MAKASSAR\r', 'N'),
('093066', 'STIK TAMALATEA MAKASSAR', 'MAKASSAR\r', 'N'),
('093067', 'STIP COKROMINOTO PALOPO', 'PALOPO\r', 'N'),
('093068', 'STIE PIONER MANADO', 'MANADO\r', 'N'),
('093069', 'STMIK HANDAYANI MAKASSAR', 'MAKASSAR\r', 'N'),
('093070', 'STIP TANRA PATTANA BALI MAMUJU', 'MAMUJU\r', 'N'),
('093071', 'STIKOM FAJAR MAKASSAR', 'MAKASSAR\r', 'N'),
('093072', 'STIM YAPIM MAROS', 'MAROS\r', 'N'),
('093073', 'STIP DDI POLMAS', 'POLMAS\r', 'N'),
('093074', 'ST. TEOLOGIA INTIM MAKASSAR', 'MAKASSAR\r', 'N'),
('093075', 'STIP PUANGRIMANGGALATUNG (PRIMA)', 'SENGKANG\r', 'N'),
('093076', 'STIE REZKY', 'MAKASSAR\r', 'N'),
('093077', 'STIE MUHAMMADIYAH MAMUJU', 'MAMUJU\r', 'N'),
('093078', 'STIE PETRA', 'BITUNG\r', 'N'),
('093079', 'STIE SWADAYA', 'MANADO\r', 'N'),
('093080', 'STIK MAKASSAR', 'MAKASSAR\r', 'N'),
('093081', 'STIE MAKASSAR MAJU', 'MAKASSAR\r', 'N'),
('093082', 'STIE TRI DHARMA NUSANTARA', 'MAKASSAR\r', 'N'),
('093083', 'STIE BINA TARUNA', 'GORONTALO\r', 'N'),
('093084', 'STIE NOBEL MAKASSAR', 'MAKASSAR\r', 'N'),
('093085', 'STIE PARIWISATA', 'MANADO\r', 'N'),
('093086', 'STIE PATRIA ARTHA', 'MAKASSAR\r', 'N'),
('093087', 'STIFA PELITA MAS', 'PALU\r', 'N'),
('093088', 'STIK INDONESIA JAYA', 'PALU\r', 'N'),
('093089', 'STIBA BUMI BERINGIN', 'MANADO\r', 'N'),
('093090', 'STIH PENGAYOMAN', 'BONE\r', 'N'),
('093091', 'STIM PUBLIK MAKASSAR', 'MAKASSAR\r', 'N'),
('093092', 'STITEK BINA TARUNA', 'GORONTALO\r', 'N'),
('093093', 'STMIK KHARISMA', 'MAKASSAR\r', 'N'),
('093094', 'STMIK ICHSAN', 'GORONTALO\r', 'N'),
('093095', 'STIE ICHSAN', 'GORONTALO\r', 'N'),
('093096', 'STISIPOL MUHAMMADIYAH SIDRAP', 'SIDRAP\r', 'N'),
('093097', 'STKIP ANDI MATTAPPA', 'PANGKEP\r', 'N'),
('093098', 'STIH LAMADUKKELLENG', 'SENGKANG\r', 'N'),
('093099', 'STIE LAMADUKKELLENG', 'SENGKANG\r', 'N'),
('093100', 'STIE WIRA BHAKTI', 'MAKASSAR\r', 'N'),
('093101', 'STITEK BARAMULI', 'PINRANG\r', 'N'),
('093102', 'STIE WIDYA DHARMA KOTA MUBAGU', 'MUBAGU\r', 'N'),
('093103', 'STMIK BINA MULYA', 'PALU\r', 'N'),
('093104', 'STIE LAMAPPOLEONRO', 'SOPPENG\r', 'N'),
('093105', 'STITEK DIRGANTARA', 'MAKASSAR\r', 'N'),
('093106', 'STIA PEMBANGUNAN', 'PALU\r', 'N'),
('093107', 'STIH AMSIR', 'PARE-PARE\r', 'N'),
('093108', 'STIE YAPI', 'PANGKEP\r', 'N'),
('093109', 'STIP YAPI', 'PANGKEP\r', 'N'),
('093110', 'STIA PRIMA', 'BONE\r', 'N'),
('093111', 'STMIK ADHI GUNA', 'PALU\r', 'N'),
('093112', 'STIK SVICENNA', 'KENDARI\r', 'N'),
('093113', 'STI KEPERAWATAN FAMIKA', 'GOWA\r', 'N'),
('093114', 'STIMLASH MAKASSAR', 'MAKASSAR\r', 'N'),
('093115', 'STITEK KELAUTAN BALIK DIWA', 'MAKASSAR\r', 'N'),
('093116', 'STMIK PARNARAYA', 'MANADO\r', 'N'),
('093117', 'STISIPOL BARUDA', 'MANADO\r', 'N'),
('093118', 'STIH COKROMINOTO', 'PINRANG\r', 'N'),
('093119', 'STISIPOL.T. PATTANABALI', 'MAMUJU\r', 'N'),
('093120', 'STISIP MUJAHIDIN TOLI-TOLI', 'TOLI-TOLI\r', 'N'),
('093121', 'STIE MUHADIDIN TOLI-TOLI', 'TOLI-TOLI\r', 'N'),
('093122', 'STIMIK BINA BANGSA', 'KENDARI\r', 'N'),
('093123', 'STIP MUHAMMADIYAH SINJAI', 'SINJAI\r', 'N'),
('093124', 'STT MENONGGA KOLAKA', 'KOLAKA\r', 'N'),
('094001', 'AKADEMI BAHASA ASING MUSLIM', 'MAKASSAR\r', 'N'),
('094002', 'AKADEMI MARITIM INDONESIA', 'MAKASSAR\r', 'N'),
('094003', 'AKADEMI MANAJEMEN PERS MAKASSAR', 'MAKASSAR\r', 'N'),
('094004', 'AKADEMI BAHASA ASING ATMAJAYA MAKASSAR', 'MAKASSAR\r', 'N'),
('094005', 'AKADEMI ILMU GIZI YPAG MAKASSAR', 'MAKASSAR\r', 'N'),
('094006', 'AKADEMI PARIWISATA YPAG MAKASSAR', 'MAKASSAR\r', 'N'),
('094007', 'AKADEMI SEKRETARI MANAJEMEN INDONESIA PUBLIK', 'MAKASSAR\r', 'N'),
('094008', 'AKADEMI MARITIM IND. VETERAN MAKASSAR', 'MAKASSAR\r', 'N'),
('094009', 'AKADEMI MANAJEMEN PERUSAHAAN EBEN HAEZER MANADO', 'MANADO\r', 'N'),
('094010', 'AKADEMI SEKRETARI MANAJEMEN ATMAJAYA MAKASSAR', 'MAKASSAR\r', 'N'),
('094011', 'AKADEMI PARIWISATA MANADO', 'MANADO\r', 'N'),
('094012', 'AKADEMI TEKNIK KENDARI', 'KENDARI\r', 'N'),
('094013', 'AKADEMI MARITIM INDONESIA BITUNG', 'BITUNG\r', 'N'),
('094014', 'AKADEMI MANAJ. INFOR. KOM. REZKI MAKASSAR', 'MAKASSAR\r', 'N'),
('094015', 'AKADEMI PERAWATAN MUHAMMADIYAH MAKASSAR', 'MAKASSAR\r', 'N'),
('094016', 'AKADEMI SEK. MANAJ. INDO. B.T. GORONTALO', 'GORONTALO\r', 'N'),
('094017', 'AKADEMI PARIWISATA DIAN RANA R. PAO', 'PAO\r', 'N'),
('094018', 'AKADEMI TEKNIK SOROAKO', 'SOROAKO\r', 'N'),
('094019', 'AKADEMI MANAJ. IND. KOM. TRI DHARMA PALU', 'PALU\r', 'N'),
('094020', 'AKADEMI PARIWISATA AIR MADIDI', 'MADIDI\r', 'N'),
('094021', 'AKADEMI KEUANGAN PERBANKAN TAHUNA', 'TAHUNA\r', 'N'),
('094022', 'AKADEMI MANAJ. INF. KOM. MILAN KENDARI', 'KENDARI\r', 'N'),
('094023', 'AKADEMI SEKERTAARIS MANAJ. IND. KLABAT', 'KLABAT\r', 'N'),
('094024', 'AKADEMI MANAJ. INFOR. KOM. YAPENAS KENDARI', 'KENDARI\r', 'N'),
('094025', 'AKADEMI MANAJ. INFOR. KOM IBNU KH. PALOPO', 'PALOPO\r', 'N'),
('094026', 'AKADEMI MANAJ. INF. KOM. CATUR SAKTI KENDARI', 'KENDARI\r', 'N'),
('094027', 'AKADEMI MANAJEMEN INF. KOM. MAKASSAR', 'MAKASSAR\r', 'N'),
('094028', 'AKADEMI PERDAGANGAN LASH MAKASSAR', 'MAKASSAR\r', 'N'),
('094029', 'AKADEMI SEK. MANAJ. IND LPI MAKASSAR', 'MAKASSAR\r', 'N'),
('094030', 'AKADEMI SEK. MANAJ. AMSIR PARE-PARE', 'PARE-PARE\r', 'N'),
('094031', 'AKADEMI TEKNIK PRATAMA MAKALE', 'MAKALE\r', 'N'),
('094032', 'AKADEMI MANAJ. INF. KOM. PROF MAKASSAR', 'MAKASSAR\r', 'N'),
('094033', 'AKADEMI ILMU KOMPUTER AKBA MAKASSAR', 'MAKASSAR\r', 'N'),
('094034', 'AKADEMI MANAJ. INF. KOM. BLOBAL KENDARI', 'KENDARI\r', 'N'),
('094035', 'AKADEMI SEK. MANAJ. INF. YAPIKA MAKASSAR', 'MAKASSAR\r', 'N'),
('094036', 'AKADEMI HIPERKES MAKASSAR', 'MAKASSAR\r', 'N'),
('094037', 'AKADEMI MANAJ. INF. KOM. PARNARAYA MANADO', 'MANADO\r', 'N'),
('094038', 'AKADEMI TEKNOL. INFUSTRI DEWANTARA PALOPO', 'PALOPO\r', 'N'),
('094039', 'AKADEMI MANAJEMEN BITUNG', 'BITUNG\r', 'N'),
('094040', 'AKADEMI KETALAK PELAYARAN NIAGA/KEPLB KENDARI', 'KENDARI\r', 'N'),
('094041', 'AKADEMI MANAJ. INF. KOM. LAMAPPAPOLEONRO SOPPENG', 'SOPPENG\r', 'N'),
('094042', 'AKADEMI BAHASA ASING BARAKATI KENDARI', 'KENDARI\r', 'N'),
('094043', 'AKADEMI ANALIS KIMIA YAPIKA MAKASSAR', 'MAKASSAR\r', 'N'),
('094044', 'AKADEMI PARIWISATA FAJAR MAKASSAR', 'MAKASSAR\r', 'N'),
('094045', 'AKADEMI AKUNTANSI FAJAR MAKASSAR', 'MAKASSAR\r', 'N'),
('094046', 'AKADEMI PERTAMBANGAN MAKASSAR', 'MAKASSAR\r', 'N'),
('094047', 'AKADEMI MANAJ. INF. KOM. TOMAKAKA MAMUJU', 'MAMUJU\r', 'N'),
('094048', 'AKADEMI MANAJ. INF. KOM. YAMIL KOLAKA', 'KOLAKA\r', 'N'),
('094049', 'AKADEMI FARMASI BINA HUSADA KENDARI', 'KENDARI\r', 'N'),
('094050', 'AKADEMI KEPERAWATAN YAPENAS 21 MAROS', 'MAROS\r', 'N'),
('094051', 'AKADEMI KEBIDANAN SALEWANGAN MAROS', 'MAROS\r', 'N'),
('094052', 'AKADEMI KEPERAWATAN NUSANTARA JAYA MAKASSAR', 'MAKASSAR\r', 'N'),
('095001', 'POLITEKNIK NUR BADAR MAKASSAR', 'MAKASSAR\r', 'N'),
('101001', 'UNIVERSITAS BUNG HATTA', '\r', 'N'),
('101002', 'UNIVERSITAS MUHAMMADIYAH SUMBAR', '\r', 'N'),
('101003', 'UNIVERSITAS EKASAKTI', '\r', 'N'),
('101004', 'UNIVERSITAS MAHAPUTRA M. YAMIN SOLOK', '\r', 'N'),
('101005', 'UNIVERSITAS TAMANSISWA', '\r', 'N'),
('101040', 'UNIVERSITAS ISLAM RIAU', '\r', 'N'),
('101041', 'UNIVERSITAS LANCANG KUNING', '\r', 'N'),
('101046', 'UNIVERSITAS BATANGHARI JAMBI', '\r', 'N'),
('101070', 'UNIVERSITAS BAITURRAHMAH', '\r', 'N'),
('101094', 'UNIVERSITAS BATAM', '\r', 'N'),
('101101', 'UNIVERSITAS INTERNASIONAL BATAM', '\r', 'N'),
('101115', 'UNIVERSITAS PUTRA INDONESIA (UPI) YPTK', '\r', 'N'),
('102006', 'INSTITUT SAIN DAN TEKNOLOGI (ISTPN)', '\r', 'N'),
('102130', 'INSTITUT TEKNOLOGI PADANG', '\r', 'N'),
('103007', 'STKIP ABDI PENDIDIKAN PAYAKUMBUH', '\r', 'N'),
('103008', 'STKIP AHLUSSUNNAH BUKITTINGGI', '\r', 'N'),
('103009', 'STKIP PGRI SUMBAR', '\r', 'N'),
('103010', 'STKIP YDB LUBUK ALUNG', '\r', 'N'),
('103012', 'STIH PAINAN', '\r', 'N'),
('103013', 'STIH LUBUK SIKAPING', '\r', 'N'),
('103014', 'STIM SUMBAR', '\r', 'N'),
('103015', 'STIE H. AGUS SALIM', '\r', 'N'),
('103016', 'STIE KBP', '\r', 'N'),
('103017', 'STIE LUBUK SIKAPING', '\r', 'N'),
('103018', 'STIE SUMBAR', '\r', 'N'),
('103019', 'STIA ADABIAH', '\r', 'N'),
('103020', 'STBA PRAYOGA', '\r', 'N'),
('103021', 'STIA LPPN', '\r', 'N'),
('103022', 'STISIPOL IMAM BONJOL', '\r', 'N'),
('103023', 'STISIP PADANG', '\r', 'N'),
('103024', 'STTIND PADANG', '\r', 'N'),
('103026', 'STIE PERDAGANGAN', '\r', 'N'),
('103027', 'STISIPOL PANCASAKTI BUKITTINGGI', '\r', 'N'),
('103047', 'STKIP YPM BANGKO', '\r', 'N'),
('103048', 'STIP GRAHA KARYA MUARA BULIAN', '\r', 'N'),
('103053', 'STKIP MUHAMMADIYAH SUNGAI PENUH', '\r', 'N'),
('103056', 'STIP H. AGUS SALIM LUBUK BASUNG', '\r', 'N'),
('103062', 'STT  BATAM', '\r', 'N'),
('103063', 'STIE DHARMA ANDALAS', '\r', 'N'),
('103064', 'STIE BATAM', '\r', 'N'),
('103065', 'STIE JAMBI', '\r', 'N'),
('103066', 'STIE IBNUSINA BATAM', '\r', 'N'),
('103067', 'STIE PURNA GRAHA PEKANBARU', '\r', 'N'),
('103068', 'STMIK JAMBI', '\r', 'N'),
('103069', 'STMIK RIAU', '\r', 'N'),
('103071', 'STIE RIAU', '\r', 'N'),
('103078', 'STIFI PERINTIS PADANG', '\r', 'N'),
('103087', 'STIE SRI GEMILANG TEMBILAHAN', '\r', 'N'),
('103088', 'STISIPOL RAJA HAJI TJ PINANG', '\r', 'N'),
('103089', 'STIE INDRAGIRI RENGAT', '\r', 'N'),
('103090', 'STIA LANCANG KUNING DUMAI', '\r', 'N'),
('103091', 'STIE BANGKINANG', '\r', 'N'),
('103092', 'STIA PAGARUYUNG BATUSANGKAR', '\r', 'N'),
('103095', 'STIH YPKMI PADANG', '\r', 'N'),
('103096', 'STIKES JAMBI', '\r', 'N'),
('103097', 'STIE PERBANKAN INDONESIA', '\r', 'N'),
('103098', 'STIE PAGARUYUNG BATUSANGKAR', '\r', 'N'),
('103099', 'STIE SAKTI ALAM KERINCI SUNGAI PENUH', '\r', 'N'),
('103103', 'STIE DHARMA PUTRA PEKANBARU', '\r', 'N'),
('103104', 'STISIP PERSADA BUNDA PEKANBARU', '\r', 'N'),
('103105', 'STIE PERSADA BUNDA', '\r', 'N'),
('103106', 'STIA BINA NUSANTARA MANDIRI PARIAMAN', '\r', 'N'),
('103107', 'STIE GRAHA KARYA MUARA BULIAN', '\r', 'N'),
('103109', 'STIE BENTARA PERSADA BATAM', '\r', 'N'),
('103114', 'STT BENTARA PERSADA BATAM', '\r', 'N'),
('103118', 'SEKOLAH TINGGI TEKNOLOGI UNGGULAN SWARNA DWIPA (ST', '\r', 'N'),
('103119', 'SEKOLAH TINGGI ILMU PERTANIAN SWARNA DWIPA (STP-US', '\r', 'N'),
('103123', 'STMIK INDONESIA', '\r', 'N'),
('103128', 'SEKOLAH TINGGI TEKNIK IBNU SINA BATAM', '\r', 'N'),
('103129', 'STIKOM DINAMIKA BANGSA JAMBI', '\r', 'N'),
('103132', 'STMIK JAYA NUSA PADANG', '\r', 'N'),
('103142', 'STIFAR  RIAU', '\r', 'N'),
('103145', 'STMIK PUTERA BATAM', '\r', 'N'),
('103148', 'STKIP AISYIYAH RIAU', '\r', 'N'),
('103149', 'STIKES HANGTUAH PEKANBARU', '\r', 'N'),
('103151', 'STIP MUARAO BUNGO JAMBI', '\r', 'N'),
('104028', 'APPERTA', '\r', 'N'),
('104029', 'AKADEMI PARIWISATA BUNDA PADANG', '\r', 'N'),
('104030', 'A I K P', '\r', 'N'),
('104031', 'AKBP PADANG', '\r', 'N'),
('104034', 'AKADEMI AKUNTANSI INDONESIA', '\r', 'N'),
('104035', 'AKOP SUMBAR', '\r', 'N'),
('104036', 'A S M I  PADANG', '\r', 'N'),
('104037', 'ABA H. AGUS SALIM BUKITTINGGI', '\r', 'N'),
('104042', 'ATMI  DUMAI', '\r', 'N'),
('104043', 'AMIK RIAU', '\r', 'N'),
('104044', 'AMKOP RIAU', '\r', 'N'),
('104045', 'APEPH PEKANBARU', '\r', 'N'),
('104049', 'AKADEMI ADMINISTRASI YPTSA', '\r', 'N'),
('104050', 'AKADEMI ADMINISTRASI SETIH SETIO', '\r', 'N'),
('104051', 'AMKOP GRAHA KARYA', '\r', 'N'),
('104052', 'ASM JAMBI', '\r', 'N'),
('104054', 'AKBAR RIAU', '\r', 'N'),
('104055', 'AKRI PEKANBARU', '\r', 'N'),
('104057', 'ASM PERSADA BUNDA', '\r', 'N'),
('104058', 'ATP RIAU', '\r', 'N'),
('104059', 'ATOM PEKANBARU', '\r', 'N'),
('104060', 'AKLINDO PERSADA BUNDA', '\r', 'N'),
('104061', 'AKUBANK JAMBI', '\r', 'N'),
('104072', 'AKADEMI AKUNTANSI MAHAPUTRA RIAU', '\r', 'N'),
('104073', 'AKPER BAITURRAHMAH', '\r', 'N'),
('104074', 'AKADEMI TEKNOLOGI PRATAMA', '\r', 'N'),
('104075', 'AKADEMI MARITIM SAPTA SAMUDRA', '\r', 'N'),
('104076', 'AKADEMI MANAJEMEN EL-HAKIM', '\r', 'N'),
('104077', 'AKADEMI GIZI PERINTIS PADANG', '\r', 'N'),
('104079', 'AMIK JAYA NUSA PADANG', '\r', 'N'),
('104080', 'AKADEMI AKUNTANSI TANJUNG PINANG', '\r', 'N'),
('104081', 'ABA PERSADA BUNDA PEKANBARU', '\r', 'N'),
('104082', 'AKPM PEKANBARU', '\r', 'N'),
('104083', 'AKADEMI AKUNTANSI PELITA INDONESIA PKBR.', '\r', 'N'),
('104084', 'ABA YPTN BATAM', '\r', 'N'),
('104085', 'AKADEMI AKUNTANSI YPTN BATAM', '\r', 'N'),
('104086', 'AMIK YPTN BATAM', '\r', 'N'),
('104093', 'AKADEMI HIPERKES DAN KESELAMATAN KERJA (AHKK) PADA', '\r', 'N'),
('104100', 'AKADEMI TEKNIK TAMAN SISWA (ATTS)', '\r', 'N'),
('104102', 'AMIK DUMAI', '\r', 'N'),
('104108', 'AKADEMIK BAHASA ASING ALASKA PADANG', '\r', 'N'),
('104110', 'APIKES IRIS PADANG', '\r', 'N'),
('104112', 'AKADEMI TEKNIK ADIKARYA BALAI SELASA', '\r', 'N'),
('104113', 'AKADEMI TELEKOMUNIKASI INDONESIA JAMBI', '\r', 'N'),
('104116', 'AKADEMI KESEHATAN FISIOTERAPI PAINAN', '\r', 'N'),
('104120', 'AKADEMI KESELAMATAN DAN KESEHATAN KERJA RIAU (AKES', '\r', 'N'),
('104121', 'AKADEMI FARMASI DWI FRAMA BUKITTINGGI', '\r', 'N'),
('104122', 'AMIK BUKITTINGGI', '\r', 'N'),
('104125', 'AKADEMI BAHASA ASING (ABA) TANJUNG PINANG', '\r', 'N'),
('104126', 'AMIK DATUK PERPATIAH NAN SABATANG', '\r', 'N'),
('104127', 'AMIK MITRA GAMA BENGKALIS', '\r', 'N'),
('104131', 'AKADEMI KEBIDANAN ALIFAH PADANG', '\r', 'N'),
('104133', 'AKDEMI PERPAJAKAN YPKM PADANG', '\r', 'N'),
('104135', 'AKADEMI KEBIDANAN MERCU BAKTI JAYA PADANG', '\r', 'N'),
('104136', 'AKADEMI FISIOTERAPI ABDURRAB PEKANBARU', '\r', 'N'),
('104137', 'AKADEMI KEBIDANAN DHARMA HUSADA BATAM', '\r', 'N'),
('104138', 'AKADEMI KEPERAWATAN DHARMA HUSADA BATAM', '\r', 'N'),
('104140', 'AKADEMI BAHASA ASING (ABA) NURDIN HAMZAH', '\r', 'N'),
('104141', 'AMIK MAHAPUTRA RIAU', '\r', 'N'),
('104143', 'AKPER MITRA BUNDA PERSADA BATAM', '\r', 'N'),
('104144', 'ABA PUTERA BATAM', '\r', 'N'),
('104146', 'AKADEMI KEBIDANAN PRIMA NUSANTARA BUKITTINGGI', '\r', 'N'),
('104147', 'AKADEMI KEBIDANAN LENGGOGENI PADANG', '\r', 'N'),
('104150', 'AKADEMI KEBIDANAN ABDURRAB PEKANBARU', '\r', 'N'),
('105111', 'POLITEKNIK BATAM', '\r', 'N'),
('105117', 'POLITEKNIK CALTEX RIAU', '\r', 'N'),
('105124', 'POLITEKNIK TRI DHARMA PADANG', '\r', 'N'),
('105134', 'POLITEKNIK BENGKALIS', '\r', 'N'),
('105138', 'POLITEKNIK PASIR PENGARAIAN', '\r', 'N'),
('111001', 'UNIVERSITAS 17 AGUSTUS 1945 SAMARIANDA', 'SAMARIANDA\r', 'N'),
('111002', 'UNIVERSITAS ACHMAD YANI BANJARMASIN', 'BANJARMASIN\r', 'N'),
('111003', 'UNIVERSITAS ISLAM KALIMANTAN BANJARMASIN', 'BANJARMASIN\r', 'N'),
('111004', 'UNIVERSITAS PANCA BHAKTI PONTIANAK', 'PONTIANAK\r', 'N'),
('111005', 'UNIVERSITAS TRI DHARMA BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('111006', 'UNIVERSITAS BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('111007', 'UNIVERSITAS WIDYA GAMA MAHAKAM SAMARINDA', 'SAMARINDA\r', 'N'),
('111008', 'UNIVERSITAS KUTAI KARTANEGARA TENGGARONG', 'TENGGARONG\r', 'N'),
('111009', 'UNIVERSITAS MUHAMMADIYAH PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('111010', 'UNIVERSITAS KRISTEN PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('111012', 'UNIVERSITAS TRUNAJAYA BONTANG', 'BONTANG\r', 'N'),
('111013', 'UNIVERSITAS MUHAMMADIYAH PONTIANAK', 'PONTIANAK\r', 'N'),
('111014', 'UNIVERSITAS KAPUAS SINTANG', 'SINTANG\r', 'N'),
('111015', 'UNIVERSITAS PGRI PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('111016', 'UNIVERSITAS BORNEO TARAKAN', 'TARAKAN\r', 'N'),
('112001', 'IKIP PGRI KALTIM', 'KALTIM\r', 'N'),
('113001', 'STIE INDONESIA BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113002', 'STIA BINA BANUA BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113003', 'STIMI BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113004', 'STIH SULTAN ADAM BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113005', 'STIE NASIONAL BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113006', 'STKIP PGRI BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113007', 'STIE PONTIANAK', 'PONTIANAK\r', 'N'),
('113008', 'STKIP PGRI PONTIANAK', 'PONTIANAK\r', 'N'),
('113009', 'STIH M.TSJAFIOEDDIN SINGKAWANG', 'SINGKAWANG\r', 'N'),
('113010', 'STIE BOEDI OETOMO PONTIANAK', 'PONTIANAK\r', 'N'),
('113011', 'STIE MUHAMMADIYAH SAMARINDA', 'SAMARINDA\r', 'N'),
('113012', 'STIE BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('113013', 'STIK MAHAKAM SAMARINDA', 'SAMARINDA\r', 'N'),
('113014', 'STMIK SAMARINDA', 'SAMARINDA\r', 'N'),
('113016', 'STIE PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('113017', 'STIH TAMBUN BUNGAI PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('113018', 'STIE SAMPIT', 'SAMPIT\r', 'N'),
('113019', 'STIH HABARING HURUNG SAMPIT', 'SAMPIT\r', 'N'),
('113020', 'STKIP MUHAMMADIYAH SAMPIT', 'SAMPIT\r', 'N'),
('113021', 'STIMI SAMARINDA', 'SAMARINDA\r', 'N'),
('113022', 'STIE PANCA SETIA BANJARMASIN', 'BANJARMASIN\r', 'N'),
('113023', 'STIE TARAKAN', 'TARAKAN\r', 'N'),
('113024', 'STIE NUSANTARA PANGKALAN BUN', 'PANGKALAN BUN\r', 'N'),
('113025', 'STIE SAMARINDA', 'SAMARINDA\r', 'N'),
('113026', 'STIE KUALA KAPUAS', 'KUALA KAPUAS\r', 'N'),
('113027', 'STIE MUHAMAMDIYAH TANJUNG REDEB', 'TANJUNG REDEB\r', 'N'),
('113028', 'STIBA BANJARBARU', 'BANJARBARU\r', 'N'),
('113029', 'STMIK PONTIANAK', 'PONTIANAK\r', 'N'),
('113030', 'STMIK WIDYA CIPTA DHARMA SAMARINDA', 'SAMARINDA\r', 'N'),
('113031', 'STIH KOTAWARINGIN PANGKALAN BUN', 'PANGKALAN BUN\r', 'N'),
('113032', 'STIE NASIONAL SAMARINDA', 'SAMARINDA\r', 'N'),
('113033', 'STIE WIDYA DHARMA PONTIANAK', 'PONTIANAK\r', 'N'),
('113034', 'STIE INDONESIA PONTIANAK', 'PONTIANAK\r', 'N'),
('113035', 'STIA AMUNTAI', 'AMUNTAI\r', 'N'),
('113036', 'STIE MUARA TEWEH', 'MUARA TEWEH\r', 'N'),
('113037', 'STIP PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('113038', 'STMIK BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('113039', 'STIPER MUHAMMADIYAH T.GROGOT', 'TANAH GROGOT\r', 'N'),
('113040', 'STIE WIJAYA KESUMA SAMPIT', 'SAMPIT\r', 'N'),
('113041', 'STTI BONTANG', 'BONTANG\r', 'N'),
('113042', 'STIBA PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('113043', 'STIE TANJUNG SELOR', 'TANJUNG SELOR\r', 'N'),
('113044', 'STIPER PGRI BUNTOK', 'BUNTOK\r', 'N'),
('113045', 'STIE TAMAN BUNGA BUNTOK', 'BUNTOK\r', 'N'),
('113046', 'SEKOLAH TINGGI PERTANIAN KUTAI TIMUR', 'KUTAI TIMUR\r', 'N'),
('113047', 'SEKOLAH TINGGI PERTANIAN PANCA BHAKTI', 'PONTIANAK\r', 'N'),
('114003', 'AKADEMI TEKNIK PEMBANGUNAN NASIONAL BANJ', 'BANJARMASIN\r', 'N'),
('114005', 'AKADEMI MANAJEMEN KOPERASI BARABAI', 'BARABAI\r', 'N'),
('114007', 'AKADEMI ADMINISTRASI TABALONG', 'TABALONG\r', 'N'),
('114008', 'AMP PANCA BHAKTI PONTIANAK', 'PONTIANAK\r', 'N'),
('114009', 'AKADEMI MANAJEMEN KOPERASI KOSGORO', 'PONTIANAK\r', 'N'),
('114011', 'ASM WIDYA DHARMA PONTIANAK', 'PONTIANAK\r', 'N'),
('114012', 'AKADEMI ADMINSTRASI WIDYA DHARMA PONTIANAK', 'PONTIANAK\r', 'N'),
('114014', 'ASMI BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('114016', 'ASMI KMPI SAMARINDA', 'SAMARINDA\r', 'N'),
('114017', 'AMIK BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('114020', 'AKADEMI MANAJEMEN KEUANGAN RAYA PALANGKARAYA', 'PALANGKARAY\r', 'N'),
('114021', 'ASMI PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('114022', 'AKADEMI AKUNTANSI DAN PERBANKAN PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('114023', 'AMIK BANJARMASIN', 'BANJARMASIN\r', 'N'),
('114024', 'AMIK WIDYA DHARMA PONTIANAK', 'PONTIANAK\r', 'N'),
('114025', 'AKADEMI KEUANGAN & PERBANKAN GA KATULISTIWA', 'PONTIANAK\r', 'N'),
('114028', 'AKADEMI MANAJEMEN KEUANGAN KUALA KAPUAS', 'KUALA KAPUAS\r', 'N'),
('114029', 'AKADEMI FILSAFAT GEREJA KALIMANTAN EVANGELIS', 'KAL EVANGELIS\r', 'N'),
('114031', 'AKADEMI KEUANGAN & PERBANKAN WIDYA PRAJA', 'SAMARINDA\r', 'N'),
('114032', 'AKADEMI AKUNTANSI EDITA SAMARINDA', 'SAMARINDA\r', 'N'),
('114033', 'ASMI PONTIANAK', 'PONTIANAK\r', 'N'),
('114034', 'ABA PONTIANAK', 'PONTIANAK\r', 'N'),
('114035', 'AKPARNAS SAMARINDA', 'SAMARINDA\r', 'N'),
('114036', 'AKADEMI AKUNTANSI BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('114037', 'ASMI TANJUNG SELOR', 'TANJUNG SELOR\r', 'N'),
('114038', 'AMIK PALANGKARAYA', 'PALANGKARAYA\r', 'N'),
('114039', 'AKADEMI MARITIM NUSANTARA BANJARMASIN', 'BANJARMASIN\r', 'N'),
('114040', 'AKADEMI KEUANGAN & PERBANKAN WIDYA PRAJA', 'TANAH GROGOT\r', 'N'),
('114041', 'AKADEMI BAHASA ASING BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('114042', 'AKADEMI BAHASA ASING COLORADO SAMARINDA', 'SAMARINDA\r', 'N'),
('114043', 'AKPER DIRGAHAYU SAMARINDA', 'SAMARINDA\r', 'N'),
('114044', 'AKADEMI KEGURUAN PARIS BARANTAI KOTABARU', 'KOTABARU\r', 'N'),
('114045', 'AKADEMI KESEHATAN LINGKUNGAN SAMARINDA', 'SAMARINDA\r', 'N'),
('114046', 'AKPER MUHAMMADIYAN SAMARINDA', 'SAMARINDA\r', 'N'),
('114047', 'ABA WIDYA DHARMA PONTIANAK', 'PONTIANAK\r', 'N'),
('114048', 'AMI SAMARINDA', 'SAMARINDA\r', 'N'),
('114049', 'AMIK PONTIANAK', 'PONTIANAK\r', 'N'),
('114050', 'AKADEMI PERPAJAKAN PANCA BHAKTI', 'PONTIANAK\r', 'N'),
('114051', 'AKADEMI PERTANIAN & PERKEBUNAN ABDITAMA', 'PONTIANAK\r', 'N'),
('114052', 'AKADEMI BISNIS INTERNASIONAL SAMARINDA', 'SAMARINDA\r', 'N'),
('114053', 'AKADEMI SEKRETARI INDONESIA SAMARINDA', 'SAMARINDA\r', 'N'),
('114054', 'AKADEMI FARMASI (AKFAR) SAMARINDA', 'SAMARINDA\r', 'N'),
('114055', 'AKADEMI KEPERAWATAN SUAKA INSAN', 'BANJARMASIN\r', 'N'),
('114056', 'AKADEMI KEPERAWATAN+C29 MUHAMMADIYAH', 'BANJARMASIN\r', 'N'),
('115001', 'POLITEKNIK TONGGAK EQUATOR PONTIANAK', 'PONTIANAK\r', 'N'),
('115002', 'POLITEKNIK BALIKPAPAN', 'BALIKPAPAN\r', 'N'),
('888888', 'INSTANSI BUKAN DARI PERG. TINGGI DEPDIKNAS', 'XXXXXX\r', 'N'),
('999999', 'PERGURUAN TINGGI LUAR NEGERI', 'LUAR NEGERI\r', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pmb`
--

CREATE TABLE `pmb` (
  `PMBID` varchar(20) NOT NULL DEFAULT '',
  `PMBDate` date NOT NULL DEFAULT '0000-00-00',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Sex` enum('L','P') NOT NULL DEFAULT 'L',
  `BirthPlace` varchar(50) NOT NULL DEFAULT '',
  `BirthDate` date NOT NULL DEFAULT '0000-00-00',
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `RT` varchar(5) DEFAULT NULL,
  `RW` varchar(5) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `PostalCode` varchar(20) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL,
  `AgamaID` int(11) DEFAULT NULL,
  `Nationality` varchar(20) DEFAULT NULL,
  `Grp` enum('Y','N') NOT NULL DEFAULT 'N',
  `CompanyName` varchar(50) DEFAULT NULL,
  `CompanyAddress1` varchar(100) DEFAULT NULL,
  `CompanyAddress2` varchar(100) DEFAULT NULL,
  `CompanyCity` varchar(100) DEFAULT NULL,
  `CompanyPhone` varchar(20) DEFAULT NULL,
  `CompanyFacsimile` varchar(20) DEFAULT NULL,
  `ParentName` varchar(50) DEFAULT NULL,
  `ParentWork` varchar(50) DEFAULT NULL,
  `ParentAddress1` varchar(100) DEFAULT NULL,
  `ParentAddress2` varchar(100) DEFAULT NULL,
  `ParentRT` varchar(5) DEFAULT NULL,
  `ParentRW` varchar(5) DEFAULT NULL,
  `ParentCity` varchar(50) DEFAULT NULL,
  `ParentPostalCode` varchar(50) DEFAULT NULL,
  `ParentPhone` varchar(20) DEFAULT NULL,
  `ParentMobilePhone` varchar(20) DEFAULT NULL,
  `FromSchool` varchar(100) DEFAULT NULL,
  `SchoolScore` decimal(5,2) DEFAULT '0.00',
  `SchoolType` varchar(20) DEFAULT NULL,
  `SchoolCity` varchar(50) DEFAULT NULL,
  `SchoolMajor` varchar(50) DEFAULT NULL,
  `GraduateYear` int(11) DEFAULT NULL,
  `NotGraduated` enum('Y','N') NOT NULL DEFAULT 'N',
  `CertificateNumber` varchar(20) DEFAULT NULL,
  `KodeKampus` varchar(10) DEFAULT NULL,
  `Program` varchar(10) DEFAULT NULL,
  `ProgramType` varchar(20) NOT NULL DEFAULT '',
  `ProgramSchedule` varchar(20) NOT NULL DEFAULT '',
  `TestScore` int(11) NOT NULL DEFAULT '0',
  `TestPass` enum('Y','N') NOT NULL DEFAULT 'N',
  `PMBPaid` enum('Y','N') NOT NULL DEFAULT 'N',
  `PMBPrice` int(11) NOT NULL DEFAULT '0',
  `PMBSyarat` varchar(100) DEFAULT NULL,
  `PMBKurang` varchar(100) DEFAULT NULL,
  `DosenID` int(11) NOT NULL DEFAULT '0',
  `StatusAwal` varchar(5) DEFAULT NULL,
  `StatusPotongan` varchar(5) DEFAULT NULL,
  `Setara` int(11) NOT NULL DEFAULT '0',
  `KodeBiaya` varchar(5) DEFAULT NULL,
  `MGM` enum('Y','N') NOT NULL DEFAULT 'N',
  `MGMOleh` int(11) DEFAULT NULL,
  `MGMHonor` int(11) NOT NULL DEFAULT '0',
  `Keterangan` varchar(255) DEFAULT NULL,
  `Terima` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pmb`
--

INSERT INTO `pmb` (`PMBID`, `PMBDate`, `Name`, `Email`, `Sex`, `BirthPlace`, `BirthDate`, `Address1`, `Address2`, `RT`, `RW`, `City`, `PostalCode`, `Phone`, `MobilePhone`, `AgamaID`, `Nationality`, `Grp`, `CompanyName`, `CompanyAddress1`, `CompanyAddress2`, `CompanyCity`, `CompanyPhone`, `CompanyFacsimile`, `ParentName`, `ParentWork`, `ParentAddress1`, `ParentAddress2`, `ParentRT`, `ParentRW`, `ParentCity`, `ParentPostalCode`, `ParentPhone`, `ParentMobilePhone`, `FromSchool`, `SchoolScore`, `SchoolType`, `SchoolCity`, `SchoolMajor`, `GraduateYear`, `NotGraduated`, `CertificateNumber`, `KodeKampus`, `Program`, `ProgramType`, `ProgramSchedule`, `TestScore`, `TestPass`, `PMBPaid`, `PMBPrice`, `PMBSyarat`, `PMBKurang`, `DosenID`, `StatusAwal`, `StatusPotongan`, `Setara`, `KodeBiaya`, `MGM`, `MGMOleh`, `MGMHonor`, `Keterangan`, `Terima`, `NotActive`) VALUES
('2004101', '2017-01-31', 'Yeli Novelina', '', 'P', 'Kemang', '1990-11-27', 'Desa Kemang Kec. Lembak Kab. Muara Enim', '', '', '', '', '31171', '', '082182281723', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Saptono', 'Petani', 'Desa Kemang Kec. Lembak Kab. Muara Enim', '', '', '', '', '31171', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Multi Media', 2015, 'N', 'DN 11 Mk 0010803', '023059', '62201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 11, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004102', '2017-01-31', 'Piky Brucky Mandasari', '', 'P', 'Prabumulih ', '1990-09-11', 'Jalan Sindang Lura ', '', '2', '3', '', '31116', '', '08983648085', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sarminto S', 'Wiraswasta', 'Jalan Sindang Lura ', '', '2', '3', '', '31116', '', '', 'SMA Negeri 2 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2015, 'N', 'DN 11 Ma 0009839', '023059', '62201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 11, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004103', '2017-01-31', 'Pebthy Indriani Sidiek', '', 'P', 'Prabumulih', '1940-02-05', 'Jalan Sindang Lura ', '', '2', '3', 'Prabumulih', '', '', '081272130991', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'A.	Sidik', 'Wiraswasta', 'Jalan Sindang Lura', '', '2', '3', 'Prabumulih', '31116', '', '', 'SMA Negeri 2 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2015, 'N', '', '023059', '62201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 11, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004104', '2017-01-31', 'Maya Pangastuti', '', 'P', 'Kencana Mulia ', '1940-04-14', 'Kencana Mulia Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '082282867456', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Tentrem', 'Wiraswasta', 'Kencana Mulia Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '', 'Madrasah Aliyah Negeri Prabumulih', '0.00', 'MA', 'Prabumulih', '', 2016, 'N', '', '023059', '62201', 'REG', '', 86, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 15, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004105', '2017-01-31', 'Andre Saputra', '', 'P', 'Sira Pulau Padang ', '1940-08-03', 'Gunung Ibul', '', '1', '1', 'Prabumulih', '', '', '082175371295 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Irawan', 'Karyawan Swasta', 'Gunung Ibul', '', '1', '1', 'Prabumulih', '', '', '', 'SMA Negeri 4 Bengkulu', '0.00', 'SMA', 'Bengkulu', 'IPA', 2016, 'N', 'DN 26 Ma/13 0100202', '023059', '62201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 18, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004106', '2017-01-31', 'Munzir', '', 'L', 'Lubuk Semantung', '1940-09-02', 'Karang Raja', '', '3', '5', 'Prabumulih ', '', '', '085380125283', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Fahrurozi', 'PNS', 'Karang Raja Rt. 03 Rw. 05 Kel. Karang Raja Kec. Prabumulih Timur ', '', '', '', 'Prabumulih ', '', '', '', 'Madrasah Aliyah Babul Falah Tanjung Bunut', '0.00', 'MA', 'Tanjung Bunut', 'IPS', 2017, 'N', 'MA 060007751', '023059', '62201', 'REG', '', 76, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 15, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004107', '2017-01-31', 'Denti Liyana', '', 'P', 'Baru Rambang ', '1940-02-03', 'Jalan Dusun II Rt. 02 Kel. Baru Rambang Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '085357828523 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sukron', 'Petani', 'Jalan Dusun II Rt. 02 Kel. Baru Rambang Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '085357828523 ', 'Politeknik Negeri Sriwijaya', '0.00', 'Akademi', 'Palembang', 'Akuntansi D-II', 2015, 'N', '0000063-251-2015', '023059', '62201', 'REG', '', 80, 'Y', 'N', 0, '|2|3|4|5|6|11|8|10|12|13|14|', '', 4, 'P', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004108', '2017-01-31', 'Yati Oktaviani', '', 'P', 'Muara Niru ', '1940-06-12', 'Jalan Toman Rt. 03 Rw. 02 Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', '', '085387840255 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ibnu Alamsyah', 'Petani', 'Jalan Toman Rt. 03 Rw. 02 Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma 06 0006664', '023059', '62201', 'REG', '', 89, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 4, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004109', '2017-01-31', 'Febsa Zumel', '', 'P', 'Sukamerindu ', '1940-02-22', 'Jalan Lintas Baturaja Kel. Sukamerindu Kec. Lubai Kab. Muara Enim', '', '', '', '', '', '', '081212280282', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Zumroh Taslim', 'Petani', 'Jalan Lintas Baturaja Kel. Sukamerindu Kec. Lubai Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Lubai', '0.00', 'SMA', 'Lubai', 'IPA', 2012, 'N', 'DN 11 Ma 0013769', '023059', '62201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 4, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004110', '2017-01-31', 'Annes Carera', '', 'P', 'Talang Bulang ', '1940-04-14', 'Jalan Raya Pendopo Desa Talang Bulang Kec. Talang Ubi Kab. Pali', '', '', '', '', '', '', '085664705027', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Muhamad', 'Petani', 'Jalan Raya Pendopo Desa Talang Bulang Kec. Talang Ubi Kab. Pali', '', '', '', '', '', '', '', 'SMA Negeri 1 Belimbing', '0.00', 'SMA', 'Belimbing', 'IPS', 2016, 'N', '', '023059', '62201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 12, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004111', '2017-01-31', 'Wulan Septiana', '', 'P', 'Prabumulih ', '1940-09-17', 'Jl. Prabujaya Rt. 003 Rw. 002 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081271570888', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Anang Satio', 'Karyawan Swasta', 'Jl. Prabujaya Rt. 003 Rw. 002 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Akuntansi', 2015, 'N', 'DN-11 Mk 0010867', '023059', '62201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 12, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004112', '2017-01-31', 'Anjani', '', 'P', 'Muara Sungai ', '1940-03-07', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082281784831', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'M. Ridwan', 'Petani', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'Madrasah Aliyah Negeri Prabumulih', '0.00', 'MA', 'Prabumulih', 'IPS', 2016, 'N', 'MA 062000491', '023059', '62201', 'REG', '', 87, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 12, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004113', '2017-01-31', 'Meilantika Sari', '', 'P', 'Muara Sungai ', '1940-07-13', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085382804390', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sarimin', 'PNS', 'Desa Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2015, 'N', 'DN 11 Ma 0010156', '023059', '62201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 5, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004114', '2017-01-31', 'Wulan Dari', '', 'P', 'Tanjung Raman ', '1940-03-15', 'Jalan Basuki Rahmad Rt. 004 Rw. 003 Kel. Tanjung Raman Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082178297175', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Jhon Kenedy', 'Sopir', 'Jalan Basuki Rahmad Rt. 004 Rw. 003 Kel. Tanjung Raman Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 4 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2014, 'N', 'DN 11 Ma 0010487', '023059', '62201', 'REG', '', 88, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 5, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004115', '2017-01-31', 'Angraini Pratiwi', '', 'P', 'Prabumulih ', '1940-07-20', 'Jalan Urip Sumoharjo No. 168 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '082376491558', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sukiman', '', 'Jalan Urip Sumoharjo No. 168 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK YPS Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Administrasi Perkantoran', 2014, 'N', 'DN-11 Mk 0013682', '023059', '62201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 5, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004116', '2017-01-31', 'Aisyah Gardena', '', 'P', 'Prabumulih ', '1940-07-03', 'Jalan Ade Irma Rt. 11 Rw. 05 Kel. Mangga Besar Kec. Prabumulih Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '08565881582', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'H. Sunarto, S.Pd', 'PNS', 'Jalan Ade Irma Rt. 11 Rw. 05 Kel. Mangga Besar Kec. Prabumulih Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'MA Maâ€™arif NU Kepung', '0.00', 'MA', 'Kepung', 'IPA', 2015, 'N', 'MA 160012676', '023059', '62201', 'REG', '', 79, 'Y', 'Y', 140000, '|2|4|5|6|8|10|12|13|14|', '', 14, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004117', '2017-01-31', 'Rossa Ayu Lestari Prayogie', '', 'P', 'Palembang ', '1940-05-20', 'Jalan Surip Gang Rambang Rt. 04 Rw. 04 Kel. Pasar II Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085380431761 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Nur Samin Prayogie', 'Buruh Harian Lepas', 'Jalan Surip Gang Rambang Rt. 04 Rw. 04 Kel. Pasar II Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Akuntansi', 2016, 'N', 'DN 11 Mk 13 0004041', '023059', '62201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 14, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004118', '2017-01-31', 'Popi Angraini', '', 'P', 'Prabumulih ', '1940-01-30', 'Jalan Krisna Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082374124387', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Bahrul Hadi', 'Buruh Harian Lepas', 'Jalan Krisna Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Akuntansi', 2016, 'N', 'DN 11 Mk 13 0004037', '023059', '62201', 'REG', '', 89, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 22, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004119', '2017-01-31', 'Margawan Kelana', '', 'L', 'Mesuji', '1940-11-28', 'Dusun IV Kel. Lembak Kec. Lembak Kab. Muara Enim', '', '', '', '', '', '', '082282938764 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Marzuki', 'Wiraswasta', 'Dusun IV Kel. Lembak Kec. Lembak Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Lembak', '0.00', 'SMA', 'Lembak', 'IPA', 2015, 'N', 'DN 11 Ma 0016168', '023059', '62201', 'REG', '', 76, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 22, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004120', '2017-01-31', 'Ryan Resti', '', 'P', 'Belimbing Jaya ', '1940-07-07', 'Desa Belimbing Kab. Muara Enim', '', '', '', '', '', '', '081222747037', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Kardiman', 'Petani', 'Desa Belimbing Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Gunung Megang', '0.00', 'SMA', 'Gunung Megang', 'IPS', 2016, 'N', 'DN 11 Ma/06 0010181', '023059', '62201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 23, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004121', '2017-01-31', 'Amaliya Atmawijaya', '', 'P', 'Wonogiri ', '1940-04-21', 'Jalan Jambangan Rt. 004 Rw. 003 Kel. Balepanjang Kec. Jatipurno Kab. Wonogiri Provinsi Jawa Tengah', '', '', '', '', '', '', '085730921910', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Suparno', 'Petani', 'Jalan Jambangan Rt. 004 Rw. 003 Kel. Balepanjang Kec. Jatipurno Kab. Wonogiri Provinsi Jawa Tengah', '', '', '', '', '', '', '', 'SMA Negeri 1 Jatisrono', '0.00', 'SMA', 'Jatisrono', 'IPA', 2016, 'N', 'DN 03 MA/13 0019408', '023059', '62201', 'REG', '', 85, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 13, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004122', '2017-01-31', 'Oktaria', '', 'P', 'Modong ', '1940-10-31', 'Dusun Modong Kec. Sungai Rotan Kab. Muara Enim', '', '', '', '', '', '', '081373723356', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Feri Irawan', 'Petani', 'Dusun Modong Kec. Sungai Rotan Kab. Muara Enim', '', '', '', '', '', '', '', 'SMK Negeri 1 Kotabumi', '0.00', 'SMK', 'Kotabumi', '', 2016, 'N', '', '023059', '62201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 13, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004123', '2017-01-31', 'Sastri Yani', '', 'P', 'Sugihan ', '1940-10-16', 'Jalan Perumnas Griya Medang Permai Rt. 03 Rw. 09 Kel. Sungai Medang Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '089695972539', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Nur Hidayat', 'Petani', 'Jalan Perumnas Griya Medang Permai Rt. 03 Rw. 09 Kel. Sungai Medang Kec. Cambai Kota Prabumulih', '', '', '', '', '', '', '', 'SMK PGRI 2 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Akuntansi', 2014, 'N', 'DN 11 MK 0013796', '023059', '62201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 13, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004124', '2017-01-31', 'Inda Oktriani', '', 'P', 'Prabumulih ', '1940-10-05', 'Jalan Gotong Royong Rt. 002 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085268677509', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Andi Setiawan', 'Karyawan Swasta', 'Jalan Gotong Royong Rt. 002 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', 'DN 11 Ma/06 0006506', '023059', '62201', 'REG', '', 85, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 13, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004125', '2017-02-01', 'Eko Heryani', '', 'P', 'Gunung Ibul', '1980-02-17', 'Jalan Nias No.123 Rt. 005 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '085369485839', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Herman Z', 'PNS', 'Jalan Nias No.123 Rt. 005 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMU Negeri 02 Prabumulih', '0.00', 'SMU', 'Prabumulih', 'IPA', 1998, 'N', 'No. 11 Mu 102 026764', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, 'Teman', 'Y', 'N'),
('2004126', '2017-02-01', 'Meriana', '', 'P', 'Gunung Ibul', '1987-04-12', 'Jalan Padat Karya Rt. 05 Rw. 01 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085369004242', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'H. Tarman', 'Wiraswasta', 'Jalan Padat Karya Rt. 05 Rw. 01 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA PGRI Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2005, 'N', 'DN 11 Ma 0464437', '023059', '61201', 'REG', '', 83, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004127', '2017-02-01', 'Yolan Safitri', '', 'P', 'Prabumulih ', '1940-11-21', 'Jalan M. Yusuf Wahid No. 38 Kel. Bumi Sako Damai Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '081373707426', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ahmad Syaripudin', 'Wiraswasta', 'Jalan M. Yusuf Wahid No. 38 Kel. Bumi Sako Damai Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMK Negeri 5 Palembang', '0.00', 'SMK', 'Palembang', 'Administrasi Perkantoran', 2013, 'N', 'DN 11 Mk 0002008', '023059', '61201', 'REG', '', 76, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004128', '2017-02-01', 'Tedi Rahmat Nugraha', '', 'P', 'Prabumulih ', '1940-07-13', 'Jalan Perum Prabu Indah Rt. 007 Rw. 004 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '081996532265', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Junaidi', 'Wiraswasta', 'Jalan Perum Prabu Indah Rt. 007 Rw. 004 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Yayasan Bakti Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2013, 'N', 'DN 11 Ma 0011169', '023059', '61201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004129', '2017-02-01', 'Rini Novrianti', '', 'P', 'Prabumulih ', '1940-11-14', 'Jalan Gotong Royong Karang Raja III Rt. 02 Rw. 04 Kel Karang Raja Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', '', '', '08127361958', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ria Musiarso', 'Wiraswasta', 'Jalan Gotong Royong Karang Raja III Rt. 02 Rw. 04 Kel Karang Raja Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma/06 0006658', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004130', '2017-02-01', 'Anjas Meidherwan', '', 'L', 'Tebat Agung ', '1940-05-01', 'Dusun Tebat Agung Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', '', '081373793074 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Suhadi Riyatno', 'Wiraswasta', 'Dusun Tebat Agung Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Rambang Dangku', '0.00', 'SMA', 'Rambang Dangku', 'IPS', 2016, 'N', 'DN 11 Ma/06 0010623', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004131', '2017-02-01', 'Shintia Citra Fransiska', '', 'P', 'Prabumulih ', '1940-01-29', 'Jalan Pakjo Rt. 03 Rw. 03 Kel. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085267810366', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Muhammad Fadhli', 'Wiraswasta', 'Jalan Pakjo Rt. 03 Rw. 03 Kel. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Administrasi Perkantoran', 2015, 'N', 'DN-11 Mk 0010928', '023059', '61201', 'REG', '', 82, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 3, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004132', '2017-02-01', 'Indri Rizki Lestari', '', 'P', 'Prabumulih ', '1940-06-07', 'Jalan Andalas No. 698 Rt. 01 Rw. 01 Kel. Mangga Besar Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '089606137259', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Irawan', 'Wiraswasta', 'Jalan Andalas No. 698 Rt. 01 Rw. 01 Kel. Mangga Besar Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 1 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2015, 'N', 'DN 11 Ma 0009714', '023059', '61201', 'REG', '', 84, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004133', '2017-02-01', 'Dewi Nur Afni K', '', 'P', 'Bandung ', '1978-05-18', 'Jalan Perum Griya Sejahtera Rt. 006 Rw. 005 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081996647078', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'O. Komarudin', 'Wiraswasta', 'Jalan Perum Griya Sejahtera Rt. 006 Rw. 005 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMEA Muslimin Bandung', '0.00', 'Lain-lain', 'Bandung', '', 1996, 'N', 'No. 02 OB om 0215408', '023059', '61201', 'REG', '', 87, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004134', '2017-02-01', 'Mega Yunita', '', 'P', 'Prabumulih ', '1940-03-11', 'Jalan Bangau Rt. 003 Rw. 002 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082175715600', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Abdul Khanan', 'Wiraswasta', 'Jalan Bangau Rt. 003 Rw. 002 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma/06 0006680', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004135', '2017-02-01', 'Molfi Arizki', '', 'P', 'Prabumulih ', '1940-02-24', 'Jalan Sugih waras Rt. 02 Rw. 02 Kel. Tanjung Raya Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '082280224221', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'M. Yasip', 'Petani', 'Jalan Sugih waras Rt. 02 Rw. 02 Kel. Tanjung Raya Kec. Rambang Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 2 Muara Enim', '0.00', 'SMA', 'Muara Enim', 'IPA', 2015, 'N', 'DN 11 Ma 0015218', '023059', '61201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004136', '2017-02-01', 'Naafira Waliya Arista', '', 'P', 'Prabumulih ', '1940-07-06', 'Jalan Vina Sejahtra II Blok BB No. 07 Rt. 03 Rw. 09 Kel. Prabumulih Kec. Prabumulih Timur Kota Prabu', '', '', '', 'Prabumulih ', '', '', '082282034806 ', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sutrisno', '', 'Jalan Vina Sejahtra II Blok BB No. 07 Rt. 03 Rw. 09 Kel. Prabumulih Kec. Prabumulih Timur Kota Prabu', '', '', '', 'Prabumulih', '', '', '', 'Madrasah Aliyah Negeri Prabumulih', '0.00', 'MA', 'Prabumulih', 'IPS', 2016, 'N', 'MA 062000556', '023059', '61201', 'REG', '', 82, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004137', '2017-02-01', 'M. Khoirul Khitami', '', 'L', 'Prabumulih ', '1940-03-07', 'Jalan K.H Ahmad Dahlan Kec. Prabu Jaya Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '089505800141', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'H. Sobri', 'Wiraswasta', 'Jalan K.H Ahmad Dahlan Kec. Prabu Jaya Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Muhamadiyah  1 Prabumulih', '0.00', 'SMA', 'Prabumulih', '', 2016, 'N', '', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004138', '2017-02-01', 'Nini Oktaviani', '', 'P', 'Raja ', '1940-10-09', 'Dusun I Raja Tanah Abang Kel. Tanah Abang Kab. Pali', '', '', '', '', '', '', '085382214414', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Hendrianto', 'Petani', 'Dusun I Raja Tanah Abang Kel. Tanah Abang Kab. Pali', '', '', '', '', '', '', '', 'SMA Negeri 1 Tanah Abang', '0.00', 'SMA', 'Tanah Abang', 'IPS', 2016, 'N', 'DN 11 Ma/06 0023711', '023059', '61201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 9, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004139', '2017-02-01', 'Ami Sriratna Ningsi', '', 'P', 'Purun ', '1940-01-24', 'Jalan Arimbi Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', '', '081278828283', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Awab Asno', 'Buruh Harian Lepas', 'Jalan Arimbi Kel. Prabujaya Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', '', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004140', '2017-02-01', 'Okta Sari', '', 'P', 'Sukaraja ', '1940-10-06', 'Jalan Basuki Rahmad No. 175 Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085211621179', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Bohori', 'Petani', 'Jalan Basuki Rahmad No. 175 Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 2 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2017, 'N', 'DN 11 Ma/13 0002835', '023059', '61201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004141', '2017-02-01', 'Putri Ayu Afriliany', '', 'P', 'Prabumulih ', '1940-04-26', 'Jalan Tromol Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', '', '', '082177485553', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Muhamad Rozie', 'Buruh Harian Lepas', 'Jalan Tromol Kel. Sukaraja Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 2 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma/13 0002841', '023059', '61201', 'REG', '', 87, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004142', '2017-02-01', 'Winda Nopiyani', '', 'P', 'Prabumulih ', '1940-11-03', 'Jalan Suban Mas No. 29 Rt. 04 Rw. 02 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085267292008', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sudianto', 'Buruh Harian Lepas', 'Jalan Suban Mas No. 29 Rt. 04 Rw. 02 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', '', '', '', 'SMK Negeri 2 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Tata Busana', 2016, 'N', 'DN 11 Mk/06 0006711', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004143', '2017-02-01', 'Resty Julianti Rosalinda Sopandi', '', 'P', 'Bandung ', '1940-07-11', 'Jalan Srikandi No. 49 Rt. 05 Rw. 03 Kel. Muntang Tapus Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', '', '', '081373566434', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Tetet Sopandi', 'Pensiunan', 'Jalan Srikandi No. 49 Rt. 05 Rw. 03 Kel. Muntang Tapus Kec. Prabumulih Barat Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', '', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004144', '2017-02-01', 'Oviyani', '', 'P', 'Gunung Ibul', '1940-10-03', 'Jalan Padat Karya Gang. Cempedak Rt. 006 Rw. 001 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumu', '', '', '', '', '', '', '081271535018', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Mat Nasim', 'Petani', 'Jalan Padat Karya Gang. Cempedak Rt. 006 Rw. 001 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumu', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2014, 'N', 'DN 11 Ma 0010654', '023059', '61201', 'REG', '', 76, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004145', '2017-02-01', 'Meri Susanti', '', 'P', 'Prabumulih ', '1984-08-23', 'Jalan Kenari No. 04 Rt. 028 Rw. 010 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082186218060', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Harison', 'Buruh Harian Lepas', 'Jalan Kenari No. 04 Rt. 028 Rw. 010 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '', 'SMU PGRI Prabumulih', '0.00', 'SMU', 'Prabumulih', 'IPS', 2003, 'N', 'No 11 Mu 0424554', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 6, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004146', '2017-02-01', 'Era Hustri', '', 'P', 'Tanah Abang ', '1977-12-03', 'Jalan Nusa I.A No. 02 Rt. 003 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081271510600', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sukarman', 'Wiraswasta', 'Jalan Nusa I.A No. 02 Rt. 003 Rw. 002 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'Akademi Keuangan dan Perbankan Mulia Darma Pratama Palembang', '0.00', 'Akademi', 'Palembang', 'Akuntansi', 2002, 'N', '001/IV/D3/KP/2002', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|3|4|5|6|11|8|10|12|13|14|', '', 20, 'P', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004147', '2017-02-01', 'Karmila Yanti ', '', 'P', 'Suban Jeriji ', '1940-08-06', 'Jalan Bima Taman Baka Kel. Prabujaya Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082186637873', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Cik Agus', 'Karyawan Swasta', 'Jalan Bima Taman Baka Kel. Prabujaya Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 2 Prabumulih', '0.00', 'SMA', 'SMA Negeri 2 Prabumulih', 'IPA', 2016, 'N', '', '023059', '61201', 'REG', '', 83, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004148', '2017-02-01', 'Anita Asari', '', 'P', 'Prabumulih ', '1940-11-22', 'Jalan Bangau No. 113 Rt. 02 Rw. 03 Kel. Karang Raja II Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081373515321', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Junaidi', 'Wiraswasta', 'Jalan Bangau No. 113 Rt. 02 Rw. 03 Kel. Karang Raja II Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'Madrasah Aliyah Negeri Prabumulih', '0.00', 'MA', 'Prabumulih', 'IPS', 2016, 'N', 'MA 062000490', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004149', '2017-02-01', 'Rina Hayatun Nufus', '', 'P', 'Tebing Tenggi ', '1940-02-08', 'Tanjung Makmur Kec. Tebing Tinggi', '', '', '', '', '', '', '085273301520', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Tusdi Kamril', 'PNS', 'Tanjung Makmur Kec. Tebing Tinggi', '', '', '', '', '', '', '', 'SMA Negeri 6 Tebing Tinggi', '0.00', 'SMA', 'Tebing Tinggi', 'IPA', 2016, 'N', '', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004150', '2017-02-01', 'Hesti Saskia', '', 'P', 'Muara Dua ', '1940-01-15', 'Jalan Patih Jaya Rt. 03 Rw. 03 Kel. Muara Dua Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '082371965001', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Asmuni', 'Petani', 'Jalan Patih Jaya Rt. 03 Rw. 03 Kel. Muara Dua Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMK Negeri 1 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Administrasi Perkantoran', 2016, 'N', 'DN 11 Mk/13 0004080', '023059', '61201', 'REG', '', 70, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004151', '2017-02-01', 'Neki Yan Purnama', '', 'P', 'Prabumulih ', '1987-01-11', 'Jalan Jati Baru No. 09 Rt. 4 Rw. 2 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081367710771', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Jak Idin Rambang', 'Wiraswasta', 'Jalan Jati Baru No. 09 Rt. 4 Rw. 2 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMA PGRI Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2004, 'N', 'DN 11 Mu 0300060', '023059', '61201', 'REG', '', 90, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004152', '2017-02-01', 'Nita Suryanti', '', 'P', 'Palembang ', '1940-11-12', 'Jalan Pipa Talang Salim SP 4 Rt. 007 Rw. 002 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '085658844619', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Joni', 'Sopir', 'Jalan Pipa Talang Salim SP 4 Rt. 007 Rw. 002 Kel. Patih Galung Kec. Prabumulih Barat Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK Negeri 2 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Administrasi Perkantoran', 2015, 'N', 'DN 11 Mk 0011041', '023059', '61201', 'REG', '', 89, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 20, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004153', '2017-02-01', 'Eka Ari Crusita Wijayanti', '', 'P', 'Jakarta ', '1940-08-04', 'Asmil Yonkav 5 Rt. 014 Rw. 003 Kel. Karang Endah Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', '', '082122454814', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ahmad Mansyur', 'TNI AD', 'Asmil Yonkav 5 Rt. 014 Rw. 003 Kel. Karang Endah Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Gelumbang', '0.00', 'SMA', 'Gelumbang', 'IPS', 2015, 'N', 'DN 11 Ma 0015949', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004154', '2017-02-01', 'Pipin Ardiansyah', '', 'L', 'Tambangan Kelekar ', '1940-03-10', 'Jalan Tambangan Kelekar Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', '', '082279651579', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Tazmi', 'Petani', 'Jalan Tambangan Kelekar Kec. Gelumbang Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Gelumbang', '0.00', 'SMA', 'Gelumbang', 'IPS', 2016, 'N', 'DN 11 Ma/06 0010069', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004155', '2017-02-01', 'Muhammad Ramdhan Cheba', '', 'L', 'Jakarta ', '1940-01-27', 'Jalan Gang Masjid Ar-Rahman Rt. 001 Rw. 002 Kel. Pasar Prabumulih 2 Kec. Prabumulih Utara Kota Prabu', '', '', '', 'Prabumulih', '', '', '085216895320', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Khairul', 'Karyawan BUMN', 'Jalan Gang Masjid Ar-Rahman Rt. 001 Rw. 002 Kel. Pasar Prabumulih 2 Kec. Prabumulih Utara Kota Prabu', '', '', '', '', '', '', '', 'SMK YPS Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Administrasi Perkantoran', 2016, 'N', 'DN 11 Mk/06 0006395', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004156', '2017-02-01', 'Rizky Faiz', '', 'L', 'Prabumulih ', '1940-06-25', 'Jalan Arjuna 1 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081377502095', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'M. Iriansyah', 'Wiraswasta', 'Jalan Arjuna 1 Kel. Wonosari Kec. Prabumulih Utara Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Yayasan Bakti Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2013, 'N', 'DN 11 Ma 0011135', '023059', '61201', 'REG', '', 76, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004157', '2017-02-01', 'Yuli Apriani', '', 'P', 'Suka Maju ', '1940-07-04', 'Jalan Sukamaju Kec. Talang Ubi', '', '', '', '', '', '', '082373492636', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Bambang Irawan', 'Petani', 'Jalan Sukamaju Kec. Talang Ubi', '', '', '', '', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma/06 0006316', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004158', '2017-02-01', 'Diska Utami', '', 'P', 'Raja ', '1940-11-19', 'Kel. Raja Kec. Tanah Abang Kab. Pali', '', '', '', '', '', '', '082280118234', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Darmadi', 'Petani', 'Kel. Raja Kec. Tanah Abang Kab. Pali', '', '', '', '', '', '', '', 'SMA Negeri 1 Tanah Abang', '0.00', 'SMA', 'Tanah Abang', 'IPA', 2016, 'N', '', '023059', '61201', 'REG', '', 82, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004159', '2017-02-01', 'Putra Permana', '', 'L', 'Tambangan Kelekar ', '1940-08-09', 'Jalan Mentawai Rt. 01 Rw. 02 Kel. Gunung Ibul Barat Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '081373638588', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Riza Pahlevi', 'PNS', 'Jalan Mentawai Rt. 01 Rw. 02 Kel. Gunung Ibul Barat Kec. Prabumulih Timur Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 5 Bandar Lampung', '0.00', 'SMA', 'Bandar Lampung', 'IPS', 2016, 'N', 'DN 12 Ma/06 0002408', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 16, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004160', '2017-02-01', 'Dinni Sari Devi', '', 'P', 'Prabumulih ', '1940-08-17', 'PPKR Dusun Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', '', '', '', '085381553333', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Dal Hadi', 'Buruh Harian Lepas', 'PPKR Dusun Muara Sungai Kec. Cambai Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2016, 'N', 'DN 11 Ma/06 0006369', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004161', '2017-02-01', 'Arnisa', '', 'P', 'Jemenang ', '1940-03-12', 'Dusun II Desa Jemenang Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', '', '085379327018', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ison Susemi', 'Buruh Harian Lepas', 'Dusun II Desa Jemenang Kec. Rambang Dangku Kab. Muara Enim', '', '', '', '', '', '', '', 'SMA Negeri 1 Rambang Dangku', '0.00', 'SMA', 'Rambang Dangku', 'IPA', 2016, 'N', 'DN 11 Ma/06 0010527', '023059', '61201', 'REG', '', 84, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004162', '2017-03-06', 'Adil Lastari', '', 'L', 'Palembang ', '1990-01-07', 'Jalan Hikmah Rt. 03 Rw. 02 Kel. Cambai Kec. Cambai Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Wahyu Sapto Hadi', 'Wiraswasta', 'Jalan Hikmah Rt. 03 Rw. 02 Kel. Cambai Kec. Cambai Kota Prabumulih', '', '', '', '', '', '', '', 'SMA Negeri 22 Palembang', '0.00', 'SMA', 'Palembang', 'IPS', 2014, 'N', 'DN 11 Ma 0008173', '023059', '61201', 'REG', '', 87, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004163', '2017-03-06', 'Maria Alvionitta', '', 'P', 'Prabumulih ', '1990-05-02', 'Jalan Jend. Sudirman Gang Bersama Rt. 001 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabum', '', '', '', 'Prabumulih', '', '', '', 1, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Antonius Suharta', 'Karyawan Swasta', 'Jalan Jend. Sudirman Gang Bersama Rt. 001 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabum', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', 'DN 11 Ma 0006287', '023059', '61201', 'REG', '', 88, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004164', '2017-03-06', 'Riska Andini', '', 'P', 'Prabumulih ', '1990-11-09', 'Jalan Bukit Barisan No. 49 Rt. 02 Rw. 03 Kel. Majasari Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Rizal Effendi', 'Wiraswasta', 'Jalan Bukit Barisan No. 49 Rt. 02 Rw. 03 Kel. Majasari Kec. Prabumulih Selatan Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 7 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2013, 'N', 'DN 11 Ma 0010344', '023059', '61201', 'REG', '', 72, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004165', '2017-03-06', 'Fadilla Sri Pajar Sari', '', 'P', 'Pagar Agung', '1990-09-05', 'Jalan Sugihwaras Kel. Pagar Agung Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sastri Daryadi', 'Petani', 'Jalan Sugihwaras Kel. Pagar Agung Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 6 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', 'DN 11 Ma 0006301', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 7, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004166', '2017-03-06', 'Ajeng Puspita Larasati', '', 'P', 'Prabumulih ', '1990-10-07', 'Jalan Pakjo No. 40 Rt. 003 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Indriya Herlina', 'Wiraswasta', 'Jalan Pakjo No. 40 Rt. 003 Rw. 003 Kel. Gunung Ibul Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 3 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPS', 2013, 'N', 'DN 11 Ma 0010580', '023059', '61201', 'REG', '', 70, 'Y', 'N', 0, '|2|3|4|5|6|11|8|10|12|13|14|', '', 7, 'P', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004167', '2017-03-06', 'Sulaiman', '', 'L', 'Prabumulih ', '1990-01-11', 'Jalan Ade Irma Nasution Rt. 11 Rw. 05 Kel. Mangga Besar Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Ruslan', 'Buruh Harian Lepas', 'Jalan Ade Irma Nasution Rt. 11 Rw. 05 Kel. Mangga Besar Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK PGRI 2 Prabumulih', '0.00', 'SMK', 'Prabumulih', 'Bisnis dan Manajemen', 2009, 'N', 'DN 11 Mk 0011087', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 8, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004168', '2017-03-06', 'Sugiyanto', '', 'L', 'Trenggalek', '1987-01-02', 'Jalan Sugriwa Rt. 004 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Sari', 'Karyawan Swasta', 'Jalan Sugriwa Rt. 004 Rw. 004 Kel. Karang Raja Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMK Karya Dharma I Trenggalek Jatim', '0.00', 'SMK', 'Jatim', 'Teknik Mesin', 2005, 'N', 'DN 05 Mk 0412815', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 8, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004169', '2017-03-06', 'Basuki Rahmat ', '', 'L', 'Prabumulih ', '1985-11-11', 'Jalan A. Yani No. 082 Rt. 001 Rw. 003 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'H. Basyaruddin', 'Pensiunan', 'Jalan A. Yani No. 082 Rt. 001 Rw. 003 Kel. Tugu Kecil Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMU MUHAMMADIYAH 1 PRABUMULIH', '0.00', 'SMU', 'Prabumulih', 'IPS', 2003, 'N', '11 Mu 0425184', '023059', '61201', 'REG', '', 80, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 8, 'P', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004170', '2017-03-06', 'Nur Anisatus Solihah', '', 'P', 'Sumber Rahayu', '1990-01-25', 'Sumber Rahayu Rt. 001 Rw. 001 Desa Sumber Rahayu Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Mustofa Hadi', 'Petani', 'Sumber Rahayu Rt. 001 Rw. 001 Desa Sumber Rahayu Kec. Rambang Kab. Muara Enim', '', '', '', 'Prabumulih', '', '', '', 'Madrasah Aliyah Salafiyah Syafi&#39;iyah', '0.00', 'MA', 'Prabumulih', 'IPS', 2015, 'N', 'Ma 160058592', '023059', '61201', 'REG', '', 75, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 8, 'P', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004171', '2017-03-06', 'Mawaddah Warohmah', '', 'P', 'Prabumulih ', '1990-01-23', 'Jalan Arimbi Rt. 01 Rw. 05 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 3, 'Indonesia', 'N', '', '', '', NULL, '', '', 'Drs. H. Zulkarnain', 'Wiraswasta', 'Jalan Arimbi Rt. 01 Rw. 05 Kel. Prabujaya Kec. Prabumulih Timur Kota Prabumulih', '', '', '', 'Prabumulih', '', '', '', 'SMA Negeri 1 Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', 'DN 11 Ma 0004055', '023059', '61201', 'REG', '', 78, 'Y', 'N', 0, '|2|4|5|6|8|10|12|13|14|', '', 8, 'B', '', 0, NULL, 'N', NULL, 0, '', 'Y', 'N'),
('2004172', '2017-04-22', 'jeri agusta', 'jeriagusta3@gmail.com', 'L', 'tanjung raman', '1993-01-01', 'tanjung raman', 'prabumulih selatan', '03', '05', 'prabumulih', '31116', '', '', 3, '', 'N', '', '', '', NULL, '', '', 'darman', 'tn', 'tanjung raman', '', '03', '05', 'prabumulih', '31116', '', '', 'sma negeri 4 prabumulih', '0.00', 'SMA', 'prabumumulih', '', 2011, 'N', '234567', '023059', '61201', 'REG', '', 0, 'N', 'N', 140000, NULL, NULL, 0, NULL, NULL, 0, NULL, 'N', NULL, 0, 'kawan ', 'N', 'N'),
('2004173', '2017-04-22', 'jeri agusta', 'jeriagusta3@gmail.com', 'L', 'tanjung raman', '1952-01-01', 'tanjung raman', '', '03', '05', 'prabumulih', '31116', '', '', 3, '', 'N', '', '', '', NULL, '', '', 'darman', 'tn', 'tanjung raman', '', '03', '05', '', '31116', '', '', 'sma negeri 4 prabumulih', '0.00', 'SMA', 'prabumumulih', '', 2017, 'N', '', '023059', '61201', 'REG', '', 0, 'N', 'N', 140000, NULL, NULL, 0, NULL, NULL, 0, NULL, 'N', NULL, 0, 'kawan', 'N', 'N'),
('2004174', '2017-04-22', 'Ranti Yuliningsih', 'stieprabumulihypp@gmail.com', 'L', 'Prabumulih', '1940-01-01', 'Prabumulih', '', '', '', '', '31116', '', '', 3, '', 'N', '', '', '', NULL, '', '', 'Sudarto', '', '', '', '', '', '', '', '', '', 'SMA Negeri 2Prabumulih', '0.00', 'SMA', 'Prabumulih', 'IPA', 2016, 'N', '', '023059', '62201', 'REG', '', 0, 'Y', 'Y', 140000, NULL, NULL, 0, NULL, NULL, 0, NULL, 'N', NULL, 0, '', 'Y', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pmbsyarat`
--

CREATE TABLE `pmbsyarat` (
  `ID` int(11) NOT NULL,
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Rank` smallint(6) NOT NULL DEFAULT '0',
  `Nama` varchar(255) NOT NULL DEFAULT '',
  `StatusAwal` varchar(100) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pmbsyarat`
--

INSERT INTO `pmbsyarat` (`ID`, `Kode`, `Rank`, `Nama`, `StatusAwal`, `NotActive`) VALUES
(2, 'Tran', 2, 'Transkrip Legalisir', 'B,P,', 'N'),
(3, 'KetPT', 3, 'Surat Keterangan dari Perguruan Tinggi Asal', 'P,', 'N'),
(4, 'KetSek', 4, 'Surat Keterangan dari Sekolah', 'B,', 'N'),
(5, 'Foto', 5, 'Pas Foto', 'B,P,', 'N'),
(6, 'Akte', 6, 'Akte Lahir / KTP', 'B,P,', 'N'),
(11, 'Wwcr', 7, 'Form Wawancara', 'P,', 'N'),
(8, 'Test', 8, 'Hasil Test', 'B,', 'N'),
(14, 'IJZ', 13, 'Ijazah Legalisir', 'B,P,', 'N'),
(10, 'Form', 10, 'Kelengkapan Pengisian Form', 'B,P,', 'N'),
(12, 'NAZA', 11, 'Surat Keterangan Bebas NAZA', 'B,P,', 'N'),
(13, 'KTP', 12, 'Kartu Tanda Penduduk', 'B,P,', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `polling`
--

CREATE TABLE `polling` (
  `PollID` int(11) NOT NULL,
  `PollDate` date NOT NULL DEFAULT '0000-00-00',
  `Title` varchar(50) NOT NULL DEFAULT '',
  `Description` varchar(255) DEFAULT NULL,
  `unip` varchar(20) NOT NULL DEFAULT '',
  `Author` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `polling`
--

INSERT INTO `polling` (`PollID`, `PollDate`, `Title`, `Description`, `unip`, `Author`, `Email`) VALUES
(1, '2003-04-06', 'Angket Distro Linux', 'Distribusi Linux kegemaran Anda adalah:', 'dewo', 'Dewo', 'setio_dewo@telkom.net'),
(2, '2003-04-06', 'Angket Perang Irak', 'Setujukah Anda akan agresi AS dan sekutunya terhadap Irak?', 'dewo', 'Dewo', 'setio_dewo@telkom.net'),
(5, '2003-04-07', 'Pelajaran Tersulit', 'Menurut Anda, manakah di bawah ini pelajaran yang paling sulit?', 'dewo', 'dewo', 'dewo@telkom.net'),
(4, '2003-04-07', 'Merek Notebook Favorit Anda', 'Pilih salah satu merek notebook favorit Anda.', 'dewo', 'metallica', 'metallica@com'),
(6, '2003-04-07', 'Merek Handphone Favorit Anda', 'Menurut Anda merek handphone yang cool adalah:', 'dewo', 'E. Setio Dewo', 'setio_dewo@telkom.net'),
(7, '2003-05-17', 'Nilai Rapor Anda', 'Nilai rata-rata rapor Anda adalah:', 'dewo', 'E. Setio Dewo', 'setio_dewo@telkom.net'),
(8, '2003-07-07', 'SPMB', 'Bagaimana tanggapan Anda tentang SPMB', 'dewo', 'E. Setio Dewo', 'setio_dewo@telkom.net');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pollitem`
--

CREATE TABLE `pollitem` (
  `PollItemID` int(11) NOT NULL,
  `PollID` int(11) NOT NULL DEFAULT '0',
  `Description` varchar(100) NOT NULL DEFAULT '',
  `Count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pollitem`
--

INSERT INTO `pollitem` (`PollItemID`, `PollID`, `Description`, `Count`) VALUES
(1, 1, 'Mandrake', 14),
(2, 1, 'RedHat', 10),
(3, 1, 'SuSe', 5),
(4, 1, 'Slackware', 3),
(5, 1, 'Debian', 1),
(6, 1, 'Lain-lain', 3),
(7, 1, 'Tidak tahu', 1),
(8, 2, 'Sangat tidak setuju', 11),
(9, 2, 'Tidak setuju', 4),
(10, 2, 'Setuju', 0),
(11, 2, 'Sangat setuju', 2),
(12, 2, 'Tidak ada komentar', 1),
(13, 2, 'Tidak tahu', 0),
(14, 4, 'Toshiba', 3),
(15, 4, 'Acer', 0),
(16, 4, 'Compaq', 0),
(17, 4, 'Dell', 0),
(18, 4, 'Asus', 2),
(19, 4, 'ECS - Desknote', 1),
(20, 4, 'Hewlet Packard', 0),
(21, 4, 'Sony', 0),
(22, 4, 'Fujitsu', 0),
(23, 4, 'Mitac', 0),
(24, 4, 'NEC', 0),
(25, 4, 'Lain-lain', 1),
(26, 5, 'Matematika', 2),
(27, 5, 'Fisika', 1),
(28, 5, 'Kimia', 1),
(29, 5, 'Biologi', 0),
(30, 5, 'Bahasa Inggris', 0),
(31, 5, 'Semuanya mudah bagi saya', 0),
(32, 6, 'Nokia', 5),
(33, 6, 'Sony Erricson', 1),
(34, 6, 'Siemens', 3),
(35, 6, 'Phillips', 0),
(36, 6, 'Motorolla', 0),
(37, 6, 'LG', 1),
(38, 6, 'Lain-lain', 0),
(39, 7, '9', 0),
(40, 7, '8', 0),
(41, 7, '7', 0),
(42, 7, '6', 0),
(43, 7, '5', 0),
(44, 7, '4', 0),
(45, 7, '<4', 0),
(46, 8, 'Setuju', 51),
(47, 8, 'Tidak setuju', 17),
(48, 8, 'Tidak tahu', 2),
(49, 8, 'Masa bodoh', 116);

-- --------------------------------------------------------

--
-- Struktur dari tabel `prasyaratmk`
--

CREATE TABLE `prasyaratmk` (
  `ID` int(11) NOT NULL,
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `PraID` int(11) NOT NULL DEFAULT '0',
  `PraKodeMK` varchar(10) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `predikat`
--

CREATE TABLE `predikat` (
  `ID` int(11) NOT NULL,
  `Kode` varchar(25) DEFAULT NULL,
  `BatasAtas` decimal(5,2) NOT NULL DEFAULT '0.00',
  `BatasBawah` decimal(5,2) NOT NULL DEFAULT '0.00',
  `Bobot` decimal(5,2) NOT NULL DEFAULT '0.00',
  `Keterangan` varchar(50) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `predikat`
--

INSERT INTO `predikat` (`ID`, `Kode`, `BatasAtas`, `BatasBawah`, `Bobot`, `Keterangan`, `NotActive`) VALUES
(1, 'D3-S1', '2.75', '2.00', '0.00', 'Memuaskan', 'N'),
(2, 'D3-S1', '3.50', '2.76', '0.00', 'Sangat Memuaskan', 'N'),
(3, 'D3-S1', '4.00', '3.51', '0.00', 'Dengan Pujian', 'N'),
(4, 'S2', '3.40', '2.75', '0.00', 'Memuaskan', 'N'),
(5, 'S2', '3.70', '3.41', '0.00', 'Sangat Memuaskan', 'N'),
(6, 'S2', '4.00', '3.71', '0.00', 'Dengan Pujian', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prgbpppokok`
--

CREATE TABLE `prgbpppokok` (
  `ID` int(11) NOT NULL,
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `KodeProgram` varchar(10) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `program`
--

CREATE TABLE `program` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama_Indonesia` varchar(100) NOT NULL DEFAULT '',
  `Nama_English` varchar(100) NOT NULL DEFAULT '',
  `KodeFakultas` varchar(10) NOT NULL DEFAULT '',
  `KodeKampus` varchar(10) NOT NULL DEFAULT '',
  `Keterangan` varchar(255) NOT NULL DEFAULT '',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `program`
--

INSERT INTO `program` (`Kode`, `Nama_Indonesia`, `Nama_English`, `KodeFakultas`, `KodeKampus`, `Keterangan`, `Login`, `Tgl`, `NotActive`) VALUES
('REG', 'Reguler', 'Reguler', '', '023059', '', '', '0000-00-00 00:00:00', 'N'),
('EXEC', 'Eksekutif', 'Eksekutif', '', '', '', '', '0000-00-00 00:00:00', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `programschedule`
--

CREATE TABLE `programschedule` (
  `Rank` int(11) NOT NULL DEFAULT '0',
  `ProgramSchedule` varchar(20) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `programtype`
--

CREATE TABLE `programtype` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Rank` int(11) NOT NULL DEFAULT '0',
  `ProgramType` varchar(20) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prop`
--

CREATE TABLE `prop` (
  `ID` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prop`
--

INSERT INTO `prop` (`ID`, `Nama`, `NotActive`) VALUES
('01', 'DKI JAKARTA\r', 'N'),
('02', 'JAWA BARAT\r', 'N'),
('03', 'JAWA TENGAH\r', 'N'),
('04', 'D I YOGYAKARTA\r', 'N'),
('05', 'JAWA TIMUR\r', 'N'),
('06', 'NANGROVE ACEH DARUSALAM\r', 'N'),
('07', 'SUMATERA UTARA\r', 'N'),
('08', 'SUMATERA BARAT\r', 'N'),
('09', 'RIAU\r', 'N'),
('10', 'JAMBI\r', 'N'),
('11', 'SUMATERA SELATAN\r', 'N'),
('12', 'LAMPUNG\r', 'N'),
('13', 'KALIMANTAN BARAT\r', 'N'),
('14', 'KALIMANTAN TENGAH\r', 'N'),
('15', 'KALIMANTAN SELATAN\r', 'N'),
('16', 'KALIMANTAN TIMUR\r', 'N'),
('17', 'SULAWESI UTARA\r', 'N'),
('18', 'SULAWESI TENGAH\r', 'N'),
('19', 'SULAWESI SELATAN\r', 'N'),
('20', 'SULAWESI TENGGARA\r', 'N'),
('21', 'MALUKU\r', 'N'),
('22', 'BALI\r', 'N'),
('23', 'NUSA TENGGARA BARAT\r', 'N'),
('24', 'NUSA TENGGARA TIMUR\r', 'N'),
('25', 'IRIAN JAYA\r', 'N'),
('26', 'BENGKULU\r', 'N'),
('27', 'TIMOR TIMUR\r', 'N'),
('28', 'BANTEN\r', 'N'),
('29', 'MALUKU UTARA\r', 'N'),
('30', 'BANGKA BELITUNG\r', 'N'),
('31', 'GORONTALO\r', 'N'),
('99', 'LUAR NEGERI', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rptmgr`
--

CREATE TABLE `rptmgr` (
  `ID` int(11) NOT NULL,
  `Rpt` varchar(100) NOT NULL DEFAULT '',
  `NoDok` varchar(100) DEFAULT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `PerTahun` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerMhsw` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerJur` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerPrg` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerDoc` enum('Y','N') NOT NULL DEFAULT 'N',
  `Bahasa` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerJnsByr` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerNamaByr` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerTglAwal` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerTglAkhir` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerBulan` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerStatusMhsw` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerAngkDari` enum('Y','N') NOT NULL DEFAULT 'N',
  `PerAngkSampai` enum('Y','N') NOT NULL DEFAULT 'N',
  `PakaiScript` enum('Y','N') NOT NULL DEFAULT 'Y',
  `Script` varchar(100) DEFAULT NULL,
  `Header` text,
  `Detail` text,
  `Footer` text,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Author` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rptmgr`
--

INSERT INTO `rptmgr` (`ID`, `Rpt`, `NoDok`, `Nama`, `PerTahun`, `PerMhsw`, `PerJur`, `PerPrg`, `PerDoc`, `Bahasa`, `PerJnsByr`, `PerNamaByr`, `PerTglAwal`, `PerTglAkhir`, `PerBulan`, `PerStatusMhsw`, `PerAngkDari`, `PerAngkSampai`, `PakaiScript`, `Script`, `Header`, `Detail`, `Footer`, `NotActive`, `Author`) VALUES
(1, 'cetakkhs', '008/Trans/Ket/STIE-S/07/2003', 'Kartu Hasil Studi', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/cetakkhs', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(5, 'keuperjur', '001/keu', 'Laporan Keuangan Per Jurusan', 'Y', 'N', 'Y', 'N', 'N', 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/keuperjur', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(6, 'kewajibanmhsw', '', 'Laporan Kewajiban Mahasiswa', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'N', 'N', 'Y', 'rpt/kewajibanmhsw', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(7, 'cetaktranskrip', '', 'Cetak Transkrip Nilai', 'N', 'Y', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/transkripnilai', NULL, NULL, NULL, 'N', 'Admin'),
(8, 'transkripsementara', '', 'Cetak Transkrip Nilai Sementara', 'N', 'Y', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/transkripsementara', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(9, 'laporanpenyetaraan', '', 'Hasil Penyetaraan', 'N', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/cetakpenyetaraan', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(10, 'laptrmbyr', '', 'Laporan Penerimaan', 'N', 'N', 'N', 'Y', 'N', 'N', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/laptrmbyr', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(11, 'dsnbimbing', '', 'Dosen Pembimbing Akademik', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'rpt/dsnbimbing', NULL, NULL, NULL, 'N', 'Admin'),
(12, 'rekaphonordosen', '', 'Rekapitulasi Honor Dosen', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/rekaphonordosen', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(13, 'rekaphadirdosen', '', 'Rekapitulasi Kehadiran Dosen', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'N', 'N', 'N', 'Y', 'rpt/rekaphadirdosen', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(14, 'potonganmhsw', '', 'Laporan Potongan Mahasiswa', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'N', 'N', 'Y', 'rpt/kewajibanmhsw', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(15, 'cetakipkmhsw', '', 'Laporan IPK Mahasiswa', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'Y', 'Y', 'rpt/cetakipkmhsw', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(16, 'evaluasi4smt', '', 'Evaluasi 4 Semester', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'N', 'Y', 'rpt/evaluasi4smt', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(17, 'transkriprincian', '', 'r- Rincian Transkrip Nilai', 'N', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/transkriprincian', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(18, 'lapbyrmhsw', '', 'r- Laporan Pembayaran Mhsw', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/lapbyrmhsw', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(19, 'lapbyrmhswrekap', '', 'r- Rekap Pembayaran Mhsw', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/lapbyrmhswrekap', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(20, 'lapbyrmhswharian', '', 'Laporan Harian Pembayaran Mahasiswa', 'Y', 'N', 'Y', 'Y', 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', 'N', 'N', 'N', 'Y', 'rpt/lapbyrmhswharian', NULL, NULL, NULL, 'N', 'E. Setio Dewo'),
(21, 'lapbyrlain', '', 'Laporan Rincian Pembayaran Lain', 'N', 'N', 'N', 'Y', 'N', 'N', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'rpt/lapbyrlain', NULL, NULL, NULL, 'N', 'E. Setio Dewo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `KodeKampus` varchar(10) NOT NULL DEFAULT '',
  `Lantai` varchar(10) NOT NULL DEFAULT '',
  `Kapasitas` int(11) NOT NULL DEFAULT '0',
  `KapasitasUjian` int(11) NOT NULL DEFAULT '0',
  `Keterangan` varchar(255) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`Kode`, `Nama`, `KodeKampus`, `Lantai`, `Kapasitas`, `KapasitasUjian`, `Keterangan`, `NotActive`, `Login`, `Tgl`) VALUES
('R101', 'R101', 'SISFO', '1', 40, 20, 'LCD Proyektor + Screen, Sound system, Komputer [CPU + Mouse + Keyboard]', 'N', 'admin', '2005-03-21 00:48:39'),
('R102', 'R102', 'SISFO', '1', 40, 20, 'LCD Projector + Screen, CPU', 'N', 'admin', '2005-03-21 00:49:12'),
('R103', 'R103', 'SISFO', '1', 40, 20, 'LCD Proyektor + Screen, Sound system, Komputer [CPU + Mouse + Keyboard]', 'N', 'admin', '2005-03-21 00:49:38'),
('D104', 'Ruang Dosen Tetap', '', '1', 30, 0, '5 unit komputer, dispenser', 'Y', 'admin', '2005-03-21 00:50:30'),
('S105', 'Studi Room 105', 'SISFO', '1', 4, 0, '', 'N', 'admin', '2005-03-21 00:51:54'),
('S106', 'Study Room 106', 'SISFO', '1', 4, 0, '', 'N', 'admin', '2005-03-21 00:52:31'),
('S107', 'Study Room 107', 'SISFO', '1', 4, 0, '', 'N', 'admin', '2005-03-21 00:53:03'),
('S108', 'Study Room 106', 'SISFO', '1', 4, 0, '', 'N', 'admin', '2005-03-21 00:53:25'),
('D104A', 'Ruang Dosen Luar', '', '1', 6, 0, 'Dispenser', 'Y', 'admin', '2005-03-21 00:54:11'),
('K201', 'Rektorat', 'SISFO', '2', 0, 0, '', 'N', 'admin', '2005-03-21 00:55:07'),
('K202', 'Administratif', 'SISFO', '2', 0, 0, '', 'N', 'admin', '2005-03-21 00:55:50'),
('K203', 'Keuangan', 'SISFO', '2', 0, 0, '', 'N', 'admin', '2005-03-21 00:56:17'),
('P301', 'Perpustakaan', 'SISFO', '3', 0, 0, '', 'N', 'admin', '2005-03-21 00:57:07'),
('R1A', 'R1A', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:17:16'),
('R1B', 'R1B', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:17:40'),
('R1C', 'R1C', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:18:13'),
('R1D', 'R1D', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:18:32'),
('R1E', 'R1E', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:18:52'),
('R1F', 'R1F', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:19:14'),
('R1G', 'R1G', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:19:35'),
('R1H', 'R1H', '023059', '1', 30, 25, '', 'N', 'admin', '2017-04-30 09:19:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `schooltype`
--

CREATE TABLE `schooltype` (
  `Rank` int(11) NOT NULL DEFAULT '0',
  `SchoolType` varchar(20) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `schooltype`
--

INSERT INTO `schooltype` (`Rank`, `SchoolType`, `NotActive`) VALUES
(100, 'SMU', 'N'),
(200, 'SMK', 'N'),
(300, 'MA', 'N'),
(400, 'Akademi', 'N'),
(500, 'Universitas', 'N'),
(600, 'Institut', 'N'),
(700, 'Sekolah Tinggi', 'N'),
(800, 'Lain-lain', 'N'),
(150, 'SMA', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setara`
--

CREATE TABLE `setara` (
  `ID` int(11) NOT NULL,
  `KodeSetara` varchar(20) NOT NULL DEFAULT '',
  `NamaSetara` varchar(50) NOT NULL DEFAULT '',
  `KodeJenisMK` varchar(10) NOT NULL DEFAULT '',
  `KodeFakultas` varchar(10) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `SKSSetara` varchar(10) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) NOT NULL DEFAULT '',
  `NamaMK` varchar(50) NOT NULL DEFAULT '',
  `SKS` int(11) NOT NULL DEFAULT '0',
  `LoginAdd` varchar(10) NOT NULL DEFAULT '',
  `TanggalAdd` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LoginEdit` varchar(10) NOT NULL DEFAULT '',
  `TanggalEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setup`
--

CREATE TABLE `setup` (
  `PMBPrefix` varchar(10) NOT NULL DEFAULT '',
  `PMBDescription` varchar(100) DEFAULT NULL,
  `PMBDigit` int(11) NOT NULL DEFAULT '0',
  `PMBNumber` int(11) NOT NULL DEFAULT '0',
  `PMBPrice` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setup`
--

INSERT INTO `setup` (`PMBPrefix`, `PMBDescription`, `PMBDigit`, `PMBNumber`, `PMBPrice`) VALUES
('20041', 'September 2004', 2, 200300001, 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setupkeuwajib`
--

CREATE TABLE `setupkeuwajib` (
  `ID` int(11) NOT NULL,
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Kali` int(11) NOT NULL DEFAULT '1',
  `Nama` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setupnim`
--

CREATE TABLE `setupnim` (
  `NIMPrefix` varchar(10) NOT NULL DEFAULT '',
  `NIMDescription` varchar(100) DEFAULT NULL,
  `NIMDigit` int(11) NOT NULL DEFAULT '0',
  `NIMNumber` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setupnim`
--

INSERT INTO `setupnim` (`NIMPrefix`, `NIMDescription`, `NIMDigit`, `NIMNumber`) VALUES
('2016', 'Tahun Ajaran Baru 2016', 4, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusawalmhsw`
--

CREATE TABLE `statusawalmhsw` (
  `Kode` char(1) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusawalmhsw`
--

INSERT INTO `statusawalmhsw` (`Kode`, `Nama`, `NotActive`) VALUES
('B', 'Baru', 'N'),
('P', 'Pindahan', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusdosen`
--

CREATE TABLE `statusdosen` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusdosen`
--

INSERT INTO `statusdosen` (`Kode`, `Nama`, `NotActive`) VALUES
('H', 'Honorer', 'N'),
('T', 'Tetap', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statuskerjadosen`
--

CREATE TABLE `statuskerjadosen` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statuskerjadosen`
--

INSERT INTO `statuskerjadosen` (`Kode`, `Nama`, `NotActive`) VALUES
('A', 'Dosen Biaya Yayasan', 'N'),
('B', 'Dosen PNS Dipekerjakan', 'N'),
('C', 'Dosen PNS', 'N'),
('D', 'Dosen Luar Biasa', 'N'),
('E', 'Dosen Kontrak', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusmhsw`
--

CREATE TABLE `statusmhsw` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `Nilai` smallint(6) NOT NULL DEFAULT '0',
  `Keluar` smallint(6) NOT NULL DEFAULT '0',
  `Def` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusmhsw`
--

INSERT INTO `statusmhsw` (`Kode`, `Nama`, `Nilai`, `Keluar`, `Def`, `NotActive`) VALUES
('A', 'Aktif', 1, 0, 'N', 'N'),
('C', 'Cuti', 0, 0, 'N', 'N'),
('D', 'Drop-Out/Pts Studi', 0, 1, 'N', 'N'),
('K', 'Keluar', 0, 1, 'N', 'N'),
('L', 'Lulus', 0, 1, 'N', 'N'),
('N', 'Non-Aktif', 0, 0, 'N', 'N'),
('M', 'Matrikulasi', 1, 0, 'N', 'N'),
('P', 'Pasif', 0, 0, 'Y', 'N'),
('S', 'Pasif', 0, 0, 'Y', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusmkmhsw`
--

CREATE TABLE `statusmkmhsw` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `Def` enum('Y','N') NOT NULL DEFAULT 'N',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusmkmhsw`
--

INSERT INTO `statusmkmhsw` (`Kode`, `Nama`, `Def`, `NotActive`) VALUES
('A', 'Penyetaraan', 'N', 'N'),
('B', 'Nilai Supra', 'Y', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statuspotongan`
--

CREATE TABLE `statuspotongan` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statuspotongan`
--

INSERT INTO `statuspotongan` (`Kode`, `Nama`, `NotActive`) VALUES
('G', 'Group Kalbe', 'N'),
('R1', 'Ranking 1', 'N'),
('B1', 'Beasiswa', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `Kode` varchar(5) NOT NULL DEFAULT '',
  `Nama` varchar(100) NOT NULL DEFAULT '',
  `KodeProgram` varchar(10) NOT NULL DEFAULT '',
  `KodeJurusan` varchar(10) NOT NULL DEFAULT '',
  `Tgl` date NOT NULL DEFAULT '0000-00-00',
  `unip` varchar(10) NOT NULL DEFAULT '',
  `ProsesBuka` int(11) NOT NULL DEFAULT '0',
  `TglProsesBuka` date NOT NULL DEFAULT '0000-00-00',
  `Proses` int(11) NOT NULL DEFAULT '0',
  `TglProses` date NOT NULL DEFAULT '0000-00-00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`Kode`, `Nama`, `KodeProgram`, `KodeJurusan`, `Tgl`, `unip`, `ProsesBuka`, `TglProsesBuka`, `Proses`, `TglProses`, `NotActive`) VALUES
('20162', 'Tahun Ajaran 2016/2017 Genap', 'REG', '62201', '2017-04-11', 'admin', 1, '0000-00-00', 0, '0000-00-00', 'N'),
('20161', 'Tahun Ajaran 2016-2017', 'REG', '62201', '2017-04-30', 'admin', 1, '0000-00-00', 1, '0000-00-00', 'N'),
('20162', 'Tahun Ajaran 2016/2017 Genap', 'REG', '61201', '2017-04-11', 'admin', 1, '0000-00-00', 0, '0000-00-00', 'N'),
('20161', 'Tahun Ajaran 2016-2017', 'REG', '61201', '2017-04-30', 'admin', 1, '0000-00-00', 1, '0000-00-00', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `totbayar`
--

CREATE TABLE `totbayar` (
  `ID` int(11) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `KodeBiaya` varchar(5) NOT NULL DEFAULT '',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `NamaBayar` varchar(100) DEFAULT NULL,
  `JenisTrx` int(11) NOT NULL DEFAULT '0',
  `Kali` int(11) NOT NULL DEFAULT '0',
  `JenisBayar` int(11) DEFAULT NULL,
  `Currency` varchar(5) NOT NULL DEFAULT 'IDR',
  `Kurs` decimal(8,6) NOT NULL DEFAULT '1.000000',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `Denda` enum('Y','N') NOT NULL DEFAULT 'N',
  `HariDenda` int(11) NOT NULL DEFAULT '0',
  `HariBebas` int(11) NOT NULL DEFAULT '0',
  `HargaDenda` int(11) NOT NULL DEFAULT '0',
  `Catatan` varchar(100) DEFAULT NULL,
  `BuktiBayar` varchar(50) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `usermodul`
--

CREATE TABLE `usermodul` (
  `UserModulID` int(11) NOT NULL,
  `Level` int(11) NOT NULL DEFAULT '5',
  `UserID` int(11) NOT NULL DEFAULT '0',
  `GroupModul` varchar(20) NOT NULL DEFAULT '',
  `ModulID` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_dosen`
--

CREATE TABLE `_dosen` (
  `ID` int(11) NOT NULL,
  `OldID` varchar(10) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(10) NOT NULL DEFAULT '',
  `Description` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(50) NOT NULL DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N',
  `Gelar` varchar(100) NOT NULL DEFAULT '',
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `KodeGolongan` varchar(10) DEFAULT NULL,
  `TglMasuk` date DEFAULT '0000-00-00',
  `TglKeluar` date DEFAULT '0000-00-00',
  `KodeStatus` varchar(10) DEFAULT NULL,
  `InstansiInduk` varchar(10) DEFAULT NULL,
  `KodeDosen` varchar(10) DEFAULT NULL,
  `Alamat1` varchar(100) DEFAULT NULL,
  `Alamat2` varchar(100) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `Propinsi` varchar(50) DEFAULT NULL,
  `Negara` varchar(50) DEFAULT NULL,
  `KodePos` varchar(20) DEFAULT NULL,
  `TempatLahir` varchar(100) DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `Sex` char(1) DEFAULT NULL,
  `AgamaID` int(11) DEFAULT NULL,
  `JabatanOrganisasi` varchar(10) DEFAULT NULL,
  `JabatanAkademik` char(1) DEFAULT NULL,
  `JabatanDikti` char(1) DEFAULT NULL,
  `KTP` varchar(50) DEFAULT NULL,
  `JenjangDosen` char(1) DEFAULT NULL,
  `LulusanPT` varchar(100) DEFAULT NULL,
  `Ilmu` varchar(100) DEFAULT NULL,
  `Akta` enum('Y','N','T') NOT NULL DEFAULT 'N',
  `Ijin` enum('Y','N','T') NOT NULL DEFAULT 'N',
  `Bank` varchar(100) DEFAULT NULL,
  `AccountName` varchar(100) DEFAULT NULL,
  `AccountNumber` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_krs`
--

CREATE TABLE `_krs` (
  `ID` int(11) NOT NULL,
  `IDKRS` int(11) NOT NULL DEFAULT '0',
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT '',
  `SMT` varchar(5) NOT NULL DEFAULT '',
  `Sesi` int(11) NOT NULL DEFAULT '0',
  `IDJadwal` int(11) NOT NULL DEFAULT '0',
  `IDMK` int(11) NOT NULL DEFAULT '0',
  `KodeMK` varchar(10) DEFAULT NULL,
  `NamaMK` varchar(255) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `Status` varchar(5) DEFAULT NULL,
  `Program` varchar(10) DEFAULT NULL,
  `IDDosen` int(11) NOT NULL DEFAULT '0',
  `unip` varchar(20) NOT NULL DEFAULT '',
  `Tanggal` datetime DEFAULT NULL,
  `hr_1` char(2) DEFAULT NULL,
  `hr_2` char(2) DEFAULT NULL,
  `hr_3` char(2) DEFAULT NULL,
  `hr_4` char(2) DEFAULT NULL,
  `hr_5` char(2) DEFAULT NULL,
  `hr_6` char(2) DEFAULT NULL,
  `hr_7` char(2) DEFAULT NULL,
  `hr_8` char(2) DEFAULT NULL,
  `hr_9` char(2) DEFAULT NULL,
  `hr_10` char(2) DEFAULT NULL,
  `hr_11` char(2) DEFAULT NULL,
  `hr_12` char(2) DEFAULT NULL,
  `hr_13` char(2) DEFAULT NULL,
  `hr_14` char(2) DEFAULT NULL,
  `hr_15` char(2) DEFAULT NULL,
  `hr_16` char(2) DEFAULT NULL,
  `hr_17` char(2) DEFAULT NULL,
  `hr_18` char(2) DEFAULT NULL,
  `hr_19` char(2) DEFAULT NULL,
  `hr_20` char(2) DEFAULT NULL,
  `Hadir` decimal(5,2) DEFAULT '0.00',
  `Tugas1` decimal(5,2) DEFAULT '0.00',
  `Tugas2` decimal(5,2) DEFAULT '0.00',
  `Tugas3` decimal(5,2) DEFAULT '0.00',
  `Tugas4` decimal(5,2) DEFAULT '0.00',
  `Tugas5` decimal(5,2) DEFAULT '0.00',
  `NilaiMID` decimal(5,2) DEFAULT '0.00',
  `NilaiUjian` decimal(5,2) DEFAULT '0.00',
  `Nilai` decimal(5,2) DEFAULT '0.00',
  `GradeNilai` varchar(5) DEFAULT NULL,
  `Bobot` decimal(5,2) DEFAULT '0.00',
  `Tunda` enum('Y','N') DEFAULT 'N',
  `AlasanTunda` varchar(255) DEFAULT NULL,
  `Setara` enum('Y','N','A','B') NOT NULL DEFAULT 'N',
  `KodeSetara` varchar(10) NOT NULL DEFAULT '',
  `MKSetara` varchar(100) DEFAULT NULL,
  `SKSSetara` int(11) DEFAULT '0',
  `GradeSetara` varchar(5) DEFAULT NULL,
  `Dispensasi` enum('Y','N') NOT NULL DEFAULT 'N',
  `TglDispensasi` date DEFAULT NULL,
  `KetDispensasi` varchar(100) DEFAULT NULL,
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_krs`
--

INSERT INTO `_krs` (`ID`, `IDKRS`, `NIM`, `Tahun`, `SMT`, `Sesi`, `IDJadwal`, `IDMK`, `KodeMK`, `NamaMK`, `SKS`, `Status`, `Program`, `IDDosen`, `unip`, `Tanggal`, `hr_1`, `hr_2`, `hr_3`, `hr_4`, `hr_5`, `hr_6`, `hr_7`, `hr_8`, `hr_9`, `hr_10`, `hr_11`, `hr_12`, `hr_13`, `hr_14`, `hr_15`, `hr_16`, `hr_17`, `hr_18`, `hr_19`, `hr_20`, `Hadir`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `NilaiMID`, `NilaiUjian`, `Nilai`, `GradeNilai`, `Bobot`, `Tunda`, `AlasanTunda`, `Setara`, `KodeSetara`, `MKSetara`, `SKSSetara`, `GradeSetara`, `Dispensasi`, `TglDispensasi`, `KetDispensasi`, `NotActive`) VALUES
(1, 7, '2016110001', '20161', '0', 1, 15, 64, 'MKK 201', 'PENGANTAR EKONOMI MIKRO', 3, '', 'REG', 16, '2016110001', '2017-04-30 10:56:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.60', '87.60', 'A', '4.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(2, 6, '2016110001', '20161', '0', 1, 13, 65, 'MKK 202', 'PENGANTAR AKUNTANSI', 3, '', 'REG', 3, '2016110001', '2017-04-30 10:55:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '78.50', '78.50', 'B', '3.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(3, 4, '2016110001', '20161', '0', 1, 16, 66, 'MKK 203', 'MATEMATIKA EKONOMI', 3, '', 'REG', 9, '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '80.00', 'B', '3.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(4, 780, '2016110001', '20162', '0', 2, 36, 67, 'MKK 204', 'PENGANTAR EKONOMI MAKRO', 3, '', 'REG', 21, 'admin', '2017-04-18 14:38:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(5, 777, '2016110001', '20162', '0', 2, 37, 68, 'MKK 205', 'PENGANTAR MANAJEMEN', 3, '', 'REG', 16, 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(6, 781, '2016110001', '20162', '0', 2, 38, 69, 'MKK 206', 'PENGANTAR BISNIS', 3, '', 'REG', 16, 'admin', '2017-04-18 14:38:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(7, 783, '2016110001', '20162', '0', 2, 42, 70, 'MKK 207', 'STATISTIK', 3, '', 'REG', 10, 'admin', '2017-04-18 14:38:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(8, 779, '2016110001', '20162', '0', 2, 41, 75, 'MKK 212', 'PENGANTAR APLIKASI KOMPUTER', 3, '', 'REG', 19, 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(9, 778, '2016110001', '20162', '0', 2, 39, 76, 'MKK 213', 'PENGANTAR EKONOMI PEMBANGUNAN', 3, '', 'REG', 21, 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(10, 782, '2016110001', '20162', '0', 2, 40, 78, 'MPB 401', 'ENGLISH FOR BUSINESS', 3, '', 'REG', 6, 'admin', '2017-04-18 14:38:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(11, 1, '2016110001', '20161', '0', 1, 10, 57, 'MPK 101', 'PENDIDIKAN AGAMA', 2, '', 'REG', 8, '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '87.30', '87.30', 'A', '4.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(12, 2, '2016110001', '20161', '0', 1, 12, 58, 'MPK 102', 'PENDIDIKAN PANCASILA', 2, '', 'REG', 8, '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '95.80', '95.80', 'A', '4.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(13, 3, '2016110001', '20161', '0', 1, 14, 59, 'MPK 103', 'ILMU ALAMIAH DASAR', 2, '', 'REG', 8, '2016110001', '2017-04-30 10:52:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', 'A', '4.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(14, 776, '2016110001', '20162', '0', 2, 35, 60, 'MPK 104', 'KEWARGANEGARAAN', 2, '', 'REG', 8, 'admin', '2017-04-18 14:37:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'E', '0.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(15, 8, '2016110001', '20161', '0', 1, 17, 61, 'MPK 105', 'BAHASA INDONESIA', 2, '', 'REG', 8, '2016110001', '2017-04-30 10:56:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', '90.00', 'A', '4.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N'),
(16, 5, '2016110001', '20161', '0', 1, 11, 62, 'MPK 106', 'BAHASA INGGRIS', 3, '', 'REG', 8, '2016110001', '2017-04-30 10:55:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', '80.00', 'B', '3.00', 'N', '', 'N', '', '', 0, '', 'N', '0000-00-00', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_kur`
--

CREATE TABLE `_kur` (
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `_KodeFakultas` varchar(5) DEFAULT NULL,
  `_KodeJurusan` varchar(5) DEFAULT NULL,
  `KodeMK` varchar(10) DEFAULT NULL,
  `NAKUR` varchar(5) DEFAULT NULL,
  `MinNilai` varchar(5) DEFAULT NULL,
  `UNCKUR` varchar(5) DEFAULT NULL,
  `Sesi` varchar(5) DEFAULT NULL,
  `SKS` char(3) NOT NULL DEFAULT '0',
  `SKSPT` char(3) NOT NULL DEFAULT '0',
  `SKSPL` char(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_matakuliah`
--

CREATE TABLE `_matakuliah` (
  `ID` int(11) NOT NULL,
  `Kode` varchar(10) NOT NULL DEFAULT '',
  `Nama_Indonesia` varchar(100) NOT NULL DEFAULT '',
  `Nama_English` varchar(100) DEFAULT NULL,
  `KurikulumID` int(11) DEFAULT NULL,
  `KodeFakultas` varchar(10) DEFAULT NULL,
  `KodeJurusan` varchar(10) DEFAULT NULL,
  `SKS` int(11) NOT NULL DEFAULT '0',
  `SKSTatapMuka` int(11) NOT NULL DEFAULT '0',
  `SKSPraktikum` int(11) NOT NULL DEFAULT '0',
  `SKSPraktekLap` int(11) NOT NULL DEFAULT '0',
  `SKSMin` int(11) NOT NULL DEFAULT '0',
  `IPMin` decimal(5,2) NOT NULL DEFAULT '0.00',
  `GradeMin` varchar(5) DEFAULT NULL,
  `KodeJenisMK` varchar(10) NOT NULL DEFAULT '',
  `Wajib` enum('Y','N') NOT NULL DEFAULT 'N',
  `DoubleMajor` varchar(10) DEFAULT NULL,
  `Sesi` int(11) NOT NULL DEFAULT '1',
  `Login` varchar(10) NOT NULL DEFAULT '',
  `Tgl` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NotActive` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_mhswlalu`
--

CREATE TABLE `_mhswlalu` (
  `NIM` varchar(20) NOT NULL DEFAULT '',
  `Status` varchar(5) NOT NULL DEFAULT '',
  `Tahun` varchar(5) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_mk`
--

CREATE TABLE `_mk` (
  `RESTATBKMK` varchar(5) DEFAULT NULL,
  `KodeMK` varchar(10) DEFAULT NULL,
  `Nama_Indonesia` varchar(255) DEFAULT NULL,
  `Nama_English` varchar(255) DEFAULT NULL,
  `SKS` varchar(5) DEFAULT NULL,
  `MKDU` char(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_prasyaratmk`
--

CREATE TABLE `_prasyaratmk` (
  `KodeMK` varchar(20) DEFAULT NULL,
  `Pra1` varchar(20) DEFAULT NULL,
  `GradeMin1` varchar(20) DEFAULT NULL,
  `SKSPra1` varchar(20) DEFAULT NULL,
  `Pra2` varchar(20) DEFAULT NULL,
  `GradeMin2` varchar(20) DEFAULT NULL,
  `SKSPra2` varchar(20) DEFAULT NULL,
  `Pra3` varchar(20) DEFAULT NULL,
  `GradeMin3` varchar(20) DEFAULT NULL,
  `SKSPra3` varchar(20) DEFAULT NULL,
  `Pra4` varchar(20) DEFAULT NULL,
  `GradeMin4` varchar(20) DEFAULT NULL,
  `SKSPra4` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_survey_hasil`
--

CREATE TABLE `_survey_hasil` (
  `IDNilai` int(11) NOT NULL,
  `NIM` varchar(20) NOT NULL,
  `IDDosen` int(11) NOT NULL,
  `KodeMK` varchar(10) NOT NULL,
  `IDPertanyaan` int(11) NOT NULL,
  `Jawaban_essay` text NOT NULL,
  `IDRanking` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_survey_pertanyaan`
--

CREATE TABLE `_survey_pertanyaan` (
  `id` int(11) NOT NULL,
  `pertanyaan` text NOT NULL,
  `deskripsi` text NOT NULL,
  `jenis` enum('essay','ranking') NOT NULL DEFAULT 'ranking'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `_survey_rangking`
--

CREATE TABLE `_survey_rangking` (
  `IDRangking` int(11) NOT NULL,
  `nilai` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Login` (`Login`);

--
-- Indexes for table `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`AgamaID`);

--
-- Indexes for table `akreditasi`
--
ALTER TABLE `akreditasi`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `aktivitasdosen`
--
ALTER TABLE `aktivitasdosen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Login` (`Login`),
  ADD KEY `PMBID` (`PMBID`);

--
-- Indexes for table `bahasa`
--
ALTER TABLE `bahasa`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `bataskrs`
--
ALTER TABLE `bataskrs`
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `KodeProgram` (`KodeProgram`);

--
-- Indexes for table `bayar`
--
ALTER TABLE `bayar`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `NIM` (`NIM`);

--
-- Indexes for table `bayar2`
--
ALTER TABLE `bayar2`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `BayarID` (`BayarID`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`Kode`,`KodeProgram`),
  ADD KEY `KodeBPPP` (`KodeBPPP`);

--
-- Indexes for table `biaya2`
--
ALTER TABLE `biaya2`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KodeBiaya` (`KodeBiaya`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `Otomatis` (`Otomatis`),
  ADD KEY `Nama` (`Nama`);

--
-- Indexes for table `biayamhsw`
--
ALTER TABLE `biayamhsw`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `KodeBiaya` (`KodeBiaya`),
  ADD KEY `IDBiaya2` (`IDBiaya2`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `IDBayar` (`IDBayar`);

--
-- Indexes for table `bimbinganta`
--
ALTER TABLE `bimbinganta`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bpppokok`
--
ALTER TABLE `bpppokok`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bpppokok1`
--
ALTER TABLE `bpppokok1`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KodeBPPP` (`KodeBPPP`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`Symbol`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `groupmodul`
--
ALTER TABLE `groupmodul`
  ADD PRIMARY KEY (`GroupModulID`),
  ADD KEY `GroupModul` (`GroupModul`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `honormgm`
--
ALTER TABLE `honormgm`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jabatanakademik`
--
ALTER TABLE `jabatanakademik`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `jabatandikti`
--
ALTER TABLE `jabatandikti`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `jabatanorganisasi`
--
ALTER TABLE `jabatanorganisasi`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `IDDosen` (`IDDosen`),
  ADD KEY `Global` (`Global`),
  ADD KEY `Hari` (`Hari`),
  ADD KEY `KodeKampus` (`KodeKampus`);

--
-- Indexes for table `jadwalassdsn`
--
ALTER TABLE `jadwalassdsn`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDJadwal` (`IDJadwal`,`IDDosen`);

--
-- Indexes for table `jadwalgantidsn`
--
ALTER TABLE `jadwalgantidsn`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jenisbayar`
--
ALTER TABLE `jenisbayar`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jenismatakuliah`
--
ALTER TABLE `jenismatakuliah`
  ADD PRIMARY KEY (`Kode`,`KodeFakultas`,`Jenjang`);

--
-- Indexes for table `jenistrx`
--
ALTER TABLE `jenistrx`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jenjangps`
--
ALTER TABLE `jenjangps`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `jurprg`
--
ALTER TABLE `jurprg`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `KodeProgram` (`KodeProgram`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `kampus`
--
ALTER TABLE `kampus`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kewajibanmengajar`
--
ALTER TABLE `kewajibanmengajar`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDJadwal` (`IDJadwal`);

--
-- Indexes for table `khs`
--
ALTER TABLE `khs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `krs`
--
ALTER TABLE `krs`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `Program` (`Program`),
  ADD KEY `IDDosen` (`IDDosen`),
  ADD KEY `IDJadwal` (`IDJadwal`);

--
-- Indexes for table `krs1`
--
ALTER TABLE `krs1`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `Program` (`Program`),
  ADD KEY `IDDosen` (`IDDosen`);

--
-- Indexes for table `krskonv`
--
ALTER TABLE `krskonv`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `Program` (`Program`),
  ADD KEY `IDDosen` (`IDDosen`),
  ADD KEY `IDJadwal` (`IDJadwal`),
  ADD KEY `krsID` (`krsID`);

--
-- Indexes for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`Level`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Kode` (`Kode`),
  ADD KEY `KodeJenisMK` (`KodeJenisMK`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `KurikulumID` (`KurikulumID`);

--
-- Indexes for table `maxsks`
--
ALTER TABLE `maxsks`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KodeJur` (`KodeJurusan`);

--
-- Indexes for table `mbrgetmbr`
--
ALTER TABLE `mbrgetmbr`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mhsw`
--
ALTER TABLE `mhsw`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Login` (`Login`),
  ADD KEY `PMBID` (`PMBID`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`ModulID`),
  ADD KEY `GroupModul` (`GroupModul`),
  ADD KEY `InMenu` (`InMenu`),
  ADD KEY `Level` (`Level`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`NewsID`),
  ADD KEY `Language` (`Language`);

--
-- Indexes for table `newscategory`
--
ALTER TABLE `newscategory`
  ADD PRIMARY KEY (`Category`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Nilai` (`Nilai`);

--
-- Indexes for table `org`
--
ALTER TABLE `org`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `orgmbr`
--
ALTER TABLE `orgmbr`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pengawasujian`
--
ALTER TABLE `pengawasujian`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pmb`
--
ALTER TABLE `pmb`
  ADD PRIMARY KEY (`PMBID`);

--
-- Indexes for table `pmbsyarat`
--
ALTER TABLE `pmbsyarat`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `polling`
--
ALTER TABLE `polling`
  ADD PRIMARY KEY (`PollID`),
  ADD KEY `unip` (`unip`);

--
-- Indexes for table `pollitem`
--
ALTER TABLE `pollitem`
  ADD PRIMARY KEY (`PollItemID`),
  ADD KEY `PollID` (`PollID`);

--
-- Indexes for table `prasyaratmk`
--
ALTER TABLE `prasyaratmk`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `predikat`
--
ALTER TABLE `predikat`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `prgbpppokok`
--
ALTER TABLE `prgbpppokok`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Tahun` (`Tahun`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `prop`
--
ALTER TABLE `prop`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rptmgr`
--
ALTER TABLE `rptmgr`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`Kode`),
  ADD KEY `KodeKampus` (`KodeKampus`);

--
-- Indexes for table `setara`
--
ALTER TABLE `setara`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `setupkeuwajib`
--
ALTER TABLE `setupkeuwajib`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `statusawalmhsw`
--
ALTER TABLE `statusawalmhsw`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `statuskerjadosen`
--
ALTER TABLE `statuskerjadosen`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `statusmhsw`
--
ALTER TABLE `statusmhsw`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `statusmkmhsw`
--
ALTER TABLE `statusmkmhsw`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `statuspotongan`
--
ALTER TABLE `statuspotongan`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`Kode`,`KodeJurusan`,`KodeProgram`);

--
-- Indexes for table `totbayar`
--
ALTER TABLE `totbayar`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `NIM` (`NIM`);

--
-- Indexes for table `usermodul`
--
ALTER TABLE `usermodul`
  ADD PRIMARY KEY (`UserModulID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `GroupModul` (`GroupModul`),
  ADD KEY `ModulID` (`ModulID`),
  ADD KEY `Level` (`Level`);

--
-- Indexes for table `_dosen`
--
ALTER TABLE `_dosen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `_krs`
--
ALTER TABLE `_krs`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NIM` (`NIM`),
  ADD KEY `Tahun` (`Tahun`),
  ADD KEY `Program` (`Program`),
  ADD KEY `IDDosen` (`IDDosen`),
  ADD KEY `IDJadwal` (`IDJadwal`);

--
-- Indexes for table `_matakuliah`
--
ALTER TABLE `_matakuliah`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Kode` (`Kode`),
  ADD KEY `KodeJenisMK` (`KodeJenisMK`),
  ADD KEY `KodeJurusan` (`KodeJurusan`),
  ADD KEY `KurikulumID` (`KurikulumID`);

--
-- Indexes for table `_survey_hasil`
--
ALTER TABLE `_survey_hasil`
  ADD PRIMARY KEY (`IDNilai`);

--
-- Indexes for table `_survey_pertanyaan`
--
ALTER TABLE `_survey_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_survey_rangking`
--
ALTER TABLE `_survey_rangking`
  ADD PRIMARY KEY (`IDRangking`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `aktivitasdosen`
--
ALTER TABLE `aktivitasdosen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bayar`
--
ALTER TABLE `bayar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bayar2`
--
ALTER TABLE `bayar2`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `biaya2`
--
ALTER TABLE `biaya2`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `biayamhsw`
--
ALTER TABLE `biayamhsw`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bimbinganta`
--
ALTER TABLE `bimbinganta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bpppokok`
--
ALTER TABLE `bpppokok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bpppokok1`
--
ALTER TABLE `bpppokok1`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `groupmodul`
--
ALTER TABLE `groupmodul`
  MODIFY `GroupModulID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=604;
--
-- AUTO_INCREMENT for table `honormgm`
--
ALTER TABLE `honormgm`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `jadwalassdsn`
--
ALTER TABLE `jadwalassdsn`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jadwalgantidsn`
--
ALTER TABLE `jadwalgantidsn`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jenisbayar`
--
ALTER TABLE `jenisbayar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jenistrx`
--
ALTER TABLE `jenistrx`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jurprg`
--
ALTER TABLE `jurprg`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kewajibanmengajar`
--
ALTER TABLE `kewajibanmengajar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `khs`
--
ALTER TABLE `khs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `krs`
--
ALTER TABLE `krs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1112;
--
-- AUTO_INCREMENT for table `krs1`
--
ALTER TABLE `krs1`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `krskonv`
--
ALTER TABLE `krskonv`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kurikulum`
--
ALTER TABLE `kurikulum`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2018;
--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `maxsks`
--
ALTER TABLE `maxsks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `mbrgetmbr`
--
ALTER TABLE `mbrgetmbr`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mhsw`
--
ALTER TABLE `mhsw`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `modul`
--
ALTER TABLE `modul`
  MODIFY `ModulID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `NewsID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `org`
--
ALTER TABLE `org`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orgmbr`
--
ALTER TABLE `orgmbr`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pengawasujian`
--
ALTER TABLE `pengawasujian`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `pmbsyarat`
--
ALTER TABLE `pmbsyarat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `polling`
--
ALTER TABLE `polling`
  MODIFY `PollID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pollitem`
--
ALTER TABLE `pollitem`
  MODIFY `PollItemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `prasyaratmk`
--
ALTER TABLE `prasyaratmk`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `predikat`
--
ALTER TABLE `predikat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `prgbpppokok`
--
ALTER TABLE `prgbpppokok`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rptmgr`
--
ALTER TABLE `rptmgr`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `setara`
--
ALTER TABLE `setara`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setupkeuwajib`
--
ALTER TABLE `setupkeuwajib`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `totbayar`
--
ALTER TABLE `totbayar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usermodul`
--
ALTER TABLE `usermodul`
  MODIFY `UserModulID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_dosen`
--
ALTER TABLE `_dosen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_krs`
--
ALTER TABLE `_krs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `_matakuliah`
--
ALTER TABLE `_matakuliah`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_survey_hasil`
--
ALTER TABLE `_survey_hasil`
  MODIFY `IDNilai` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_survey_pertanyaan`
--
ALTER TABLE `_survey_pertanyaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_survey_rangking`
--
ALTER TABLE `_survey_rangking`
  MODIFY `IDRangking` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
