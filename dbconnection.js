var mysql = require('mysql');
var connection = mysql.createPool({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'ams'
});

module.exports = connection;