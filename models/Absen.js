var db 		= require('../dbconnection'),
	Absen 	= {
		getAll: function( callback )
		{
			return db.query( "select * from absensi order by Kode asc", callback );
		},
		getCount: function( callback )
		{
			return db.query( "select count( Kode ) as Total from absensi", callback );
		},
		getPartWhere: function( Field, Kode, offset, limit, callback )
		{
			return db.query( "select * from absensi where ? like %?% limit ?, ?", [Field, Kode, offset, limit], callback );
		},
		getPart: function( offset, limit, callback )
		{
			return db.query( "select * from absensi limit ?, ?", [offset, limit], callback );
		},
		getByKode: function( Kode, callback )
		{
			return db.query( "select * from where Kode=?", Kode, callback );
		},
		add: function( Absen, callback )
		{
			return db.query( "insert into absensi (Kode, Nama, Nilai, NotActive) values( ?,?,?,? ) ",[Absen.Kode, Absen.Nama, Absen.Nilai, Absen.NotActive], callback );
		},
		update: function( Kode, Absen, callback )
		{
			return db.query( "update absensi set Nama=?, Nilai=?, NotActive=? where Kode=?",[Absen.Nama, Absen.Nilai, Absen.NotActive, Kode], callback );
		},
		delete: function( Kode, callback )
		{
			return db.query( "delete from absensi where Kode=?", Kode, callback );
		}
	};