var db 		= require('../dbconnection'),
	Agama	= {
		getAll: function( callback )
		{
			return db.query( "select * from agama order by AgamaID ASC", callback );
		},
		getPart: function( Offset, Limit, callback )
		{
			return db.query( "select * from agama limit ?,?", [ Offset, Limit ], callback );
		},
		getPartWhere: function( Field, Value, Offset, Limit, callback )
		{
			return db.query( "select * from agama where ? like %?% limit ?,?", [Field, Value, Offset, Limit], callback );
		},
		getByID: function( AgamaID, callback )
		{
			return db.query( "select * from agama where AgamaID=? limit 1", AgamaID, callback );
		},
		getCount: function( callback )
		{
			return db.query( "select count( AgamaID ) as Total from agama", callback );
		},
		getDynamicSingle: function( Field, Value, callback )
		{
			return db.query( "select * from agama where ?=? limit 1", [Field, Value], callback );
		},
		add: function( Agama, callback )
		{
			return db.query( "insert into agama (Agama, NotActive) values( ?,?)", [Agama.Agama, Agama.NotActive], callback );
		},
		update: function( AgamaID, Agama, callback )
		{
			return db.query( "update agama set Agama=?, NotActive=? where AgamaID=?", [Agama.Agama, Agama.NotActive, AgamaID], callback );
		},
		delete: function( AgamaID, callback )
		{
			return db.query( "delete from agama where AgamaID=?", AgamaID, callback );
		}
	};