var db 			= require('../dbconnection'),
	Akreditasi	= {
		getAll: function( callback )
		{
			return db.query( "select * from akreditasi order by Kode asc", callback );
		},
		getPart: function( Offset, Limit, callback )
		{
			return db.query( "select * from akreditasi limit ?,?", [Offset, Limit], callback );
		},
		getPartWhere: function( Field, Value, Offset, Limit, callback )
		{
			return db.query( "select * from akreditasi where ? like %?% limit ?,?", [Field, Value, Offset, Limit], callback );
		},
		getCount: function( callback )
		{
			return db.query( "select count( Kode ) as Total from akreditasi ", callback );
		},
		getByID: function( Kode, callback )
		{
			return db.query( "select * from akreditasi where Kode=?", Kode, callback );
		},
		getDynamicSingle: function( Field, Value, callback )
		{
			return db.query( "select * from akreditasi where ? = ? limit 1", [Field, Value], callback );
		},
		add: function( Akreditasi, callback )
		{
			return db.query( "insert into akreditasi (Nama, NotActive) values(?,?)", [Akreditasi.Nama, Akreditasi.NotActive], callback );
		},
		update: function( Kode, Akreditasi, callback )
		{
			return db.query( "update akreditasi set Nama=?, NotActive=? where Kode=?", [Akreditasi.Nama, Akreditasi.NotActive, Kode], callback );
		},
		delete: function( Kode, callback )
		{
			return db.query( "delete from akreditasi where Kode=?", Kode, callback );
		}
	};