var db 				= require('../dbconnection'),
	AktifitasDosen	= {
		getPart: function( Offset, Limit, callback )
		{
			return db.query( "select * from aktivitasdosen limit ?,?", [Offset, Limit], callback );
		},
		getPartWhere: function( Field, Value, Offset, Limit, callback )
		{
			return db.query( "select * from aktivitasdosen where ? like %?% limit ?,?", [Field, Value, Offset, Limit], callback );
		},
		getCount: function( callback )
		{
			return db.query( "select count( ID ) Total from aktivitasdosen ", callback );
		},
		getByID: function( ID, callback )
		{
			return db.query( "select * from aktivitasdosen where ID=? limit 1", ID, callback );
		},
		getDynamic: function( Field, Value, callback )
		{
			return db.query( "select * from aktivitasdosen where ? = ?", [Field, Value], callback );
		},
		add: function( AktifitasDosen, callback )
		{
			return db.query( "insert into aktivitasdosen (IDDosen, Tanggal, Aktivitas, Judul, Publikasi, Deskripsi, Lokasi, Keterangan, Audit, TglAudit, Login, TglLogin, NotActive) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", [AktifitasDosen.IDDosen, AktifitasDosen.Tanggal, AktifitasDosen.Aktivitas, AktifitasDosen.Judul, AktifitasDosen.Publikasi, AktifitasDosen.Deskripsi, AktifitasDosen.Lokasi, AktifitasDosen.Keterangan, AktifitasDosen.Audit, AktifitasDosen.TglAudit, AktifitasDosen.Login, AktifitasDosen.TglLogin, AktifitasDosen.NotActive], callback );
		},
		update: function( ID, AktifitasDosen, callback )
		{
			return db.query( "update aktivitasdosen set IDDosen=?, Tanggal=?, Aktivitas=?, Judul=?, Publikasi=?, Deskripsi=?, Lokasi=?, Keterangan=?, Audit=?, TglAudit=?, Login=?, TglLogin=?, NotActive=? where ID=?", [AktifitasDosen.IDDosen, AktifitasDosen.Tanggal, AktifitasDosen.Aktivitas, AktifitasDosen.Judul, AktifitasDosen.Publikasi, AktifitasDosen.Deskripsi, AktifitasDosen.Lokasi, AktifitasDosen.Keterangan, AktifitasDosen.Audit, AktifitasDosen.TglAudit, AktifitasDosen.Login, AktifitasDosen.TglLogin, AktifitasDosen.NotActive, ID], callback );
		},
		delete: function( ID, callback )
		{
			return db.query( "delete from aktivitasdosen where ID=?", ID, callback );
		}
	};