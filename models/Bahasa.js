var db		= require('../dbconnection'),
	Bahasa	= {
		getPart: function( Offset, Limit, callback )
		{
			return db.query("select * from bahasa limit ?,?", [Offset, Limit], callback);
		},
		getPartWhere: function( Field, Value, Offset, Limit, callback )
		{
			return db.query("select * from bahasa where ? like %?% Limit ?,?", [Field, Value, Offset, Limit], callback);
		},
		getCount: function( callback )
		{
			return db.query("select count( Kode ) Total from bahasa", callback);
		},
		getByID: function( Kode, callback )
		{
			return db.query("select * from bahasa where Kode=? limit 1", Kode, callback);
		},
		getSingleDynamic: function( Field, Value, callback )
		{
			return db.query( "select * from bahasa where ? = ? limit 1", [Field, Value], callback );
		},
		add: function( Bahasa, callback )
		{
			return db.query("insert into bahasa (Nama, Def, NotActive) values(?,?,?)", [Bahasa.Nama, Bahasa.Def, Bahasa.NotActive], callback);
		},
		update: function( Kode, Bahasa, callback )
		{
			return db.query( "update bahasa set Nama=?, Def=?, NotActive=? where Kode=?", [Bahasa.Nama, Bahasa.Def, Bahasa.NotActive, Kode], callback );
		},
		delete: function( Kode, callback )
		{
			return db.query( "delete from bahasa where Kode=?", Kode, callback );
		}
	};