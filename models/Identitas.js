var db = require('../dbconnection'),
	Institusi = {
		getAll: function( callback )
		{
			return db.query( "select * from identitas order by Kode ASC", callback );
		},
		getPart: function( offset, limit, callback )
		{
			return db.query( "select * from identitas limit ?,?", [offset, limit], callback );
		},
		getPartWhere: function( field, Kode, offset, limit, callback )
		{
			return db.query( "select * from identitas where ? like %?% limit ?,?",  [field, Kode, offset, limit], callback);
		},
		getCount: function( callback )
		{
			return db.query( "select count(Kode) as Total from identitas", callback );
		},
		getByKode : function( Kode, callback )
		{
			return db.query( "select * from identitas where Kode=? limit 1", Kode, callback );
		},
		getByDomain: function( Domain, callback )
		{
			return db.query( "select * from identitas where domain=?", Domain, callback );
		},
		getByIP: function( IP, callback )
		{
			return db.query( "select * from identitas where IP=?", IP, callback );
		},
		update: function( Kode, Institusi, callback )
		{
			return db.query( "update institusi set domain=?, IP=?, KodeHukum=?, Nama=?, TglMulai=?, Alamat1=?, Alamat2=?, Kota=?, KodePos=?, Telp=?, Fax=?, Email=?, Website=?, NoAkta=?, TglAkta=?, NoSah=?, TglSah=?, Logo=? where Kode=?", [ Institusi.domain, Institusi.IP, Institusi.KodeHukum, Institusi.Nama, Institusi.TglMulai, Institusi.Alamat1, Institusi.Alamat2, Institusi.Kota, Institusi.KodePos, Institusi.Telp, Institusi.Fax, Institusi.Email, Institusi.Website, Institusi.NoAkta, Institusi.TglAkta, Institusi.NoSah, Institusi.TglSah, Institusi.Logo, Kode ], callback );
		},
		delete: function( Kode, callback )
		{
			return db.query( "delete from institusi where Kode=?", Kode, callback );
		},
		generateNewToken: function( Kode, NewToken, callback )
		{
			return db.query( "update institusi set Token=? where Kode=?", [ NewToken, Kode ], callback );
		}
	};