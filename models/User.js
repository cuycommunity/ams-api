var db 		= require("../dbconnection"),
	User	= {
		getAllUser: function( callback ){
			return db.query("select * from admin order by ID asc", callback);
		},
		getUserById: function( ID, callback ){
			return db.query("select * from admin where ID=? limit 1", [ID], callback);
		},
		getUserByUsername: function( username, callback ){
			return db.query("select * from admin where Login=? limit 1", [username], callback);
		},
		getUserByCustom: function( field, value, callback ){
			return db.query("select * from admin where ?=?", [field, value], callback);
		},
		addUser: function( User, callback ){
			return db.query("insert into admin (Login, Password, Description, Name, Email, Phone, NotActive) values(?, ?, ?, ?, ?, ?, ?)", [ User.Login, User.Password, User.Description, User.Name, User.Email, User.Phone, User.NotActive ], callback);
		},
		editUserByID: function( ID, User, callback ){
			return db.query("update admin set Login=?, Password=?, Description=?, Name=?, Email=?, Phone=?, NotActive=? where ID=?", [User.Login, User.Password, User.Description, User.Name, User.Email, User.Phone, User.NotActive, ID], callback);
		},
		editUserByUsername: function( username, User, callback ){
			return db.query("update admin set Password=?, Description=?, Name=?, Email=?, Phone=?, NotActive=? where Login=?", [User.Password, User.Description, User.Name, User.Email, User.Phone, User.NotActive, username], callback);
		},
		deleteUserByID: function( ID, callback ){
			return db.query("delete from admin where ID=?", [ID], callback);
		},
		deleteUserByUsername: function( username, callback ){
			return db.query("delete from admin where Login=?", [username], callback);
		},
		prosesLogin: function( username, password, callback )
		{
			return db.query("select * from admin where Login=? and Password=?", [username, password], callback);
		}
	};

module.exports = User;