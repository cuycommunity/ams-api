var express = require('express'),
	router  = express.Router(),
	User	= require('../models/User');

function Login( User ){ this.User = User }

Login.prototype = {
	prosesLogin: function( req, res, next )
	{
		this.User.prosesLogin( req.params.username, req.params.password, function( err, rows ){
			if( err )
			{
				res.json( err );
			}
			else
			{
				if( Object.keys( rows ).length )
				{
					res.json( rows );
				}
				else
				{
					var err = new Error('No Content');
					err.status 	= "204";
					err.message = "No Content";
					res.json( err );
				}
			}
		} );
	}
};

var log = new Login( User );
router.get('/:username/:password', function( req, res, next ){ log.prosesLogin( req, res, next ); });
module.exports = router;