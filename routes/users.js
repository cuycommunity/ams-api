var express  = require('express'),
	router 	 = express.Router();
var User     = require('../models/User');
/* Class Controller Users */
function Users(User){ this.User = User }

Users.prototype = {
    allData: function( req, res, next ){
        this.User.getAllUser( function( err, rows ){
            if( err )
            {
                res.json( err );
            }
            else
            {
                if( Object.keys( rows ).length )
                {
                    res.json( rows );
                }
                else
                {
                    var err = new Error('No Content');
                    err.status = 204;
                    err.message = "No Content";
                    res.json( err );
                }
            }
        } );
    },
    dataGetId: function(req, res, next)
    {
        this.User.getUserById(req.params.ID, function( err, rows ){
            if( err )
            {
                res.json( err );
            }
            else
            {
                if( Object.keys( rows ).length )
                {
                    res.json( rows );
                }
                else
                {
                    var err = new Error("No Content");
                    err.status = 204;
                    err.message = "No Content";
                    res.json(err);
                }
            }
        });
       
    },
    addUserAction: function( req, res, next )
    {
        //res.send( req.body.Login );
        this.User.addUser( req.body, function( err, rows ){
            if( err )
            {
                res.json( err );
            }
            else
            {
                res.json( rows );
            }
        } );
    },
    editUserAction: function( req, res, next )
    {
        this.User.editUserById( req.body.ID, req.body, function( err, rows ){
            if( err )
            {
                res.json( err );
            }
            else
            {
                res.json( rows );
            }
        } );
    }
};

/* Get routes and proses into controller */
var usr = new Users(User);
router.get('/', function( req, res, next ){ usr.allData( req, res, next ); });
router.get('/:ID', function( req, res, next ){ usr.dataGetId( req, res, next ); });
router.post('/add', function( req, res, next ){ usr.addUserAction( req,res, next ); });
module.exports = router;
